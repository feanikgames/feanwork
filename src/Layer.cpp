#include "Feanwork/Layer.h"
#include "Feanwork/Entity.h"
#include "Feanwork/EventListener.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Spatial/SpatialBase.h"
#include "Feanwork/GameConsole.h"

#include "Feanwork/Module/PhysicsModule.h"

Layer::Layer(Scene* scenePtr, float coef)
{
	mCoef     = coef;
	mPosition = Vector2f(0, 0);
	mScenePtr = scenePtr;
}

Layer::~Layer()
{
	for(auto entity: mEntities)
		delete entity;

	mEntities.clear();
	clearLayer();
}

void Layer::update()
{
	//Cull destroyed entities
	vector<Entity*>::iterator it = mEntities.begin();
	for(; it != mEntities.end(); it++)
	{
		if((*it)->getEntityState() == EntityState_Destroyed)
		{
			if(mScenePtr->getSpatial())
				mScenePtr->getSpatial()->erase(*it);

			delete (*it);
			*it = nullptr;
			it  = mEntities.erase(it);

			if(it == mEntities.end())
				break;
		}
	}

	// Pre Update
    //for (auto entity: mEntities)
	for(uint32_t i = 0; i < mEntities.size(); i++)
		mEntities[i]->update(UpdateStage_Pre);

    // Current Update
    for(uint32_t i = 0; i < mEntities.size(); i++)
    {
        if(!mEntities[i]->update(UpdateStage_Current))
			continue;

        PhysicsModule* mod = mEntities[i]->getPhysicsMod();

        if(mod && (mod->getPhysicsType() == PhysicsType_Dynamic || mod->getPhysicsType() == PhysicsType_Actor))
        {
			vector<Entity*> ents = mScenePtr->getSpatial()->queryRange(mEntities[i]->getAABB());
			if(!ents.empty())
			{
				vector<Collision*> cols = mod->collidesWith(ents);
				mod->collisionResolution(cols);
			}
        }
    }

    // Post Update
	for(uint32_t i = 0; i < mEntities.size(); i++)
		mEntities[i]->update(UpdateStage_Post);
}

void Layer::render(RenderTarget* target)
{
	//TODO: Spatial hashing for rendering!
	for(auto entity: mEntities)
		entity->render(target);
}

void Layer::tell(EventFlag flags, ListenerArgs* args)
{
}

Entity* Layer::insert(Entity* entity)
{
	// Let camera if it can look at the entity?
	entity->addListener(this);
	mEntities.push_back(entity);
	return mEntities.back();
}

vector<Entity*> Layer::insert(vector<Entity*> entities, bool alpha)
{
	for(auto entity: mEntities)
		insert(entity);

	if(alpha) SceneManager::getInstance()->resetAlpha();
	return    mEntities;
}

Entity*	Layer::findEntity(int32_t entityID)
{
	for(auto entity: mEntities)
	{
		if(entity->getMaskID() == entityID)
			return entity;
	}

	GameConsole::getInstance()->log("Could not locate an entity with the ID of " + entityID);
	return nullptr;
}

float Layer::getCoef()
{
	return mCoef;
}

Vector2f Layer::getPosition()
{
	return mPosition;
}

vector<Entity*> Layer::getEntities()
{
	return mEntities;
}

void Layer::clearLayer()
{
	mEntities.clear();
}

void Layer::setPosition(Vector2f position)
{
	mPosition = position;
}

void Layer::setID(int32_t id)
{
	IDMask::setMaskID(id);
}

int32_t Layer::getID()
{
	return IDMask::getMaskID();
}

