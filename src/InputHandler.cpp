#include "Feanwork/InputHandler.h"
#include "Feanwork/SceneManager.h"

InputHandler* InputHandler::instance()
{
	static InputHandler handler;
	return &handler;
}

void InputHandler::update()
{
	for(auto state: mKeystates)
	{
		if(state->onCooldown)
			state->current += SceneManager::getInstance()->getDelta();
	}
}

bool InputHandler::isButtonActive(string name)
{
	for(auto state: mKeystates)
	{
		if(state->inputName == name && (state->current >= state->cooldown))
		{
			state->onCooldown = false;
			if(state->key != -1 && Keyboard::isKeyPressed((Keyboard::Key)state->key))
			{
				state->current    = 0;
				state->onCooldown = true;
				return true;
			}
			else if(state->button != -1 && Joystick::isButtonPressed(0, state->button))
			{
				state->current    = 0;
				state->onCooldown = true;
				return true;
			}
			else if(state->axis != -1 && Joystick::getAxisPosition(0, (Joystick::Axis)state->axis))
			{
				state->current    = 0;
				state->onCooldown = true;
				return true;
			}
		}
	}
	return false;
}

void InputHandler::addKeystate(Keystate* state)
{
	mKeystates.push_back(state);
}

void InputHandler::destroy()
{
	for(auto key: mKeystates)
		delete key;

	mKeystates.clear();
}
