#include "Feanwork/Util/Define.h"
#include "Feanwork/Util/FeanRandom.h"

#include "Feanwork/SceneManager.h"
#include "Feanwork/SoundManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Camera.h"
#include "Feanwork/InputHandler.h"
#include "Feanwork/ResourceCache.h"

#include "Feanwork/Module/Module.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Module/EmitterModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/IntelligenceModule.h"
#include "Feanwork/Module/SoundModule.h"

#include "Feanwork/Interface/InterfaceController.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/Interface/ControlBase.h"
#include "Feanwork/Interface/LabelControl.h"
#include "Feanwork/Interface/ToggleControl.h"
#include "Feanwork/Interface/SelectionControl.h"
#include "Feanwork/Interface/ProgressControl.h"
#include "Feanwork/Interface/DropControl.h"
#include "Feanwork/Interface/ScrollControl.h"
#include "Feanwork/Interface/FieldControl.h"
#include "Feanwork/Interface/TooltipControl.h"

#include "Feanwork/Lighting/LightManager.h"
#include "Feanwork/Lighting/PointLight.h"

#include <fstream>

using namespace FeanRandom;

// Test Stuff
Entity* player;
Entity*	floor1;
Entity* background;
Entity* crate1;
Entity* crate2;
Entity* crate3;
Entity* crate4;
ToggleControl* toggle;

// Shadow Callbacks
void ShadowSwitch(MouseButtonEventArgs args)
{
	bool		  flag	= toggle->isActive();
	LightManager* lgMgr = SceneManager::getInstance()->getScene(0)->getLightMgr();

	for(auto light: lgMgr->getLights())
		light->setShadowCaster(flag);
}

void playerInput(Entity* caller)
{
	PhysicsModule* phys = caller->getPhysicsMod();
	if(phys)
	{
			 if(InputHandler::instance()->isButtonActive("up"))    phys->setAcceleration(Vector2f(0, -0.000005f));
		else if(InputHandler::instance()->isButtonActive("down"))  phys->setAcceleration(Vector2f(0,  0.000005f));
		else if(InputHandler::instance()->isButtonActive("left"))  phys->setAcceleration(Vector2f(-0.000005f, 0));
		else if(InputHandler::instance()->isButtonActive("right")) phys->setAcceleration(Vector2f( 0.000005f, 0));
	}
	else
	{
			 if(InputHandler::instance()->isButtonActive("up"))    caller->addPosition(0, -15.f);
		else if(InputHandler::instance()->isButtonActive("down"))  caller->addPosition(0,  15.f);
		else if(InputHandler::instance()->isButtonActive("left"))  caller->addPosition(-15.f, 0);
		else if(InputHandler::instance()->isButtonActive("right")) caller->addPosition( 15.f, 0);
	}
}

int32_t main()
{
	ResourceCache::getInstance()->addDirectory("../Resources");
	SceneManager::getInstance()->init("game");
	SceneManager::getInstance()->getScene(0)->startLightManager();
	SceneManager::getInstance()->getScene(0)->startInterfaceController();
	SceneManager::getInstance()->getScene(0)->setShader("blue");
	SceneManager::getInstance()->setIcon("../Resources/icon.png");

	PointLight* light  = new PointLight(Vector2f(200, 500),  Colour(255, 50, 200, 255), 200, 200);
	PointLight* light1 = new PointLight(Vector2f(50,   500), Colour(255, 255, 0,  255), 200, 200);
	PointLight* light2 = new PointLight(Vector2f(0, 0),      Colour(255, 255, 255,     255), 200, 200);
	PointLight* light3 = new PointLight(Vector2f(120, -75.f), Colour(255, 0, 155, 255), 200, 200);
	PointLight* light4 = new PointLight(Vector2f(120,  75.f), Colour(25, 25, 255, 255),  200, 200);

	SceneManager::getInstance()->getScene(0)->getLightMgr()->addLight(light);
	SceneManager::getInstance()->getScene(0)->getLightMgr()->addLight(light1);
	SceneManager::getInstance()->getScene(0)->getLightMgr()->addLight(light2);
	SceneManager::getInstance()->getScene(0)->getLightMgr()->addLight(light3);
	SceneManager::getInstance()->getScene(0)->getLightMgr()->addLight(light4);

	/* Interface Init */
	Container* container  = new Container(Vector2f(250, 420), 600, 800);
	toggle				  = new ToggleControl(Vector2f(10, -275), "btnActive", "btnHover", "btnInactive", container);
	ControlBase* button   = new ControlBase(Vector2f(15, -370), "button-test", container);
	LabelControl* label   = new LabelControl(Vector2f(-100, -330), "Shadow Casting Toggle", container, 20U);
	SelectionControl* sel = new SelectionControl(Vector2f(-50, -240), "rbActive", "rbInactive", SelectionType_Radio, container);
	ProgressControl* prog = new ProgressControl(Vector2f(6, -80), "bartest", container);
	DropControl* drop     = new DropControl(Vector2f(0, -45), "dField", "dMask", "dSelection", "dDrop", container);
	ScrollControl* scroll = new ScrollControl(Vector2f(120, -100), "bar", "thumb", "arrow", 2.5f, ScrollAxis_Vertical, container);
	FieldControl* field   = new FieldControl(Vector2f(0, 150), "frame", container, true);
	TooltipControl* tool  = new TooltipControl(Vector2f(0, 340), "frame", "You known nothing\nJon Snow", 14, container, false);

	toggle->setColour(Colour(255, 255, 255, 255));
	prog->setColour(Colour(255, 255, 255, 255));
	button->setColour(Colour(255, 255, 255, 255));
	sel->setColour(Colour(255, 255, 255, 255));
	drop->setColour(Colour(255, 255, 255, 255));
	scroll->setColour(Colour(255, 255, 255, 255));
	field->setColour(Colour(255, 255, 255, 255));

	Interval* tempInterval  = nullptr;

	/*tempInterval = prog->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = toggle->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = button->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = sel->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = drop->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = scroll->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));
	tempInterval = field->traverseColour(18000.f, Colour(255, 255, 255, 0), Colour(255, 255, 255, 255));*/

	/*prog->traverseColour(3500.0f, Colour(255, 255, 255, 255), Colour(255, 255, 255, 0), tempInterval2);
	toggle->traverseColour(3500.0f, Colour(255, 255, 255, 255), Colour(255, 255, 255, 0), tempInterval2);
	button->traverseColour(3500.0f, Colour(255, 255, 255, 255), Colour(255, 255, 255, 0), tempInterval2);*/

	sel->addOption("Testing");
	sel->addOption("The");
	sel->addOption("Selection");
	sel->addOption("Buttons");
	toggle->onPressed.push_back(ShadowSwitch);

	drop->addField("This", 14);
	drop->addField("Is", 14);
	drop->addField("A", 14);
	drop->addField("Drop", 14);
	drop->addField("Field", 14);
	drop->addField("Test", 14);

	container->insert(drop);
	container->insert(toggle);
	container->insert(label);
	container->insert(button);
	container->insert(sel);
	container->insert(prog);
	container->insert(scroll);
	container->insert(field);
	container->insert(tool);
	SceneManager::getInstance()->getScene(0)->getInterface()->addContainer(container);

	// ============ Keys =============
	// Left
	Keystate* left	= new Keystate;
	left->inputName = "left";
	left->key		= Keyboard::A;
	InputHandler::instance()->addKeystate(left);

	// Right
	Keystate* right  = new Keystate;
	right->inputName = "right";
	right->key		 = Keyboard::D;
	InputHandler::instance()->addKeystate(right);

	// Up
	Keystate* up  = new Keystate;
	up->inputName = "up";
	up->key		  = Keyboard::W;
	InputHandler::instance()->addKeystate(up);

	// Down
	Keystate* down	= new Keystate;
	down->inputName = "down";
	down->key		= Keyboard::S;
	InputHandler::instance()->addKeystate(down);
	// ===============================

	Scene* scene			  = SceneManager::getInstance()->getScene(0);

	player					  = new Entity(Vector2f(250.f, 250.f));
	RenderModule* render	  = new RenderModule("player");
	AnimationModule* anim	  = new AnimationModule();
	EmitterModule* emitter	  = new EmitterModule();
	PhysicsModule* physics	  = new PhysicsModule(PhysicsType_Dynamic);
	IntelligenceModule* intel = new IntelligenceModule();

	anim->addAnimation("idle", 500.f, true);
	anim->addAnimation("walk", 800.f, true);
	player->onInput = bind(&playerInput, placeholders::_1);

	for(uint32_t i = 0; i < 5; i++)
	{
		Frame* newFrame  = new Frame();
		newFrame->width  = 48;
		newFrame->height = 80;
		newFrame->position = Vector2f(48 * i, 0);
		anim->getAnimation("idle")->addFrame(newFrame);
	}

	for(uint32_t i = 0; i < 5; i++)
	{
		Frame* newFrame  = new Frame();
		newFrame->width  = 48;
		newFrame->height = 80;
		newFrame->position = Vector2f(48 * i, 80);
		anim->getAnimation("walk")->addFrame(newFrame);
	}

	Emitter* newEmitter = new Emitter(Vector2f(400.f, 400.f), Vector2f(1.f, 0.f), EmitterType_Directional, "tex");
	newEmitter->init(100.f, 2000.f, 1000.f, 0.f, 200.f, Vector2f(20.f, 20.f));
	newEmitter->mColours[DenoteType_Start]		  = Colour(randRange(50, 175), 0, 0, 200);
	newEmitter->mColours[DenoteType_End]		  = Colour(0, randRange(100, 235), randRange(20, 255), 20);
	newEmitter->mColours[DenoteType_StartVariant] = Colour(0, 0, 0, 0);
    newEmitter->mColours[DenoteType_EndVariant]	  = Colour(0, 0, 0, 0);

	newEmitter->mSizes[DenoteType_Start]		= 1.0f;
    newEmitter->mSizes[DenoteType_End]			= 1.0f;
    newEmitter->mSizes[DenoteType_StartVariant] = .0f;
    newEmitter->mSizes[DenoteType_EndVariant]   = .0f;

    newEmitter->mSpeeds[DenoteType_Start]		 = 0.04f;
    newEmitter->mSpeeds[DenoteType_End]			 = 0.02f;
    newEmitter->mSpeeds[DenoteType_StartVariant] = 0.005f;
    newEmitter->mSpeeds[DenoteType_EndVariant]	 = 0.005f;

	emitter->addEmitter(newEmitter);

	player->addModule(render);
	player->addModule(anim);
	player->addModule(emitter);
	player->addModule(physics);

	physics->initVars(0.3f, 0.6f, 0.8f, 0.8f);
	physics->createBoundingBox();
	player->initDebugQuad();

	// Floor
	floor1					  = new Entity(Vector2f(50.0f, 600.0f));
	RenderModule*  floorRnd   = new RenderModule("floor");
	PhysicsModule* floorPhys  = new PhysicsModule(PhysicsType_Static);

	floor1->addModule(floorRnd);
	floor1->addModule(floorPhys);
	floorPhys->createBoundingBox();
	floor1->initDebugQuad();

	// Background 
	background			  = new Entity(Vector2f(0.f, 0.f));
	RenderModule* backRnd = new RenderModule("background");

	background->addModule(backRnd);
	background->initDebugQuad();

	// Crates
	crate1					= new Entity(Vector2f(-75.f,   0.f));
	RenderModule* crate1Rnd = new RenderModule("crate");
	PhysicsModule* crate1Phys = new PhysicsModule(PhysicsType_Static);

	crate1Phys->initVars(0, 0.2, 0.4, 0.1);
	//crate1->addModule(crate1Phys);
	crate1->addModule(crate1Rnd);
	crate1->initDebugQuad();

	crate2					  = new Entity(Vector2f( 75.f,   0.f));
	RenderModule* crate2Rnd   = new RenderModule("crate");
	PhysicsModule* crate2Phys = new PhysicsModule(PhysicsType_Static);

	crate2Phys->initVars(0, 0.2, 0.4, 0.1);
	crate2->addModule(crate2Phys);
	crate2->addModule(crate2Rnd);
	crate2->initDebugQuad();

	crate3					  = new Entity(Vector2f(  0.f,  75.f));
	RenderModule* crate3Rnd   = new RenderModule("crate");
	PhysicsModule* crate3Phys = new PhysicsModule(PhysicsType_Static);

	crate3Phys->initVars(0, 0.2, 0.4, 0.1);
	crate3->addModule(crate3Phys);
	crate3->addModule(crate3Rnd);
	crate3->initDebugQuad();

	crate4					  = new Entity(Vector2f(  0.f, -75.f));
	RenderModule* crate4Rnd   = new RenderModule("crate");
	PhysicsModule* crate4Phys = new PhysicsModule(PhysicsType_Static);

	crate4Phys->initVars(0, 0.2, 0.4, 0.1);
	crate4->addModule(crate4Phys);
	crate4->addModule(crate4Rnd);
	crate4->initDebugQuad();

	scene->addEntity(0, background);
	scene->addEntity(0, player);
	scene->addEntity(0, floor1);
	scene->getCamera(0)->lookAt(player);

	scene->addEntity(0, crate1);
	scene->addEntity(0, crate2);
	scene->addEntity(0, crate3);
	scene->addEntity(0, crate4);

	/*player->traverseColour(10000.f, Color(255, 0, 255));*/
	crate1->traversePosition(6000.f, Vector2f(700, 500));

	SceneManager::getInstance()->run();
	SceneManager::getInstance()->destroy();
	return 0;
}

// TODO: Once the framework is complete add this back for DLL compilation
/*#ifdef _WINDOWS
BOOL APIENTRY DllMain(HANDLE hModule,
						DWORD ul_reason_for_call,
						LPVOID lpReserved)
{
	return TRUE;
}
#endif*/
