#include "Feanwork/Directory.h"

using namespace std;

Directory::Directory(string path) :
	mHandle(INVALID_HANDLE)
{
	mPath = path;
}

Directory::~Directory()
{
	if(mHandle != INVALID_HANDLE)
		releaseHandle();
}

// TODO: Search sub-directories for files too!
bool Directory::getFirstFile(FileInfo& info)
{
#ifdef _WINDOWS
	WIN32_FIND_DATA data;
	mHandle = FindFirstFile((mPath + "/*").c_str(), &data);

	if(mHandle == INVALID_HANDLE) 
		return false;

	info.type	  = (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? FileType_Folder : FileType_File;
	splitFilename(data.cFileName, info.path, info.fileName, info.extension);
	info.path	  = mPath;
#elif __linux
	mHandle = opendir(mPath.c_str());
	if(!mHandle)
		return false;

	dirent *data = readdir(mHandle);

	if(!data)
		return false;

	if(data->d_type != DT_REG || data->d_type != DT_DIR)
		return false;

	info.type = (data->d_type == DT_REG ? FileType_File : FileType_Folder);
	splitFilename(data->d_name, info.path, info.fileName, info.extension);
	info.path = mPath;
#endif

	return true;
}

bool Directory::getNextFile(FileInfo& info)
{
#ifdef _WINDOWS
	WIN32_FIND_DATA data;
	if(!FindNextFile(mHandle, &data))
		return false;

	info.type	  = (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? FileType_Folder : FileType_File;
	splitFilename(data.cFileName, info.path, info.fileName, info.extension);
	info.path	  = mPath;
#elif __linux
	dirent *data = readdir(mHandle);

	if(!data)
		return false;

	if(data->d_type != DT_REG || data->d_type != DT_DIR)
		return false;

	info.type = (data->d_type == DT_REG ? FileType_File : FileType_Folder);
	splitFilename(data->d_name, info.path, info.fileName, info.extension);
	info.path = mPath;
#endif

	return true;
}

void Directory::releaseHandle()
{
#ifdef _WINDOWS
	FindClose(mHandle);
#elif __linux
	closedir(mHandle);
#endif

	mHandle = INVALID_HANDLE;
}

bool Directory::getAllFiles(vector<FileInfo>& files)
{
	FileInfo info;
	if(!getFirstFile(info))
		return false;

	// Find all file info
	do files.push_back(info);
	while(getNextFile(info));

	releaseHandle();
	return true;
}

void Directory::splitFilename(string input, string& path, string& file, string& ext)
{
	size_t pos = input.find_last_of('\\');
	if(pos == string::npos)
		pos = input.find_last_of('/');

	if(pos == string::npos)
	{
		path = "";
		pos  = -1;
	}
	else path = input.substr(0, pos + 1);

	size_t pos2 = input.find_last_of('.');
	if(pos2 == string::npos || pos2 == input.length() - 1)
	{
		ext  = "";
		pos2 = (input.length() - 1);
	}
	else ext  = input.substr(pos2 + 1);

	file = input.substr(pos + 1, pos2 - pos - 1);
}
