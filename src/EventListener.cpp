#include "Feanwork/EventListener.h"
#include "Feanwork/Module/PhysicsModule.h"

EventListener::EventListener()
{
}

void EventListener::tell(EventFlag flags, ListenerArgs* args)
{
}

// Listener Args Def
ListenerArgs::ListenerArgs(Vector2f pos)	
{ 
	position = pos; 
	keyCode  = (sf::Keyboard::Key)-1;
}

ListenerArgs::ListenerArgs(Dimension dim)		
{ 
	keyCode    = (sf::Keyboard::Key)-1;
	dimensions = dim;    
}

ListenerArgs::ListenerArgs(Keyboard::Key key)	
{ 
	keyCode = key;	   
}

ListenerArgs::ListenerArgs(const vector<Collision*>& cols) 
{ 
	keyCode = (sf::Keyboard::Key)-1;
	for(auto col: cols)
		collisions.push_back(col->target);
}
