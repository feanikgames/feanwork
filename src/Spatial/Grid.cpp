#include "Feanwork/Spatial/Grid.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Module/PhysicsModule.h"


////////////////////////////////////////////////////////////
Grid::Grid(Vector2f position, Vector2i size, Vector2f spacing) :
	SpatialBase(SpatialType_Grid)
{
	mSpacing  = spacing;
	mPosition = position;
	mSize	  = size;
	mNumCells = mSize.x * mSize.y;
	mCells = new Cell[mNumCells];

	for(int32_t x = 0; x < mSize.x; x++)
	{
		for(int32_t y = 0; y < mSize.y; y++)
			mCells[x * mSize.y + y].position = mPosition + Vectorial::vectorHadamard(Vector2f(x, y), mSpacing);
	}
}


////////////////////////////////////////////////////////////
Grid::~Grid()
{
	delete[] mCells;
}


////////////////////////////////////////////////////////////
bool Grid::update()
{
	//Reset entity tags
	/*for(uint32_t c = 0; c < mNumCells; c++)
	{
		CellEntitySet& ents = mCells[c].entities;

		for(int32_t e = 0; e < ents.size(); e++)
		{
			ents[e].gridEnt->inserted = false;
			ents[e].updated = false;
		}
	}*/

	//Update all the cells
	for(uint32_t c = 0; c < mNumCells; c++)
	{
		CellEntitySet&			ents = mCells[c].entities;
		CellEntitySet::iterator it   = ents.begin();
		GridEntitySet			tmp;

		for(; it != ents.end(); it++)
		{
			//if(it->updated)
			//	continue;

			PhysicsModule* mod = it->gridEnt->ent->getPhysicsMod();
			if(mod && (mod->getPhysicsType() == PhysicsType_Dynamic || mod->getPhysicsType() == PhysicsType_Actor) &&
			   it->gridEnt->ent->hasMoved())
			{
				//if(!it->gridEnt->inserted)
				if(it->gridEnt->ent)
					tmp.push_back(it->gridEnt);
				//it->gridEnt->inserted = true;

				it = ents.erase(it);
				if(it == ents.end())
					break;
			}

			it->updated = true;
		}

		for(auto ent: tmp)
		{
			_insert(ent);
		}
	}

	return true;
}


////////////////////////////////////////////////////////////
bool Grid::insert(Entity* entity)
{
	return _insert(new GridEntity(entity));
}


////////////////////////////////////////////////////////////
bool Grid::erase(Entity* entity)
{
	//TODO: brute force = evil ... or is it
	for(uint32_t i = 0; i < mNumCells; i++)
	{
		CellEntitySet&			ents = mCells[i].entities;
		CellEntitySet::iterator it   = ents.begin();

		for(; it != ents.end(); it++)
		{
			if(entity == it->gridEnt->ent)
			{
				//delete it->gridEnt;
				it = ents.erase(it);

				if(it == ents.end())
					break;
			}
		}
	}
	
	return true;
}


////////////////////////////////////////////////////////////
EntitySet Grid::queryRange(AABB range)
{
	EntitySet result;
	Vector2i  minIndices;
	Vector2i  maxIndices;

	if(!getIndexRange(range, &minIndices, &maxIndices))
		return result;

	for (int y = minIndices.y; y <= maxIndices.y; y++)
	{
		for (int x = minIndices.x; x <= maxIndices.x; x++)
		{
			for(auto& cellEnt : mCells[x * mSize.y + y].entities)
				result.push_back(cellEnt.gridEnt->ent);
		}
	}
	return result;
}


////////////////////////////////////////////////////////////
Vector2i Grid::posToIndices(Vector2f position)
{
	//TODO: Correct precision problems
	Vector2i indices;
	Vector2f relPos = (position - mPosition);

    indices.x = (int)relPos.x / (int)mSpacing.x;
    indices.y = (int)relPos.y / (int)mSpacing.y;
    return indices;
}


////////////////////////////////////////////////////////////
bool Grid::getIndexRange(AABB range, Vector2i* min, Vector2i* max)
{
	// Make sure the output variables exist
	if(!min || !max)
		return false;

	// Set variables with closest indices using the range
	*min = posToIndices(range.getMin());
	*max = posToIndices(range.getMax());

	// Return if indices are actually inside the grid
	return (min->x >= 0.f && min->x < mSize.x && 
		    min->y >= 0.f && min->y < mSize.y &&
		    max->x >= 0.f && max->x < mSize.x && 
		    max->y >= 0.f && max->y < mSize.y);
}


////////////////////////////////////////////////////////////
Vector2f Grid::getPosition()
{
	return mPosition;
}


////////////////////////////////////////////////////////////
Vector2i Grid::getSize()
{
	return mSize;
}


////////////////////////////////////////////////////////////
Grid::Cell* Grid::getCell(int32_t x, int32_t y)
{
	return &mCells[x * mSize.y + y];
}


////////////////////////////////////////////////////////////
void Grid::debugDraw(RenderTarget* target)
{
	RectangleShape shape;
	Text		   text;
	
	text.setFont(*ResourceCache::getInstance()->retrieveFont("OpenSans"));
	text.setCharacterSize(14);
	text.setColor(Colour::Green);

	shape.setSize(mSpacing);
	shape.setOutlineColor(Colour::Green);
	shape.setFillColor(Colour::Transparent);
	shape.setOutlineThickness(1);

	for(int32_t x = 0; x < mSize.x; x++)
	{
		for(int32_t y = 0; y < mSize.y; y++)
		{
			int32_t index = (x * mSize.y + y);
			string  txt   = to_string(mCells[index].entities.size());

			text.setString(txt);
			text.setPosition(Vectorial::vectorHadamard(Vector2f(x, y), mSpacing) + mPosition + Vector2f(20, 20));
			shape.setPosition(Vectorial::vectorHadamard(Vector2f(x, y), mSpacing) + mPosition);

			target->draw(shape);
			target->draw(text);
		}
	}
}


////////////////////////////////////////////////////////////
bool Grid::_insert(GridEntity* entity)
{
	Vector2i  minIndices;
	Vector2i  maxIndices;
	bool	  result = false;

	if(!getIndexRange(entity->ent->getAABB(), &minIndices, &maxIndices))
		return false;

	for(int y = minIndices.y; y <= maxIndices.y; y++)
	{
		for (int x = minIndices.x; x <= maxIndices.x; x++)
		{
			//TODO: Implement a more efficient solution if necessary
			CellEntitySet& cellEnts = mCells[x * mSize.y + y].entities;
			bool skip			    = false;

			for(auto& ent: cellEnts)
			{
				if(ent.gridEnt == entity)
				{
					skip = true;
					break;
				}
			}

			result += !skip;

			if(skip)
				continue;

			cellEnts.push_back(CellEntity(entity));
		}
	}

	return result;
}
