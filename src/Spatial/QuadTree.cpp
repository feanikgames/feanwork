#include "Feanwork/Spatial/QuadTree.h"
#include "Feanwork/Entity.h"

QuadTree::QuadTree(AABB* boundary, int32_t nodeCapacity) :
	SpatialBase(SpatialType_QuadTree)
{
	mNodeCapacity = nodeCapacity;
    mBoundary     = boundary;
    
    mNorthWest = nullptr;
    mNorthEast = nullptr;
    mSouthWest = nullptr;
    mSouthEast = nullptr;
}

bool QuadTree::update()
{
	return true;
}

QuadTree::~QuadTree()
{
	delete mNorthWest;
    delete mNorthEast;
    delete mSouthWest;
    delete mSouthEast;
    delete mBoundary;
    
    for(auto entity: mEntities)
    	delete entity;
    	
    mEntities.clear();
}

/// <summary>
/// Insert a point into the QuadTree, if it does not fit subdivide this QuadTree.
/// </summary>
bool QuadTree::insert(Entity* entity)
{
	if (!mBoundary->containsPoint(entity->getPosition()))
    	return false;

    if (mEntities.size() < mNodeCapacity)
    {
        mEntities.push_back(entity);
        return true;
    }

    if (mNorthWest == nullptr)
        subdivide();

    if (mNorthWest->insert(entity)) return true;
    if (mNorthEast->insert(entity)) return true;
    if (mSouthWest->insert(entity)) return true;
    if (mSouthEast->insert(entity)) return true;
                                    return false;
}

void QuadTree::subdivide()
{
	// Create four QuadTree children
    Vector2f quarterDim = mBoundary->getHalfDim() / 2.0f;
    mNorthWest          = new QuadTree(new AABB(mBoundary->getOrigin().x - quarterDim.x, mBoundary->getOrigin().y - quarterDim.y, quarterDim), mNodeCapacity);
    mNorthEast          = new QuadTree(new AABB(mBoundary->getOrigin().x + quarterDim.x, mBoundary->getOrigin().y - quarterDim.y, quarterDim), mNodeCapacity);
    mSouthWest          = new QuadTree(new AABB(mBoundary->getOrigin().x - quarterDim.x, mBoundary->getOrigin().y + quarterDim.y, quarterDim), mNodeCapacity);
    mSouthEast          = new QuadTree(new AABB(mBoundary->getOrigin().x + quarterDim.x, mBoundary->getOrigin().y + quarterDim.y, quarterDim), mNodeCapacity);

    // For each existing point find which QuadTree child it should exist in
    for(auto entity: mEntities)
    	insert(entity);
}

/// <summary>
/// Returns all the points connected to this QuadTree's range
/// </summary>
vector<Entity*> QuadTree::queryRange(AABB range)
{
	vector<Entity*> entitiesInRange;
    if (!mBoundary->intersects(&range))
        return entitiesInRange;

    for(int32_t p = 0; p < mEntities.size(); p++)
    {
        if (range.containsPoint(mEntities[p]->getPosition()))
            entitiesInRange.push_back(mEntities[p]);
    }

    if(mNorthWest == nullptr)
    	return entitiesInRange;

	vector<Entity*> nwEntities = mNorthWest->queryRange(range);
	vector<Entity*> neEntities = mNorthEast->queryRange(range);
	vector<Entity*> swEntities = mSouthWest->queryRange(range);
	vector<Entity*> seEntities = mSouthEast->queryRange(range);

    entitiesInRange.insert(entitiesInRange.end(), nwEntities.begin(), nwEntities.end());
    entitiesInRange.insert(entitiesInRange.end(), neEntities.begin(), neEntities.end());
    entitiesInRange.insert(entitiesInRange.end(), swEntities.begin(), swEntities.end());
    entitiesInRange.insert(entitiesInRange.end(), seEntities.begin(), seEntities.end());
    
    return entitiesInRange;
}

/// <summary>
/// Draw this Quad Tree and all child QuadTree's to the screen
/// </summary>
void QuadTree::debugDraw(RenderTarget* target)
{
	RectangleShape shape(mBoundary->getHalfDim() * 2.f);
    shape.setPosition(mBoundary->getOrigin() - mBoundary->getHalfDim());
    shape.setFillColor(Colour::Transparent);
    shape.setOutlineColor(Colour::Green);
    shape.setOutlineThickness(2);

    target->draw(shape);
    if (mNorthWest) mNorthWest->debugDraw(target);
    if (mNorthEast) mNorthEast->debugDraw(target);
    if (mSouthWest) mSouthWest->debugDraw(target);
    if (mSouthEast) mSouthEast->debugDraw(target);
}
