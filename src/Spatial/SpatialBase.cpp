#include "Feanwork/Spatial/SpatialBase.h"

SpatialBase::SpatialBase()
{
	mSpatialType = SpatialType_Undeclared;
}

SpatialBase::SpatialBase(SpatialType type)
{
	mSpatialType = type;
}

SpatialType SpatialBase::getSpatialType()
{
	return mSpatialType;
}
