#include "Feanwork/ResourceCache.h"
#include "Feanwork/Directory.h"
#include "Feanwork/Util/Define.h"

#include "Feanwork/Parser/ConfigParser.h"
#include "Feanwork/GameConsole.h"

// RESOURCE DEFINITION
Resource::Resource(ResourceType type, string path, string name)
{
	mType   = type;
	mPath   = path;
	mName   = name;
	mExists = false;
}

FeanTexture::FeanTexture(string path, string name) : 
	Resource(ResourceType_Texture, path, name)
{
}

bool FeanTexture::load()
{
	mExists = this->loadFromFile(mPath);
	return mExists;
}

FeanFont::FeanFont(string path, string name) :
	Resource(ResourceType_Font, path, name)
{
}

bool FeanFont::load()
{
	mExists = this->loadFromFile(mPath);
	return mExists;
}

FeanConfig::FeanConfig(string path, string name) : 
	Resource(ResourceType_Config, path, name),
	mConfigName(name)
{
}

bool FeanConfig::load()
{
	ConfigParser parser(mConfigName, &mConfigData);
	mExists = parser.load();
	return mExists;
}

FeanSound::FeanSound(string path, string name) : 
	Resource(ResourceType_Audio, path, name),
	mSoundName(name)
{
}

bool FeanSound::load()
{
	SoundBuffer* sndBuffer = new SoundBuffer();
	mExists				   = sndBuffer->loadFromFile(mPath);
	
	if(mExists)
		setBuffer(*sndBuffer);

	return mExists;
}

FeanShader::FeanShader(string vertPath, string fragPath, string name) :
	Resource(ResourceType_Shader, vertPath, name)
{
	mFragPath = fragPath;
}

bool FeanShader::load()
{
	mExists = this->loadFromFile(mPath, mFragPath);
	return mExists;
}

// RESOURCE CACHE
ResourceCache* ResourceCache::getInstance()
{
	static ResourceCache cache;
	return &cache;
}

FeanTexture* ResourceCache::retrieveTexture(string tex)
{
	// Check if resource exists
	TextureIt it = mTextureCache.find(tex);
	if(it == mTextureCache.end())
		return nullptr;

	// Check if resource is loaded, otherwise try and load it
	if(!it->second->exists())
	{
		if(!it->second->load())
			return nullptr;
	}
	return it->second;
}

FeanFont* ResourceCache::retrieveFont(string font)
{
	// Check if resource exists
	FontIt it = mFontCache.find(font);
	if(it == mFontCache.end())
		return nullptr;

	// Check if resource is loaded, otherwise try and load it
	if(!it->second->exists())
	{
		if(!it->second->load())
			return nullptr;
	}
	return it->second;
}

FeanConfig* ResourceCache::retrieveConfig(string cfg)
{
	// Check if resource exists
	ConfigIt it = mConfigCache.find(cfg);
	if(it == mConfigCache.end())
		return nullptr;

	// Check if resource is loaded, otherwise try and load it
	if(!it->second->exists())
	{
		if(!it->second->load())
			return nullptr;
	}
	return it->second;
}

FeanSound* ResourceCache::retrieveSound(string sound)
{
	// Check if resource exists
	SoundIt it = mSoundCache.find(sound);
	if(it == mSoundCache.end())
		return nullptr;

	// Check if resource is loaded, otherwise try and load it
	if(!it->second->exists())
	{
		if(!it->second->load())
			return nullptr;
	}
	return it->second;
}

FeanShader* ResourceCache::retrieveShader(string shader)
{
	// Check if resource exists
	ShaderIt it = mShaderCache.find(shader);
	if(it == mShaderCache.end())
		return nullptr;

	// Check if resource is loaded, otherwise try and load it
	if(!it->second->exists())
	{
		if(!it->second->load())
		{
			GameConsole::getInstance()->log("Unable to load %s even though it exists", shader.c_str());
			return nullptr;
		}
	}
	return it->second;
}

void ResourceCache::addDirectory(string dir)
{
	mDirectories.push_back(dir);
	refreshCache();
}

void ResourceCache::unloadAllResources()
{
	TextureIt texIter    = mTextureCache.begin();
	FontIt	  fontIter   = mFontCache.begin();
	ConfigIt  cfgIter    = mConfigCache.begin();
	SoundIt	  sndIter    = mSoundCache.begin();
	ShaderIt  shaderIter = mShaderCache.begin();

	for(; texIter     != mTextureCache.end(); texIter++)    delete texIter->second;
	for(; fontIter    != mFontCache.end();    fontIter++)   delete fontIter->second;
	for(; cfgIter     != mConfigCache.end();  cfgIter++)    delete cfgIter->second;
	for(; sndIter     != mSoundCache.end();   sndIter++)    delete sndIter->second;
	for(; shaderIter  != mShaderCache.end();  shaderIter++) delete shaderIter->second;

	mTextureCache.clear();
	mFontCache.clear();
	mConfigCache.clear();
	mSoundCache.clear();
	mShaderCache.clear();
	mDirectories.clear();
}

void ResourceCache::destroy()
{
	unloadAllResources();
}

Directories ResourceCache::getDirectories()
{
	return mDirectories;
}

void ResourceCache::refreshCache()
{
	for(auto dir: mDirectories)
    {
		Directory					directory(dir);
		uint32_t					cached = 0;
		vector<FragFile>		    frags;
        vector<Directory::FileInfo> files;
		directory.getAllFiles(files);

		// Search through the files and push them to the corresponding cache map
        for(auto f: files)
        {
			string extension = f.extension.c_str();
            if(extension == "jpg" || extension == "png")
			{
				if(mTextureCache.find(f.fileName) == mTextureCache.end()) 
				   mTextureCache[f.fileName] = new FeanTexture(f.getFullPath(), f.fileName);
			}
			else if(extension == "ttf")
			{
				if(mFontCache.find(f.fileName) == mFontCache.end()) 
				   mFontCache[f.fileName] = new FeanFont(f.getFullPath(), f.fileName);
			}
			else if(extension == "cfg")
			{
				if(mConfigCache.find(f.fileName) == mConfigCache.end())
					mConfigCache[f.fileName] = new FeanConfig(f.getFullPath(), f.fileName);
			}
			else if(extension == "wav" || extension == "ogg")
			{
				if(mSoundCache.find(f.fileName) == mSoundCache.end())
					mSoundCache[f.fileName] = new FeanSound(f.getFullPath(), f.fileName);
			}
			else if(extension == "frag")
			{
				if(mShaderCache.find(f.fileName) != mShaderCache.end())
				{
					FeanShader* shader = mShaderCache[f.fileName];
					shader->mFragPath = f.getFullPath();
					shader->mFragName = f.fileName;
				}
				else frags.push_back(FragFile(f.getFullPath(), f.fileName));
			}
			else if(extension == "vert")
			{
				if(mShaderCache.find(f.fileName) == mShaderCache.end())
					mShaderCache[f.fileName] = new FeanShader(f.getFullPath(), "", f.fileName);

				// Find out if frag file was already found, if it was add it to the vert shader
				for(auto frag: frags)
				{
					if(frag.fragName == f.fileName)
					{
						mShaderCache[f.fileName]->mFragName = frag.fragName;
						mShaderCache[f.fileName]->mFragPath = frag.fragPath;
					}
				}
			}
        }
    }
}
