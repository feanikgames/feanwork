#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Entity.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/GameConsole.h"

AnimationModule::AnimationModule(const AnimationModule& module) :
	Module(ModuleType_Animation, module.mParent)
{
	mCurrentAnimation = module.mCurrentAnimation;
	mAnimationQueue   = module.mAnimationQueue;

	for(auto animation: module.mAnimations)
		mAnimations.push_back(new Animation(*animation));
}

AnimationModule::AnimationModule(Entity* parent) : 
	Module(ModuleType_Animation, parent),
	mCurrentAnimation(0)
{
}

AnimationModule::~AnimationModule()
{
	for(auto anim: mAnimations)
		delete anim;

	mAnimations.clear();
	mAnimationQueue.clear();
}

void AnimationModule::update(UpdateStage stage)
{
	if(stage == UpdateStage_Current)
	{
		if(mAnimationQueue.size() > 0 &&
		   mAnimations.at(mCurrentAnimation)->hasEnded() &&
		   mAnimations.at(mCurrentAnimation)->animationMode() != AnimationMode_Once)
		{
			mCurrentAnimation = mAnimationQueue.front();
			// TODO: pop animation from queue
		}
		mAnimations.at(mCurrentAnimation)->update(Module::mParent);
	}
}

Module* AnimationModule::duplicate()
{
	// Return duplicate of this module
	AnimationModule* module = new AnimationModule(*this);
	return module;
}

bool AnimationModule::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		// Store animation count
		int32_t animCount = mAnimations.size();
		stream.write((char*)&animCount, sizeof(int32_t));

		// Write animations
		for(auto anim: mAnimations)
		{
			// Group variables
			int32_t frameCount = anim->frameCount();
			float   speed	   = anim->getSpeed();
			int16_t animMode   = (int16_t)anim->animationMode();
			string  name	   = anim->getName();
			int32_t nameSize   = name.length() + 1;

			// Write variables to file
			stream.write((char*)&frameCount, sizeof(int32_t));
			stream.write((char*)&speed,		 sizeof(float));
			stream.write((char*)&animMode,   sizeof(int16_t));
			stream.write((char*)&nameSize,   sizeof(int32_t));
			stream.write(name.c_str(),		 nameSize);

			// Write out frame data
			for(auto frame: anim->getFrames())
			{
				stream.write((char*)&frame->position.x, sizeof(float));
				stream.write((char*)&frame->position.y, sizeof(float));
				stream.write((char*)&frame->width,		sizeof(uint32_t));
				stream.write((char*)&frame->height,		sizeof(uint32_t));
			}
		}

		return true;
	}
	else if(mode == ParseIn)
	{
		// Clear old animation set
		if(mAnimations.size() > 0)
		{
			for(auto anim: mAnimations)
				delete anim;

			mAnimations.clear();
		}

		// Read in animation count
		int32_t animCount = 0;
		stream.read((char*)&animCount, sizeof(int32_t));

		// Read animations
		for(int32_t i = 0; i < animCount; i++)
		{
			// Declare variables to be read in
			int32_t frameCount = 0;
			float   speed	   = 0;
			int16_t animMode   = 0;
			string  name	   = "";
			int32_t nameSize   = 0;

			// Read in variables
			stream.read((char*)&frameCount, sizeof(int32_t));
			stream.read((char*)&speed,		sizeof(float));
			stream.read((char*)&animMode,   sizeof(int16_t));
			stream.read((char*)&nameSize,   sizeof(int32_t));
			getline(stream, name, '\0');

			// Read frame in
			Animation* newAnim = new Animation(name, speed, (AnimationMode)animMode);
			for(int32_t l = 0; l < frameCount; l++)
			{
				Frame* newFrame = new Frame();
				stream.read((char*)&newFrame->position.x, sizeof(float));
				stream.read((char*)&newFrame->position.y, sizeof(float));
				stream.read((char*)&newFrame->width,	  sizeof(uint32_t));
				stream.read((char*)&newFrame->height,	  sizeof(uint32_t));
				newAnim->addFrame(newFrame);
			}

			// Add new animation to Animation Set
			mAnimations.push_back(newAnim);
		}

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse AnimationModule, failed due to incorrect passing of parameters");
	return false;
}

void AnimationModule::addAnimation(string name, float speed, AnimationMode mode)
{
	// Construct new animation and push back
	Animation* anim = new Animation(name, speed, mode);
	mAnimations.push_back(anim);

	// Output to console
	GameConsole::getInstance()->log("Info: Added new Animation %s to entity %p", name.c_str(), mParent);
}

void AnimationModule::addAnimation(Animation* anim)
{
	// Push back new animation
	mAnimations.push_back(anim);

	// Output to console
	GameConsole::getInstance()->log("Info: Added new Animation %s to entity %p", anim->getName().c_str(), mParent);
}

void AnimationModule::setAnimation(int index)
{
	mCurrentAnimation = index;
	mAnimations.at(mCurrentAnimation)->reset();
	mAnimations.at(mCurrentAnimation)->update(Module::mParent);
	mAnimationQueue.clear();
}

void AnimationModule::setAnimation(string animation)
{
	mAnimationQueue.clear();
	for(int32_t i = 0; i < mAnimations.size(); i++)
	{
		if(mAnimations[i]->getName() == animation)
		{
			mCurrentAnimation = i;
			mAnimations.at(mCurrentAnimation)->reset();
			mAnimations.at(mCurrentAnimation)->update(Module::mParent);
			break;
		}
	}
}

void AnimationModule::queueAnimation(int index) { mAnimationQueue.push_back(index); }
void AnimationModule::queueAnimation(string animation)
{
	for(int32_t i = 0; i < mAnimations.size(); i++)
	{
		if(mAnimations[i]->getName() == animation)
			mAnimationQueue.push_back(i);
	}
}

Animation* AnimationModule::getCurrentAnimation() { return mAnimations.at(mCurrentAnimation); }
Animation* AnimationModule::getAnimation(string animation)
{
	for(auto anim: mAnimations)
	{
		if(anim->getName() == animation)
			return anim;
	}
	return nullptr;
}

AnimationSet& AnimationModule::getAnimations()
{
	return mAnimations;
}

int32_t AnimationModule::animationCount()
{
	return mAnimations.size();
}

void AnimationModule::replaceAnimations(AnimationSet& anims)
{
	for(auto anim: mAnimations)
		delete anim;

	mAnimations.clear();
	mAnimations.insert(mAnimations.end(), anims.begin(), anims.end());
}

// ANIMATION CLASS
// ===============
Animation::Animation(const Animation& animation, AnimationMode mode) :
	mAnimationDirection(AnimationDirection_Right)
{
	mSpeed		  = animation.mSpeed;
	mName		  = animation.mName;
	mCurrentFrame = animation.mCurrentFrame;
	mHasEnded	  = animation.mHasEnded;
	mAccum		  = animation.mAccum;

	mStartFrame = 0;
	mEndFrame   = 0;

	// Push back new frames;
	for(auto frame: animation.mFrames)
		mFrames.push_back(new Frame(*frame));

	// Push back any passed frames and set mode
	_setAnimationMode(mode);
}

Animation::Animation(string name, float speed, AnimationMode mode) :
	mAnimationDirection(AnimationDirection_Left)
{
	mName  = name;
	mSpeed = speed;

	mHasEnded	  = false;
	mAccum		  = .0f;
	mCurrentFrame = 0;

	mStartFrame = 0;
	mEndFrame   = 0;

	// Push back any passed frames and set mode
	_setAnimationMode(mode);
}

Animation::~Animation()
{
	// Delete all frames
	// TODO: Delete frames without crashing
	/*for(auto frame: mFrames)
		delete frame;*/

	// Clear all frames from the set
	mFrames.clear();
}

void Animation::update(Entity* entity)
{
	// Add accumulated frame time to the accumulator
	mAccum += SceneManager::getInstance()->getDelta();

	if(mFrames.size() > 0 && mAccum > mSpeed)
	{
		// take away speed of animation from the accumulator
		mAccum -= mSpeed;

		// AnimationMode: Wrapping and Once
		if(mAnimationMode == AnimationMode_Wrap || 
		   mAnimationMode == AnimationMode_Once)
		{	
			if(mCurrentFrame < mEndFrame) mCurrentFrame++;  // Frames count in right direction
			else						  mHasEnded = true; // hit the end of the frame set, turn on ended flag

			if(mHasEnded)
			{
				// If the Animation Mode is set to run once and the loop has ended, just return
				if(mAnimationMode == AnimationMode_Once) 
					return;

				// If the Animation Mode is set to wrap, reset current frame and turn ended flag off
				if(mAnimationMode == AnimationMode_Wrap)
				{
					mCurrentFrame = 0;
					mHasEnded	  = false;
				}
			}
		}

		// AnimationMode: Ping Pong
		else if(mAnimationMode == AnimationMode_PingPong)
		{
			// Increment frames in AnimationDirection_Right
			if(mAnimationDirection == AnimationDirection_Right)
			{
				if(mHasEnded)
					mAnimationDirection = AnimationDirection_Left;

				if(mCurrentFrame < mEndFrame) mCurrentFrame++;  // Frames count in right direction
				else						  mHasEnded = true; // hit the end of the frame set, turn on ended flag
			}

			// Increment frames in AnimationDirection_Left
			else if(mAnimationDirection == AnimationDirection_Left)
			{
				if(mHasEnded)
					mAnimationDirection = AnimationDirection_Right;

				if(mCurrentFrame > mStartFrame) mCurrentFrame--;  // Frames count in left direction
				else							mHasEnded = true; // hit the end of the frame set, turn on ended flag
			}
		}

		// AnimationMode: Reverse
		else if(mAnimationMode == AnimationMode_Reverse)
		{
			// Same as wrapping but in reverse
			if(mCurrentFrame > mStartFrame) mCurrentFrame--;  // Frames count in right direction
			else							mHasEnded = true; // hit the end of the frame set, turn on ended flag

			// If the Animation is at the end of its life, reset current frame to last frame and restart
			if(mHasEnded)
			{
				mCurrentFrame = mFrames.size();
				mHasEnded	  = false;
			}
		}
	}

	// Update the sprite with the current frame
	_setFrame(entity, mCurrentFrame);
}

void Animation::reset()
{
	mHasEnded	  = false;
	mCurrentFrame = mStartFrame;
	mAccum		  = .0f;
}

string Animation::getName()
{
	return mName;
}

bool Animation::hasEnded()
{
	return mHasEnded;
}

float Animation::getSpeed()
{
	return mSpeed;
}

void Animation::addFrame(Frame* frame)
{
	mFrames.push_back(frame);
	_updateFrameRange();
}

void Animation::setFrames(FrameSet frames)
{
	mFrames = frames;
	_updateFrameRange();
}

bool Animation::setFrameRange(int32_t startFrame, int32_t endFrame)
{
	// Check frame boundaries to make sure they are valid
	if(mStartFrame < mFrames.size() && mStartFrame >= 0 &&
	   mEndFrame < mFrames.size()   && mEndFrame >= 0)
	{
		// Set frame limits
		mStartFrame = startFrame;
		mEndFrame   = endFrame;
	
		// Output info message about new frames
		GameConsole::getInstance()->log("Info: Animation %s's start frame set to %i and end frame set to %i", mName.c_str(), mStartFrame, mEndFrame);
		return true;
	}

	// Specified limits were out of bounds, output warning and return false
	GameConsole::getInstance()->log("Warning: Specified frame limits for %s are out of range", mName.c_str());
	return false;
}

Frame* Animation::getCurrentFrame()
{
	return mFrames.at(mCurrentFrame);
}

FrameSet& Animation::getFrames()
{
	return mFrames;
}

AnimationDirection Animation::animationDirection()
{
	return mAnimationDirection;
}

AnimationMode Animation::animationMode()
{
	return mAnimationMode;
}

int32_t Animation::frameCount()
{
	return mFrames.size();
}

void Animation::_setFrame(Entity* entity, int32_t frame)
{
	// Get current frame and set mCurrentFrame
	Frame* currentFrame = mFrames.at(frame);
	mCurrentFrame		= frame;

	// Clip the sprite using the current frame
	entity->getRenderMod()->clipRect(currentFrame->position, Vector2f(currentFrame->width, currentFrame->height));
	entity->getRenderMod()->tell(EventFlag_Pos, nullptr);
}

void Animation::_setAnimationMode(AnimationMode mode)
{
	// Change animation mode
	mAnimationMode = mode;

	// Start direction is the same for modes: Once, Wrap and PingPong
	if(mode == AnimationMode_Once || mode == AnimationMode_Wrap || mode == AnimationMode_PingPong)
		mAnimationDirection = AnimationDirection_Right;

	// If reversed, change direction to left
	if(mode == AnimationMode_Reverse)
		mAnimationDirection = AnimationDirection_Left;

	// Update frame range
	_updateFrameRange();
}

void  Animation::_updateFrameRange()
{
	// Update frame range
	mStartFrame = 0;
	mEndFrame   = mFrames.size() - 1;

	// If mode is: Once, Wrap or Ping Pong set current frame to start frame;
	if(mAnimationMode == AnimationMode_Once || 
	   mAnimationMode == AnimationMode_Wrap || 
	   mAnimationMode == AnimationMode_PingPong)
	{
		mCurrentFrame = mStartFrame;
	}

	// If mode is reverse set current frame to end frame
	if(mAnimationMode == AnimationMode_Reverse)
		mCurrentFrame = mEndFrame;
}
