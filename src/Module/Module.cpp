#include "Feanwork/Module/Module.h"
#include "Feanwork/Entity.h"
#include "Feanwork/EventListener.h"

Module::Module(ModuleType _type, Entity* parent)
{
	mType	= _type;
	mParent = parent;
}

Module::~Module()
{
}

void Module::render(RenderTarget* target)
{
}

void Module::update(UpdateStage stage)
{
}

void Module::tell(EventFlag flags, ListenerArgs* args)
{
}

void Module::tellNetwork(Packet* object)
{
}

void Module::setParent(Entity* parent)
{
	mParent = parent;
}

ModuleType Module::getType()
{
	return mType;
}

Entity* Module::getParent()
{
	return mParent;
}
