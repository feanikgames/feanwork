#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Entity.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Spatial/SpatialBase.h"
#include "Feanwork/GameConsole.h"

const Vector2f PhysicsModule::mGravity = Vector2f(0.f, 0.00000/*981f*/);

PhysicsModule::PhysicsModule(const PhysicsModule& module) :
	Module(ModuleType_Physics, module.mParent)
{
	mPhysicsType = module.mPhysicsType;
	mAnchorPoint = module.mAnchorPoint;
	mDirection   = module.mDirection;

	mMaxSpeed	  = module.mMaxSpeed;
	mAcceleration = module.mAcceleration;
	mVelocity	  = module.mVelocity;

	mLastCollision = module.mLastCollision;
	mBoundingBox   = module.mBoundingBox;
	mOrigin		   = module.mOrigin;
	mBitmaskGroup  = module.mBitmaskGroup;

	mPlatform = module.mPlatform;
	mMaterial = module.mMaterial;
}

PhysicsModule::PhysicsModule(Entity* parent) : 
	Module(ModuleType_Physics, parent)
{
	mPhysicsType = PhysicsType_Static;
}

PhysicsModule::PhysicsModule(PhysicsType physicsType, Entity* parent) :
	Module(ModuleType_Physics, parent)
{
	mPhysicsType = physicsType;
}

PhysicsModule::~PhysicsModule()
{
	mPlatform = nullptr;
}

/// <summary>
/// Init physics variables (not required unless you want entities to be affected by and to affect physics/entities)
/// </summary>
void PhysicsModule::initVars(float maxSpeed, float dynamicFriction, float staticFriction, float restitution, uint32_t maskGroup)
{
	mMaxSpeed	  = maxSpeed;
	mBitmaskGroup = maskGroup;

    mMaterial.dynamicFriction = dynamicFriction;
    mMaterial.staticFriction  = staticFriction;
    mMaterial.restitution     = restitution;

    mVelocity.x  = 0.f;
    mVelocity.y  = 0.f;
}

/// <summary>
/// Creates the bounding box for the physics module. If no variables are specified the function will look for the parameters itself
/// </summary>
void PhysicsModule::createBoundingBox(int32_t x, int32_t y, int32_t width, int32_t height)
{
    if (mParent)
    {
        mBoundingBox.left   = (int32_t)mParent->getPosition(Edge_Topleft).x;
        mBoundingBox.top    = (int32_t)mParent->getPosition(Edge_Topleft).y;
        mBoundingBox.width  = (int32_t)mParent->getDim().x;
        mBoundingBox.height = (int32_t)mParent->getDim().y;
    }

    if (x > 0) mBoundingBox.left = x;
    if (y > 0) mBoundingBox.top  = y;

    if (width > 0)  mBoundingBox.width  = width;
    if (height > 0) mBoundingBox.height = height;

    mOrigin.x      = mBoundingBox.width  / 2.f;
    mOrigin.y      = mBoundingBox.height / 2.f;
    mLastCollision = false;
}

/// <summary>
/// Updates the velocity of an entity and updates the position
/// </summary>
void PhysicsModule::update(UpdateStage stage)
{
    if (stage == UpdateStage_Pre)
    {
        if (mPhysicsType == PhysicsType_Actor || mPhysicsType == PhysicsType_Dynamic)
        {
            mVelocity += (mAcceleration + mGravity) * SceneManager::getInstance()->getDelta();
            if (Vectorial::vectorLengthFSq(mVelocity) > mMaxSpeed)
                mVelocity = Vectorial::vectorNormal(mVelocity) * mMaxSpeed;
        }
    }
    else if (stage == UpdateStage_Post)
    {
        mParent->addPosition(mVelocity * SceneManager::getInstance()->getDelta());
    }
}

Module* PhysicsModule::duplicate()
{
	PhysicsModule* module = new PhysicsModule(*this);
	return module;
}

/// <summary>
/// Intersection query between a mass amount of entities
/// </summary>
vector<Collision*> PhysicsModule::collidesWith(vector<Entity*> entities)
{
	vector<Collision*> collisionList;
    Collision* collision = nullptr;

    for (int i = 0; i < entities.size(); i++)
    {
        if (entities[i] != mParent && entities[i])
        {
            collision = collidesWith(entities[i]);
            if (collision != nullptr)
                collisionList.push_back(collision);
        }
    }
    return collisionList;
}

/// <summary>
/// Collision check between this entity and another single entity. 
/// </summary>
Collision* PhysicsModule::collidesWith(Entity* entity)
{
    PhysicsModule* targetPhys = entity->getPhysicsMod();
    Collision* collision      = nullptr;

    if (!targetPhys)
        return nullptr;

	// TODO: Bitmasks not working in Release mode, WTF O_o ... or are they? o_O
	if((targetPhys->getBitmaskGroup() & mBitmaskGroup) == 0)
		return nullptr;

	/*Scene*			scene = mParent->getScenePtr();
	vector<Entity*> ents  = scene->getSpatial()->queryRange(mParent->getPosition(Edge_Topleft),
															mParent->getPosition(Edge_Bottomright));*/

    Vector2f dist        = (targetPhys->getParent()->getPosition() - mParent->getPosition());
    Vector2f combinedDim = (mParent->getHalfDim() + targetPhys->getParent()->getHalfDim());
    Vector2f overlap     = combinedDim - Vectorial::vectorFabs(dist);

    if (overlap.x < 0.0f || overlap.y < 0.0f)
        return nullptr;

    // Otherwise calculate normals and create a collision
    collision = new Collision();
    if (overlap.x < overlap.y)
    {
        collision->overlap = overlap.x;
        if (dist.x < 0.0f)
        {
            collision->normal.x =  1.0f;
            collision->normal.y =  0.0f;
        }
        else
        {
            collision->normal.x = -1.0f;
            collision->normal.y =  0.0f;
        }
    }
    else
    {
        collision->overlap = overlap.y;
        if (dist.y < 0.0f)
        {
            collision->normal.x =  0.0f;
            collision->normal.y =  1.0f;
        }
        else
        {
            collision->normal.x =  0.0f;
            collision->normal.y = -1.0f;
        }
    }

    collision->collider = mParent;
    collision->target   = entity;
    return collision;
}

/// <summary>
/// Collision check with a ray that passes through the entities radius (intersection circle generated based on bounding box)
/// </summary>
RayCollision* PhysicsModule::collidesWith(Ray& ray)
{
	if((mBitmaskGroup & ray.mBitmaskGroup) == 0)
		return nullptr;

    float radiusSq = getOuterRadiusSq();
    float alpha    = 2 * Vectorial::vectorDotprod(mParent->getPosition() + ray.mOrigin, ray.mDirection);
    float beta     = Vectorial::vectorLengthFSq(mParent->getPosition() - ray.mOrigin) - radiusSq;

    if (beta < alpha * alpha / 4.0f)
        return nullptr;

    RayCollision* col = new RayCollision();
    col->ray          = ray;
    col->target       = mParent;

    float root = sqrt((double)(alpha * alpha / 4.0f - beta));
    float f    = alpha / 2.0f;

    if (fabs(f + root) > ray.mLength && fabs(f - root) > ray.mLength)
        return nullptr;

    col->factorA = (f - root);
    col->factorB = (f + root);
    col->pointA  = ray.mOrigin + ray.mDirection * col->factorA;
    col->pointB  = ray.mOrigin + ray.mDirection * col->factorB;
    return col;
}

/// <summary>
/// Resolves a collision between two entities by seperation
/// </summary>
void PhysicsModule::collisionResolution(vector<Collision*>& collisions)
{
    if (collisions.size() == 0)
        return;

	bool resolve = true;
	mParent->broadcast(EventFlag_Collision, new ListenerArgs(collisions));
	if(onCollision)
		resolve = onCollision(mParent, collisions);

	if(resolve)
	{
		for(auto col: collisions)
		{
			PhysicsModule* mod = col->target->getPhysicsMod();
			Vector2f relative  = (mVelocity - mod->mVelocity);
			float dot          = Vectorial::vectorDotprod(col->normal, mVelocity);

			if (dot > 0.0f)
				continue;

			//  Step 1: Calculate the impulse
			//TODO: Take both restitution values into account
			float epsilon    = mod->mMaterial.restitution;
			float j          = -(1.0f + epsilon) * dot;
			Vector2f impulse = j * col->normal;

			// Step 2: Apply impulse to both entities
			mVelocity += impulse;
			if (mod->mPhysicsType != PhysicsType_Static)
				mod->mVelocity -= impulse;

			// Step 3: Correct position
			Vector2f correction = (col->overlap * 0.2f) * col->normal;
			mParent->addPosition(correction);
			if (mod->mPhysicsType != PhysicsType_Static)
				col->target->addPosition(-correction);

			// Step 4: Solve velocity and apply friction
			Vector2f tangent = relative - Vectorial::vectorDotprod(relative, col->normal) * col->normal;
			tangent          = Vectorial::vectorNormal(tangent);

			float magnitude = -Vectorial::vectorDotprod(relative, tangent);
			float mu        = (mMaterial.staticFriction + mod->mMaterial.staticFriction) * 0.5f;

			Vector2f frictionImpulse;
			if (fabs(magnitude) < j * mu)
				frictionImpulse = magnitude * tangent;
			else
			{
				mu              = (mMaterial.dynamicFriction + mod->mMaterial.dynamicFriction) * 0.5f;
				frictionImpulse = -j * tangent * mu;
			}

			mVelocity += frictionImpulse;
			if (mod->mPhysicsType != PhysicsType_Static)
				mod->mVelocity -= frictionImpulse;
		}
	}

	for(auto col: collisions)
		delete col;
}

void PhysicsModule::applyImpulse(float x, float y)
{
	applyImpulse(Vector2f(x, y));
}

void PhysicsModule::applyImpulse(Vector2f impulse)
{
	mVelocity += impulse;

	if(Vectorial::vectorLengthFSq(mVelocity) > mMaxSpeed)
	{
		mVelocity  = Vectorial::vectorNormal(mVelocity);
		mVelocity *= mMaxSpeed;
	}
}

float PhysicsModule::getSpeed()
{
	// Return the current speed of the velocity (the length)
	return Vectorial::vectorLengthF(mVelocity);
}

float PhysicsModule::getMaxSpeed()
{
	return mMaxSpeed;
}

float PhysicsModule::getOuterRadius()
{
	//Radius of the outer circle = max(width, height)*sqrt(0.5)
	float   edge = max(mBoundingBox.width, mBoundingBox.height);
	return (edge * 0.707107f);
}

float PhysicsModule::getOuterRadiusSq()
{
	//Radius of the outer circle squared = max(width, height)^2/2
    float   edge = max(mBoundingBox.width, mBoundingBox.height);
    return (0.5f * edge * edge);
}

Vector2f PhysicsModule::getVelocity()
{
	return mVelocity;
}

PhysicsType PhysicsModule::getPhysicsType()
{
	return mPhysicsType;
}

uint32_t PhysicsModule::getBitmaskGroup()
{
	return mBitmaskGroup;
}

void PhysicsModule::setAcceleration(Vector2f value)
{
	mAcceleration = value;
}

void PhysicsModule::setVelocity(Vector2f value)
{
	setVelocity(value.x, value.y);
}

void PhysicsModule::setVelocity(float x, float y)
{
	mVelocity.x = x;
	mVelocity.y = y;
}

void PhysicsModule::setBitmaskGroup(uint32_t value)
{
	mBitmaskGroup = value;
}

bool PhysicsModule::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		stream.write((char*)&mPhysicsType,	 sizeof(int32_t));
		stream.write((char*)&mDirection,	 sizeof(int32_t));
		stream.write((char*)&mAnchorPoint.x, sizeof(float));
		stream.write((char*)&mAnchorPoint.y, sizeof(float));

		stream.write((char*)&mMaxSpeed,			  sizeof(float));
		stream.write((char*)&mBoundingBox.left,   sizeof(int32_t));
		stream.write((char*)&mBoundingBox.top,	  sizeof(int32_t));
		stream.write((char*)&mBoundingBox.width,  sizeof(int32_t));
		stream.write((char*)&mBoundingBox.height, sizeof(int32_t));

		stream.write((char*)&mMaterial.restitution,		sizeof(float));
		stream.write((char*)&mMaterial.dynamicFriction, sizeof(float));
		stream.write((char*)&mMaterial.staticFriction,  sizeof(float));

		return true;
	}
	else if(mode == ParseIn)
	{
		stream.read((char*)&mPhysicsType,   sizeof(int32_t));
		stream.read((char*)&mDirection,		sizeof(int32_t));
		stream.read((char*)&mAnchorPoint.x, sizeof(float));
		stream.read((char*)&mAnchorPoint.y, sizeof(float));

		stream.read((char*)&mMaxSpeed,			 sizeof(float));
		stream.read((char*)&mBoundingBox.left,   sizeof(int32_t));
		stream.read((char*)&mBoundingBox.top,	 sizeof(int32_t));
		stream.read((char*)&mBoundingBox.width,  sizeof(int32_t));
		stream.read((char*)&mBoundingBox.height, sizeof(int32_t));

		stream.read((char*)&mMaterial.restitution,	   sizeof(float));
		stream.read((char*)&mMaterial.dynamicFriction, sizeof(float));
		stream.read((char*)&mMaterial.staticFriction,  sizeof(float));

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse in PhysicsModule, failed due to incorrect passing of parameters");
	return false;
}
