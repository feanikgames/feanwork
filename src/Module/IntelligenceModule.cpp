#include "Feanwork/Module/IntelligenceModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Util/FeanRandom.h"
#include "Feanwork/Util/PriorityQueueMin.h"
#include "Feanwork/Entity.h"
#include "Feanwork/SceneManager.h"

#include <unordered_set>

bool AIGroup::isWithinRadius(Entity* entityA, Entity* entityB)
{
	Vector2f dist = (entityB->getPosition() - entityA->getPosition());
	return (Vectorial::vectorLengthFSq(dist) < mRadiusSq);
}

// -- IntelligenceModule Impl --
// =============================
IntelligenceModule::IntelligenceModule(const IntelligenceModule& module) :
	Module(ModuleType_Intel, module.mParent)
{
	mWanderTarget	 = module.mWanderTarget;
	mMaxWanderOffset = module.mMaxWanderOffset;
	mWanderRadius	 = module.mWanderRadius;
	mWanderDist	     = module.mWanderDist;
	mObstAvoidDist   = module.mObstAvoidDist;
	mPanicDistSq     = module.mPanicDistSq;

	mGroup		= module.mGroup;
	mNodeDistSq = module.mNodeDistSq;
	mPath		= new Path(*module.mPath);
}

IntelligenceModule::IntelligenceModule(Entity* parent) :
	Module(ModuleType_Intel, parent)
{
    mNodeDistSq  = 100.0f;
    mPanicDistSq = 100.0f;

	mPath   = nullptr;
	mTarget	= nullptr;
	mGroup  = nullptr;
}

IntelligenceModule::~IntelligenceModule()
{
	delete mPath;
	delete mGroup;
	mTarget = nullptr;
}

void IntelligenceModule::update(UpdateStage stage)
{
    PhysicsModule* module = mParent->getPhysicsMod();
    module->setAcceleration(calculate());
}

Module* IntelligenceModule::duplicate()
{
	IntelligenceModule* module = new IntelligenceModule(*this);
	return module;
}

bool IntelligenceModule::parse(fstream& stream, ParseMode mode)
{
	// TODO: Parse the intelligence module
	return true;
}

Vector2f IntelligenceModule::calculate()
{
	Vector2f steeringForce;
    Vector2i mouse         = Mouse::getPosition(*SceneManager::getInstance()->getScreen());
    Vector2f newMouse      = Vector2f(mouse.x, mouse.y);

    /*steeringForce += avoidObstacles(mObstacles) * .8f;
	if(mTarget)
		steeringForce += pursue(mTarget) * .2f;
    steeringForce += separation() * .8f;
    steeringForce += alignment()  * .5f;
    steeringForce += cohesion()   * .5f;*/
    //mParent->getAnimationMod()->setAnimation("fly");

    return steeringForce;
}

/// <summary>
/// Calculate the hiding position for an obstacle
/// </summary>
Vector2f IntelligenceModule::getHidingPosition(Vector2f positionOb, float radiusOb, Vector2f positionTar)
{
    const float boundaryDistance = 30.0f;
    float       distance         = radiusOb + boundaryDistance;
    Vector2f    toObstacle       = Vectorial::vectorNormal(positionOb - positionTar);

    return (toObstacle * distance) + positionOb;
}

 /// <summary>
/// Used to seek a specific location on the map (warning: if you don't use this with obstacle avoidance the player will just walk through the map)
/// </summary>
Vector2f IntelligenceModule::seek(Vector2f target)
{
    PhysicsModule* mod = mParent->getPhysicsMod();
    Vector2f velocity  = (target - mParent->getPosition());
    velocity           = Vectorial::vectorNormal(velocity) * mod->getMaxSpeed();
    return (velocity - mod->getVelocity());
}

/// <summary>
/// Selects a point on the map for the entity to flee at a point
/// </summary>
Vector2f IntelligenceModule::flee(Vector2f target)
{
    PhysicsModule* mod =  mParent->getPhysicsMod();
    Vector2f velocity  = (mParent->getPosition() - target);
    velocity           = Vectorial::vectorNormal(velocity) * mod->getMaxSpeed();
    return (velocity - mod->getVelocity());
}

/// <summary>
/// Pursues the target by calculating slightly ahead of time where they will be
/// </summary>
Vector2f IntelligenceModule::pursue(Entity* target)
{
	if(!target)
		return Vector2f();

    Vector2f dist        = (mParent->getPosition() - target->getPosition());
    PhysicsModule* physT = target->getPhysicsMod();
    PhysicsModule* physP = mParent->getPhysicsMod();

    //If straight ahead and facing, seek
    if ((Vectorial::vectorDotprod(dist, physP->getVelocity()) > 0) &&
        (Vectorial::vectorDotprod(physP->getVelocity(), physT->getVelocity()) < -0.96))
        return seek(target->getPosition());

    //Predict target's future position in order to intercept it
    float deltaTime = Vectorial::vectorLengthF(dist) / (physP->getMaxSpeed() + physT->getSpeed());
    return seek(target->getPosition() + physP->getVelocity() * deltaTime);
}

/// <summary>
/// Evade another entity on the map by passing a target
/// </summary>
Vector2f IntelligenceModule::evade(Entity* target)
{
	if(!target)
		return Vector2f();

    PhysicsModule* mod =  mParent->getPhysicsMod();
    Vector2f dist      = (mParent->getPosition() - target->getPosition());

    if (Vectorial::vectorLengthFSq(dist) > mPanicDistSq)
        return Vector2f(0, 0);

    float deltaTime = Vectorial::vectorLengthF(dist) / (mod->getMaxSpeed() + target->getPhysicsMod()->getSpeed());
    return flee(target->getPosition() + mod->getVelocity() * deltaTime);
}

/// <summary>
/// Moves the entity in a specified direction to a location but also decelerates them at a nice speed for realism (and non-jitter on arrival)
/// </summary>
Vector2f IntelligenceModule::arrive(Vector2f target, float deceleration)
{
    PhysicsModule* mod = mParent->getPhysicsMod();
    Vector2f velocity  = (target - mParent->getPosition());
    float bias         = 4.0f;
    float dist         = Vectorial::vectorLengthFSq(velocity);

    if (dist <= bias)
        return Vector2f(0, 0);

    dist        = sqrt(dist);
    float speed = dist / deceleration;
    speed       = min(speed, mod->getMaxSpeed());
    velocity   *= speed / dist;
    return (velocity - mod->getVelocity());
}

//NOTE: Rather expensive! Use with care
//TODO: Think of a more efficient implementation
/// <summary>
/// Forces the entity to wander around the level
/// </summary>
Vector2f IntelligenceModule::wander()
{
    mWanderTarget += Vector2f(FeanRandom::randRangeF(-1, 1), FeanRandom::randRangeF(-1, 1));
    mWanderTarget *= mMaxWanderOffset;
    mWanderTarget  = Vectorial::vectorNormal(mWanderTarget) * mWanderRadius;

    //Calculate global coords of wander target
    Vector2f target = mWanderTarget + Vectorial::vectorNormal(mParent->getPhysicsMod()->getVelocity()) * mWanderDist;
    return seek(target);
}

/// <summary>
/// Moves the player to an imaginary 'midpoint' between two entities
/// </summary>
Vector2f IntelligenceModule::interpose(Entity* entityA, Entity* entityB)
{
	if(!entityA || !entityB)
		return Vector2f();

    Vector2f midPoint = (entityA->getPosition() - entityB->getPosition()) / 2.f;
    float timeToReach = Vectorial::vectorLengthF(mParent->getPosition() - midPoint) / mParent->getPhysicsMod()->getMaxSpeed();

    Vector2f aPos = entityA->getPosition() + entityA->getPhysicsMod()->getVelocity() * timeToReach;
    Vector2f bPos = entityB->getPosition() + entityB->getPhysicsMod()->getVelocity() * timeToReach;

    midPoint = (aPos + bPos) / 2.0f;
    return arrive(midPoint, 0.2f);
}

/// <summary>
/// Find the best hiding spot that puts an obstacle between you and a specified target
/// </summary>
Vector2f IntelligenceModule::hide(Entity* target, vector<Entity*>& obstacles)
{
    if (!target || obstacles.size() == 0)    
		return Vector2f();

	Vector2f bestSpot;
    float    distanceToClosest = numeric_limits<float>::max();

    for(auto entity: obstacles)
    {
        Vector2f spot  = getHidingPosition(entity->getPosition(), entity->getPhysicsMod()->getOuterRadius(), target->getPosition());
        float distance = Vectorial::vectorLengthFSq(spot + mParent->getPosition());

        if (distance < distanceToClosest)
        {
            distanceToClosest = distance;
            bestSpot          = spot;
        }
    }

    if (distanceToClosest == numeric_limits<float>::max())
        return evade(target);

    return arrive(bestSpot, 0.2f);
}

// Grouped Steering Behaviours
Vector2f IntelligenceModule::separation()
{
	if(!mGroup)
		return Vector2f();

    Vector2f force;
    for(auto entity: mGroup->mNeighbours)
    {
        if (entity != mParent && mGroup->isWithinRadius(mParent, entity))
        {
            Vector2f distance = mParent->getPosition() - entity->getPosition();
            float length      = Vectorial::vectorLengthF(distance);
            force            += distance / (length * length);
        }
    }
    return force;
}

Vector2f IntelligenceModule::alignment()
{
	if(!mGroup)
		return Vector2f();

    Vector2f heading;
    int32_t  neighbourCount = 0;

    for(auto entity: mGroup->mNeighbours)
    {
        if ((entity != mParent) && (mGroup->isWithinRadius(mParent, entity)))
        {
            heading += Vectorial::vectorNormal(entity->getPhysicsMod()->getVelocity());
            ++neighbourCount;
        }
    }

    if (neighbourCount > 0)
    {
        heading /= (float)neighbourCount;
        heading -= Vectorial::vectorNormal(mParent->getPhysicsMod()->getVelocity());
    }
    return heading;
}

Vector2f IntelligenceModule::cohesion()
{
	if(!mGroup)
		return Vector2f();

    Vector2f centreMass, steeringForce;
    int32_t  neighbourCount = 0;

    for(auto entity: mGroup->mNeighbours)
    {
        if ((entity != mParent) && (mGroup->isWithinRadius(mParent, entity)))
        {
            centreMass += entity->getPosition();
            ++neighbourCount;
        }
    }

    if (neighbourCount > 0)
    {
        centreMass   /= (float)neighbourCount;
        steeringForce = seek(centreMass);
    }
    return steeringForce;
}
// ---------------------------

/// <summary>
/// Creates a ray from the point of the player across the game to check for upcoming obstacles, then returns a force pointing away from them
/// </summary>
Vector2f IntelligenceModule::avoidObstacles(vector<Entity*>& entities)
{
    PhysicsModule* phys = mParent->getPhysicsMod();
    Ray ray(mParent->getPosition(), Vectorial::vectorNormal(phys->getVelocity()), mObstAvoidDist);

    Vector2f closestPoint;
    float    closestDist = numeric_limits<float>::max();

    for(auto ent: entities)
    {
        RayCollision* col = ent->getPhysicsMod()->collidesWith(ray);
        if (!col)
            continue;

        //Find closest intersection
        Vector2f point;
        float    dist;

        if (col->factorA < 0.0f)
        {
            if (col->factorB < 0.0f) continue;
            else
            {
                point = col->pointB;
                dist  = col->factorB;
            }
        }
        else
        {
            point = col->pointA;
            dist  = col->factorA;
        }

        if (dist < closestDist)
        {
            closestPoint = point;
            closestDist  = dist;
        }
    }

    if (closestDist == numeric_limits<float>::max())
        return Vector2f();

    Vector2f vel  = Vectorial::vectorInvert(ray.mDirection * phys->getMaxSpeed());
    vel          *= (1.0f - closestDist / mObstAvoidDist);
    return vel;
}

/// <summary>
/// Follows a specified path stored in a Node list (Warning: Must construct math in A* first!)
/// </summary>
Vector2f IntelligenceModule::followPath()
{
    if (!mPath)
        return Vector2f(0, 0);

    Vector2f dist = mPath->getCurrentNode()->getPosition() - mParent->getPosition();
    if (Vectorial::vectorLengthFSq(dist) < mNodeDistSq)
        mPath->nextNode();

    if (!mPath->isFinished()) return seek(mPath->getCurrentNode()->getPosition());
    else					  return arrive(mPath->getCurrentNode()->getPosition(), 0.2f);
}

// -- Path Class Impl --
// =====================
Path::Path()
{
	mCurIndex  = 0;
	mMode	   = PathMode_OneWay;
	mDirection = 1;
	mFinished  = false;
}

/// <summary>
/// Search for the next node that exists in the Path
/// </summary>
void Path::nextNode()
{
    if(mFinished)
        return;

    mCurIndex += mDirection;
    if (mCurIndex == mNodes.size() || mCurIndex == -1)
    {
        switch(mMode)
        {
            case PathMode_OneWay:
                mFinished = true;
                break;
            case PathMode_Patrol:
                mDirection *= -1;
                mCurIndex  += mDirection;
                break;
            case PathMode_Loop:
                mCurIndex -= mDirection * mNodes.size();
                break;
        }
    }
}

NavNode* Path::getCurrentNode()
{
    return mNodes[mCurIndex];
}

bool Path::isFinished()
{
	return mFinished;
}

vector<NavNode*>& Path::getNodes()
{
	return mNodes;
}

// -- A* Pathfinding Impl --
// =========================
AStar::AStar(NavGraph* graph)
{
    mGCosts = new float[graph->numNodes()];
    mFCosts = new float[graph->numNodes()];
    mGraph  = graph;
	mPath   = nullptr;
}

AStar::~AStar()
{
	delete[] mGCosts;
	delete[] mFCosts;
	delete   mPath;

	mSPT.clear();
}

/// <summary>
/// A* Pathfinding Algorithm. Searches for the fastest path between two nodes and then gives that result to the path.
/// </summary>
Path* AStar::findPath(int32_t start, int32_t target)
{
    if (start == target)
        return nullptr;

    mSPT.clear();

    PriorityQueueMin<int32_t> openList(mGraph->numNodes(), bind(&AStar::compareNodes, this, placeholders::_1, placeholders::_2));
    unordered_set<int32_t>	  closedList;
    int32_t*				  cameFrom = new int32_t[mGraph->numNodes()];

    openList.insert(start);
    mGCosts[start] = 0;
    mFCosts[start] = Vectorial::manhattanDistance(mGraph->mNodes[start].getPosition(), mGraph->mNodes[target].getPosition());

    while (!openList.isEmpty())
    {
        // Find closest node and add to Shortest Path Tree (SPT)
        int32_t closestNode = openList.pop(0);
        mSPT.push_back(&mGraph->mNodes[closestNode]);
        closedList.insert(closestNode);

        if (closestNode == target)
        {
            Path*   path = new Path();
            int32_t i	 = target;

            do
            {
                path->getNodes().insert(path->getNodes().begin(), &mGraph->mNodes[i]);
                i = cameFrom[i];
            }
            while (i != start);
            path->getNodes().insert(path->getNodes().begin(), &mGraph->mNodes[start]);
            mPath = path;
            return path;
        }

        // Expand Current Node
        for(auto edge: mSPT.back()->getEdges())
        {
            if (closedList.find(edge->mTo) != closedList.end())
                continue; //Already evaluated node

            float gCost = mGCosts[closestNode] + edge->getCost();
            float hCost = Vectorial::manhattanDistance(mGraph->mNodes[edge->mTo].getPosition(), mGraph->mNodes[target].getPosition());

            bool open = openList.contains(edge->mTo);
            if (!open || gCost < mGCosts[edge->mTo])
            {
                cameFrom[edge->mTo] = closestNode;
                mFCosts[edge->mTo]  = gCost + hCost;
                mGCosts[edge->mTo]  = gCost;

                if (!openList.contains(edge->mTo)) openList.insert(edge->mTo);
                else                               openList.updatePriority(edge->mTo);
            }
        }
    }

    return nullptr;
}

bool AStar::compareNodes(int32_t& a, int32_t& b)
{
    return (mFCosts[a] < mFCosts[b]);
}

void AStar::setGraph(NavGraph* graph)
{
   delete[] mGCosts;
   delete[] mFCosts;
   delete   mPath;

   mGCosts = new float[graph->numNodes()];
   mFCosts = new float[graph->numNodes()];
   mGraph  = graph;
}

void AStar::debugRender(RenderTarget* target)
{
	if(!mPath)
		return;

    CircleShape shape(2.0f, 16);
    shape.setFillColor(Colour(224, 29, 7));

    for(auto node: mPath->getNodes())
    {
        if(!node)
			continue;

        shape.setPosition(node->getPosition());
        target->draw(shape);
    }
}

/// <summary>
/// Returns the Shortest Path Tree calculated by the findPath() function
/// </summary>
vector<NavNode*> AStar::getPath() const
{
	return mSPT;
}

// -- Abstract Navigation Graph Base --
// ====================================
NavGraph::~NavGraph()
{
	delete[] mNodes;
}

uint32_t NavGraph::numNodes()
{
	return mNodeCount;
}

SimpleNavGraph::SimpleNavGraph(int width, int height, float spacing, Vector2f origin)
{
	mSpacing   = spacing;
    mNodes     = new NavNode[width * height];
	mNodeCount = width * height;
    mWidth     = width;
    mHeight    = height;
    mTopleft   = origin - Vector2f(width, height) / 2.0f;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (x > 5 && x < 15 && (y < 2 || y > 12))
                continue;

            int index = y * width + x;
            mNodes[index].setPosition(Vector2f(x, y) * mSpacing + mTopleft);

            //Create edges
            if (x > 0)          mNodes[index].getEdges().push_back(new NavEdge(index, index - 1, 1.0f));
            if (x < width - 1)  mNodes[index].getEdges().push_back(new NavEdge(index, index + 1, 1.0f));
            if (y > 0)          mNodes[index].getEdges().push_back(new NavEdge(index, index - width, 1.0f));
            if (y < height - 1) mNodes[index].getEdges().push_back(new NavEdge(index, index + width, 1.0f));

            if (x > 0 && y > 0)                  mNodes[index].getEdges().push_back(new NavEdge(index, index - 1 - width, 1.0f));
            if (x > 0 && y < height - 1)         mNodes[index].getEdges().push_back(new NavEdge(index, index - 1 + width, 1.0f));
            if (x < width - 1 && y > 0)          mNodes[index].getEdges().push_back(new NavEdge(index, index + 1 - width, 1.0f));
            if (x < width - 1 && y < height - 1) mNodes[index].getEdges().push_back(new NavEdge(index, index + 1 + width, 1.0f));
        }
    }
}

SimpleNavGraph::~SimpleNavGraph()
{
}

/// <summary>
/// Returns the closest node to a specified point in the world
/// </summary>
int32_t SimpleNavGraph::getClosestNode(Vector2f position)
{
    position -= mTopleft;
    position /= mSpacing;

    position.x = max(min(position.x, mWidth  - 1.0f), 0.0f);
    position.y = max(min(position.y, mHeight - 1.0f), 0.0f);
    return (mWidth * (int32_t)position.y + (int32_t)position.x); // Can we please fix this instead of talking shit? sure go ahead -.-
}


// Debugging Functionality
void SimpleNavGraph::debugRender(RenderTarget* target)
{
    CircleShape shape(2.0f, 16);
    shape.setFillColor(Colour(0, 162, 232));

    for(uint32_t i = 0; i < mNodeCount; i++)
    {
        shape.setPosition(mNodes[i].getPosition());
        target->draw(shape);
    }
}

// -- Navigation Node Definition --
// ================================
NavNode::NavNode()
{
}

NavNode::NavNode(Vector2f position)
{
	mPosition = position;
}

NavNode::~NavNode()
{
	for(auto edge: mEdges)
		delete edge;

	mEdges.clear();
}

vector<NavEdge*>& NavNode::getEdges()
{
	return mEdges;
}

Vector2f NavNode::getPosition()
{
	return mPosition;
}

void NavNode::setPosition(Vector2f position)
{
	mPosition = position;
}

void NavNode::mergeNodes(vector<NavEdge*>& edges)
{
	for(auto edge: edges)
		mEdges.push_back(edge);
}

NavEdge::NavEdge(int32_t from, int32_t to, float cost)
{
	mFrom = from;
	mTo   = to;
	mCost = cost;
}

NavEdge::~NavEdge()
{
}

void NavEdge::setCost(float cost)
{
	mCost = cost;
}

float NavEdge::getCost()
{
	return mCost;
}
