#include "Feanwork/Module/SoundModule.h"
#include "Feanwork/GameConsole.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Scene.h"
#include "Feanwork/SoundManager.h"

SoundModule::SoundModule(const SoundModule& module) :
	Module(ModuleType_Sound, module.mParent)
{
	mLoops  = module.mLoops;
	mSounds = module.mSounds;
}

SoundModule::SoundModule(bool loops, Entity* parent) :
	Module(ModuleType_Sound, parent),
	mLoops(loops)
{
}

SoundModule::~SoundModule()
{
	mSounds.clear();
}

Module* SoundModule::duplicate()
{
	// Return copy of SoundModule
	SoundModule* module = new SoundModule(*this);
	return module;
}

void SoundModule::insert(string name)
{
	// Store sound into temporary variable
	FeanSound* sound = ResourceCache::getInstance()->retrieveSound(name);

	// If the sound exists push it back
	if(sound) 
		mSounds.push_back(sound);
	
	// Couldn't insert the sound, it didn't exist
	else 
		GameConsole::getInstance()->log("Warning: Could not find sound %s in ResourceCache!", name);
}

void SoundModule::trigger(string soundName)
{
	// Loop through sounds, if found send to Sound Manager
	for(auto sound: mSounds)
	{
		if(sound->mSoundName == soundName)
		{
			mParent->getScenePtr()->getSoundMgr()->addSoundFX(sound, mParent->getPosition());
			return;
		}
	}

	// If the program reaches this point, the sound was not found
	GameConsole::getInstance()->log("Warning: Sound %s was not found", soundName.c_str());
}

FeanSound* SoundModule::getSound(string soundName)
{
	// Loop through sounds, if found send to Sound Manager
	for(auto sound: mSounds)
	{
		if(sound->mSoundName == soundName)
			return sound;
	}

	// If the program reaches this point, the sound was not found
	GameConsole::getInstance()->log("Warning: Sound %s was not found", soundName.c_str());
	return nullptr;
}

bool SoundModule::parse(fstream& stream, ParseMode mode)
{
	if (mode == ParseOut)
	{
		// Write loop flag to file
		stream.write((char*)mLoops, sizeof(bool));

		// Write out sound names to file
		for(auto sound: mSounds)
		{
			string sndName = sound->getName();
			stream.write(sndName.c_str(), sndName.length() + 1);
		}

		return true;
	}
	else if (mode == ParseIn)
	{
		// Read loop flag into program
		stream.read((char*)mLoops, sizeof(bool));

		// Read in sound names and load them in
		for(auto sound: mSounds)
		{
			string sndName = "";
			getline(stream, sndName, '\0');
			mSounds.push_back(ResourceCache::getInstance()->retrieveSound(sndName));
		}

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse SoundModule, failed due to incorrect passing of parameters");
	return false;
}
