#include "Feanwork/Module/EmitterModule.h"
#include "Feanwork/Util/FeanRandom.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Entity.h"
#include "Feanwork/ResourceCache.h"
#include "Feanwork/GameConsole.h"

using namespace FeanRandom;

EmitterModule::EmitterModule(const EmitterModule& module) :
	Module(ModuleType_Emitter, module.mParent)
{
	for(auto emitter: mEmitters)
		mEmitters.push_back(new Emitter(*emitter));
}

EmitterModule::EmitterModule(Entity* parent) : 
	Module(ModuleType_Emitter, parent)
{
}

EmitterModule::~EmitterModule(void)
{
	for(auto emitter: mEmitters)
		delete emitter;

	mEmitters.clear();
}

void EmitterModule::update(UpdateStage stage)
{
    if(stage == UpdateStage_Pre)
    {
        for(auto emitter: mEmitters)
        {
            emitter->setPosition(Module::mParent->getPosition());
            emitter->update();
        }
    }
}

void EmitterModule::render(RenderTarget* target)
{
    for(auto emitter: mEmitters)
        emitter->render(target);
}

Module* EmitterModule::duplicate()
{
	EmitterModule* module = new EmitterModule(*this);
	return module;
}

void EmitterModule::addEmitter(Emitter* newEmitter) 
{ 
	mEmitters.push_back(newEmitter); 
}

bool EmitterModule::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		int32_t emitterCount = mEmitters.size();
		stream.write((char*)&emitterCount, sizeof(int32_t));

		for(auto emitter: mEmitters)
		{
			Vector2f position  = emitter->getPosition();
			Vector2f direction = emitter->getDirection();

			stream.write(emitter->resourceString().c_str(), emitter->resourceString().length() + 1);
			stream.write((char*)&position.x,  sizeof(float));
			stream.write((char*)&position.y,  sizeof(float));
			stream.write((char*)&direction.x, sizeof(float));
			stream.write((char*)&direction.y, sizeof(float));

			int32_t  emitType	 = (int32_t)emitter->getEmitType();
			uint32_t partCount	 = emitter->getParticleCount();
			float	 birthRate	 = emitter->getBirthRate();
			float	 pulseRate	 = emitter->getPulseRate();
			float	 pulseLength = emitter->getPulseLength();
			bool	 canLoop	 = emitter->canLoop();

			stream.write((char*)&emitType,    sizeof(int32_t));
			stream.write((char*)&partCount,   sizeof(uint32_t));
			stream.write((char*)&birthRate,   sizeof(float));
			stream.write((char*)&pulseRate,	  sizeof(float));
			stream.write((char*)&pulseLength, sizeof(float));
			stream.write((char*)&canLoop,	  sizeof(bool));

			Vector2f posVariant		  = emitter->getPositionVariant();
			float	 lifeLimit		  = emitter->getLifeLimit();
			float	 lifeLimitVariant = emitter->getLifeLimitVariant();

			stream.write((char*)&posVariant.x,	   sizeof(float));
			stream.write((char*)&posVariant.y,	   sizeof(float));
			stream.write((char*)&lifeLimit,		   sizeof(float));
			stream.write((char*)&lifeLimitVariant, sizeof(float));

			// Write colours
			for(int32_t i = 0; i < 4; i++)
			{
				Colour color = emitter->mColours[i];
				stream.write((char*)&color.r, sizeof(Uint8));
				stream.write((char*)&color.g, sizeof(Uint8));
				stream.write((char*)&color.b, sizeof(Uint8));
				stream.write((char*)&color.a, sizeof(Uint8));
			}

			// Write sizes
			for(int32_t i = 0; i < 4; i++)
			{
				float size = emitter->mSizes[i];
				stream.write((char*)&size, sizeof(float));
			}

			// Write speeds
			for(int32_t i = 0; i < 4; i++)
			{
				float speed = emitter->mSpeeds[i];
				stream.write((char*)&speed, sizeof(float));
			}
		}

		return true;
	}
	else if(mode == ParseIn)
	{
		int32_t emitterCount = 0;
		stream.read((char*)&emitterCount, sizeof(int32_t));

		for(int32_t e = 0; e < emitterCount; e++)
		{
			Vector2f position;
			Vector2f direction;
			Vector2f force;
			string	 resource = "";

			getline(stream, resource, '\0');
			stream.read((char*)&position.x,  sizeof(float));
			stream.read((char*)&position.y,  sizeof(float));
			stream.read((char*)&direction.x, sizeof(float));
			stream.read((char*)&direction.y, sizeof(float));

			int32_t  emitType	 = 0;
			uint32_t partCount	 = 0;
			float	 birthRate	 = 0;
			float	 pulseRate	 = 0;
			float	 pulseLength = 0;
			bool	 canLoop	 = 0;

			stream.read((char*)&emitType,    sizeof(int32_t));
			stream.read((char*)&partCount,   sizeof(uint32_t));
			stream.read((char*)&birthRate,   sizeof(float));
			stream.read((char*)&pulseRate,	 sizeof(float));
			stream.read((char*)&pulseLength, sizeof(float));
			stream.read((char*)&canLoop,	 sizeof(bool));

			Vector2f posVariant;
			float	 lifeLimit		  = 0;
			float	 lifeLimitVariant = 0;

			stream.read((char*)&posVariant.x,	  sizeof(float));
			stream.read((char*)&posVariant.y,	  sizeof(float));
			stream.read((char*)&lifeLimit,		  sizeof(float));
			stream.read((char*)&lifeLimitVariant, sizeof(float));

			Emitter* emitter = new Emitter(position, direction, (EmitterType)emitType, resource, canLoop);
			emitter->init(partCount, lifeLimit, lifeLimitVariant, pulseRate, pulseLength, posVariant);
			
			// Write colours
			for(int32_t i = 0; i < 4; i++)
			{
				Colour color;
				stream.read((char*)&color.r, sizeof(Uint8));
				stream.read((char*)&color.g, sizeof(Uint8));
				stream.read((char*)&color.b, sizeof(Uint8));
				stream.read((char*)&color.a, sizeof(Uint8));
				emitter->mColours[i] = color;
			}

			// Write sizes
			for(int32_t i = 0; i < 4; i++)
			{
				float size;
				stream.read((char*)&size, sizeof(float));
				emitter->mSizes[i] = size;
			}

			// Write speeds
			for(int32_t i = 0; i < 4; i++)
			{
				float speed = 0;
				stream.read((char*)&speed, sizeof(float));
				emitter->mSpeeds[i] = speed;
			}
			addEmitter(emitter);
		}

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse in EmitterModule, failed due to incorrect passing of parameters");
	return false;
}

// Emitter Impl
// ============
Emitter::Emitter(const Emitter& emitter)
{
	mColours[0] = emitter.mColours[0];
	mColours[1] = emitter.mColours[1];
	mColours[2] = emitter.mColours[2];
	mColours[3] = emitter.mColours[3];

	mSizes[0] = emitter.mSizes[0];
	mSizes[1] = emitter.mSizes[1];
	mSizes[2] = emitter.mSizes[2];
	mSizes[3] = emitter.mSizes[3];

	mSpeeds[0] = emitter.mSpeeds[0];
	mSpeeds[1] = emitter.mSpeeds[1];
	mSpeeds[2] = emitter.mSpeeds[2];
	mSpeeds[3] = emitter.mSpeeds[3];

	mResource  = emitter.mResource;
	mPosition  = emitter.mPosition;
	mDirection = emitter.mDirection;
	mForce	   = emitter.mForce;

	mEmitterType   = emitter.mEmitterType;
	mParticleCount = emitter.mParticleCount;
	mBirthRate	   = emitter.mBirthRate;
	mPulseRate	   = emitter.mPulseRate;
	mPulseLength   = emitter.mPulseLength;

	mParticles = new Particle[mParticleCount];
	for(int32_t i = 0; i < mParticleCount; i++)
		mParticles[i] = emitter.mParticles[i];

	mAccumulated = emitter.mAccumulated;
	mCanLoop	 = emitter.mCanLoop;
	mActive		 = emitter.mActive;

	mLifeLimit		  = emitter.mLifeLimit;
	mLifeLimitVariant = emitter.mLifeLimitVariant;
	mPositionVariant  = emitter.mPositionVariant;
}

Emitter::Emitter(Vector2f pos, Vector2f direction, EmitterType type, string resource, bool canLoop)
{
	mResource    = resource;
    mPosition    = pos;
    mDirection   = direction;
    mForce       = Vector2f(0, -0.0001f);
    mEmitterType = type;

    mParticles     = nullptr;
    mParticleCount = 0;

    mAccumulated = 0.0f;
    mCanLoop     = canLoop;
    mActive      = true;
}

Emitter::~Emitter()
{
	delete[] mParticles;
}

void Emitter::init(uint32_t count, float lifeLimit, float lifeLimitVariant, float pulseRate, float pulseLength, Vector2f posVariant)
{
	mLifeLimit        = lifeLimit;
	mLifeLimitVariant = lifeLimitVariant;
	mPositionVariant  = posVariant;
	mPulseRate        = pulseRate;
	mPulseLength      = pulseLength;
	setParticleCount(count);
}

/// <summary>
/// Update all the particles inside the emit. Kill them if they exceed their life and update their variables.
/// </summary>
void Emitter::update()
{
    if(!mActive)
        return;

	float d				  = SceneManager::getInstance()->getDelta();
    mAccumulated		 += d;
    uint32_t newParticles = mBirthRate * mAccumulated;
    bool resume           = false;

    if(newParticles > 0)
        mAccumulated = 0.0f;

    Particle* currentParticle;
    for (uint32_t i = 0; i < mParticleCount; i++)
    {
        currentParticle         = &mParticles[i];
        currentParticle->mLife += d;

        if (currentParticle->mLife > currentParticle->mLifeLimit)
        {
            if (newParticles > 0)
            {
                if (!mCanLoop && (currentParticle->mLifeLimit > 0.0f))
                    continue;

                emitParticle(*currentParticle);
                newParticles--;
            }

            resume = true;
            continue;
        }

        currentParticle->mDirection += (mForce * d);
        currentParticle->move(currentParticle->mDirection * currentParticle->mSpeed * d);

        float ratio        = currentParticle->mLife / currentParticle->mLifeLimit;
        float inverseRatio = (1.0f - ratio);

        if (ratio > 1.0f)
            ratio = 1.0f;

        float scale             = (inverseRatio * currentParticle->mStartSize + ratio  * currentParticle->mEndSize);
        currentParticle->mSpeed = (inverseRatio * currentParticle->mStartSpeed + ratio * currentParticle->mEndSpeed);
        currentParticle->setScale(scale, scale);

        Colour newColour;
        newColour.r = (int8_t)(inverseRatio * currentParticle->mStartColour.r + ratio * currentParticle->mEndColour.r);
        newColour.g = (int8_t)(inverseRatio * currentParticle->mStartColour.g + ratio * currentParticle->mEndColour.g);
        newColour.b = (int8_t)(inverseRatio * currentParticle->mStartColour.b + ratio * currentParticle->mEndColour.b);
        newColour.a = (int8_t)(inverseRatio * currentParticle->mStartColour.a + ratio * currentParticle->mEndColour.a);

        currentParticle->setColour(newColour);
        resume = true;
    }

    if (!mCanLoop && !resume)
        mActive = false;
}

/// <summary>
/// Render all active sprites to the screen with the additive blend flag
/// </summary>
void Emitter::render(RenderTarget* target)
{
    if (!mActive)
        return;

    for(uint32_t i = 0; i < mParticleCount; i++)
    {
        if (mParticles[i].mLife < mParticles[i].mLifeLimit)
            target->draw(*mParticles[i].getSprite(), RenderStates(BlendAdd));
    }
}

/// <summary>
/// Once an unused particle is found re-emit the particle from the specified emitter coordinates
/// </summary>
void Emitter::emitParticle(Particle& particle)
{
    if (mEmitterType == EmitterType_Circular)    particle.mDirection = randomUnitVector();
    if (mEmitterType == EmitterType_Directional) particle.mDirection = this->mDirection;

    particle.setPosition((randRangeF(-1.f, 1.f)    * mPositionVariant) + mPosition);
    particle.mStartSpeed = ((randRangeF(-1.f, 1.f) * mSpeeds[DenoteType_StartVariant]) + mSpeeds[DenoteType_Start]);
    particle.mEndSpeed   = ((randRangeF(-1.f, 1.f) * mSpeeds[DenoteType_EndVariant])   + mSpeeds[DenoteType_End]);
    particle.mStartSize  = ((randRangeF(-1.f, 1.f) * mSizes[DenoteType_EndVariant])    + mSizes[DenoteType_Start]);
    particle.mEndSize    = ((randRangeF(-1.f, 1.f) * mSizes[DenoteType_EndVariant])    + mSizes[DenoteType_End]);
    particle.mLifeLimit  = ((randRangeF(-1.f, 1.f) * mLifeLimitVariant) + mLifeLimit);
    particle.mLife       = 0.0f;

    Colour newColour;
    newColour.r           = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_StartVariant].r) + mColours[DenoteType_Start].r);
    newColour.g           = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_StartVariant].g) + mColours[DenoteType_Start].g);
    newColour.b           = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_StartVariant].b) + mColours[DenoteType_Start].b);
    newColour.a           = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_StartVariant].a) + mColours[DenoteType_Start].a);
    particle.mStartColour = newColour;

    newColour.r         = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_EndVariant].r) + mColours[DenoteType_End].r);
    newColour.g         = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_EndVariant].g) + mColours[DenoteType_End].g);
    newColour.b         = (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_EndVariant].b) + mColours[DenoteType_End].b);
    newColour.a			= (Uint8)((randRangeF(-1.f, 1.f) * mColours[DenoteType_EndVariant].a) + mColours[DenoteType_End].a);
    particle.mEndColour = newColour;

    particle.mSpeed = particle.mStartSpeed;
    particle.setScale(particle.mStartSize, particle.mStartSize);
}

/// <summary>
/// Calculates the rate of birth for particles inside the emitter based on life limits
/// </summary>
void Emitter::calculateBirthRate()
{
    static float offset = 0.01f;
    if (mPulseRate > 0.0f)
    {
        mBirthRate  = (mParticleCount / mPulseLength);
        mBirthRate += (mBirthRate * offset);
        mLifeLimit  = (1.0f / mPulseRate) * 1000.0f;
    }
    else
    {
        mBirthRate  = (float)(sqrt(mParticleCount + (mLifeLimit * mLifeLimit) / 4.0f) - (mLifeLimit / 2.0f));
        mBirthRate -= (mBirthRate * offset);
    }
}

/// <summary>
/// Resizes the counter for amount of particles that can be active any one time and resets them
/// </summary>
void Emitter::setParticleCount(uint32_t num)
{
    if (mParticles != nullptr)
        delete[] mParticles;

    mParticleCount = num;
    mParticles     = new Particle[mParticleCount];

    for (uint32_t i = 0; i < mParticleCount; i++)
        mParticles[i].setSprite(mResource);

    calculateBirthRate();
}

void Emitter::setPosition(Vector2f _pos)      
{
	mPosition = _pos;                 
}

void Emitter::setPosition(float _x, float _y) 
{ 
	mPosition = Vector2f(_x, _y); 
}

Vector2f Emitter::getPosition()                   
{ 
	return mPosition;                 
}

Vector2f Emitter::getDirection()
{
	return mDirection;
}

string Emitter::resourceString()
{
	return mResource;
}

bool Emitter::isActive()                      
{ 
	return mActive;                   
}

void Emitter::setEmitType(EmitterType _type)  
{ 
	mEmitterType = _type;             
}

EmitterType Emitter::getEmitType()
{
	return mEmitterType;
}

uint32_t Emitter::getParticleCount()
{
	return mParticleCount;
}

float Emitter::getBirthRate()
{
	return mBirthRate;
}

float Emitter::getPulseRate()
{
	return mPulseRate;
}

float Emitter::getPulseLength()
{
	return mPulseLength;
}

bool Emitter::canLoop()
{
	return mCanLoop;
}

Vector2f Emitter::getPositionVariant()
{
	return mPositionVariant;
}

float Emitter::getLifeLimit()
{
	return mLifeLimit;
}

float Emitter::getLifeLimitVariant()
{
	return mLifeLimitVariant;
}

// Particle Impl
// =============
Particle::Particle(const Particle& particle)
{
	mDirection = particle.mDirection;
	mSpeed	   = particle.mSpeed;
	mLife	   = particle.mLife;
	mLifeLimit = particle.mLifeLimit;

	mStartColour = particle.mStartColour;
	mEndColour	 = particle.mEndColour;
	mStartSize	 = particle.mStartSize;
	mEndSize	 = particle.mEndSize;
	mStartSpeed  = particle.mStartSpeed;
	mEndSpeed	 = particle.mEndSpeed;

	setSprite(particle.mSpriteName);
}

Particle::Particle(string particleSprite)
{
    mLife       = 0.0f;
    mLifeLimit  = 1.0f;
	mSprite	    = new Sprite;
	mSpriteName = particleSprite;

    if (particleSprite != "")
	{
		mSpriteTexture = ResourceCache::getInstance()->retrieveTexture(mSpriteName);
        mSprite->setTexture(*mSpriteTexture);
	}
}

Particle::~Particle()
{
	delete mSprite;
}

void Particle::move(Vector2f amount)               
{ 
	mSprite->setPosition(mSprite->getPosition() + amount);                  
}

void Particle::setPosition(Vector2f num)           
{ 
	mSprite->setPosition(num);                      
}

void Particle::setScale(float scaleX, float scaleY) 
{ 
	mSprite->setScale(scaleX, scaleY); 
}

void Particle::setColour(Colour colour)             
{ 
	mSprite->setColor(colour);                      
}

void Particle::setSprite(Sprite* spr)  
{ 
	mSprite = spr;                           
}

void Particle::setSprite(FeanTexture* texture) 
{ 
	if(texture)
	{
		mSpriteTexture = texture;
		mSprite->setTexture(*mSpriteTexture);
	}
}

void Particle::setSprite(string texture) 
{ 
	mSpriteName	   = texture;
	mSpriteTexture = ResourceCache::getInstance()->retrieveTexture(mSpriteName);
	mSprite->setTexture(*mSpriteTexture);
}

Sprite* Particle::getSprite()           
{ 
	return mSprite;                           
}
