#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/Module.h"
#include "Feanwork/Entity.h"
#include "Feanwork/EventListener.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Layer.h"
#include "Feanwork/GameConsole.h"
#include "Feanwork/ResourceCache.h"

RenderModule::RenderModule(const RenderModule& module) :
	Module(ModuleType_Render, module.mParent)
{
	mGraphic = new Sprite();
	loadGraphic(module.mGraphicName);
}

RenderModule::RenderModule(Entity* parent) : 
	Module(ModuleType_Render, parent),
	EventListener()
{
	mTexture = nullptr;
	mShader  = nullptr;
	mGraphic = new Sprite();
}

RenderModule::RenderModule(string texture, Entity* parent) : 
	Module(ModuleType_Render, parent),
	EventListener()
{
	mGraphic	 = new Sprite();
	mGraphicName = texture;
	loadGraphic(texture);

	if(mParent != nullptr)
	{
		mGraphic->setPosition(mParent->getPosition());
		mParent->setDimensions((float)mGraphic->getTextureRect().width, (float)mGraphic->getTextureRect().height);
	}
}

RenderModule::~RenderModule()
{
	delete mGraphic;
}

void RenderModule::render(RenderTarget* target)
{
	if(mParent && mGraphic)
	{
		Vector2f layerPos  = SceneManager::getInstance()->getLayers()[mParent->getLayerIndex()]->getPosition();
        layerPos.y         = 0;
        mGraphic->move(layerPos);

        target->draw(*mGraphic);
        mGraphic->move(-layerPos);
        return;
	}
	target->draw(*mGraphic);
}

void RenderModule::update(UpdateStage stage)
{
}

void RenderModule::tell(EventFlag flags, ListenerArgs* args)
{
	if(flags & EventFlag_Pos)
		mGraphic->setPosition(mParent->getPosition(Edge_Topleft));
}

Module* RenderModule::duplicate()
{
	RenderModule* module = new RenderModule(*this);
	return module;
}

bool RenderModule::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		string texName = mTexture->getName();
		string sdrName = mShader->getName();

		stream.write(texName.c_str(), texName.length() + 1);
		stream.write(sdrName.c_str(), sdrName.length() + 1);

		return true;
	}
	else if(mode == ParseIn)
	{
		string texName = "";
		string sdrName = "";

		getline(stream, texName, '\0');
		getline(stream, sdrName, '\0');
		loadGraphic(texName, sdrName);
	
		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse RenderModule, failed due to incorrect passing of parameters");
	return true;
}

void RenderModule::clipRect(Vector2f dimensions)
{
	IntRect clip;
	clip.left   = (uint32_t)mParent->getPosition(Edge_Topleft).x;
	clip.top    = (uint32_t)mParent->getPosition(Edge_Topleft).y;
	clip.width  = (uint32_t)dimensions.x;
	clip.height = (uint32_t)dimensions.y;

	mGraphic->setTextureRect(clip);
	mParent->setDimensions((float)clip.width, (float)clip.height);
}

void RenderModule::clipRect(Vector2f dimensions, Edge edge)
{
	// Calculate the clip
	IntRect clip;
	clip.left   = (uint32_t)mParent->getPosition(Edge_Topleft).x;
	clip.top    = (uint32_t)mParent->getPosition(Edge_Topleft).y;
	clip.width  = (uint32_t)dimensions.x;
	clip.height = (uint32_t)dimensions.y;

	// Modify the new position of the rectange based on the edge being passed
	Vector2f newPosition;
	if(edge == Edge_Topleft)
		newPosition = Vector2f(mParent->getPosition(edge).x + clip.width  / 2.0f, 
							   mParent->getPosition(edge).y + clip.height / 2.f);
	
	else if(edge == Edge_Bottomleft)
		newPosition = Vector2f(mParent->getPosition(edge).x + clip.width  / 2.0f, 
							   mParent->getPosition(edge).y - clip.height / 2.f);

	else if(edge == Edge_Bottomright)
		newPosition = Vector2f(mParent->getPosition(edge).x - clip.width  / 2.0f, 
							   mParent->getPosition(edge).y - clip.height / 2.f);

	else if(edge == Edge_Topright)
		newPosition = Vector2f(mParent->getPosition(edge).x - clip.width  / 2.0f, 
							   mParent->getPosition(edge).y + clip.height / 2.f);

	// Apply all calculated values
	mGraphic->setTextureRect(clip);
	mParent->setDimensions((float)clip.width, (float)clip.height);
	mParent->setPosition(newPosition);
}

void RenderModule::clipRect(Vector2f relativePos, Vector2f dimensions)
{
	IntRect clip;
	clip.left   = (uint32_t)relativePos.x;
	clip.top    = (uint32_t)relativePos.y;
	clip.width  = (uint32_t)dimensions.x;
	clip.height = (uint32_t)dimensions.y;

	mGraphic->setTextureRect(clip);
	mParent->setDimensions((float)clip.width, (float)clip.height);
}

void RenderModule::setParent(Entity* parent)
{
	if(parent != nullptr)
	{
		Module::setParent(parent);
		mParent->setDimensions((float)mGraphic->getTextureRect().width, (float)mGraphic->getTextureRect().height);
		mGraphic->setPosition(mParent->getPosition(Edge_Topleft));
	}
}

void RenderModule::loadGraphic(string texture, string shader)
{
	mGraphicName = texture;
	if(mTexture = ResourceCache::getInstance()->retrieveTexture(mGraphicName))
		mGraphic->setTexture(*mTexture);
	else
		GameConsole::getInstance()->log("Could not find texture %s in Resource Cache", texture);

	// Assign Shader
	if(shader != "")
		mShader = ResourceCache::getInstance()->retrieveShader(shader);
}

void RenderModule::loadGraphic(FeanTexture* texture, string shader)
{
	if(texture)
	{
		mTexture	 = texture;
		mGraphicName = texture->getName();
		mGraphic->setTexture(*mTexture);

		// Assign shader
		if(shader != "")
			mShader = ResourceCache::getInstance()->retrieveShader(shader);
	}
}

Sprite* RenderModule::getGraphic()
{
	return mGraphic;
}

FeanTexture* RenderModule::getTexture()
{
	return mTexture;
}

FeanShader* RenderModule::getShader()
{
	return mShader;
}
