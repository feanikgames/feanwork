#include "Feanwork/Module/Module.h"
#include "Feanwork/Entity.h"
#include "Feanwork/EventListener.h"
#include "Feanwork/GameConsole.h"
#include "Feanwork/Scene.h"
#include "Feanwork/SceneManager.h"

#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/EmitterModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/IntelligenceModule.h"
#include "Feanwork/Module/SoundModule.h"


////////////////////////////////////////////////////////////
Entity::Entity()
{
	mEntityState = EntityState_Active;
	mDrawDebug	 = false;
	mHasMoved    = true;
	mLayerIndex	 = -1;
	mCustomType	 = 0;

	mScenePtr   = nullptr;
	onInput	    = nullptr;
	onUpdate    = nullptr;
	onDestroyed = nullptr;
}


////////////////////////////////////////////////////////////
Entity::Entity(Vector2f origin, ModuleSet* modules, uint8_t customType)
{
	mOrigin		 = origin;
	mEntityState = EntityState_Active;
	mDrawDebug	 = false;
	mHasMoved	 = true;
	mLayerIndex	 = 0;
	mCustomType	 = customType;

	mScenePtr   = nullptr;
	onInput	    = nullptr;
	onUpdate    = nullptr;
	onDestroyed = nullptr;

	if(modules != nullptr && modules->size() > 0)
	{
		for(auto module: mModules)
			mModules.push_back(module);
	}
}


////////////////////////////////////////////////////////////
Entity::Entity(Vector2f origin, uint8_t customType) :
	Entity(origin, nullptr, customType)
{
}


////////////////////////////////////////////////////////////
Entity::~Entity()
{	
	destroyModules();
	mListeners.clear();
}


////////////////////////////////////////////////////////////
void Entity::initDebugQuad(float outline)
{
	mQuad.setSize(getDim());
	mQuad.setOutlineThickness(outline);
	mQuad.setPosition(getPosition(Edge_Topleft));
	mQuad.setOutlineColor(Colour(255, 0, 0, 255));
	mQuad.setFillColor(Colour(0, 0, 0, 0));
	mDrawDebug = true;
}


////////////////////////////////////////////////////////////
void Entity::initDebugQuad(Colour colour, float outline)
{
	initDebugQuad(outline);
	mQuad.setOutlineColor(colour);
}


////////////////////////////////////////////////////////////
void Entity::manipulateQuad(float width, float height)
{
	if(mDrawDebug)
	{
		if(width > .0f)  mQuad.setSize(Vector2f(width, mQuad.getSize().x));
		if(height > .0f) mQuad.setSize(Vector2f(mQuad.getSize().y, height));
						 mQuad.setPosition(getPosition(Edge_Topleft));
	}
}


////////////////////////////////////////////////////////////
bool Entity::update(UpdateStage stage)
{
	// Make sure entity is active before updating
	bool active = (mEntityState == EntityState_Active);

	if(active)
	{
		// Pre Update
		if(stage == UpdateStage_Pre)
		{
			// Reset movement flag
			mHasMoved = false;

			// Check if there is an input callback, if there is make it happen
			if(onInput)
				onInput(this);

			// Update Intervals
			if(!mIntervals.empty())
			{
				IntervalIt it = mIntervals.begin();
				for(; it < mIntervals.end(); it++)
				{
					it->update(SceneManager::getInstance()->getDelta());

					if(it->state() == IntervalState_Finished) 
					{
						it = mIntervals.erase(it);
						if(it == mIntervals.end())
							break;
					}
				}
			}
		}

		for(auto module: mModules)
			module->update(stage);

		// Trigger update callback if it exists
		if(onUpdate)
			onUpdate(this, stage);

		// Debug Update
		if(stage == UpdateStage_Post)
		{
			if(mDrawDebug)
			{
				mQuad.setPosition(getPosition(Edge_Topleft));
				mQuad.setSize(getDim());
			}
		}
	}

	return active;
}


////////////////////////////////////////////////////////////
bool Entity::render(RenderTarget* target)
{
	// Make sure entity is active before rendering
	bool active = (mEntityState == EntityState_Active);

	if(active)
	{
		// Render Modules
		for(auto module: mModules)
			module->render(target);

		// Render Debug Quad
		if(mDrawDebug)
			target->draw(mQuad);
	}

	return active;
}


////////////////////////////////////////////////////////////
bool Entity::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		// Write entity variables to stream
		stream.write((char*)&mOrigin.x,			 sizeof(float));
		stream.write((char*)&mOrigin.y,			 sizeof(float));
		stream.write((char*)&mDimensions.width,  sizeof(float));
		stream.write((char*)&mDimensions.height, sizeof(float));
		
		int32_t entityState = (int32_t)mEntityState;
		stream.write((char*)&entityState, sizeof(int32_t));
		stream.write((char*)&mLayerIndex, sizeof(int32_t));
		stream.write((char*)&idMask,	  sizeof(int32_t));
		stream.write((char*)&mDrawDebug,  sizeof(bool));

		// Also write the debug box if it is active
		if(mDrawDebug)
		{
			stream.write((char*)&mQuad.getPosition().x, sizeof(float));
			stream.write((char*)&mQuad.getPosition().y, sizeof(float));
			stream.write((char*)&mQuad.getSize().x,		sizeof(float));
			stream.write((char*)&mQuad.getScale().y,	sizeof(float));

			float outline = mQuad.getOutlineThickness();
			stream.write((char*)&outline, sizeof(float));
		}

		// Write modules
		_parseModules(stream, mode);
		return true;
	}
	else if(mode == ParseIn)
	{
		// Read back in variables to same location
		stream.read((char*)&mOrigin.x,			sizeof(float));
		stream.read((char*)&mOrigin.y,			sizeof(float));
		stream.read((char*)&mDimensions.width,  sizeof(float));
		stream.read((char*)&mDimensions.height, sizeof(float));
		
		int32_t entityState = (int32_t)mEntityState;
		stream.read((char*)&entityState, sizeof(int32_t));
		stream.read((char*)&mLayerIndex, sizeof(int32_t));
		stream.read((char*)&idMask,		 sizeof(int32_t));
		stream.read((char*)&mDrawDebug,  sizeof(bool));
		mEntityState		= (EntityState)entityState;

		if(mDrawDebug)
		{
			stream.read((char*)&mQuad.getPosition().x, sizeof(float));
			stream.read((char*)&mQuad.getPosition().y, sizeof(float));
			stream.read((char*)&mQuad.getSize().x,	   sizeof(float));
			stream.read((char*)&mQuad.getScale().y,	   sizeof(float));

			float outline = mQuad.getOutlineThickness();
			stream.read((char*)&outline, sizeof(float));
		}

		// Read modules
		_parseModules(stream, mode);
		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse Entity, failed due to incorrect passing of parameters");
	return true;
}


////////////////////////////////////////////////////////////
void Entity::addListener(EventListener* listener)
{
	mListeners.push_back(listener);
}


////////////////////////////////////////////////////////////
void Entity::addListener(int32_t listenerID)
{
	if(mScenePtr)
	{
		EventListener* listener = mScenePtr->findListener(listenerID);
		if(listener)
			mListeners.push_back(listener);
	}
}


////////////////////////////////////////////////////////////
void Entity::addListeners(vector<int32_t>& listenerIDs)
{
	if(mScenePtr)
	{
		for(auto id: listenerIDs)
			mListeners.push_back(mScenePtr->findListener(id));
	}
	else GameConsole::getInstance()->log("Attempted to add listeners with IDs failed: ScenePtr not assigned");
}


////////////////////////////////////////////////////////////
void Entity::addModule(Module* module)
{
	if(module != nullptr)
	{
		module->setParent(this);
		mModules.push_back(module);
	}
}


////////////////////////////////////////////////////////////
Interval* Entity::addInterval(Interval interval)
{
	if(interval.funcExists())
	{
		mIntervals.push_back(Interval(interval));
		return &mIntervals.back();
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
void Entity::broadcast(EventFlag flags, ListenerArgs* args)
{
	// Broadcast Event to Modules
	for(auto module: mModules)
		module->tell(flags, args);

	// Broadcast Event to Listeners
	for(auto listener: mListeners)
		listener->tell(flags, args);

	delete args;
}


////////////////////////////////////////////////////////////
void Entity::applyPrefab(Prefab* prefab)
{
	// Assign variables and reset entity modules
	mOrigin		= prefab->origin;
	mCustomType = prefab->customType;
	destroyModules();

	// Search for all the listeners and re-bind them
	for(auto id: prefab->listenerIDs)
	{
		if(mScenePtr->findListener(id))
			addListener(mScenePtr->findListener(id));
	}

	// Re-insert the modules through duplication
	for(auto module: prefab->modules)
		addModule(module->duplicate());
}


////////////////////////////////////////////////////////////
void Entity::destroyModules()
{
	for(auto module: mModules)
		delete module;

	mModules.clear();
}


////////////////////////////////////////////////////////////
void Entity::setDimensions(float width, float height)
{
	mDimensions.set(width, height);
	broadcast(EventFlag_Dim, new ListenerArgs(mDimensions));
}


////////////////////////////////////////////////////////////
void Entity::addPosition(Vector2f num) { addPosition(num.x, num.y); }
void Entity::addPosition(float x, float y)
{
	if(x != .0f || y != .0f)
	{
		if(x != .0f)
		{
			mOrigin.x += x;
			broadcast(EventFlag_PosX, new ListenerArgs(mOrigin));
		}

		if(y != .0f)
		{
			mOrigin.y += y;
			broadcast(EventFlag_PosY, new ListenerArgs(mOrigin));
		}

		mHasMoved = true;
		broadcast(EventFlag_Pos, new ListenerArgs(mOrigin));
	}
}


////////////////////////////////////////////////////////////
void Entity::setPosition(Vector2f num) { setPosition(num.x, num.y); }
void Entity::setPosition(float x, float y)
{
	mHasMoved = true;
	mOrigin.x = x;
	mOrigin.y = y;

	broadcast(EventFlag_Pos,  new ListenerArgs(mOrigin));
	broadcast(EventFlag_PosX, new ListenerArgs(mOrigin));
    broadcast(EventFlag_PosY, new ListenerArgs(mOrigin));
}


////////////////////////////////////////////////////////////
void Entity::setLayerIndex(int32_t index)
{
	mLayerIndex = index;
}


////////////////////////////////////////////////////////////
void Entity::setCustomType(uint8_t type)
{
	mCustomType = type;
}


////////////////////////////////////////////////////////////
void Entity::setUserData(void* data)
{
	mUserData = data;
}


////////////////////////////////////////////////////////////
void Entity::setColour(Colour colour)
{
	RenderModule* rend = getRenderMod();
	if(rend)
		rend->getGraphic()->setColor(colour);
}


////////////////////////////////////////////////////////////
Colour Entity::getColour()
{
	RenderModule* rend = getRenderMod();
	if(rend) return rend->getGraphic()->getColor();
	else	 return Colour();
}


////////////////////////////////////////////////////////////
Vector2f Entity::getPosition()
{
	return mOrigin;
}


////////////////////////////////////////////////////////////
Vector2f Entity::getPosition(Edge edge)
{
	Vector2f _new;
	switch(edge)
	{
		case Edge_Topleft:
			_new.x = mOrigin.x - mDimensions.halfWidth;
			_new.y = mOrigin.y - mDimensions.halfHeight;
			break;
		case Edge_Topright:
			_new.x = mOrigin.x + mDimensions.halfWidth;
			_new.y = mOrigin.y - mDimensions.halfHeight;
			break;
		case Edge_Bottomleft:
			_new.x = mOrigin.x - mDimensions.halfWidth;
			_new.y = mOrigin.y + mDimensions.halfHeight;
			break;
		case Edge_Bottomright:
			_new.x = mOrigin.x + mDimensions.halfWidth;
			_new.y = mOrigin.y + mDimensions.halfHeight;
			break;
	}
	return _new;
}


////////////////////////////////////////////////////////////
Vector2f Entity::getDim()
{
	return Vector2f(mDimensions.width, mDimensions.height);
}


////////////////////////////////////////////////////////////
Vector2f Entity::getHalfDim()
{
	return Vector2f(mDimensions.halfWidth, mDimensions.halfHeight);
}


////////////////////////////////////////////////////////////
AABB Entity::getAABB()
{
	return AABB(mOrigin, getHalfDim());
}


////////////////////////////////////////////////////////////
Module* Entity::getModule(ModuleType type)
{
	for(auto module: mModules)
	{
		if(module->getType() == type)
			return module;
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
int32_t Entity::getLayerIndex()
{
	return mLayerIndex;
}


////////////////////////////////////////////////////////////
EntityState Entity::getEntityState()
{
	return mEntityState;
}


////////////////////////////////////////////////////////////
uint8_t Entity::getCustomType()
{
	return mCustomType;
}


////////////////////////////////////////////////////////////
void* Entity::getUserData()
{
	return mUserData;
}


////////////////////////////////////////////////////////////
ListenerSet Entity::getListeners()
{
	return mListeners;
}


////////////////////////////////////////////////////////////
ModuleSet Entity::getModules()
{
	return mModules;
}


////////////////////////////////////////////////////////////
bool Entity::canDebug()
{
	return mDrawDebug;
}


////////////////////////////////////////////////////////////
bool Entity::hasMoved()
{
	return mHasMoved;
}


////////////////////////////////////////////////////////////
void Entity::setID(int32_t id)
{
	setMaskID(id);
}


////////////////////////////////////////////////////////////
int32_t Entity::getID()
{
	return getMaskID();
}

////////////////////////////////////////////////////////////
void Entity::kill()	   { mEntityState = EntityState_Inactive;  }
void Entity::revive()  { mEntityState = EntityState_Active;    }
void Entity::destroy() { mEntityState = EntityState_Destroyed; }


////////////////////////////////////////////////////////////
void Entity::setScenePtr(Scene* scene)
{
	mScenePtr = scene;
}


////////////////////////////////////////////////////////////
Scene* Entity::getScenePtr()
{
	return mScenePtr;
}


////////////////////////////////////////////////////////////
RenderModule* Entity::getRenderMod()
{
	for(auto module: mModules)
	{
		if(module->getType() == ModuleType_Render)
			return reinterpret_cast<RenderModule*>(module);
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
AnimationModule* Entity::getAnimationMod()
{
	for(auto module: mModules)
	{
		if(module->getType() == ModuleType_Animation)
			return reinterpret_cast<AnimationModule*>(module);
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
EmitterModule* Entity::getEmitterMod()
{
	for(auto module: mModules)
	{
		if(module->getType() == ModuleType_Emitter)
			return reinterpret_cast<EmitterModule*>(module);
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
PhysicsModule* Entity::getPhysicsMod()
{
	if(!mModules.empty())
	{
		for(auto module: mModules)
		{
			if(module->getType() == ModuleType_Physics)
				return reinterpret_cast<PhysicsModule*>(module);
		}
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
IntelligenceModule* Entity::getIntelMod()
{
	for(auto module: mModules)
	{
		if(module->getType() == ModuleType_Intel)
			return reinterpret_cast<IntelligenceModule*>(module);
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
SoundModule* Entity::getSoundMod()
{
	for (auto module : mModules)
	{
		if (module->getType() == ModuleType_Sound)
			return reinterpret_cast<SoundModule*>(module);
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
Interval* Entity::traversePosition(float traversalTime, Vector2f targetPos, Interval* subscriber)
{
	// Create new interval
	Interval newInterval(bind(&Entity::_traversePosition, placeholders::_1, placeholders::_2), traversalTime, this);
	
	// If there is a subscriber, subscribe it!
	if(subscriber)
		newInterval.subscribe(subscriber);
	
	// Set traverse params and add interval
	mStartPos  = getPosition();
	mTargetPos = targetPos;
	return addInterval(newInterval);
}


////////////////////////////////////////////////////////////
Interval* Entity::traverseSize(float traversalTime, Vector2f targetSize, Interval* subscriber)
{
	RenderModule* rend = getRenderMod();
	if(rend)
	{
		// Create new interval
		Interval newInterval(bind(&Entity::_traverseSize, placeholders::_1, placeholders::_2), traversalTime, this);
		
		// If there is a subscriber, subscribe it!
		if(subscriber)
			newInterval.subscribe(subscriber);

		// Set traverse params and add interval
		mStartSize  = rend->getGraphic()->getScale();
		mTargetSize = targetSize;
		return addInterval(newInterval);
	}
	return nullptr; 
}


////////////////////////////////////////////////////////////
Interval* Entity::traverseColour(float traversalTime, Colour startColour, Colour targetColour, Interval* subscriber)
{
	RenderModule* rend = getRenderMod();
	if(rend)
	{
		// Create new interval
		Interval newInterval(bind(&Entity::_traverseColour, placeholders::_1, placeholders::_2), traversalTime, this);
		
		// If there is a subscriber, subscribe it!
		if(subscriber)
			newInterval.subscribe(subscriber);

		// Set traverse params and add interval
		mStartColour  = startColour;
		mTargetColour = targetColour;
		return addInterval(newInterval);
	}
	return nullptr; 
}


////////////////////////////////////////////////////////////
Interval* Entity::traverseRotation(float traversalTime, float targetRotation, Interval* subscriber)
{
	RenderModule* rend = getRenderMod();
	if(rend)
	{
		// Create new interval
		Interval newInterval(bind(&Entity::_traverseSize, placeholders::_1, placeholders::_2), traversalTime, this);
		
		// If there is a subscriber, subscribe it!
		if(subscriber)
			newInterval.subscribe(subscriber);

		// Set traverse params and add interval
		mStartRotation  = rend->getGraphic()->getRotation();
		mTargetRotation = targetRotation;
		return addInterval(newInterval);
	}
	return nullptr; 
}


////////////////////////////////////////////////////////////
void Entity::_traversePosition(void* entity, float ratio)
{
	Entity* ent      = reinterpret_cast<Entity*>(entity);
	float   invRatio = (1.0f - ratio);
	ent->setPosition(invRatio * ent->mStartPos + ratio * ent->mTargetPos);
}


////////////////////////////////////////////////////////////
void Entity::_traverseSize(void* entity, float ratio)
{
	Entity* ent        = reinterpret_cast<Entity*>(entity);
	RenderModule* rend = ent->getRenderMod();

	if(rend)
	{
		Vector2f scale = rend->getGraphic()->getScale();
		float invRatio = (1.0f - ratio);

		scale = (invRatio * ent->mStartSize + ratio * ent->mTargetSize);
		rend->getGraphic()->setScale(scale);
	}
}


////////////////////////////////////////////////////////////
void Entity::_traverseColour(void* entity, float ratio)
{
	Entity* ent        = reinterpret_cast<Entity*>(entity);
	RenderModule* rend = ent->getRenderMod();

	if(rend)
	{
		Colour tempColour = ent->mStartColour;
		float invRatio    = (1.0f - ratio);

		tempColour.r = (invRatio * ent->mStartColour.r + ratio * ent->mTargetColour.r);
		tempColour.g = (invRatio * ent->mStartColour.g + ratio * ent->mTargetColour.g);
		tempColour.b = (invRatio * ent->mStartColour.b + ratio * ent->mTargetColour.b);
		tempColour.a = (invRatio * ent->mStartColour.a + ratio * ent->mTargetColour.a);
		rend->getGraphic()->setColor(tempColour);
	}
}


////////////////////////////////////////////////////////////
void Entity::_traverseRotation(void* entity, float ratio)
{
	Entity* ent        = reinterpret_cast<Entity*>(entity);
	RenderModule* rend = ent->getRenderMod();

	if(rend)
	{
		float scale    = rend->getGraphic()->getRotation();
		float invRatio = (1.0f - ratio);

		scale = (invRatio * ent->mStartRotation + ratio * ent->mTargetRotation);
		rend->getGraphic()->setRotation(scale);
	}
}


////////////////////////////////////////////////////////////
void Entity::_parseModules(fstream& stream, ParseMode mode)
{
	int32_t moduleCount = mModules.size();
	if(mode == ParseOut)
	{
		stream.write((char*)&moduleCount, sizeof(int32_t));
		for(int32_t i = 0; i < moduleCount; i++)
		{
			int32_t type = mModules[i]->getType();
			stream.write((char*)&type, sizeof(int32_t));
			mModules[i]->parse(stream, mode);
		}
	}
	else if(mode == ParseIn)
	{
		// Read modules back based on type
		stream.read((char*)&moduleCount, sizeof(int32_t));
		for(int32_t i = 0; i < moduleCount; i++)
		{
			int32_t type = 0;
			stream.read((char*)&type, sizeof(int32_t));

			if(type == ModuleType_Render)
			{
				RenderModule* renderMod = new RenderModule(this);
				renderMod->parse(stream, ParseIn);
				mModules.push_back(renderMod);
			}
			else if(type == ModuleType_Animation)
			{
				AnimationModule* animMod = new AnimationModule(this);
				animMod->parse(stream, ParseIn);
				mModules.push_back(animMod);
			}
			else if(type == ModuleType_Intel)
			{
				IntelligenceModule* intelMod = new IntelligenceModule(this);
				intelMod->parse(stream, ParseIn);
				mModules.push_back(intelMod);
			}
			else if(type == ModuleType_Physics)
			{
				PhysicsModule* physMod = new PhysicsModule(this);
				physMod->parse(stream, ParseIn);
				mModules.push_back(physMod);
			}
			else if(type == ModuleType_Emitter)
			{
				EmitterModule* emitterMod = new EmitterModule(this);
				emitterMod->parse(stream, ParseIn);
				mModules.push_back(emitterMod);
			}
			else if (type == ModuleType_Sound)
			{
				SoundModule* soundMod = new SoundModule(this);
				soundMod->parse(stream, ParseIn);
				mModules.push_back(soundMod);
			}

			// TODO: Finish these
			/* ModuleType_Script */
		}
	}
}
