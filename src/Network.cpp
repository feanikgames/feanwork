#include "Feanwork/Network.h"
#include "Feanwork/GameConsole.h"

Network* Network::instance()
{
	static Network network;
	return &network;
}

void Network::init(NetworkType type, uint16_t port)
{
	// Set up network type and specify a port
	mNetworkType = type;
	mPort		 = port;
	
	// Bind the specified port to our socket
	mSocket.bind(mPort);
}

void Network::connect(string ip)
{
	// Add the new client to our pending list
	IpAddress address(ip);
	mPending.push_back(address);

	// Ping the new client to test our connection
	Packet newPacket;
	newPacket << -1 << "->";
	mSocket.send(newPacket, address, mPort);
}

void Network::sendPacket(uint16_t eventID, Packet* packet)
{
	// Send a packet of data to all connected clients
	if(!mClients.empty())
	{
		for(auto client: mClients)
		{
			Packet newPacket;
			newPacket << eventID << packet;
			mSocket.send(newPacket, client, mPort);
		}
	}
}

void Network::receivePackets()
{
	// Search through pending IP Addresses, if we get a response add them to the client list
	if(!mPending.empty())
	{
		vector<IpAddress>::iterator it;
		for(it = mPending.begin(); it != mPending.end(); it++)
		{
			Packet testPacket;
			if(mSocket.receive(testPacket, *it, mPort) != UdpSocket::Done)
			{
				GameConsole::getInstance()->log("Unable to get a response from IP %s, dropping the IP from the pending list", it->toString());
				mPending.erase(it);
			}
			else
			{
				mClients.push_back(*it);
				mPending.erase(it);
			}
		}
	}

	// TODO: Get packets
	
}
