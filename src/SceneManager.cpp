#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/InputHandler.h"
#include "Feanwork/Camera.h"
#include "Feanwork/GameConsole.h"
#include <sstream>

void SceneManager::init(string gameConfig)
{	
	map<string, string> mGameConfig;
	mGameConfig = ResourceCache::getInstance()->retrieveConfig(gameConfig)->mConfigData;

	mRunning = true;
	mName    =	  mGameConfig.at("name");
	istringstream(mGameConfig.at("width"))  >> mWidth;
	istringstream(mGameConfig.at("height")) >> mHeight;

	GameConsole::getInstance()->init(Vector2f(5.f, mHeight - 170.f));
	GameConsole::getInstance()->log("*Game init*      cfg file: %s.cfg", gameConfig.c_str());

	mWindow  = new RenderWindow(VideoMode(mWidth, mHeight), mName, Style::Close);
	mWindow->setFramerateLimit(60);
	mScenes.push_back(new Scene);

	mDeltaClock.restart();
	srand(std::time(nullptr));

	navGraph = new SimpleNavGraph(20, 20, 20.f, Vector2f(300.f, 100.f));
	aStar    = new AStar(navGraph);
	aStar->findPath(0, 399);
}

void SceneManager::setIcon(string icon)
{
	Image img;
	img.loadFromFile(icon);
	mWindow->setIcon(32, 32, img.getPixelsPtr());
}

void SceneManager::run()
{
	while (mRunning)
	{
		mDelta = mDeltaClock.getElapsedTime();
		mDeltaClock.restart();

		Event event;
		while(mWindow->pollEvent(event))
		{
			if(event.type == Event::Closed)
				mRunning = false;
		}

		GameConsole::getInstance()->update();
		InputHandler::instance()->update();
		mWindow->clear();

		for(auto scene : mScenes)
		{
			scene->update();
			scene->render();
		}
		
		GameConsole::getInstance()->render(mWindow);
		/*navGraph->debugRender(mWindow);
		aStar->debugRender(mWindow);*/

		static char bugger[1024];
		sprintf_s(bugger, "%s - %.2f ms/frame", mName.c_str(), getDelta());
		mWindow->setTitle(bugger);
		mWindow->display();
	}
}

void SceneManager::destroy()
{
	mRunning = false;
	for(auto scene: mScenes)
		delete scene;

	delete mWindow;
	delete navGraph;
	delete aStar;

	mScenes.clear();
	InputHandler::instance()->destroy();
	ResourceCache::getInstance()->destroy();
	GameConsole::getInstance()->destroy();

#ifdef _DEBUG
	//Output memory leaks
	//MemoryTracker::getSingleton()->compileReport();
#endif
}

void SceneManager::resetAlpha()
{
	// TODO
}

RenderWindow* SceneManager::getScreen()
{
	return mWindow;
}

uint32_t SceneManager::getWidth()
{
	return mWidth;
}

uint32_t SceneManager::getHeight()
{
	return mHeight;
}

vector<Layer*> SceneManager::getLayers()
{
	vector<Layer*> layers;
	for(auto scene: mScenes)
	{
		for(auto layer: scene->getLayers())
			layers.push_back(layer);
	}
	return layers;
}

Scene* SceneManager::getScene(int index)
{
	return mScenes.at(index);
}

float SceneManager::getDelta()
{
	return mDelta.asMilliseconds();
}

void SceneManager::toDefault()
{
	mWindow->setView(mWindow->getDefaultView());
}

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
}

SceneManager* SceneManager::getInstance()
{
	static SceneManager sceneMgr;
	return &sceneMgr;
}
