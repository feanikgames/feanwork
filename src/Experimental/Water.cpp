#include "Feanwork/Experimental/Water.h"
#include "Feanwork/SceneManager.h"

Water::Water(Vector2f position, Vector2f size, Colour colour)
{
	// Plot four points for the rectangle
	Vector2f halfSize = size / 2.f;
	mVertices.setPrimitiveType(TrianglesStrip);

	/*mVertices.append(Vertex(position - halfSize, colour));
	mVertices.append(Vertex(Vector2f(position.x - halfSize.x, position.y + halfSize.y), colour));
	mVertices.append(Vertex(position + halfSize, colour));
	mVertices.append(Vertex(Vector2f(position.x + halfSize.x, position.y - halfSize.y), colour));*/

	// Plot waveform points
	const float res	   = 0.5f; //pts per unit
	uint32_t numPoints = (uint32_t)(size.x * res);
	float xPos		   = position.x - halfSize.x;
	float difference   = size.x / numPoints;

	for(uint32_t i = 0; i <= numPoints; i++)
	{
		mVertices.append(Vertex(Vector2f(xPos + (difference * i), position.y - halfSize.y), colour));
		mVertices.append(Vertex(Vector2f(xPos + (difference * i), position.y + halfSize.y), colour));
	}
}

Water::~Water()
{
}

void Water::render(RenderTarget* target)
{
	FeanShader* shader = ResourceCache::getInstance()->retrieveShader("water");
	static float accum = 0.0f;
	accum += SceneManager::getInstance()->getDelta()/10.0f;
	shader->setParameter("time", accum);
	target->draw(mVertices, RenderStates(BlendAlpha, Transform::Identity, nullptr, shader));
}
