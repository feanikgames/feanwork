#include "Feanwork/Experimental/SpaceInvaders/SpaceInvaders.h"
#include "Feanwork/Experimental/SpaceInvaders/Aliens.h"
#include "Feanwork/Experimental/SpaceInvaders/Player.h"

#include "Feanwork/Entity.h"
#include "Feanwork/Scene.h"
#include "Feanwork/SceneManager.h"

#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Parser/PrefabParser.h"

void initSpaceInvaders()
{
	// Get instances
	SceneManager* sceneMgr  = SceneManager::getInstance();
	ResourceCache* resCache = ResourceCache::getInstance();

	// load and init
	resCache->addDirectory("../resources");
	sceneMgr->init("game");
	
	Scene* rootScene = sceneMgr->getScene(0);

	Player player(rootScene);
	AlienFactory factory;

	Prefab* prefab			   = new Prefab();
	PrefabParser* prefabParser = new PrefabParser("testPrefab", prefab);
	prefabParser->load();

	addWall(512, 35, 1100, 20);
	addWall(512, 630, 1100, 20);
	addWall(10, 320, 20, 790);
	addWall(1014, 320, 20, 790);

	// Run the program
	sceneMgr->run();
	sceneMgr->destroy();
}

void addWall(float x, float y, float w, float h)
{
	//Add a wall :3
	Entity*        wall		= new Entity(Vector2f(x, y));
	PhysicsModule* wallPhys = new PhysicsModule(PhysicsType_Static);

	wallPhys->initVars(0, 0.1, 0.1, 0.9, 6);

	wall->addModule(wallPhys);
	wall->setDimensions(w, h);
	wall->initDebugQuad();

	SceneManager::getInstance()->getScene(0)->addEntity(0, wall);
}
