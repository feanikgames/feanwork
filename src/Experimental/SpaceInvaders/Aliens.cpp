#include "Feanwork/Experimental/SpaceInvaders/Aliens.h"
#include "Feanwork/Experimental/SpaceInvaders/Player.h"
#include "Feanwork/Experimental/SpaceInvaders/SpaceInvaders.h"

#include "Feanwork/Entity.h"
#include "Feanwork/InputHandler.h"
#include "Feanwork/Scene.h"
#include "Feanwork/SceneManager.h"

#include "Feanwork/Util/Timer.h"
#include "Feanwork/Util/FeanRandom.h"

#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/SoundModule.h"
#include "Feanwork/Module/AnimationModule.h"

Alien::Alien(Entity* ent)
{
	mHealth    = 1;
	mEntity    = ent;
	mAlive     = true;
	mFireDelay = FeanRandom::randRangeF(2000.0f, 4000.0f);
	
	mFireRateTimer = new Timer();
	mFireRateTimer->restart();

	mEntity->onUpdate = bind(&Alien::onUpdate, placeholders::_1, placeholders::_2);
}

Alien::~Alien()
{
	delete mFireRateTimer;
}

void Alien::onHit()
{
	if(--mHealth == 0)
	{
		// Kill the alien
		mEntity->destroy();
		mAlive = false;
	}
}

void Alien::onUpdate(Entity* caller, UpdateStage stage)
{
	if(stage != UpdateStage_Current)
		return;

	Alien* alien = reinterpret_cast<Alien*>(caller->getUserData());

	if(alien->mFireRateTimer->getElapsedTimeMs() > alien->mFireDelay)
	{
		Scene* scene = SceneManager::getInstance()->getScene(0);
		Entity* bullet = scene->addEntity(0, mBulletPrefab, caller->getPosition() + Vector2f(0, 40));
		//bullet->initDebugQuad();
		bullet->getPhysicsMod()->onCollision = bind(&Alien::onBulletHit, placeholders::_1, placeholders::_2);
		bullet->getSoundMod()->trigger("shoot");

		PhysicsModule* mod = bullet->getPhysicsMod();
		mod->setVelocity(0, 0.25f);

		alien->mFireDelay = 3000;
		alien->mFireRateTimer->restart();
	}
}

bool Alien::onBulletHit(Entity* caller, vector<Collision*>& collisions)
{
	//Damage hit enemies
	for(auto c: collisions)
	{
		if(c->target->getCustomType() == ObjectType_Player)
		{
			Player* player = reinterpret_cast<Player*>(c->target->getUserData());
			player->onHit();
		}

		if(c->target->getCustomType() != ObjectType_Bullet &&
			c->target->getCustomType() != ObjectType_Alien)
		{
			caller->destroy();
		}
	}

	return false;
}

AlienFactory::AlienFactory()
{
	// Create Alien Prefab
	mAlienPrefab		         = new Prefab();
	RenderModule*    alienRender = new RenderModule("Enemy");
	AnimationModule* alienAnim   = new AnimationModule();
	PhysicsModule*   alienPhys   = new PhysicsModule(PhysicsType_Static);

	Animation* alienFrames = new Animation("alienAnim", 100.f, AnimationMode_Wrap);
	for(int32_t i = 0; i < 4; i++)
	{
		Frame* frame = new Frame;
		frame->position = Vector2f(i * 25.f, 0.f);
		frame->width    = 25;
		frame->height   = 20;
		alienFrames->addFrame(frame);
	}

	alienAnim->addAnimation(alienFrames);
	alienPhys->initVars(0.1f, 0.2f, 1.5f, 0.000001f);
	alienPhys->setBitmaskGroup(2);

	mAlienPrefab->modules.push_back(alienRender);
	mAlienPrefab->modules.push_back(alienAnim);
	mAlienPrefab->modules.push_back(alienPhys);

	// Make the bullet prefab 
	RenderModule*  render = new RenderModule("bullet");
	PhysicsModule* phys   = new PhysicsModule(PhysicsType_Dynamic);
	SoundModule*   snd	  = new SoundModule("shoot");

	phys->initVars(10.0f, 0.2, 0.5f, 0.0001f, 5);
	//phys->setVelocity(Vector2f(0.f, -.25f));
	snd->insert("shoot");

	Alien::mBulletPrefab->modules.push_back(render);
	Alien::mBulletPrefab->modules.push_back(phys);
	Alien::mBulletPrefab->modules.push_back(snd);
	Alien::mBulletPrefab->customType = ObjectType_Bullet;

	//Create a few aliens
	Scene* scene = SceneManager::getInstance()->getScene(0);
	for (int32_t y = 0; y < 4; y++)
	{
		for (int32_t i = 0; i < 14; i++)
		{
			Entity* ent = new Entity();
			ent->applyPrefab(mAlienPrefab);
			ent->setCustomType(ObjectType_Alien);
			ent->getAnimationMod()->setAnimation("alienAnim");
			//ent->initDebugQuad(Colour::Blue);
			ent->setPosition(180.f + (50.f * i), 100.f + (50.f * y));

			mAliens.push_back(new Alien(ent));
			ent->setUserData(mAliens.back());
			scene->addEntity(0, ent);
		}
	}
}

AlienFactory::~AlienFactory()
{
	for(auto a: mAliens)
		delete a;

	delete mAlienPrefab;
}

// Static Definition
Prefab* Alien::mBulletPrefab = new Prefab;
