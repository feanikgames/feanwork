#include "Feanwork/Experimental/SpaceInvaders/Player.h"
#include "Feanwork/Experimental/SpaceInvaders/SpaceInvaders.h"
#include "Feanwork/Experimental/SpaceInvaders/Aliens.h"

#include "Feanwork/Entity.h"
#include "Feanwork/InputHandler.h"
#include "Feanwork/Scene.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Util/Timer.h"

#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/SoundModule.h" 

Player::Player(Scene* scene)
{
	// Make the player
	mEntity				 = new Entity(Vector2f(512.f, 620.f), nullptr, 1);
	RenderModule* render = new RenderModule("cannon");
	PhysicsModule* phys  = new PhysicsModule(PhysicsType_Dynamic);
	SoundModule* snd	 = nullptr;
		
	mEntity->addModule(render);
	mEntity->addModule(phys);
	mEntity->initDebugQuad();

	phys->initVars(10.0f, 0.2f, 0.5f, 0.0001f);

	InputHandler* input = InputHandler::instance();
	Keystate* left		= new Keystate;
	left->inputName		= "Left";
	left->key			= Keyboard::A;

	Keystate* right  = new Keystate;
	right->inputName = "Right";
	right->key	     = Keyboard::D;

	Keystate* space  = new Keystate;
	space->inputName = "Fire";
	space->key	     = Keyboard::W;
		
	input->addKeystate(left);
	input->addKeystate(right);
	input->addKeystate(space);

	// Make the bullet prefab 
	render = new RenderModule("bullet");
	phys   = new PhysicsModule(PhysicsType_Dynamic);
	snd	   = new SoundModule("shoot");

	phys->initVars(10.0f, 0.2, 0.5f, 0.0001f);
	phys->setBitmaskGroup(2);
	//phys->setVelocity(Vector2f(0.f, -.25f));
	snd->insert("shoot");

	mBulletPrefab->modules.push_back(render);
	mBulletPrefab->modules.push_back(phys);
	mBulletPrefab->modules.push_back(snd);
	mBulletPrefab->customType = ObjectType_Bullet;

	// Create callbacks
	mEntity->setUserData(this);
	mEntity->setCustomType(ObjectType_Player);
	mEntity->onInput = bind(&Player::onInput, placeholders::_1);

	// Add to scene
	scene->addEntity(0, mEntity);
}

void Player::onInput(Entity* caller)
{
	// Get input handler instance
	InputHandler* input = InputHandler::instance();

			if(input->isButtonActive("Left"))  caller->getPhysicsMod()->setVelocity(Vector2f(-.1f, 0));
	else if(input->isButtonActive("Right")) caller->getPhysicsMod()->setVelocity(Vector2f( .1f, 0));
	else									caller->getPhysicsMod()->setVelocity(Vector2f(0, 0));

	if(input->isButtonActive("Fire"))
	{
		static float fireDelay = 250.0f;

		if(mFireRateTimer.getElapsedTimeMs() > fireDelay)
		{
			for(int32_t i = -1; i < 2; i++)
			{
				Scene* scene = SceneManager::getInstance()->getScene(0);
				Entity* bullet = scene->addEntity(0, mBulletPrefab, caller->getPosition() + Vector2f(0, -40));
				//bullet->initDebugQuad();
				bullet->getPhysicsMod()->onCollision = bind(&Player::onBulletHit, placeholders::_1, placeholders::_2);
				bullet->getSoundMod()->trigger("shoot");

				PhysicsModule* mod = bullet->getPhysicsMod();
				mod->setVelocity(i*0.05f, -0.25f);
			}

			mFireRateTimer.restart();
		}
	}
}

bool Player::onBulletHit(Entity* caller, vector<Collision*>& collisions)
{
	//Damage hit enemies
	for(auto c: collisions)
	{
		if(c->target->getCustomType() == ObjectType_Alien)
		{
			Alien* alien = reinterpret_cast<Alien*>(c->target->getUserData());
			alien->onHit();
		}

		if(c->target->getCustomType() != ObjectType_Bullet)
			caller->destroy();
	}

	return false;
}

void Player::onHit()
{
	mEntity->destroy();
}

Prefab* Player::mBulletPrefab = new Prefab;
Timer   Player::mFireRateTimer;
