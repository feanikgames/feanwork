#include "Feanwork/SoundManager.h"

SoundManager::SoundManager()
{
}

SoundManager::~SoundManager()
{
	clearSounds();
}

void SoundManager::update(UpdateStage stage)
{
	if(stage == UpdateStage_Post)
	{
		vector<Sound*>::iterator it;
		for(it = mSounds.begin(); it != mSounds.end(); ++it)
			mSounds.erase(it);
	}
}

void SoundManager::setMusic(string music)
{
	mMusic.openFromFile(music);
	mMusic.play();
}

void SoundManager::setAmbiance(string file)
{
	mAmbiance.openFromFile(file);
	mAmbiance.play();
}

void SoundManager::playAmbiance()
{
	if(mAmbiance.getStatus() != Music::Playing)
		mAmbiance.play();
}

void SoundManager::pauseAmbiance()
{
	if(mAmbiance.getStatus() != Music::Paused)
		mAmbiance.pause();
}

void SoundManager::stopAmbiance()
{
	if(mAmbiance.getStatus() != Music::Stopped)
		mAmbiance.stop();
}

void SoundManager::playMusic()
{
	if(mMusic.getStatus() != Music::Playing)
		mMusic.play();
}

void SoundManager::pauseMusic()
{
	if(mMusic.getStatus() != Music::Paused)
		mMusic.pause();
}

void SoundManager::stopMusic()
{
	if(mMusic.getStatus() != Music::Stopped)
		mMusic.stop();
}

void SoundManager::addSoundFX(Sound* sound, Vector2f position, float offset)
{
	// TODO: Add sound positioning to make it dynamic
	//sound->setPosition(position.x, position.y, 0);
	sound->setPlayingOffset(milliseconds(offset));
	sound->play();
	mSounds.push_back(sound);
}

void SoundManager::removeSoundFX(Sound* sound)
{
	if(!sound)
		mSounds.erase(find(mSounds.begin(), mSounds.end(), sound));
}

void SoundManager::removeSoundFX(int32_t index)
{
	mSounds.erase(mSounds.begin() + index);
}

void SoundManager::playAll()
{
	for(auto sound: mSounds)
		sound->play();

	mMusic.play();
	mAmbiance.play();
}

void SoundManager::pauseAll()
{
	for(auto sound: mSounds)
		sound->pause();
	
	mMusic.pause();
	mAmbiance.pause();
}

void SoundManager::stopAll()
{
	for(auto sound: mSounds)
		sound->stop();

	mMusic.stop();
	mAmbiance.stop();
}

void SoundManager::clearSounds()
{
	mSounds.clear();
}

void SoundManager::changeVolume(int32_t soundIndex, float volume)
{
	mSounds[soundIndex]->setVolume(volume);
}

void SoundManager::changeVolume(Sound* sound, float volume)
{
	(*find(mSounds.begin(), mSounds.end(), sound))->setVolume(volume);
}

void SoundManager::changePitch(int32_t soundIndex, float pitch)
{
	mSounds[soundIndex]->setPitch(pitch);
}

void SoundManager::changePitch(Sound* sound, float pitch)
{
	(*find(mSounds.begin(), mSounds.end(), sound))->setPitch(pitch);
}
