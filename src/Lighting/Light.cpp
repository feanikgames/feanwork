#include "Feanwork/Lighting/Light.h"
#include "Feanwork/Entity.h"
#include "Feanwork/SceneManager.h"
#include <SFML/OpenGL.hpp>

Light::Light(Vector2f origin, Colour colour, uint8_t intensity)
{
	mColour	   = colour;
	mIntensity = intensity;
	mLightType = LightType_Unknown;
	mOrigin    = origin;

	mShadowCaster = true;
	mTarget 	  = nullptr;
	mMaxSpeed 	  = 0;
	mTransform.translate(mOrigin);
}

Light::Light(Vector2f origin, Colour colour, uint8_t intensity, Entity*  target)
{
	mColour	   = colour;
	mIntensity = intensity;
	mLightType = LightType_Unknown;
	mOrigin    = origin;

	mTarget   = target;
	mTransform.translate(mOrigin);
}

Light::Light(Vector2f origin, Colour colour, uint8_t intensity, float maxSpeed, Vector2f target)
{
	mColour	   = colour;
	mIntensity = intensity;
	mLightType = LightType_Unknown;
	mOrigin    = origin;

	mTarget   = nullptr;
	mPoint    = target;
	mMaxSpeed = maxSpeed;
	mTransform.translate(mOrigin);
}

Light::~Light()
{
	if(mTarget)
		mTarget = nullptr;
}

void Light::render(RenderTarget* target)
{
	if(mVertices.getVertexCount() == 0)
		return;
	
	target->draw(mVertices, RenderStates(mTransform));

	if(mShadowCaster)
	{
		for(auto& shadow: mShadowGeoms)
			target->draw(shadow/*, RenderStates(mTransform)*/);
	}
}

void Light::update(UpdateStage stage)
{
	if(stage == UpdateStage_Current)
	{
		if(mTarget)
		{
			mOrigin = mTarget->getPosition();
		}
		else if(mMaxSpeed > 0)
		{
			Vector2f diff = (mPoint - mOrigin);
			diff          = Vectorial::vectorNormal(diff) * mMaxSpeed;
			mOrigin		 += (diff * SceneManager::getInstance()->getDelta());
		}

		if(mShadowCaster)
		{
			vector<Entity*> objects;
			if(needsUpdate(&objects))
				updateShadowGeom(objects);
		}
	}
	else if(stage == UpdateStage_Post)
	{
		mTransform = Transform::Identity;
		mTransform.translate(mOrigin);
	}
}

void Light::parse(fstream& stream, ParseMode mode)
{
}

Colour Light::getColour()
{
	return mColour;
}

uint8_t Light::getIntensity()
{
	return mIntensity;
}

LightType Light::getLightType()
{
	return mLightType;
}

bool Light::isShadowCaster()
{
	return mShadowCaster;
}

void Light::setShadowCaster(bool flag)
{
	mShadowCaster = flag;
}
