#include "Feanwork/Lighting/PointLight.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Module/PhysicsModule.h"

PointLight::PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity) :
	Light(origin, colour, intensity)
{
	init(colour, radius, intensity);
}

PointLight::PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity, Entity* target) :
	Light(origin, colour, intensity, target)
{
	init(colour, radius, intensity);
}

PointLight::PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity, float maxSpeed, Vector2f target) :
	Light(origin, colour, intensity, maxSpeed, target)
{
	init(colour, radius, intensity);
}

bool PointLight::needsUpdate(vector<Entity*>* objects)
{
	//TODO: could probably be optimised
	vector<Entity*> ents		= SceneManager::getInstance()->getScene(0)->getAllEntities();
	bool			needsUpdate = false;

	for(auto ent: ents)
	{
		//TODO: Check if object actually wants to cast a shadow
		if(mVertices.getBounds().intersects(FloatRect(ent->getPosition(), ent->getDim())))
		{
			PhysicsModule* mod = ent->getPhysicsMod();
			if(mod)
				needsUpdate += (mod->getPhysicsType() == PhysicsType_Dynamic);

			objects->push_back(ent);
		}
	}

	return needsUpdate;
}

void PointLight::updateShadowGeom(vector<Entity*>& objects)
{
	//vector<Entity*> objects = SceneManager::getInstance()->getScene(0)->getAllEntities();
	bool edgeTags[4]; //true: back-facing, false: front-facing
	mShadowGeoms.clear();

	for(auto ent: objects)
	{
		// Treat all objects as boxes
		BoxHull box(ent->getPosition(), ent->getDim());

		// Tag edges as front- or back-facing
		for(uint8_t e = 0; e < 4; e++)
		{
			Vector2f diff = (box.points[e] - mOrigin);
			edgeTags[e]   = (Vectorial::vectorDotprod(diff, box.getNormal(e)) > 0);
		}

		// Find starting point
		uint8_t i = 0;
				if(!edgeTags[0] && edgeTags[1]) i = 1;
		else if(!edgeTags[1] && edgeTags[2]) i = 2;
		else if(!edgeTags[2] && edgeTags[3]) i = 3;
		else if(!edgeTags[3] && edgeTags[0]) i = 0;
		else								 continue; //Light inside object

		//Construct shadow geometry
		mShadowGeoms.push_back(VertexArray());
		mShadowGeoms.back().setPrimitiveType(TrianglesStrip);

		for(uint8_t p = 0; p < 4; p++)
		{
			uint32_t index = i + p;
			if(index > 3)
				index -= 4;

			Vector2f ext = 20.0f * (box.points[index] - mOrigin) + mOrigin;
			mShadowGeoms.back().append(Vertex(box.points[index], Colour::Black));
			mShadowGeoms.back().append(Vertex(ext, Colour::Black));

			//Check if done
			if(!edgeTags[index])
				break;
		}
	}
}

void PointLight::parse(fstream& stream, ParseMode mode)
{
	// TODO point light parsing
}

void PointLight::init(Colour colour, float radius, uint8_t intensity)
{
	mLightType = LightType_PointLight;
	mRadius    = radius;

	//Create light geometry
	static uint32_t pointCount = 34; //32 outer + centre + repeated first
	mVertices.resize(pointCount);

	mVertices[0].position = Vector2f(0, 0);
	mVertices[0].color    = Colour(intensity * colour.r/255.0f, intensity * colour.g/255.0f, intensity * colour.b/255.0f);

	for(uint32_t i = 1; i < pointCount - 1; i++)
	{
		float angle = TWO_PI * (float)i / (float)(pointCount - 2.0f);
		float x		= mRadius * cos(angle);
		float y		= mRadius * sin(angle);

		mVertices[i].position = Vector2f(x, y);
		mVertices[i].color	  = Colour(0, 0, 0);
	}

	mVertices[pointCount-1]	= mVertices[1];
	mVertices.setPrimitiveType(TrianglesFan);
}
