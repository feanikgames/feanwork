#include "Feanwork/Lighting/LightManager.h"
#include "Feanwork/Lighting/Light.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Camera.h"
#include <SFML/OpenGL.hpp>

LightManager::LightManager()
{
	Vector2f screenSize = SceneManager::getInstance()->getScreen()->getView().getSize();
	mLightBuffer = new RenderTexture();
	mLightBuffer->create(screenSize.x, screenSize.y);

	mAccumBuffer = new RenderTexture();
	mAccumBuffer->create(screenSize.x, screenSize.y);
}

LightManager::~LightManager()
{
	for(auto light: mLights)
		delete light;

	delete mLightBuffer;
	delete mAccumBuffer;
	mLights.clear();
}

void LightManager::render(RenderTarget* target)
{
	if(getLightCount() == 0)
		return;

	Scene* scene = SceneManager::getInstance()->getScene(0); //TODO: support more than one scene *derp*
	scene->getActiveCamera()->toCamera(mLightBuffer);

	mAccumBuffer->clear(Colour(20, 20, 20, 255));
	for(auto light: mLights)
	{
		mLightBuffer->clear(Colour(0, 0, 0, 0));
		light->render(mLightBuffer);
		mLightBuffer->display();

		mAccumBuffer->draw(Sprite(mLightBuffer->getTexture()), RenderStates(BlendAdd));
	}
	mAccumBuffer->display();

	RenderStates states(BlendCustom);
	states.blendSrc = GL_ZERO;
	states.blendDst = GL_SRC_COLOR;

	scene->getActiveCamera()->toDefault(target);
	target->draw(Sprite(mAccumBuffer->getTexture()), states);
}

void LightManager::update()
{
	for(auto light: mLights)
		light->update(UpdateStage_Current);
}

void LightManager::addLight(Light* light)
{
	mLights.push_back(light);
}

Light* LightManager::getLight(int32_t index)
{
	return mLights[index];
}

vector<Light*>& LightManager::getLights()
{
	return mLights;
}

int32_t	LightManager::getLightCount()
{
	return mLights.size();
}
