/*
 * VarLookupTable.hpp
 *
 *  Created on: Mar 18, 2014
 *      Author: bryce
 */

#include "LookupTable.h"
#include "InstructionSet.h" //Defines Function/Variable

#include <iostream>

using namespace std;
uint32_t VarLookupTable::mID = 0;

VarLookupTable::VarLookupTable()
{
}


VarLookupTable::~VarLookupTable()
{
}

Variable* VarLookupTable::find(const KeyType& _key, std::initializer_list<std::string> _scopes )
{
	for(auto scope : _scopes)
	{
		auto itr = mLookup.find(scope);

		if(itr == mLookup.end())
			continue;

		auto f_itr = (*itr).second.find(_key);
		if(f_itr != (*itr).second.end())
			return (*f_itr).second;
	}

	return nullptr;
}



std::size_t VarLookupTable::count_of(std::initializer_list<std::string> _scopes)
{
	size_t count = 0;
	for(auto scope : _scopes)
	{
		count += mLookup[scope].size();
	}

	 return count;
}


void VarLookupTable::insert(const std::string& _scope, const KeyType& _key,const Variable& _obj)
{
	Variable* var = mLookup[_scope][_key];
	if(var)
	{
		*var = _obj;
		return;
	}

	mLookup[_scope][_key] = new Variable(_obj);
}

void VarLookupTable::replace(const std::string& _scope, const KeyType& _key,
		const Variable& _obj)
{
	auto itr = mLookup.find(_scope);
	if(itr == mLookup.end())
	{
		cerr << "Failed to replace value because no scope named '" << _scope.c_str() << "' exists.\n";
		return;
	}

	auto f_itr = (*itr).second.find(_key);

	if(f_itr == (*itr).second.end())
	{
		cerr << "Failed to replace value because no item with key named " << _key.name << "' exists.\n";
		return;
	}

	delete (*f_itr).second;
	(*f_itr).second = new Variable(_obj);
}

std::pair< VarLookupTable::item_iterator, VarLookupTable::item_iterator>
VarLookupTable::equal_range(const std::string& _scope)
{
	return std::make_pair(mLookup[_scope].begin(), mLookup[_scope].end());
}


std::vector<Variable*> VarLookupTable::getList(std::initializer_list<std::string> _scopes)
{
	std::vector<Variable*> v;

	for(auto scope : _scopes)
	{
		if(mLookup.find(scope) == mLookup.end())
		{
			cerr << "Could not locate scope '" << scope <<"'! Function results are undefined.\n";
			continue;
		}
		for(auto pair : mLookup[scope])
		{
			v.push_back(pair.second);
		}
	}

	return v;
}

////////////////////////////////////////////////

FuncLookupTable::FuncLookupTable()
{
}


FuncLookupTable::~FuncLookupTable()
{
}

Function* FuncLookupTable::find(const KeyType& _key, std::initializer_list<std::string> _scopes)
{
	for(auto scope : _scopes)
	{
		auto itr = mLookup.find(scope);

		if(itr == mLookup.end())
			return nullptr;

		auto f_itr = (*itr).second.find(_key);
		if(f_itr != (*itr).second.end())
			return (*f_itr).second;
	}

	return nullptr;
}



inline std::size_t FuncLookupTable::count_of(std::initializer_list<std::string> _scopes)
{
	size_t count = 0;
	for(auto scope : _scopes)
	{
		count += mLookup[scope].size();
	}

	 return count;
}


void FuncLookupTable::insert(const std::string& _scope, const KeyType& _key,const Function& _obj)
{
	Function* func = mLookup[_scope][_key];
	if(func)
	{
		*func = _obj;
		return;
	}

	mLookup[_scope][_key] = new Function(_obj);
}




std::pair< FuncLookupTable::item_iterator, FuncLookupTable::item_iterator>
FuncLookupTable::equal_range(const std::string& _scope)
{
	return std::make_pair(mLookup[_scope].begin(), mLookup[_scope].end());
}


std::vector<Function*> FuncLookupTable::getList(std::initializer_list<std::string> _scopes)
{
	std::vector<Function*> v;
	for(auto scope : _scopes)
	{
		if(mLookup.find(scope) == mLookup.end())
		{
			cerr << "Could not locate scope '" << scope <<"'! Function results are undefined.\n";
			continue;
		}

		for(auto pair : mLookup[scope])
		{
			v.push_back(pair.second);
		}
	}

	return v;
}

