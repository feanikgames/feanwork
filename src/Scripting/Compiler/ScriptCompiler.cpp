/*
 * ScriptCompiler.cpp
 *
 *  Created on: Oct 19, 2013
 *      Author: bryce
 */

#include "ScriptCompiler.h"
#include "InstructionSet.h"
#include "Expression.h"
#include "Defines.h"
#include "Token.h"

#include <iostream>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <stack>
#include <cstdlib>
#include <string>

//Defines.h
std::vector<std::string> gKeywords =
{
	"var", "func", "decl", "while", "for", "if", "then", "else", "elif", "end", "break", "in",
	"out", "true", "false" ,"extern", "eqt", "gtn", "ltn" ,"gteq", "lteq", "and", "or", "not",
	"endfunc", "endif", "endloop", "return", "do"
};

std::vector<std::string> gTypeStrings =
{
	"ValueType_Unknown", "ValueType_String", "ValueType_Number", "ValueType_Delimiter",
	"ValueType_Separater", "ValueType_Keyword", "ValueType_OpenGroup", "ValueType_CloseGroup",
	"ValueType_Operand", "ValueType_Boolean", "ValueType_Variable", "ValueType_Function",
	"ValueType_Temporary"
};

//TODO: Add line number tracking, or some other form of code tracking for error output
//TODO: Integrate the LookupTable system implemented in the interpreter

using namespace std;

ScriptCompiler::ScriptCompiler() :
		mCurrentToken(0), mNextTemp(0), mLevel(0)
{
}

ScriptCompiler::~ScriptCompiler()
{
}

ScriptCompiler::ProgramData ScriptCompiler::compile(const char* data)
{
	/*
	 * Begin by breaking down the file into groups of expressions.
	 * Note that inside braces, what would be a separate expressions are considered
	 * part of a single expression at this point.
	 */

	mTokens = generateTokens(data);

	//Instructions at global scope
	InstructionSet* set = new InstructionSet("global");

	/*
	 * Create a new set of expressions from the tokens. This will group relevant
	 * sets of tokens together. For example 'func' + 'myFunction' + '(' + ')' + ';'
	 * would be an expression representing a function declaration.
	 */

	ExpressionList list = generateExpressions();

	/*
	 * Create Instructions based on the expressions. This will compile a table of function
	 * and variable names for lookup and error checking.
	 */

	for (Expression* expr : list)
	{
		//expr->render();
		generateInstructions(expr, set);
	}

	set->render();

	set->finalize();

	cout << "Number Global Definitions: " << list.size() << "\n";
	cout << "Number Instructions: " << set->getInstructionCount() << "\n";
	cout << "Compiled Program Size: " << set->getDataSize() << " bytes\n";

	return
	{	set->getCompiledData(), set->getDataSize(), set->getInstructionCount()};

}

ScriptCompiler::TokenList ScriptCompiler::generateTokens(std::string _exp)
{
	TokenList tokens;
	uint32_t line = 0;
	size_t pIdx = 0, idx = 0, commentEnd = 0;
	string quote;

	const string delims = " +-/*^%([])=<>!;\",\n";

	while ((idx = _exp.find_first_of(delims, pIdx)) != string::npos)
	{
		string token = _exp.substr(pIdx, idx - pIdx);

		if(_exp[idx] == '\n')
		{
			++line;
		}

		if (_exp[idx] == '/' && _exp[idx + 1] == '/')
		{
			//Make sure this isn't a commented line
			if ((commentEnd = _exp.find("\n", idx)) == string::npos)
			{
				//WTF? It HAS to be the end of the file
				pIdx = idx + 1;
				continue;
			}

			//Remove the entire line
			_exp.erase(idx, commentEnd - idx);
			//Replace it with whitespace (which will later be ignored)
			_exp.insert(idx, commentEnd - idx, ' ');

			pIdx = idx + 1;
			continue;
		}

		if (_exp[idx] == '\"')
		{
			//XXX: Maybe put line incrementing in side of while loop
			pIdx = idx + 1; //Increment

			quote += _exp[idx];
			while ((idx = _exp.find_first_of('\"', pIdx)) != string::npos)
			{
				quote += _exp.substr(pIdx, idx - pIdx);
				quote += _exp[idx];
				pIdx = idx + 1; //Increment
			}

			tokens.push_back(Token(quote, line));
			quote.clear();

			continue; //We've already incremented so continue like normal
		}

		/*if (_exp[idx] == '-')
		{
			//If the token between the negative sign and the last token is empty (or a space)
			//then the negative sign needs to be added to the next token
			if(token.empty())
			{
				//Skip ahead to the next token, without setting the sign as the previous so that the parser reads
				//the token and the minus sign as a single token
				token.assign("-1 *");
			}

			//Remove tabs from tokens
			token.erase(std::remove(token.begin(), token.end(), '\t'), token.end());
			tokens.push_back(Token(token, line));

			pIdx = idx + 1;
			continue;
		}*/

		if (!token.empty())
		{
			//Remove tabs from tokens
			token.erase(std::remove(token.begin(), token.end(), '\t'), token.end());
			if (!token.empty()) //Make sure it's still not empty
				tokens.push_back(Token(token, line));
		}

		//Skip spaces and newlines
		if (_exp[idx] == ' ' || _exp[idx] == '\n')
		{
			pIdx = idx + 1; //Increment
			continue; //Continue to next delimiter
		}

		//Save delimiters that aren't spaces
		tokens.push_back(Token(string(1, _exp[idx]), line));

		pIdx = idx + 1;
	}

	return tokens;
}

ScriptCompiler::ExpressionList ScriptCompiler::generateExpressions()
{
	ExpressionList exprs;

	while (hasTokens())
	{
		Token& t = getToken();

		//t.render();
		if(t.is(ValueType_Keyword))
		{
			if (t.is("var"))
			{
				Expression* s = createVariableExpression(t);
				s->validate();//Do final checks
				exprs.push_back(s);
			}

			if (t.is("func"))
			{
				Expression* s = createFunctionalExpression(t);
				s->validate();
				exprs.push_back(s);
			}

			if (t.is("decl"))
			{
				Expression* s = createDeclarationExpression(t);
				s->validate();
				exprs.push_back(s);
			}
		}
	}

	return exprs;
}

Token& ScriptCompiler::getToken()
{
	return mTokens[mCurrentToken++];
}

Expression* ScriptCompiler::createVariableExpression(Token& _start)
{
	//Token Form: Keyword + Unknown(s) + Operand (=) + Value + Delimiter (;)
	Expression* x = new Expression(ExpressionType_VariableDec);
	x->addToken(_start);

	do
	{
		Token& t = getToken();

		if (t.is("="))
			x->setType(ExpressionType_VariableDef);

		if (t.is(";"))
		{
			x->addToken(t);
			break;
		}

		x->addToken(t);
	} while (hasTokens());

	return x;

}

Expression* ScriptCompiler::createDeclarationExpression(Token& _start)
{
	Expression* x = new Expression(ExpressionType_FunctionDec);
	x->addToken(_start);
	do
	{
		Token& t = getToken();

		if (t.is("="))
		{
			throw std::runtime_error("Error:"+ to_string(t.getLine())+":Illegal definition in declaration.");
		}

		if (t.is(";"))
		{
			x->addToken(t);
			break;
		}

		x->addToken(t);
	} while (hasTokens());

	return x;
}

Expression* ScriptCompiler::createFunctionalExpression(Token& _start)
{
	/*
	 * Token Form: Keyword + OpenGroup(parenthesis) + Args w/ separators
	 * + CloseGroup + OpenGroup(curly brace) + FuncBody + CloseGroup
	 */

	Expression* x = new Expression(ExpressionType_FunctionDef);
	x->addToken(_start); //'func'
	x->addToken(getToken()); //Function name

	do
	{
		Token& t = getToken();

		//t.render();

		if (t.is("if") || t.is("while") || t.is("for"))	//Check for nested blocks
		{
			handleNestedBlock(t, x);
			continue;
		}

		if (t.is("var"))					//Check for in-function variables
		{
			handleNestedDeclaration(t, x);
			continue;
		}


		if (t.is("return"))
		{
			handleReturn(t, x);
			continue;
		}

		if (t.is(ValueType_Unknown)) //Check for function calls and assignments
		{
			if (!hasTokens()) //There's a random unknown token o.O
				throw runtime_error("Error:"+ to_string(t.getLine())+ ": Stray token '" + t.getData() + "'");

			Token t1 = peekToken();

			if (t1.is("="))
			{
				handleAssignments(t, x);
				continue;
			}

			if (t1.is("(")) //See if it's a parameter list, if so, then it's a function call
			{
				handleFunctionCall(t, x);
				continue;
			}

		}

		if (t.is("endfunc"))
		{
			//Check to see if we've recursed to the top and have closed the function
			x->addToken(t); //Add the closing brace to the current expression before recursing
			break;
		}

		x->addToken(t);

	} while (hasTokens());

	if (x->getParent())
		cout << "Warning: Child expression being returned!\n";

	return x;
}

void ScriptCompiler::generateInstructions(Expression* _e, InstructionSet* _s)
{
	switch (_e->getType())
	{
		//Look for declarations
		case ExpressionType_VariableDec:
			declareVar(_e, _s);
			break;

		case ExpressionType_VariableDef:
			defineVar(_e, _s);
			break;

		case ExpressionType_FunctionDec:
			declareFunc(_e, _s);
			break;

		case ExpressionType_FunctionDef:
			defineFunc(_e, _s);
			break;

		case ExpressionType_FunctionCall:
			callFunc(_e, _s);
			break;

		case ExpressionType_Assignment:
			assignVar(_e, _s);
			break;

		case ExpressionType_Return:
			_s->add(new Instruction(RTRN));
			break;

		case ExpressionType_IfBlock:
			defineIf(_e, _s);
			break;
		case ExpressionType_WhileLoop:
			break;
		case ExpressionType_ForLoop:
			break;

		default:
			break;
	}
}

Token ScriptCompiler::peekToken()
{
	return mTokens[mCurrentToken];
}

/*
 * *************************************
 *
 * NESTED-EXPRESSION AUXILLIARY FUNCTIONS
 *
 * *************************************
 */
void ScriptCompiler::handleNestedBlock(const Token& _keyword, Expression* _parent)
{
	if (!_parent)
		throw runtime_error("Error:"+to_string(_keyword.getLine())+":Nested block without parent expression!");

	Expression* block = nullptr;

	if (_keyword.is("if"))
		block = _parent->createChild(ExpressionType_IfBlock);
	else if (_keyword.is("while"))
		block = _parent->createChild(ExpressionType_WhileLoop);
	else if (_keyword.is("for"))
		block = _parent->createChild(ExpressionType_ForLoop);

	if (!block)
		throw logic_error("Unhandled nested specifier '" + _keyword.getData() + "' encountered.");

	block->addToken(_keyword);
	do
	{
		Token& t = getToken();

		if (t.is("if") || t.is("while") || t.is("for"))	//Check for nested blocks
		{
			handleNestedBlock(t, block);
			continue;
		}

		if (t.is("var"))					//Check for in-function variables
		{
			handleNestedDeclaration(t, block);
			continue;
		}

		if (t.is("return"))
		{
			handleReturn(t, block);
			continue;
		}


		if (t.is(ValueType_Unknown)) //Check for function calls
		{
			if (!hasTokens()) //There's a random unknown token o.O
				throw runtime_error("Error:"+ to_string(t.getLine())+": Unknown token '" + t.getData() + "'");

			Token t1 = peekToken();

			if (t1.is("="))
			{
				handleAssignments(t, block);
				continue;
			}

			if (t1.is("(")) //See if it's a parameter list, if so, then it's a function call
			{
				handleFunctionCall(t, block);
				continue;
			}

		}

		if (t.is("func"))
			throw runtime_error("Error:"+ to_string(t.getLine()) + ": Function definition/declaration within nested block!");

		if (t.is("endif") || t.is("endloop"))
		{
			block->addToken(t);
			break;
		}

		block->addToken(t);

	} while (hasTokens());
}

void ScriptCompiler::handleReturn(const Token& _keyword, Expression* _parent)
{
	if (!_parent)
		throw runtime_error("Error:"+ to_string(_keyword.getLine())+": Nested declaration statement without parent expression!");

	Expression* block = _parent->createChild(ExpressionType_Return);

	block->addToken(_keyword);
	do
	{
		Token& t = getToken();
		if (t.is(";"))
		{
			block->addToken(t);
			break;
		}

		block->addToken(t);

	} while (hasTokens());

}

void ScriptCompiler::handleNestedDeclaration(const Token& _keyword, Expression* _parent)
{
	if (!_parent)
		throw runtime_error("Error:"+ to_string(_keyword.getLine())+":Nested declaration statement without parent expression!");

	Expression* block = nullptr;

	if (_keyword.is("var"))
		block = _parent->createChild(ExpressionType_VariableDec);

	if (!block)
		throw logic_error("Unhandled nested specifier '" + _keyword.getData() + "' encountered.");

	block->addToken(_keyword);
	do
	{
		Token& t = getToken();
		if (t.is(";"))
		{
			block->addToken(t);
			break;
		}

		block->addToken(t);

	} while (hasTokens());
}

void ScriptCompiler::handleFunctionCall(const Token& _func, Expression* _parent)
{
	if (!_parent)
		throw runtime_error("Error:"+ to_string(_func.getLine())+":Function call without parent expression!");

	Expression* block = _parent->createChild(ExpressionType_FunctionCall);

	block->addToken(_func);
	do
	{
		Token& t = getToken();
		if (t.is(";"))
		{
			block->addToken(t);
			break;
		}

		block->addToken(t);

	} while (hasTokens());
}

void ScriptCompiler::handleAssignments(const Token& _target, Expression* _parent)
{
	if (!_parent)
		throw runtime_error("Error:"+ to_string(_target.getLine())+":Nested assignment without parent expression!");

	Expression* block = _parent->createChild(ExpressionType_Assignment);

	block->addToken(_target);
	do
	{
		Token& t = getToken();
		if (t.is(";"))
		{
			block->addToken(t);
			break;
		}

		block->addToken(t);

	} while (hasTokens());
}

void ScriptCompiler::declareVar(Expression* _e, InstructionSet* _s)
{
	const Token& t = _e->getToken(1); //Variable name

	//Confirm it's unknown
	_s->add(new Instruction(DECV, t));
}

void ScriptCompiler::defineVar(Expression* _e, InstructionSet* _s)
{

	const Token& t = _e->getToken(1);

	//Declare the variable...
	_s->add(new Instruction(DECV, t));

	_s->add(new Instruction(DEFV, t));

	//...then send it's contents as an assignment
	assignVar(_e, _s);

	_s->add(new Instruction(EODF));
}

void ScriptCompiler::declareFunc(Expression* _e, InstructionSet* _s)
{
	const Token& t = _e->getToken(1); //Function name

	if(t.is(ValueType_Keyword) && t.is("extern"))
	{
		const Token& name = _e->getToken(2);
		_s->add(new Instruction(DEFF, name)); //Set it as defined, it must be resolved at runtime
	}else{
	//Confirm it's unknown
	_s->add(new Instruction(DECF, t));
	}

	std::vector<Token> args = _e->getArgumentList();

	for(auto &t : args)
	{
		//Ignore separators
		if (t.is(ValueType_Separator))
			continue;

		if(!t.is(ValueType_Keyword))
			_s->add(new Instruction(DECV, t));

		_s->add(new Instruction(ARG, t));
	}

	_s->add(new Instruction(ESIG));
	_s->add(new Instruction(EOFN));
}

void ScriptCompiler::defineFunc(Expression* _e, InstructionSet* _s)
{
	//TODO: Support overloading functions?
	_e->getToken(0); //'func'
	const Token& t = _e->getToken(1); //Function name

	_s->add(new Instruction(DEFF, t));

	//Get argument list. Here it serves to declare/define input and output variables.
	//Ignore if there is no argument list
	std::vector<Token> args = _e->getArgumentList();

	for(auto &t : args)
	{
		//Ignore separators
		if (t.is(ValueType_Separator))
			continue;

		_s->add(new Instruction(ARG, t));
	}

	_s->add(new Instruction(ESIG));

	//Add instructions recursively
	Expression::ExpressionList children = _e->getChildren();
	for (Expression* c : children)
	{
		generateInstructions(c, _s);
	}

	_s->add(new Instruction(EOFN));
}

void ScriptCompiler::callFunc(Expression* _e, InstructionSet* _s)
{
	const Token& t = _e->getToken(0); //Function name

	_s->add(new Instruction(CALL, t));

	//Get argument list
	int32_t argb = _e->find(0, "(");

	//Function calls must have argument list even if there are no arguments in the definition
	if (argb == -1)
	{
		throw runtime_error("Error: Function call missing argument list! Line: " + to_string(t.getLine()));
	}

	int32_t arge = _e->findMatching(argb);

	//Count tokens between between opening and closing parenthesis
	uint32_t argc = (arge - argb);

	for (uint32_t i = 1; i < argc; ++i)
	{
		const Token& t = _e->getToken(argb + i);

		//Ignore separators
		if (t.is(ValueType_Separator))
			continue;

		_s->add(new Instruction(ARG, t));
	}

	_s->add(new Instruction(ESIG));
}

void ScriptCompiler::assignVar(Expression* _e, InstructionSet* _s)
{
	//We've got to break the assignment into binary parts.
	decomposeArithmetic(_e, _s);

	int32_t eq = _e->find(0, "=");

	_s->add(new Instruction(ASN));
	_s->add(new Instruction(ARG, _e->getToken(eq - 1)));
	_s->add(new Instruction(ARG, _e->getToken(eq + 1)));
}

void ScriptCompiler::defineIf(Expression* _e, InstructionSet* _s)
{
	_s->add(new Instruction(CHK));

	int32_t open = _e->find(0, "(");
	int32_t close = _e->findMatching(open);

	//Count tokens between between opening and closing parenthesis
	uint32_t argc = (close - open) - 1;


	if (argc == 1)
	{
		_s->add(new Instruction(ARG, _e->getToken(open + 1)));
		_s->add(new Instruction(EQT));
		_s->add(new Instruction(ARG, Token("true")));

		return;
	}

	//Discard the outer sets of parenthesis
	_e->getToken(open).setPriority(Priority_Discard);
	_e->getToken(close).setPriority(Priority_Discard);

	Expression* condition = new Expression(_e->getArgumentList());
	decomposeBoolean(condition, _s);

	_s->add(new Instruction(EQT));
	_s->add(new Instruction(ARG, condition->getToken(0)));
	_s->add(new Instruction(ARG, Token("true")));

	//Handle the body
	_s->add(new Instruction(DOIF));
		Expression::ExpressionList children = _e->getChildren();
		for (Expression* c : children)
		{
			generateInstructions(c, _s);
		}
	_s->add(new Instruction(END));
}

void ScriptCompiler::decomposeBoolean(Expression* _e, InstructionSet* _s)
{
	//The concept is the same as the arithmetic decomposition

	Priority priority = (Priority)0;
	int32_t index = _e->getHighestPriorityToken(&priority);
	std::string temp;
	while (index != -1)
	{
		//_e->render();
		Expression::TokenList tokens = _e->getTokens();

		uint32_t lh, rh;
		if (index > 0)
			lh = index - 1;

		if ((uint32_t) index < tokens.size() && tokens.size() > 1)
			rh = index + 1;

		switch (priority)
		{
			case Priority_One: //Parenthesis
			{

				int32_t closeIndex = _e->findMatching(index);

				Expression sub;
				if (closeIndex == -1)
					throw runtime_error("Error:"+ to_string(_e->getToken(index).getLine())+": Parenthetical mismatch");

				//Extract the contents of the parenthesis and push their commands first
				for (int32_t i = index; i < closeIndex; ++i)
				{
					//Skip the parenthesis
					if (tokens[i].is("(") || tokens[i].is(")"))
						continue;

					sub.addToken(tokens[i]);
				}

				decomposeBoolean(&sub, _s);

				//Remove the parenthesis and replace it with the value of it's contents (stored as a named temporary)
				_e->replaceRange(index, closeIndex, sub.getToken(0));

			}
				break;

			case Priority_Two: //Modifiers
				_s->add(new Instruction(MOD));

				if(tokens[index].is("not"))
					_s->add(new Instruction(NOT));

				break;

			case Priority_Three: //Operators

				if(tokens[index].is("eqt"))
					_s->add(new Instruction(EQT));
				else if(tokens[index].is("and"))
					_s->add(new Instruction(AND));
				else if(tokens[index].is("or"))
					_s->add(new Instruction(OR));
				else if(tokens[index].is("gtn"))
					_s->add(new Instruction(GTN));
				else if(tokens[index].is("ltn"))
					_s->add(new Instruction(LTN));
				else if(tokens[index].is("gteq"))
					_s->add(new Instruction(GTEQ));
				else if(tokens[index].is("lteq"))
					_s->add(new Instruction(LTEQ));
				else
					throw runtime_error("Error:" + to_string(tokens[index].getLine())+
							": Encountered invalid boolean operator '" + tokens[index].getData() + "'");


				_s->add(new Instruction(ARG, tokens[lh]));
				_s->add(new Instruction(ARG, tokens[rh]));

				temp = getTemporaryName();
				_s->add(new Instruction(STO, Token(ValueType_Temporary, temp)));
				_e->replaceRange(lh, rh, Token(ValueType_Temporary, temp));

				break;

			default:
				break;
		}

		index = _e->getHighestPriorityToken(&priority);
	}

}

void ScriptCompiler::decomposeArithmetic(Expression* _e, InstructionSet* _s)
{

	Priority priority = (Priority)0;
	int32_t index = _e->getHighestPriorityToken(&priority);
	std::string temp;
	while (index != -1)
	{

		Expression::TokenList tokens = _e->getTokens();

		uint32_t lh, rh;
		if (index > 0)
			lh = index - 1;

		if ((uint32_t) index < tokens.size() && tokens.size() > 1)
			rh = index + 1;

		switch (priority)
		{
			case Priority_One: //Parenthesis
			{
				int32_t closeIndex = _e->findMatching(index);
				Expression sub;
				if (closeIndex == -1)
					throw runtime_error("Error:"+ to_string(_e->getToken(index).getLine())+": Parenthetical mismatch.");

				//Extract the contents of the parenthesis and push their commands first
				for (int32_t i = index; i < closeIndex; ++i)
				{
					//Skip the parenthesis
					if (tokens[i].is("(") || tokens[i].is(")"))
						continue;

					sub.addToken(tokens[i]);
				}

				decomposeArithmetic(&sub, _s);

				//Remove the parenthesis and replace it with the value of it's contents (stored as a named temporary)
				_e->replaceRange(index, closeIndex, sub.getToken(0));

			}
				break;

			case Priority_Two: //Exponents
				_s->add(new Instruction(EXP));
				_s->add(new Instruction(ARG, tokens[lh]));
				_s->add(new Instruction(ARG, tokens[rh]));

				temp = getTemporaryName();
				_s->add(new Instruction(STO, Token(ValueType_Temporary, temp)));
				_e->replaceRange(lh, rh, Token(ValueType_Temporary, temp));
				break;

			case Priority_Three: //Multiply and Divide
				if (tokens[index].is("*"))
					_s->add(new Instruction(MUL));
				else
					_s->add(new Instruction(DIV));

				_s->add(new Instruction(ARG, tokens[lh]));
				_s->add(new Instruction(ARG, tokens[rh]));

				temp = getTemporaryName();
				_s->add(new Instruction(STO, Token(ValueType_Temporary, temp)));
				_e->replaceRange(lh, rh, Token(ValueType_Temporary, temp));
				break;

			case Priority_Four:
				if (tokens[index].is("+"))
					_s->add(new Instruction(ADD));
				else
					_s->add(new Instruction(SUB));

				_s->add(new Instruction(ARG, tokens[lh]));
				_s->add(new Instruction(ARG, tokens[rh]));

				temp = getTemporaryName();
				_s->add(new Instruction(STO, Token(ValueType_Temporary, temp)));
				_e->replaceRange(lh, rh, Token(ValueType_Temporary, temp));
				break;

			default:
				break;
		}

		index = _e->getHighestPriorityToken(&priority);
	}
}

std::string ScriptCompiler::getTemporaryName()
{
	return string("infx_tmp#") + std::to_string(mNextTemp++);
}

