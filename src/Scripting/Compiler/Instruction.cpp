/*
 * Instruction.cpp
 *
 *  Created on: Oct 21, 2013
 *      Author: bryce
 */

#include "Instruction.h"

#include <iostream>
#include <stdexcept>
#include <algorithm>

using namespace std;

vector<string> opCodes =
{
	"DECV", //Declare Var"
	"DEFV", //Define Var
	"DECF", //Declare Function
	"DEFF", //Define Function
	"EOFN", //End of function
	"EODF", //End of (var) definition
	"CHK", //Check conditional
	"JMP", //Branch to group
	"BGP", //Begin Group
	"EGP", //End Group
	"ESIG", //End (function) signature

	"EXP", //Exponent
	"MUL", //Multiply
	"DIV", //Divide
	"ADD", //Add
	"SUB", //Subtract
	"ASN", //Assign
	"STO", //Store

	"GTN", //Greater than
	"LTN", //Less than
	"EQT", //Equal to
	"GTEQ", //Greater than/ equal to
	"LTEQ", //Less than/equal to
	"AND",
	"OR",
	"NOT",

	"RTRN",
	"DOIF",
	"END",

	"ARG", //Argument name
	"MOD", //Modifier
	"CALL", //Call (function)
};

Instruction::~Instruction()
{
}

void Instruction::render()
{
	cout << opCodes[mOpCode] << " " << mID.getData() << "\n";
}
