/*
 * Expression.cpp
 *
 *  Created on: Oct 20, 2013
 *      Author: bryce
 */

#include "Expression.h"
#include "InstructionSet.h"
#include "Instruction.h"
#include "Defines.h"

#include <iostream>
#include <stdexcept>
#include <regex>

using namespace std;

vector<string> Expression::mExpectingStrings = vector<string>(
	{
		"';'",
		"'endloop'",
		"'endif'",
		"'endfunc'",
		"')'",
	}
);

vector<string> Expression::mExpressionStrings = vector<string>(

	{
		"ExpressionType_VariableDec",
		"ExpressionType_VariableDef",
		"ExpressionType_FunctionDec",
		"ExpressionType_FunctionCall",
		"ExpressionType_FunctionDef",
		"ExpressionType_ForLoop",
		"ExpressionType_WhileLoop",
		"ExpressionType_IfBlock",
		"ExpressionType_Assignment",
		"ExpressionType_Return",
		"ExpressionType_Unknown",
	}
);

Expression::Expression(ExpressionType _type) :
		mParent(nullptr), mIndex(0), mType(_type)
{
}


Expression::Expression(const std::vector<Token>& _tokens, ExpressionType _type) : mParent(nullptr),
		mTokens(_tokens), mIndex(0), mType(_type)
{
}


Expression::~Expression()
{
}

void Expression::render()
{

	cout << "\tBegin Expression: " << toString(mType) << "\n";

	uint32_t index = 0;
	for(Token& t : mTokens)
	{
		t.render();


		for(auto& child : mChildren)
			if(child->mIndex == index)
				child->render();

		++index;
	}

	cout << "\tEnd Expression: \n";
	cout << endl;
}

Expression* Expression::createChild(ExpressionType _type)
{
	Expression* e = new Expression(_type);
	e->mIndex = mTokens.size() - 1
			;
	mChildren.push_back(e);

	return e;
}

void Expression::addToken(const Token& _t)
{
	mTokens.push_back(_t);


}

void Expression::removeToken(const Token& _t)
{
	//XXX: Functions that remove tokens do not adjust for children because they
	//for the most part should not have any. If this becomes a problem, re-evaluate these
	//functions
	TokenList::iterator itr = std::find(mTokens.begin(), mTokens.end(), _t);

	if (itr == mTokens.end())
		return; //Throw error?

	mTokens.erase(itr);
}

int32_t Expression::rfindMatching(uint32_t _endGroup)
{
	assert(_endGroup < mTokens.size());
	assert(mTokens[_endGroup].is(ValueType_CloseGroup));

	int32_t level = 0, found = -1;
	for (uint32_t find = _endGroup + 1; find < mTokens.size(); find--)
	{
		Token& t = mTokens[find];
		if (t.is(ValueType_OpenGroup))
			level--;

		if (t.is(ValueType_CloseGroup))
			level++;

		if (level < 0)
		{
			found = find;
			break;
		}
	}

	return found;
}

int32_t Expression::findMatching(uint32_t _startGroup) const
{
	assert(_startGroup < mTokens.size());
	//assert(mTokens[_startGroup].is(ValueType_OpenGroup));

	int32_t level = 0, found = -1;
	for (uint32_t find = _startGroup + 1; find < mTokens.size(); find++)
	{
		const Token& t = mTokens[find];

		if (t.is(ValueType_OpenGroup))
			level++;

		if (t.is(ValueType_CloseGroup))
			level--;

		if (level < 0)
		{
			found = find;
			break;
		}
	}

	return found;
}

int32_t Expression::find(uint32_t _start, const std::string& _token) const
{
	for (uint32_t i = _start; i < mTokens.size(); ++i)
	{
		//const Token& debug = mTokens[i];
		if (mTokens[i].is(_token))
			return (int32_t) i;
	}

	return -1;
}

void Expression::replaceRange(uint32_t _begin, uint32_t _end, const Token& _t)
{
	//XXX: Functions that remove tokens do not adjust for children because they
	//for the most part should not have any. If this becomes a problem, re-evaluate these
	//functions
	assert(_begin >= 0);
	assert(_end < mTokens.size());
	assert(_begin < _end);

	mTokens.erase(mTokens.begin() + _begin, mTokens.begin() + _end + 1);
	mTokens.insert(mTokens.begin() + _begin, _t);
}

int32_t Expression::getHighestPriorityToken(Priority* _code)
{

	int32_t index = -1;
	Priority hp = Priority_Discard;
	for (uint32_t i = 0; i < mTokens.size(); ++i)
	{
		Token& t = mTokens[i];
		if (t.getPriority() > hp)
		{
			hp = t.getPriority();
			*_code = hp;
			index = i;
		}
	}

	return index;
}

std::string Expression::toString(ExpressionType _et)
{
	assert((int)_et > -1);
	assert((int)_et < mExpressionStrings.size());

	return mExpressionStrings[_et];
}

void Expression::validate()
{
	for(auto &_t : mTokens)
	{
		if(_t.is("if"))
			mQueue.push(Expecting_EndIf);

		if(_t.is("while") || _t.is("for"))
			mQueue.push(Expecting_EndLoop);

		if(_t.is("func"))
			mQueue.push(Expecting_EndFunc);

		if(_t.is("("))
			mQueue.push(Expecting_RParenthesis);


		if(_t.is(ValueType_CloseGroup))
		{
			if(mQueue.empty())
				throw runtime_error("Error " + to_string(_t.getLine()) + ": Unexpected '" + _t.getData() + "'");

			if(_t.is("endif") && (mQueue.top() != Expecting_EndIf))
			{
				throw runtime_error("Error: ln" + to_string(_t.getLine()) + ": Expected " +
						translate(mQueue.top()) + " before 'endif'");
			}

			if(_t.is("endloop") && (mQueue.top() != Expecting_EndLoop))
			{
				throw runtime_error("Error: ln" + to_string(_t.getLine()) + ": Expected " +
							translate(mQueue.top()) + " before 'endloop'");
			}

			if(_t.is("endfunc") && (mQueue.top() != Expecting_EndFunc))
			{
				throw runtime_error("Error: ln" + to_string(_t.getLine()) + ": Expected " +
							translate(mQueue.top()) + " before 'endfunc'");
			}

			if(_t.is(")") && (mQueue.top() != Expecting_RParenthesis))
			{
				throw runtime_error("Error: ln" + to_string(_t.getLine()) + ": Expected " +
							translate(mQueue.top()) + " before ')'");
			}
			mQueue.pop();
		}
	}

	if(!mQueue.empty())
	{
		throw runtime_error("Error: ln?: Expected " + translate(mQueue.top()));
	}

	for(auto &c : mChildren)
	{
		c->validate();
	}
}

std::vector<Token> Expression::getArgumentList() const
{
	assert(mType != ExpressionType_FunctionDef || mType != ExpressionType_FunctionCall || mType != ExpressionType_FunctionCall);

	std::vector<Token> args;

	int32_t begin = find(0,"(");

	if (begin == -1)
		return args; //No argument list means no args


	int32_t end = findMatching(begin);


	for (int32_t i = begin + 1; i < end; ++i)
	{
		args.push_back(mTokens[i]);
	}

	return args;

}

std::string Expression::translate(Expecting _e)
{
	assert((int)_e > -1);
	assert((int)_e < mExpectingStrings.size());

	return mExpectingStrings[_e];
}
