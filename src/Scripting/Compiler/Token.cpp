/*
 * Token.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: bryce
 */

#include "Token.h"
#include "Defines.h"

#include <stdexcept>
#include <iostream>

using namespace std;

Token::Token(const std::string _token, uint32_t _line) :
		mData(_token), mPriority(Priority_Discard), mType(0), mLine(_line)
{
	//Determine the valuetype
	for (auto keyword : gKeywords)
	{
		if (!mData.compare(keyword))
		{
			mType |= ValueType_Keyword;

			if(!mData.compare("not"))
			{
				mPriority = Priority_Two;
			}

			if( !mData.compare("or")   || !mData.compare("and") ||
				!mData.compare("gtn")  || !mData.compare("ltn") ||
				!mData.compare("eqt")  ||!mData.compare("gteq") || !mData.compare("lteq") )
			{
				mPriority = Priority_Three;
				mType |= ValueType_Boolean;
			}

			break;
		}
	}


	if (mData.find_first_of("+/-=%^!*<>") != string::npos)
	{
		mType |= ValueType_Operand;

		if (mData.find_first_of("^!") != string::npos)
		{
			mPriority = Priority_Two;
		}

		if (mData.find_first_of("*/") != string::npos)
		{
			mPriority = Priority_Three;
		}

		if (mData.find_first_of("+-") != string::npos)
		{
			mPriority = Priority_Four;
		}
	}

	if ((mData.find_first_of("([") != string::npos )|| !mData.compare("if") || !mData.compare("func") || !mData.compare("while") ||
			!mData.compare("for"))
	{
		mType |= ValueType_OpenGroup;

		if (!mData.compare("("))
		{
			mPriority = Priority_One;
		}
	}

	if ((mData.find_first_of(")]") != string::npos) || !mData.compare("endif") ||
			!mData.compare("endloop") || !mData.compare("endfunc"))
		mType |= ValueType_CloseGroup;


	if (mData.find_first_of(";") != string::npos)
		mType |= ValueType_Delimiter;

	if (mData.find_first_of(",") != string::npos)
		mType |= ValueType_Separator;

	if (mData.find_first_of("\"'") != string::npos)
		mType |= ValueType_String;

	if (mData.find_first_of("0123456789") != string::npos)
		mType |= ValueType_Number;

	if(mType == 0)
		mType = ValueType_Unknown;

	if (!mData.compare(" ") || mData.empty())
		throw std::logic_error("Empty token encountered");

}

void Token::render()
{
	cout << /*gTypeStrings[mType] <<*/ ": " << mData << "\n";
}
