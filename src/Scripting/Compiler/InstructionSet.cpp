/*
 * InstructionSet.cpp
 *
 *  Created on: Oct 21, 2013
 *      Author: bryce
 */

#include "InstructionSet.h"
#include "Expression.h"

#include <stdexcept>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

InstructionSet::InstructionSet(const std::string& _scope) :
		mRoot(nullptr), mTail(nullptr), mCompiledData(nullptr), mDataSize(0), mInstructionCount(0),
		mScope(new Scope("global")), mInSignature(false)
{
}

InstructionSet::~InstructionSet()
{
	delete[] mCompiledData;
}

void InstructionSet::add(Instruction* _i, bool _throw)
{
	//Go through the generated instructions and check for errors
		switch (_i->mOpCode)
		{
			case DEFF:
				defineFunction(_i);
				mInSignature = true;
				break;

			case DEFV:
				defineVariable(_i);
				break;

			case DECV:
				declareVariable(_i);
				break;

			case DECF:
				declareFunction(_i);
				mInSignature = true;
				break;

			case CALL:
				callFunction(_i);
				break;

			case ARG:
				//resolveArgs(_i);
				break;

			case ESIG:
				mInSignature = false;
				break;

			case EOFN:
			{
				//checkSignature(_i);
				break;
			}

			default:
				break;

		}

	mInstructionCount++;//Calculate binary data size
	mDataSize += sizeof(uint32_t) + sizeof(char) + _i->mID.getData().length() + 1;

	if (!mRoot)
	{
		mRoot = _i;
		mTail = _i;
		return;
	}

	_i->mPrev = mTail;
	mTail->mNext = _i;
	mTail = _i;


	return;
}

void InstructionSet::declareVariable(Instruction* _ins)
{
	string name = _ins->getToken().getData();

	if(_ins->mID.is(ValueType_Keyword))
		return;

	if(mVarDictionary.find(name, { mScope->name, "global" }))
		throw runtime_error("Error: Re-declaration of variable '" + name + "'.");


	mVarDictionary.insert(mScope->name, name, {name, ""} );
}

void InstructionSet::declareFunction(Instruction* _ins)
{
	string name = _ins->getToken().getData();
	if (mFuncDictionary.find(name))
		throw runtime_error("Error: Re-declaration of Function '" + name + "'.");

	Scope* funcScope = new Scope(name);
	Scope* prevScope = mScope;

	mScope = funcScope;
	mScope->prev = prevScope;


	//Add it to the dictionary with the undefined state
	mFuncDictionary.insert("global", name, { name, false} );
}

void InstructionSet::defineVariable(Instruction* _ins)
{
	string name = _ins->getToken().getData();
	if(!mVarDictionary.find(name))
		throw runtime_error("Error: Definition/Assignment of unknown variable '" + name + "'.");
}

void InstructionSet::defineFunction(Instruction* _ins)
{
	string name = _ins->getToken().getData();

	Function* f = mFuncDictionary.find(name);
	if ( f && f->isDefined )
		throw runtime_error("Error: Re-definition of function '" + name + "'.");

	Scope* funcScope = new Scope(name);
	Scope* prevScope = mScope;

	mScope = funcScope;
	mScope->prev = prevScope;

	//Add it to the dictionary with the defined state
	mFuncDictionary.insert("global", name, { name, true });
}

bool InstructionSet::hasFunction(const std::string& _name)
{
	return !mFuncDictionary.find(_name);
}

bool InstructionSet::hasFunctionDefined(const std::string& _name)
{
	return mFuncDictionary.find(_name)->isDefined;
}

bool InstructionSet::hasVariable(const std::string& _name, std::initializer_list<std::string> _additionalScopes)
{
	return mVarDictionary.find(_name, { "global", mScope->name }) || mVarDictionary.find(_name, _additionalScopes);
}

bool InstructionSet::hasVariableDefined(const std::string& _name, std::initializer_list<std::string> _additionalScopes)
{
	Variable* var1 = mVarDictionary.find(_name, { "global", mScope->name });
	Variable* var2 = mVarDictionary.find(_name, _additionalScopes);

	return (var1 ? false : var1->isDefined) || (var2 ? false : var2->isDefined);
}

void InstructionSet::callFunction(Instruction* _ins)
{
	string name = _ins->getToken().getData();
	Function* f = mFuncDictionary.find(name);
	if (f)
	{
		if (!f->isDefined)
			throw runtime_error("Error: Function '" + name + "' is declared but not defined.");
	}
	else
	{
		throw runtime_error("Error: Function '" + name + "' has not been declared.");
	}
}

void InstructionSet::resolveArgs(Instruction* _ins)
{
	static bool inOut = false;

	Variable* var = mVarDictionary.find(_ins->mID.getData(), { mScope->name, "global" });
	if(!var && _ins->mID.is(ValueType_Unknown))
		throw runtime_error("Error: Variable '" + _ins->mID.getData() + "' was not declared in this scope.");

	if(_ins->mID.is(ValueType_Keyword))
	{
		inOut = _ins->mID.is("in");
		return;
	}

	if(mScope->name == "global" || !mInSignature)
		return;


	Function* f = mFuncDictionary.find(mScope->name);

	if(f->isDefined)
		f->signature.push_back(make_pair(inOut, var));
	else
		mSignatureTable[f->name].push_back(make_pair(inOut, var));


}

void InstructionSet::checkSignature(Instruction *_ins)
{
	Function* f = mFuncDictionary.find(mScope->name);
/*

	if(mScope->name != "global")
	{
		Scope* newScope = mScope->prev;
		delete mScope;
		mScope = newScope;
	}
*/

	if(!f)
		return; //Odd, the system should catch it later

	if(!f->isDefined) //This is the declaration
		return;


	//Check it's internal signature with the one provided at definition
	auto itr = mSignatureTable.find(f->name);
	if(itr == mSignatureTable.end()) //There was no previous declaration
		return;

	auto v = (*itr).second;
	//cout << v.size() << " " << f->signature.size() << "\n";
	if(v.size() != f->signature.size())
		throw runtime_error("Inconsistent Function signature for function '" + f->name + "'!");

	for(uint32_t i = 0; i < v.size(); ++i)
	{
		pair<bool, Variable*> p1 = v[i];
		pair<bool, Variable*> p2 = f->signature[i];

		if((p1.first != p2.first) || (p1.second->name != p2.second->name))
			throw runtime_error("Inconsistent Function signature for function '" + f->name + "'!");
	}

}
void InstructionSet::render()
{
	for (Instruction* in = mRoot; in != nullptr; in = in->mNext)
	{
		in->render();
	}
}

uint8_t* InstructionSet::getCompiledData()
{
	return mCompiledData;
}

uint32_t InstructionSet::getDataSize()
{
	return mDataSize;
}

void InstructionSet::finalize()
{
	assert(mDataSize != 0);

	mCompiledData = new uint8_t[mDataSize];
	uint32_t ptr = 0;
	for (Instruction* in = mRoot; in != nullptr; in = in->mNext)
	{
		string data = in->mID.getData();
		uint32_t dataLength = data.length() + 1;

		memcpy((void*) (mCompiledData + ptr), &dataLength, sizeof(uint32_t));
		ptr += sizeof(uint32_t);

		memcpy((void*) (mCompiledData + ptr), (char*) &in->mOpCode, sizeof(char));
		ptr += sizeof(char);

		memcpy((void*) (mCompiledData + ptr), data.c_str(), dataLength);

		ptr += dataLength;
	}

}
