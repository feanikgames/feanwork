/*
 * ScriptFunction.cpp
 *
 *  Created on: Mar 12, 2014
 *      Author: bryce
 */

#include "ScriptFunction.h"
#include "Logger.h"

using namespace infernix;
using namespace std;

Variable::Variable(const std::string& _name, const std::string& _value) :
		mName(_name), mValue(_value), mType(VariableType_Undefined)
{
	//First determine what type of variable we have, whether it's numerical or a string
	if (mValue.find_first_of("\"'") != string::npos)
		mType = VariableType_String;

	if (mValue.find_first_of("0123456789") != string::npos)
		mType = VariableType_Number;
}

Variable::~Variable()
{

}

void Variable::operator+=(const Variable& _other)
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
		mValue = to_string(stof(mValue) + stof(_other.mValue));

	if (mType == VariableType_String && _other.mType == VariableType_String)
		mValue = (mValue + _other.mValue);

}

void Variable::operator-=(const Variable& _other)
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return;

	mValue = to_string(stof(mValue) - stof(_other.mValue));
}

void Variable::operator/=(const Variable& _other)
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return;

	mValue = to_string(stof(mValue) / stof(_other.mValue));
}

void Variable::operator*=(const Variable& _other)
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return;

	mValue = to_string(stof(mValue) * stof(_other.mValue));
}

Variable Variable::operator+(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
		return Variable("", to_string(stof(mValue) + stof(_other.mValue)));

	if (mType == VariableType_String && _other.mType == VariableType_String)
		return Variable("", mValue + _other.mValue);

	return *this;
}

Variable Variable::operator-(const Variable& _other) const
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return *this;

	return Variable("", to_string(stof(mValue) - stof(_other.mValue)));
}

Variable Variable::operator/(const Variable& _other) const
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return *this;

	return Variable("", to_string(stof(mValue) / stof(_other.mValue)));
}

Variable Variable::operator*(const Variable& _other) const
{
	if (mType != VariableType_Number || _other.mType != VariableType_Number)
		return *this;

	return Variable("", to_string(stof(mValue) * stof(_other.mValue)));
}

void Variable::operator=(const Variable& _other)
{
	//Just copy the data
	//this->mName = _other.mName;
	this->mType = _other.mType;
	this->mValue = _other.mValue;
}

Variable Variable::operator==(const Variable& _other) const
{
	if(mType == _other.mType && mValue == _other.mValue)
		return Variable("", "true");

	return Variable("", "false");
}

Variable Variable::operator&&(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) && stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue == _other.mValue)
			return Variable("", "true");
		else if(mValue.empty() && _other.mValue.empty())
			return Variable("", "true");
	}

	return Variable("", "false");

}

Variable Variable::operator||(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) || stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue.empty() || _other.mValue.empty())
			return Variable("", "true");
	}

	return Variable("", "false");

}

Variable Variable::operator>(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) > stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue.c_str() > _other.mValue.c_str())
			return Variable("", "true");
	}

	return Variable("", "false");

}

Variable Variable::operator<(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) < stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue.c_str() < _other.mValue.c_str())
			return Variable("", "true");
	}

	return Variable("", "false");

}

Variable Variable::operator>=(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) >= stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue.c_str() >= _other.mValue.c_str())
			return Variable("", "true");
	}

	return Variable("", "false");

}

Variable Variable::operator<=(const Variable& _other) const
{
	if (mType == VariableType_Number && _other.mType == VariableType_Number)
	{
		if(stof(mValue) <= stof(_other.mValue))
			return Variable("", "true");
	}
	else
	{
		if(mValue.c_str() <= _other.mValue.c_str())
			return Variable("", "true");
	}

	return Variable("", "false");

}

void Variable::setValue(const std::string& _value)
{
	mValue = _value;
	if (mValue.find_first_of("\"'") != string::npos)
		mType = VariableType_String;

	if (mValue.find_first_of("0123456789") != string::npos)
		mType = VariableType_Number;
}

void Variable::assign(const Variable& _other)
{
	mName = _other.mName;
	mType = _other.mType;
	mValue = _other.mValue;
}

/////////////////////////////////////////////////////////////

ScriptFunction::ScriptFunction(const ScriptFunction& _func) : mFuncLookup(_func.mFuncLookup), mVarLookup(_func.mVarLookup)
{
	mName = _func.mName;
	mCurPointer = _func.mCurPointer;
	mNumParams = _func.mNumParams;
	mHasSignature = _func.mHasSignature;
}

ScriptFunction::ScriptFunction(const std::string& _name, FuncLookupTable& _funcLookup, VarLookupTable& _varLookup) :
		mName(_name), mCurPointer(0), mNumParams(0), mHasSignature(false),  mFuncLookup(_funcLookup),
		mVarLookup(_varLookup)
{
}

ScriptFunction::~ScriptFunction()
{
}

void ScriptFunction::define(Instruction* _i)
{
	static bool waitForInput = false;
	static bool waitForOutput = false;

	if (waitForInput)
	{
		mVarLookup.insert(mName+"_inputs",_i->target,Variable(_i->target,""));
		waitForInput = false;
	}


	if (waitForOutput)
	{
		mVarLookup.insert(mName+"_outputs",_i->target,Variable(_i->target,""));
		waitForOutput = false;
	}

	if (!_i->target.compare("in"))
	{
		waitForInput = true;
	}

	if (!_i->target.compare("out"))
	{
		waitForOutput = true;
	}

}

void ScriptFunction::finish()
{
	  mHasSignature = true;
	  mNumParams = mVarLookup.count_of({mName+"_inputs", mName+"_outputs"});
}

void ScriptFunction::add(Instruction* _i)
{
	mInstructions.push_back(_i);
}

void ScriptFunction::call()
{
	std::vector<Variable> out;
	out.reserve(mNumParams);

	if(mInstructions.empty())
		return;


	size_t numInputs = mVarLookup.count_of({mName+"_inputs", mName+"_outputs"});

	if(numInputs > mNumParams)
	{
		::logMsg(LogMsgType_Warning, "Function %s called with too many input parameters!"
				" Function not called.", mName.c_str() );
		return;
	}

	if(numInputs < mNumParams)
	{
		::logMsg(LogMsgType_Warning, "Function %s called with too few input parameters! "
				"Function not called", mName.c_str() );
		return;
	}

	for (Instruction* i = getNext(); hasInstructions(); i = getNext())
	{
		switch (i->opcode)
		{
			case DEFV:
				defineVariable(i);
				break;

			case DECV:
				declareVariable(i);
				break;

			case MUL:
			case DIV:
			case ADD:
			case SUB:
				simplifyArithmetic(i);
				break;

			case CHK:
				if(!branch(i))
				{
					//Skip over the conditional if false
					while(i->opcode!=END && hasInstructions())
						i = getNext();

				}
				break;

			case RTRN:
				return;

			case CALL:
				callFunction(i);
				break;


			default:
				break;

		}
	}


}

void ScriptFunction::assignVariable(Instruction* _ins)
{
	Instruction* left = getNext();
	Instruction* right = getNext();


	Variable* var = nullptr;

	var = mVarLookup.find(left->target, { mName+"_outputs", mName });

	if(!var)
	{
		::logMsg(LogMsgType_Error, "Failed to resolve variable '%s'!", left->target.c_str());
		return;
	}

	//Determine whether the right hand side is a variable or a constant
	Variable* assign = mVarLookup.find(right->target, {  mName+"_outputs", mName } );

	if (!assign)
		var->setValue(right->target);//Assume it's a constant value
	else
		*var = *assign;


}

void ScriptFunction::callFunction(Instruction* _ins)
{
	ScriptFunction* func = mFuncLookup.find(_ins->target);
	if(!func)
	{
		::logMsg(LogMsgType_Error, "Internal Error finding function '%s' ", _ins->target.c_str());
		return;
	}

	auto inVars = mVarLookup.getOrderedList({func->getName()+"_inputs"});
	for(auto in : inVars)
	{
		Instruction* i = getNext();

		if(i->opcode == ESIG)
			break;

		Variable* var = nullptr;

		var = mVarLookup.find(i->target, {mName, mName+"_inputs",mName+"_outputs", "global"});
		if(!var)
		{
			in->setValue(i->target);
			//mVarLookup.replace(func->getName()+"_inputs", (*itr).second->getName(), Variable("", i->target));
			continue;
		}
		*in = *var;
		//mVarLookup.replace(func->getName()+"_inputs", i->target, *var);
	}


	auto outVars = mVarLookup.getOrderedList({func->getName()+"_outputs"});
	std::vector<Variable*> outs;
	for(auto out : outVars)
	{
		Instruction* i = getNext();

		if(i->opcode == ESIG)
			break;

		Variable* var = nullptr;

		var = mVarLookup.find(i->target, {mName, mName+"_inputs",mName+"_outputs", "global"});
		if(!var)
		{
			//Can't pass lvalue as output
			::logMsg(LogMsgType_Error, "Cannot pass lvalue '%s' as output to function", i->target.c_str());
			//(*itr).second->setValue(i->target);
			continue;
		}

		*out = *var;
		outs.push_back(var);
	}


	func->call();


	uint32_t i = 0;
	for(auto out : outVars)
	{
		*outs[i] = *out;
	}
}

void ScriptFunction::simplifyArithmetic(Instruction* _i)
{
	getArithmeticResult(_i);

	/*for (Instruction* i = _i; hasInstructions(); i = getNext())
	{
		if(i->opcode == ASN)
			break;
	}*/
}

void ScriptFunction::getArithmeticResult(Instruction* _i)
{
	Variable temp;//TODO: Maybe make this a member var and init once
	Instruction* i = _i;
	while(hasInstructions())
	{

		if(i->opcode == STO)
		{
			mVarLookup.insert(mName+"_temp", i->target, temp);
			i = getNext();
			continue;
		}


		Instruction* left = getNext();
		Instruction* right = getNext();

		Variable* first_temp = nullptr;
		Variable* second_temp = nullptr;

		Variable* first = mVarLookup.find(left->target, { mName+"_temp", mName+"_inputs", mName+"_outputs", "global" });;
		Variable* second = mVarLookup.find(right->target, { mName+"_temp", mName+"_inputs", mName+"_outputs", "global" });

		if (!first)
		{
			first_temp = new Variable("", left->target);
			first = first_temp;
		}


		if (!second)
		{
			second_temp = new Variable("", right->target);
			second = second_temp;
		}

		switch ((ByteCode)i->opcode)
		{
			case MUL:
				temp = ((*first) * (*second));
				break;

			case DIV:
				temp = ((*first) / (*second));
				break;

			case SUB:
				temp = ((*first) - (*second));
				break;

			case ADD:
				temp = ((*first) + (*second));
				break;

			case ASN:
			{
				Variable* var = mVarLookup.find(left->target, { mName+"_outputs", mName });

				if(!var)
				{
					::logMsg(LogMsgType_Error, "Failed to resolve variable '%s'!", left->target.c_str());
					return;
				}

				//Determine whether the right hand side is a variable or a constant
				Variable* assign = mVarLookup.find(right->target, { mName+"_temp", mName+"_outputs", mName });

				if (!assign)
					var->setValue(right->target);//Assume it's a constant value
				else
					*var = *assign;

				break;
			}

			default:
				break;
		}


		if(i->opcode == ASN)
			break;

		//delete first_temp;
		//delete second_temp;

		i = getNext();
	}

	return;
}


bool ScriptFunction::branch(Instruction* _i)
{
	Variable temp;//TODO: Maybe make this a member var and init once
	Instruction* i = getNext();
	while(hasInstructions())
	{
		if(i->opcode == STO)
		{
			mVarLookup.insert(mName+"_temp", i->target, temp);
			i = getNext();
			continue;
		}

		if(i->opcode == DOIF)
		{
			if(temp.getValue() == "true")
				return true;
			else
				return false;
		}

		Instruction* left = getNext();
		Instruction* right = getNext();

		Variable* first_temp = nullptr;
		Variable* second_temp = nullptr;

		Variable* first = mVarLookup.find(left->target, { mName+"_temp", mName+"_inputs", mName+"_outputs", "global" });;
		Variable* second = mVarLookup.find(right->target, { mName+"_temp", mName+"_inputs", mName+"_outputs", "global" });

		if (!first)
		{
			first_temp = new Variable("", left->target);
			first = first_temp;
		}

		if (!second)
		{
			second_temp = new Variable("", right->target);
			second = second_temp;
		}

		switch(i->opcode)
		{
			case LTN:
				temp = ((*first) < (*second));
				break;

			case GTN:
				temp = ((*first) > (*second));
				break;

			case LTEQ:
				temp = ((*first) <= (*second));
				break;

			case GTEQ:
				temp = ((*first) >= (*second));
				break;

			case EQT:
				temp = ((*first) == (*second));
				break;

			case AND:
				temp = ((*first)) && ((*second));
				break;

			case OR:
				temp = ((*first)) || ((*second));
				break;

			default:
				break;
		}

		i = getNext();
	}

	return false;
}


void ScriptFunction::declareVariable(Instruction* _i)
{
	mVarLookup.insert( mName, _i->target, Variable(_i->target, "") );
}

void ScriptFunction::defineVariable(Instruction* _i)
{
	for (Instruction* i = _i->next; hasInstructions(); i = getNext())
	{
		if (i->opcode == EODF)
			break;

		switch (i->opcode)
		{
			case MUL:
			case DIV:
			case SUB:
			case ADD:
				getArithmeticResult(_i);
				break;

			case CALL:
				break;	//TODO:

			case ASN:
				assignVariable(_i);
				break;


			default:
				break;
		}
	}
}

