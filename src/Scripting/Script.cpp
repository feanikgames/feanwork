/*
 * Script.cpp
 *
 *  Created on: Mar 7, 2014
 *      Author: bryce
 */

#include "Script.h"
#include "Logger.h"

#include <string>

using namespace infernix;
using namespace std;

Script::Script() :
		Resource(ResourceType_Script), mCurrent(nullptr), mCurPointer(0)
{
}

Script::~Script()
{
}

int32_t Script::execute()
{
	ScriptFunction* function = mFuncLookup.find("__entry__");
	if(!function) return -1;

	for(Instruction* i = getNext(); hasInstructions(); i = getNext())
	{
		switch(i->opcode)
		{

			case DEFV:
				defineVariable(i);
				break;

			case ASN:
				assignVariable(i);
				break;

			default:
				break;
		}
	}

	function->call();

	return 0;
}

bool Script::make(ResourceFileParser& _data)
{
	if (_data.getFileHeader().numChunks > 1)
	{
		::logMsg(LogMsgType_Error, "Script file %s contains more than one chunk!",
				_data.getFilename());
		return false;
	}

	uint32_t i_count = 0;
	for (unsigned c = 0; c < _data.getFileHeader().numChunks; c++)
	{
		_data.readHeaderData(c, &i_count, sizeof(uint32_t));

		if(i_count == 0)
		{
			::logMsg(LogMsgType_Error, "No Instructions in Script file!", 0);
			return false;
		}

		bool inFunc = false;

		ScriptFunction *curFunction = nullptr;
		//Load program data
		for(uint32_t i = 0; i < i_count; ++i)
		{

			Instruction* in = new Instruction;

			uint32_t dataLength = 0;
			uint8_t opcode = 0;

			_data.readData(c, &dataLength, sizeof(uint32_t));

			char* data = new char[dataLength];

			_data.readData(c, &opcode, sizeof(char));
			_data.readData(c, data, dataLength);

			in->opcode = (ByteCode)opcode;
			in->target = string(data);
			in->prev =  nullptr;
			in->parent = nullptr;
			in->next = nullptr;

			delete[] data;

			if(inFunc)
			{
				static bool signature = false;
				if((ByteCode)in->opcode == ESIG)
				{
					signature = true;
					//continue;
				}

				if((ByteCode)in->opcode == EOFN)
				{
					mFuncLookup.insert("global", curFunction->getName(), curFunction);
					curFunction->finish();
					signature = false;
					inFunc = false;
					continue;
				}

				if(!signature)
					curFunction->define(in);
				else
					curFunction->add(in);

				continue;
			}

			switch((ByteCode)opcode)
			{

				case DECV:
					declareVariable(in);
					break;

				case DEFF:
				case DECF:
				{
					inFunc = true;
					curFunction = mFuncLookup.find(in->target);
					if(!curFunction)
						curFunction = new ScriptFunction(in->target, mFuncLookup, mVarLookup);

					break;
				}

				default:
					break;
			}

			mInstructions.push_back(in);
		}
	}

	return true;
}

void Script::declareVariable(Instruction* _ins)
{
	if(!mVarLookup.find(_ins->target))
		mVarLookup.insert("global", _ins->target, Variable(_ins->target, ""));
}

void Script::defineVariable(Instruction* _ins)
{
	declareVariable(_ins);

	for (Instruction* i = getNext(); hasInstructions(); i = getNext())
	{
		if (i->opcode == EODF)
			break;

		switch (i->opcode)
		{
			case MUL:
			case DIV:
			case SUB:
			case ADD:
				getArithmeticResult(i);
				break;

			case CALL:
				break;	//TODO:

			case ASN:
				assignVariable(i);
				break;


			default:
				break;
		}
	}
}


void Script::assignVariable(Instruction* _ins)
{
	Instruction* left = getNext();
	Instruction* right = getNext();

	Variable* var = mVarLookup.find(left->target);

	if(!var)
	{
		::logMsg(LogMsgType_Error, "Cannot locate variable %s!", left->target.c_str());
		return;
	}

	//Determine whether the right hand side is a variable or a constant
	Variable* assign = mVarLookup.find(right->target);
	if (!assign)
	{
		//Assume it's a constant value
		*var = Variable("", right->target);
	}
	else
	{
		*var = *assign;
	}

}

void Script::getArithmeticResult(Instruction* _i)
{
	Variable temp;
	for(Instruction *i = _i; hasInstructions(); i = getNext())
	{
		Instruction* left = getNext();
		Instruction* right = getNext();

		Variable* first = mVarLookup.find(left->target, { "global_temp", "global" });;
		Variable* second = mVarLookup.find(right->target,{ "global_temp", "global" });

		if (!first)
			first = new Variable("", left->target);

		if (!second)
			second = new Variable("", right->target);

		switch ((ByteCode)i->opcode)
		{
			case MUL:
				temp = ((*first) * (*second));
				break;

			case DIV:
				temp = ((*first) / (*second));
				break;

			case SUB:
				temp = ((*first) - (*second));
				break;

			case ADD:
				temp.assign(((*first) + (*second)));
				break;

			case STO:
				mVarLookup.insert("global_temp", i->target, Variable(temp));
				break;

			default:
				break;
		}
	}

	return;
}

