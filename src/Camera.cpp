#include "Feanwork/Camera.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Layer.h"

Camera::Camera() :
	IDMask()
{
	mDimensions.set(SceneManager::getInstance()->getWidth(), SceneManager::getInstance()->getHeight());
	mCamera 	 = new View(SceneManager::getInstance()->getScreen()->getDefaultView());
	mLinkedLayer = nullptr;
}

Camera::Camera(Layer* linkLayer) :
	IDMask()
{
	mDimensions.set(SceneManager::getInstance()->getWidth(), SceneManager::getInstance()->getHeight());
	mCamera		 = new View(SceneManager::getInstance()->getScreen()->getDefaultView());
	mLinkedLayer = linkLayer;
}

Camera::~Camera()
{
	mFollowing.clear();
	mLinkedLayer = nullptr;
	delete mCamera;
}

void Camera::update()
{
}

void Camera::tell(EventFlag flags, ListenerArgs* args)
{
	if(flags & EventFlag_Pos)
		translate(args->position.x, args->position.y);

	if(flags & EventFlag_KeyPress)
	{
		switch(args->keyCode)
		{
			case Keyboard::Up:
				zoom(.95f);
				break;

			case Keyboard::Down:
				zoom(1.05f);
				break;

			case Keyboard::Left:
				rotate( 1.0f);
				break;
			
			case Keyboard::Right:
				rotate(-1.f);
				break;
		}
	}
}

void Camera::stopFollowing(int index)
{
	mFollowing.erase(mFollowing.begin() + index);
}

void Camera::stopFollowing(Entity* entity)
{
	vector<Entity*>::iterator entityIt;
	for(entityIt = mFollowing.begin(); entityIt != mFollowing.end(); entityIt++)
	{
		if(*entityIt == entity)
		{
			mFollowing.erase(entityIt);
			break;
		}
	}
}

Vector2f Camera::getEdge(Edge edge, View* view)
{
	Vector2f _new;
	Vector2f centre = view->getCenter();

	switch(edge)
	{
		case Edge_Topleft:
			_new.x = centre.x - mDimensions.halfWidth;
			_new.y = centre.y - mDimensions.halfHeight;
			break;

		case Edge_Topright:
			_new.x = centre.x + mDimensions.halfWidth;
			_new.y = centre.y - mDimensions.halfHeight;
			break;

		case Edge_Bottomleft:
			_new.x = centre.x - mDimensions.halfWidth;
			_new.y = centre.y + mDimensions.halfHeight;
			break;

		case Edge_Bottomright:
			_new.x = centre.x + mDimensions.halfWidth;
			_new.y = centre.y + mDimensions.halfHeight;
			break;
	}
	return _new;
}

// TODO: maybe deprecate?
void Camera::queryLookAt(Entity* entity)
{

}

void Camera::setLinkedLayer(Layer* layer)
{
	mLinkedLayer = layer;
}

void Camera::toCamera(RenderTarget* target)
{
	if(target) target->setView(*mCamera);
	else	   SceneManager::getInstance()->getScreen()->setView(*mCamera);
}

void Camera::toDefault(RenderTarget* target)
{
	if(target) target->setView(SceneManager::getInstance()->getScreen()->getDefaultView());
	else	   SceneManager::getInstance()->getScreen()->setView(SceneManager::getInstance()->getScreen()->getDefaultView());
}

void Camera::zoom(float zoomFactor)
{
	mCamera->zoom(zoomFactor);
}

void Camera::rotate(float rotateFactor)
{
	mCamera->rotate(rotateFactor);
}

void Camera::lookAt(Vector2f position)
{
	translate(position.x, position.y);
}

void Camera::lookAt(float x, float y)
{
	translate(x, y);
}

void Camera::lookAt(Entity* entity)
{
	entity->addListener(this);
	mFollowing.push_back(entity);
	translate(entity->getPosition().x, entity->getPosition().y);
}

void Camera::shakeCamera(Vector2f amount, float duration)
{
	// TODO: shake camera (with an offset?) over a specified amount of time
}

Vector2f Camera::getCentre()
{
	return mCamera->getCenter();
}

View* Camera::getView()
{
	return mCamera;
}

Layer* Camera::getLinkedLayer()
{
	return mLinkedLayer;
}

void Camera::setID(int32_t id)
{
	IDMask::setMaskID(id);
}

int32_t Camera::getID()
{
	return IDMask::getMaskID();
}

void Camera::translate(float x, float y)
{
	// Store new position
	Vector2f newPosition = Vector2f(x, y);

	// Calculate midpoint between all followed entities
	if(mFollowing.size() > 1)
	{
		mMinimum	= mFollowing[0]->getPosition();
		mMaximum	= mFollowing[0]->getPosition();
		newPosition = mFollowing[0]->getPosition();

		for(auto follow: mFollowing)
		{
			Vector2f currentPos = follow->getPosition();
			newPosition		   += currentPos;

			mMinimum.x = min(currentPos.x, mMinimum.x);
			mMinimum.y = min(currentPos.y, mMinimum.y);
			mMaximum.x = max(currentPos.x, mMaximum.x);
			mMaximum.y = max(currentPos.y, mMaximum.y);
		}
		newPosition /= (float)mFollowing.size();

		Vector2f viewSize = mCamera->getSize();
		Vector2f diff	  = (mMaximum - mMinimum);

		diff.x = fabs(diff.x) + 100.f;
		diff.y = fabs(diff.y) + 100.f;

		float factorX = diff.x / viewSize.x;
		float factorY = diff.y / viewSize.y;
		float factor  = max(factorX, factorY);

		zoom(factor);
	}

	// Update views
	mCamera->setCenter(newPosition);
	vector<Layer*> layers = SceneManager::getInstance()->getLayers();

	for(auto layer: SceneManager::getInstance()->getLayers())
		layer->setPosition(newPosition * layer->getCoef());
}

bool Camera::isInside()
{
	Vector2f size   = mCamera->getSize();
	Vector2f origin = mCamera->getCenter();

	if(mMinimum.x < (origin.x - size.x / 2.0f)) return false;
	if(mMinimum.y > (origin.x + size.x / 2.0f)) return false;

	if(mMaximum.x < (origin.y - size.y / 2.0f)) return false;
	if(mMaximum.y > (origin.y + size.y / 2.0f)) return false;
												return true;
}
