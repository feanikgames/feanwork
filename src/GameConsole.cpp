#include "Feanwork/GameConsole.h"
#include "Feanwork/Camera.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"

GameConsole* GameConsole::getInstance(){
	static GameConsole gameConsole;
	return &gameConsole;
}

/// <summary>
/// Initializes the console (should be done before the game starts if required)
/// </summary>
void GameConsole::init(Vector2f position)
{
	mLineSize     = 0;
	mLinePosition = 0;
	mPosition	  = position;
	mDefaultFont  = ResourceCache::getInstance()->retrieveFont("SurfsUp");
}

void GameConsole::update()
{
    int32_t count = 0;
    for (int32_t i = mLinePosition; i < mLines.size(); i++)
    {
        count++;
		mLines[i]->setPosition(Vector2f(mPosition.x, mPosition.y + (mLines[i]->getLocalBounds().height * count) + (count * 5.f)));
    }
}

void GameConsole::render(RenderTarget* target)
{
	for (int32_t i = mLinePosition; i < mLines.size(); i++)
		target->draw(*mLines[i]);
}

void GameConsole::destroy()
{
	for(auto line: mLines)
		delete line;

	mDefaultFont = nullptr;
	mLines.clear();
}

/// <summary>
/// Logs a string to the in-game console
/// </summary>
void GameConsole::log(const char* formatStr, ...)
{
	char buffer[1024] = "";

	va_list args;
	va_start(args, formatStr);
	vsprintf_s(buffer, formatStr, args);
	va_end(args);

	string tmp= to_string(mLineSize + 1);
	tmp += ") ";
	tmp += buffer;

	Text* newText = new Text(tmp, *mDefaultFont, 14);
	newText->setPosition(mPosition);
	newText->setColor(Colour(38, 145, 202, 255));
	mLines.push_back(newText);

    mLineSize++;
    cout << tmp << endl;

    mLinePosition = max((int32_t)mLines.size() - 8, 0);
    int32_t count = 0;

    for (int32_t i = mLinePosition; i < mLines.size(); i++)
    {
        count++;
		mLines[i]->setPosition(Vector2f(mPosition.x, mPosition.y + (mLines[i]->getLocalBounds().height * (count + 1))));
    }
}
