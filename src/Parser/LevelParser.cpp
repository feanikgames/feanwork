#include "Feanwork/Parser/LevelParser.h"
#include "Feanwork/GameConsole.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Layer.h"
#include "Feanwork/Camera.h"

LevelParser::LevelParser(string fileName, LevelData* data) :
	Parser(),
	mLevel(data)
{
	setFileName(fileName);
	_setExtension("level");
}

void LevelParser::initLevel(Scene* scene)
{
	// Copy over layers
	for(auto layer: mLevel->layers)
	{
		Layer* layerPtr = scene->addLayer(1.0f);
		for(auto entity: layer->getEntities())
			layerPtr->insert(entity);
	}

	// Copy over Cameras
	for(auto camera: mLevel->cameras)
		scene->addCamera(camera->getLinkedLayer());
}

LevelData* LevelParser::getLevel()
{
	return mLevel;
}

bool LevelParser::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		//stream.write((char*)0, sizeof(int32_t));

		/*int32_t entityCount = ;
		stream.write((char*)&entityCount, sizeof(int32_t));

		for(auto entity: entities)
			entity->parse(stream, ParseOut);*/
	}
	else if(mode == ParseIn)
	{

	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse a prefab, failed due to incorrect passing of parameters");
	return false;
}




/*bool LevelParser::write(vector<Entity*>& entities)
{
	fstream stream(mFile, ios_base::binary | ios_base::out);
	if(!stream.is_open())
		return false;

	return true;
}

bool LevelParser::read(vector<Entity*>& entities)
{
	fstream stream(mFile, ios_base::binary | ios_base::in);
	if(!stream.is_open())
		return false;
	
	int32_t entityCount = 0;
	stream.read((char*)&entityCount, sizeof(int32_t));

	if(entities.size() > 0)
	{
		for(auto ent: entities)
			delete ent;

		entities.clear();
	}

	for(int32_t i = 0; i < entityCount; i++)
	{
		Entity* ent = new Entity();
		ent->parse(stream, ParseIn);
		entities.push_back(ent);
	}
	return true;
}*/
