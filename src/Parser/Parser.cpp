#include "Feanwork/Parser/Parser.h"
#include "Feanwork/GameConsole.h"
#include "Feanwork/ResourceCache.h"

#include <fstream>

Parser::Parser() :
	mFileName(""),
	mExtension(""),
	mDirectory(ResourceCache::getInstance()->getDirectories()[0]),
	mTotalBytesParsed(0),
	mTotalBytes(0)
{
}

bool Parser::save(string file)
{
	// If file is not an empty string, assign it
	if(file != "")
		setFileName(file);

	// Make sure parser actually contains a File Name
	if(mFileName == "")
	{
		GameConsole::getInstance()->log("Warning: Tried to parse file without name, function 'save()' returned false");
		return false;
	}

	// Open up a new stream to the file which can be written to
	fstream stream;
	stream.open(getFullPath(), ios_base::binary | ios_base::out);

	// Make sure stream was opened successfully
	if(!stream.is_open())
	{
		GameConsole::getInstance()->log("Warning: Failed to open file %s in function 'save()', this could be due to an incorrect file name", mFileName.c_str());
		return false;
	}

	// Once file is open call overridden parse function
	parse(stream, ParseOut);
	
	// If file was succesfully opened close it safely
	if(stream.is_open())
		stream.close();

	return true;
}

bool Parser::load(string file)
{
	// If file is not an empty string, assign it
	if(file != "")
		setFileName(file);

	// Make sure parser actually contains a File Name
	if(mFileName == "")
	{
		GameConsole::getInstance()->log("Warning: Tried to parse file without name, function 'load()' returned false");
		return false;
	}

	// Open up a new stream to the file which can be read from
	fstream stream;
	stream.open(getFullPath(), ios_base::binary | ios_base::in);

	// Make sure stream was opened successfully
	if(!stream.is_open())
	{
		GameConsole::getInstance()->log("Warning: Failed to open file %s in function 'load()', this could be due to an incorrect file name", mFileName.c_str());
		return false;
	}

	// Once file is open call overridden parse function
	parse(stream, ParseIn);
	
	// If file was succesfully opened close it safely
	if(stream.is_open())
		stream.close();

	return true;
}

void Parser::setFileName(string fileName)
{
	GameConsole::getInstance()->log("Info: Parser changed file name from %s to %s", mFileName.c_str(), fileName.c_str());
	mFileName = fileName;
}

void Parser::setDirectory(string directory)
{
	GameConsole::getInstance()->log("Info: Parser changed directory path from %s to %s", mDirectory.c_str(), directory.c_str());
	mDirectory = directory;
}

string Parser::getFileName()
{
	return mFileName;
}

string Parser::getExtension()
{
	return mExtension;
}

string Parser::getDirectory()
{
	return mDirectory;
}

string Parser::getFullFile()
{
	return (mFileName + "." + mExtension);
}

string Parser::getFullPath()
{
	return (getDirectory() + "/" + getFullFile());
}

int32_t Parser::getTotalBytes()
{
	return mTotalBytes;
}

int32_t Parser::getTotalBytesParsed()
{
	return mTotalBytesParsed;
}

void Parser::_setExtension(string extension)
{
	GameConsole::getInstance()->log("Info: Parser changed file extension from %s to %s", mExtension.c_str(), extension.c_str());
	mExtension = extension;
}
