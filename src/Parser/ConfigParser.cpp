#include "Feanwork/Parser/ConfigParser.h"
#include "Feanwork/GameConsole.h"

ConfigParser::ConfigParser(string fileName, ConfigData* cfgData) :
	Parser(),
	mDataSet(cfgData)
{
	setFileName(fileName);
	_setExtension("cfg");
}

void ConfigParser::insert(string varName, string value)
{
	// Check if variable already exists, if it does add it to the data set
	if(mDataSet->find(varName) == mDataSet->end())
	{
	    (*mDataSet)[varName] = value;
		return;
	}

	// If the program reaches this point the variable already exists
	GameConsole::getInstance()->log("Warning: Attempted to add variable %s with value of %s, however it already exists", varName, value);
}

bool ConfigParser::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		// Create an iterator to go through data
		ConfigData::iterator it = mDataSet->begin();

		// Write each string into the file
		for(; it != mDataSet->end(); it++) //TODO: stop ignoring sections
			stream << it->first << " = " << it->second << "\n";

		return true;
	}
	else if(mode == ParseIn)
	{
		// TODO: Clear mDataSet before reading in


		string line = "";
		while(!stream.eof())
		{
			// Read next line into stream and trim it
			getline(stream, line);
			_trimline(line);

			// If the line is empty, is commented or is a section move onto next string
			if(line.empty() || line[0] == ';') continue;
			if(line[0] == '[')				   continue; //TODO: stop ignoring sections

			// find position, name and value of variable
			int32_t position = line.find_first_of("=");
			string key		 = line.substr(0, position);
			string value	 = line.substr(position + 1);

			// Insert the result into the data set
			mDataSet->insert(make_pair(key, value));
		}

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse a prefab, failed due to incorrect passing of parameters");
	return false;
}

string ConfigParser::getRawData(string varName)
{
	// If the data can be found return it
	if((*mDataSet)[varName] != "")
		return (*mDataSet)[varName];

	// If the program reaches this point a variable was not found
	GameConsole::getInstance()->log("Warning: Could not retrieve raw data from dataset with name %s", varName);
	return "";
}

int32_t ConfigParser::getAsInt(string varName)
{
	// If the data can be found return it as an int32_t
	if((*mDataSet)[varName] != "")
		return stoi((*mDataSet)[varName]);

	// If the program reaches this point a variable was not found
	GameConsole::getInstance()->log("Warning: Could not retrieve data as integer from dataset with name %s", varName);
	return 0;
}

float ConfigParser::getAsFloat(string varName)
{
	// If the data can be found return it as an float
	if((*mDataSet)[varName] != "")
		return stof((*mDataSet)[varName]);

	// If the program reaches this point a variable was not found
	GameConsole::getInstance()->log("Warning: Could not retrieve data as float from dataset with name %s", varName);
	return .0f;
}

bool ConfigParser::getAsBool(string varName)
{
	// If the data can be found return it as an bool
	if((*mDataSet)[varName] != "")
		return stoi((*mDataSet)[varName]);

	// If the program reaches this point a variable was not found
	GameConsole::getInstance()->log("Warning: Could not retrieve data as bool from dataset with name %s", varName);
	return false;
}

void ConfigParser::_trimline(string& line)
{
	//TODO: don't remove special chars from strings!
	// Define characters to be erased from string
	const string wschars = " \t\n\r";
	string::iterator it  = line.begin();

	// Go through each 'char', if it matches our special case remove it
	for(; it != line.end(); it++)
	{
		if(wschars.find(*it) != string::npos)
		{
			it = line.erase(it);
			if(it == line.end())
				break;
		}
	}
}
