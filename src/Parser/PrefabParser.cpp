#include "Feanwork/Parser/PrefabParser.h"
#include "Feanwork/Entity.h"
#include "Feanwork/GameConsole.h"

#include "Feanwork/Module/Module.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Module/EmitterModule.h"
#include "Feanwork/Module/IntelligenceModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/SoundModule.h"

PrefabParser::PrefabParser(string fileName, Prefab* prefab) :
	Parser(),
	mPrefab(prefab)
{
	setFileName(fileName);
	_setExtension("prefab");
}

bool PrefabParser::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		// Store module count and origin
		int32_t moduleCount = mPrefab->modules.size();
		uint32_t customType = mPrefab->customType;
		Vector2f origin     = mPrefab->origin;

		// Write data to file
		stream.write((char*)&moduleCount, sizeof(int32_t));
		stream.write((char*)&customType,  sizeof(uint32_t));
		stream.write((char*)&origin.x,	  sizeof(float));
		stream.write((char*)&origin.y,	  sizeof(float));

		// Write modules to file
		for(auto module: mPrefab->modules)
		{
			ModuleType type = module->getType();
			stream.write((char*)&type, sizeof(int32_t));
			module->parse(stream, mode);
		}

		return true;
	}
	else if(mode == ParseIn)
	{
		// Remove any modules that already exist in the prefab
		if(mPrefab->modules.size() > 0)
		{
			for (auto module : mPrefab->modules)
				delete module;

			mPrefab->origin = Vector2f();
			mPrefab->modules.clear();
		}

		// Store module count
		int32_t moduleCount = 0;

		// Read in prefab data
		stream.read((char*)&moduleCount,	     sizeof(int32_t));
		stream.read((char*)&mPrefab->customType, sizeof(uint32_t));
		stream.read((char*)&mPrefab->origin.x,   sizeof(float));
		stream.read((char*)&mPrefab->origin.y,   sizeof(float));

		// Destroy the modules and then retrieve corresponding module
		for(int32_t i = 0; i < moduleCount; i++)
		{
			ModuleType type;
			stream.read((char*)&type, sizeof(int32_t));

			if(type == ModuleType_Render)
			{
				RenderModule* module = new RenderModule();
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
			else if(type == ModuleType_Animation)
			{
				AnimationModule* module = new AnimationModule();
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
			else if (type == ModuleType_Intel)
			{
				IntelligenceModule* module = new IntelligenceModule();
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
			else if (type == ModuleType_Physics)
			{
				PhysicsModule* module = new PhysicsModule();
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
			else if (type == ModuleType_Emitter)
			{
				EmitterModule* module = new EmitterModule();
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
			else if (type == ModuleType_Sound)
			{
				SoundModule* module = new SoundModule(true);
				module->parse(stream, ParseIn);
				mPrefab->modules.push_back(module);
			}
		}
		
		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse a prefab, failed due to incorrect passing of parameters");
	return false;
}
