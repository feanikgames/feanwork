#include "Feanwork/Parser/AnimationParser.h"
#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/GameConsole.h"
#include <fstream>

AnimationParser::AnimationParser(string fileName, AnimationSet* anims) :
	Parser(),
	mAnimations(anims)
{
	setFileName(fileName);
	_setExtension("anim");
}

bool AnimationParser::parse(fstream& stream, ParseMode mode)
{
	if(mode == ParseOut)
	{
		// Write number of animations to file
		int32_t animCount = mAnimations->size();
		stream.write((char*)&animCount, sizeof(int32_t));

		// Go through each animation writing it to file
		for(auto anim: *mAnimations)
		{
			// Find animation specific variables
			int32_t fCount	 = anim->frameCount();
			int16_t	animMode = anim->animationMode();
			float	speed	 = anim->getSpeed();

			// Write animation variables to file
			stream.write((char*)&fCount,   sizeof(int32_t));
			stream.write((char*)&animMode, sizeof(int16_t));
			stream.write((char*)&speed,	   sizeof(float));

			// Write out name
			int32_t nameLen = anim->getName().size() + 1;
			stream.write((char*)&nameLen,		  sizeof(int32_t));
			stream.write(anim->getName().c_str(), nameLen);

			// Loop through and write frames
			for(auto frame: anim->getFrames())
			{
				stream.write((char*)&frame->position.x, sizeof(float));
				stream.write((char*)&frame->position.y, sizeof(float));
				stream.write((char*)&frame->width,		sizeof(uint32_t));
				stream.write((char*)&frame->height,		sizeof(uint32_t));
			}
		}

		return true;
	}
	else if(mode == ParseIn)
	{
		// Define temporary variables
		int32_t			   animCount = 0;
		stream.read((char*)animCount, sizeof(int32_t));

		for(int32_t l = 0; l < animCount; l++)
		{
			// Animation Variable Decl
			int32_t fCount	 = 0;
			int16_t	animMode = 0;
			float	speed	 = 0;
			string  name	 = "";

			// Read animation variables
			stream.read((char*)&fCount,   sizeof(int32_t));
			stream.read((char*)&animMode, sizeof(int16_t));
			stream.read((char*)&speed,	  sizeof(float));

			// Read in name
			int32_t nameLen = 0;
			stream.read((char*)&nameLen, sizeof(int32_t));
			stream.read((char*)&name,    nameLen);

			// Loop through and create frames again
			Animation* newAnim = new Animation(name, speed, (AnimationMode)animMode);
			for(int32_t i = 0; i < fCount; i++)
			{
				Vector2f pos;
				uint32_t width;
				uint32_t height;

				stream.read((char*)&pos.x,  sizeof(float));
				stream.read((char*)&pos.y,  sizeof(float));
				stream.read((char*)&width,  sizeof(uint32_t));
				stream.read((char*)&height,	sizeof(uint32_t));

				Frame* newFrame = new Frame();
				newFrame->position = pos;
				newFrame->width	   = width;
				newFrame->height   = height;
				newAnim->addFrame(newFrame);
			}
			mAnimations->push_back(newAnim);
		}

		return true;
	}

	// If the program reaches this point there is something seriously wrong
	GameConsole::getInstance()->log("Warning: Attempted to parse a prefab, failed due to incorrect passing of parameters");
	return false;
}
