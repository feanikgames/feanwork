#include "Feanwork/Util/FeanRandom.h"

using namespace FeanRandom;

int FeanRandom::randRange(int min, int max)
{
	return ((max - min) * (rand() / RAND_MAX)) + min;
}

float FeanRandom::randRangeF(float min, float max)
{
	return ((max - min) * ((float)rand() / RAND_MAX)) + min;
}

double FeanRandom::randRangeD(double min, double max)
{
	return ((max - min) * ((double)rand() / RAND_MAX)) + min;
}

/// <summary>
/// Returns a random vectorial normal
/// </summary>
Vector2f FeanRandom::randomUnitVector()
{
	float x		 = randRangeF(-1.f, 1.f);
	float y		 = randRangeF(-1.f, 1.f);
	float length = sqrt((x * x) + (y * y));

	if(length != 0)
	{
		x /= length;
		y /= length;
	}
	return Vector2f(x, y);
}
