#include "Feanwork/Util/Interval.h"

Interval::Interval(IntervalFunc func, float time, void* dataPtr)
{
	mIntervalFunc  = func;
	mIntervalState = IntervalState_Active;

	mTotalT   = time;
	mDataPtr  = dataPtr;
	mElapsedT = 0.f;
}

Interval::Interval(const Interval& interval)
{
	mIntervalFunc  = interval.mIntervalFunc;
	mIntervalState = interval.mIntervalState;
	mSubscribers   = interval.mSubscribers;

	mElapsedT = interval.mElapsedT;
	mTotalT   = interval.mTotalT;
	mDataPtr  = interval.mDataPtr;
}

Interval::~Interval()
{
	if(mDataPtr)
		mDataPtr = nullptr;
}

bool Interval::update(float deltaTime)
{
	// Check whether the function pointer exists and if this interval is active
	if(mIntervalFunc && (mIntervalState == IntervalState_Active))
	{
		mElapsedT += deltaTime;
		mIntervalFunc(mDataPtr, min(mElapsedT / mTotalT, 1.0f));

		// If we can still update return true
		if(mElapsedT < mTotalT)
			return true; 
	}
	else if(mIntervalState == IntervalState_Paused)
		return true;

	// Play all dependant subscribers
	for(auto subscriber: mSubscribers)
			subscriber->play();

	// Change state, clear subsciber list and return false
	mIntervalState = IntervalState_Finished;
	mSubscribers.clear();
	return false;
}

bool Interval::funcExists()
{
	return mIntervalFunc ? true : false;
}

void Interval::subscribe(Interval* interval)
{
	interval->pause();
	mSubscribers.push_back(interval);
}

IntervalState Interval::state()
{
	return mIntervalState;
}

void Interval::pause()
{
	mIntervalState = IntervalState_Paused;
}

void Interval::play()
{
	mIntervalState = IntervalState_Active;
}
