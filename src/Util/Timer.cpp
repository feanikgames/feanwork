#include "Feanwork/Util/Timer.h"

#include <algorithm>
#include <cassert>

Timer::Timer()
{
	restart();
}

Timer::~Timer()
{
}

void Timer::restart()
{
	mLastTime = chrono::steady_clock::now();
}

float Timer::getElapsedTimeMs() const
{
	chrono::milliseconds dur = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - mLastTime);
	return dur.count();
}

float Timer::getElapsedTimeS() const
{
	chrono::seconds dur = chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - mLastTime);
	return dur.count();
}
