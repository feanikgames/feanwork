#ifdef _DEBUG

#include "Feanwork/Util/MemoryTracker.h"

#include <fstream>
#include <string>
#include <cstring>
using namespace std;

void MemoryTracker::track(const void* _addr, const char* _file, uint16_t _line, uint32_t _size)
{
	Trackee t;
	strncpy_s(t.file, _file, 128);
	t.addr = _addr;
	t.line = _line;
	t.size = _size;
	mTrackees.push_back(t);
}

void MemoryTracker::remove(const void* _addr)
{
	bool found = false;
	for(unsigned i = 0; i < mTrackees.size(); i++)
		if(mTrackees[i].addr == _addr)
		{
			found = true;
			mTrackees.erase(mTrackees.begin() + i);
			break;
		}
}

void MemoryTracker::compileReport()
{
	const vector<MemoryTracker::Trackee>& leaks = MemoryTracker::getSingleton()->getTrackees();
	ofstream file("MemoryLeaks.html");

	file << "<html>" << "\n";
	file << "<head>" << "\n";
	file << "	<style>" << "\n";
	file << "		td { padding-left:10px; padding-right:10px; }" << "\n";
	file << "	</style>" <<"\n";
	file << "</head>" << "\n" << "\n";

	file << "<body bgcolor='292929' text='FFFFFF'>" << "\n";
	file << "<center><img src='../Resources/logo252White.png' width='128'></center>" << "\n";
	file << "<center><h2 text='black'>Memory leak report</h2></center>" << "\n";
	file << "<center>" << "\n" << "<table>" << "\n";
	uint32_t total = 0;

	file << "	<tr>" << "\n";
	file << "		<td bgcolor='#FA6121'><center><b>File</b></center></td>" << "\n";
	file << "		<td bgcolor='#FFB739'><center><b>Line</b></center></td>" << "\n";
	file << "		<td bgcolor='#FA6121'><center><b>Size</b></center></td>" << "\n";
	file << "	</tr>" << "\n";

	for(auto& l: leaks)
	{
		file << "	<tr>" << "\n";
		file << "		<td bgcolor='#FA6121'>"			  << l.file << "</td>"				   << "\n";
		file << "		<td bgcolor='#FFB739'><center>"   << l.line << "</center></td>"		   << "\n";
		file << "		<td bgcolor='#FA6121'><center> (" << l.size << " bytes)</center></td>" << "\n";
		file << "	</tr>" << "\n";
		total += l.size;
	}

	file << "</table>" << "\n" << "</center>" << "\n";
	file << "<center><h3><b>Total size - " << (float)total/1024.0f << "kb</b></h3></center>" << "\n" << "\n";

	file << "</body>" << "\n";
	file << "</html>" << "\n";
	file.close();
}

#endif //_DEBUG
