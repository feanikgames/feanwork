#include "Feanwork/Util/Vectorial.h"
using namespace Vectorial;

float Vectorial::vectorLengthF(Vector2f vec)
{
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

float Vectorial::vectorLengthFSq(Vector2f vec)
{
	return (vec.x * vec.x + vec.y * vec.y);
}

float Vectorial::vectorLengthI(Vector2i vec)
{
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

float Vectorial::vectorDotprod(Vector2f a, Vector2f b)
{
	return (a.x * b.x + a.y * b.y);
}

Vector2f Vectorial::vectorNormal(Vector2f vec)
{
	Vector2f vector;
	float    length = vectorLengthF(vec);

	if(length != .0f)
	{
		vector.x = vec.x / length;
		vector.y = vec.y / length;
	}
	return vector;
}

Vector2f Vectorial::vectorHadamard(Vector2f a, Vector2f b)
{
	return Vector2f(a.x * b.x, a.y * b.y);
}

Vector2f Vectorial::vectorInvert(Vector2f vec)
{
	return Vector2f(vec.y, vec.x);
}

void Vectorial::vectorZero(Vector2f& vec)
{
	vec.x = .0f;
	vec.y = .0f;
}

Vector2f Vectorial::vectorFabs(Vector2f vec)
{
	Vector2f result;
	result.x = fabs(vec.x);
	result.y = fabs(vec.y);
	return result;
}

Vector2i Vectorial::vectorAbs(Vector2i vec)
{
	Vector2i result;
	result.x = abs(vec.x);
	result.y = abs(vec.y);
	return result;
}

float Vectorial::manhattanDistance(Vector2f a, Vector2f b)
{
	float   diffX = abs(a.x - b.x);
	float   diffY = abs(a.y - b.y);
	return (diffX + diffY);
}
