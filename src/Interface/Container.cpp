#include "Feanwork/Interface/Container.h"
#include "Feanwork/Interface/ControlBase.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/Camera.h"

Container::Container()
{
	mAbsPosition = Vector2f(.0f, .0f);
}

Container::Container(Vector2f position, int width, int height)
{
	mAbsPosition = position;
	mDimensions.set(width, height);

	mDebugShape.setSize(Vector2f(width, height));
	mDebugShape.setPosition(mAbsPosition.x - mDimensions.halfWidth, mAbsPosition.y - mDimensions.halfHeight);
	mDebugShape.setFillColor(Colour(0, 255, 0, 100));
}

Container::~Container()
{
	for(auto control: mControls)
		delete control;

	mControls.clear();
}

void Container::update(UpdateStage stage)
{
	// Update Controls
	for(auto control: mControls)
		control->update(stage);
}

void Container::render(RenderTarget* target)
{
	//SceneManager::getInstance()->toDefault(); // TODO: Low-Level call to camera swap
	for(auto control: mControls)
		control->render(target);

	//target->draw(mDebugShape);
}

bool Container::intersects(Vector2i mousePosition)
{
	Vector2f dist = (mAbsPosition - Vector2f(mousePosition.x, mousePosition.y));
	if(fabs(dist.x) > mDimensions.halfWidth)  return false;
	if(fabs(dist.y) > mDimensions.halfHeight) return false;
											  return true;
}

void Container::setAbsPosition(Vector2f position)
{
	mAbsPosition = position;
}

Vector2f Container::getAbsPosition()
{
	return mAbsPosition;
}

void Container::insert(ControlBase* control)
{
	mControls.push_back(control);
}

// TODO:
void Container::fitControls()
{
}

ControlBase* Container::getControl(int index)
{
	return mControls.at(index);
}

vector<ControlBase*> Container::getControls()
{
	return mControls;
}

Dimension Container::getDimensions()
{
	return mDimensions;
}
