#include "Feanwork/Interface/DropControl.h"
#include "Feanwork/Module/RenderModule.h"

DropControl::DropControl(Vector2f relativePos, string fieldImg, string maskImg, string selectionImg, string dropImg, Container* parent) :
	ControlBase(relativePos, selectionImg, parent, ControlType_Drop)
{
	mCurrentSelection = 0;
	mLastSelection    = 0;
	mListActive		  = false;
	mLargestWidth	  = .0f;

	mDropTex     = ResourceCache::getInstance()->retrieveTexture(dropImg);
	mFieldTex    = ResourceCache::getInstance()->retrieveTexture(fieldImg);
	mMaskTex     = ResourceCache::getInstance()->retrieveTexture(maskImg);
	mDropSprite  = new Sprite(*mDropTex);
	mCurrentText = new Text("", *ResourceCache::getInstance()->retrieveFont("OpenSans"), 12);

	mCurrentText->setPosition(getPosition(Edge_Topleft));
	mCurrentText->setColor(Colour::Black);
	mDropSprite->setPosition(getPosition(Edge_Topleft).x + (mFieldTex->getSize().x - mDropSprite->getTexture()->getSize().x), getPosition(Edge_Topleft).y);

	float diff = getPosition().y - (mCurrentText->getPosition().y + (mCurrentText->getLocalBounds().height / 2.0f));
	mCurrentText->move(5.f, 3.f);

	onMouseMoved.push_back(bind(&DropControl::moving, this, placeholders::_1));
	onPressed.push_back(bind(&DropControl::pressed, this, placeholders::_1));
	onMouseExit.push_back(bind(&DropControl::hide, this, placeholders::_1));
}

DropControl::~DropControl()
{
	for(auto field: mFields)
		delete field;

	delete mCurrentText;
	delete mDropSprite;
	mFields.clear();
}

bool DropControl::render(RenderTarget* target)
{
	bool rendered = Entity::render(target);
	if(mDropSprite)  target->draw(*mDropSprite);
	if(mCurrentText) target->draw(*mCurrentText);

	if(mListActive)
	{
		for(auto field: mFields)
			field->render(target);
	}

	return rendered;
}

/// <summary>
/// Adds a new field to the DropControl with specified text and size
/// </summary>
void DropControl::addField(string text, uint32_t size)
{
	RenderModule* module   = Entity::getRenderMod();
	Vector2f	  position = getPosition(Edge_Topleft);
	DropField*	  newField = new DropField(position, mFieldTex, mMaskTex, text, size);
	mLargestWidth		   = max(newField->getTextWidth(), mLargestWidth);

	// Calculate factors for DropField (effects scale & rendering)
	float factorX = (mLargestWidth + 8.f) / newField->getGfxSize().x;
	newField->setWidthFactor(factorX);

	// Set position of DropField based on amount
	float    sprHeight	 = newField->getField()->getTexture()->getSize().y;
	Vector2f newPosition = Vector2f(position.x, position.y + module->getTexture()->getSize().y + (sprHeight * mFields.size()));
	newField->setPosition(newPosition);
	newField->getText()->move(5.f, 0.f);

	// Push back the new field
	mFields.push_back(newField);
	for(auto field: mFields)
		field->scaleX(factorX);
}

/// <summary>
/// Sets the current selection of the DropControl and changes the last one
/// </summary>
void DropControl::setSelection(uint32_t selection)
{
	if(mFields.at(selection))
	{
		mLastSelection    = mCurrentSelection;
		mCurrentSelection = selection;
		mCurrentText->setString(mFields.at(selection)->getText()->getString());

		// Calculate the number of letters that can fit in the selection box
		int32_t size      = 0;
		int32_t sizeCount = 0;
		int32_t compare   = mFieldTex->getSize().x;

		for(auto c: mCurrentText->getString())
		{
			Glyph g = ResourceCache::getInstance()->retrieveFont("OpenSans")->getGlyph(c, 14, false);
			if(size + g.bounds.width < compare)
			{
				size += g.bounds.width;
				sizeCount++;
			}
		}

		// Cut down the string to the correct size and assign it
		if(sizeCount < mCurrentText->getString().getSize())
		{
			sf::String text = mCurrentText->getString();
			text.erase(sizeCount + 1, String::InvalidPos);
			mCurrentText->setString(text);
		}
	}
}

// Callbacks
void DropControl::pressed(MouseButtonEventArgs args)
{
	Vector2i mouse = Vector2i(args.x, args.y);
	if(mListActive)
	{
		for(uint32_t i = 0; i < mFields.size(); i++)
		{
			if(!quickIntersect(mouse, mFields[i]->getFieldPosition(), mFields[i]->getField()->getTexture()->getSize()))
				continue;

			// In the event of an intersection set the selection and hide the drop menu
			setSelection(i);
			hide(MouseMoveEventArgs(-1, -1));
			break;
		}
	}

	if(mDropSprite)
	{
		Vector2f pos = mDropSprite->getPosition();
		Vector2u dim = mDropSprite->getTexture()->getSize();

		if(!quickIntersect(mouse, pos, dim)) return;
		if(!mListActive)					 mListActive = true;

		float height    = (mFieldTex->getSize().y * (mFields.size() + 1));
		float width     = mLargestWidth;
		
		Vector2f newPosition(getPosition(Edge_Topleft).x + width / 2.0f, getPosition(Edge_Topleft).y + height / 2.f);
		mDimensions.set(width, height);
		setPosition(newPosition);
	}
}

void DropControl::moving(MouseMoveEventArgs args)
{
	if(mListActive)
	{
		for(uint32_t i = 0; i < mFields.size(); i++)
		{
			Vector2i mouse = Vector2i(args.x, args.y);
			Vector2f pos   = mFields[i]->getFieldPosition();
			Vector2u dim   = Vector2u(mFields[i]->getGfxSize().x, mFields[i]->getGfxSize().y);

			if(!quickIntersect(mouse, pos, dim))
				continue;

			int selectionA = mLastSelection;
			setSelection(i);

			if(mFields.at(selectionA))		  mFields.at(selectionA)->setDrawMask(false);
			if(mFields.at(mCurrentSelection)) mFields.at(mCurrentSelection)->setDrawMask(true);
		}
	}
}

void DropControl::hide(MouseMoveEventArgs args)
{
	mListActive  = false;
	float width  = getRenderMod()->getGraphic()->getTexture()->getSize().x;
	float height = getRenderMod()->getGraphic()->getTexture()->getSize().y;

	Vector2f newPosition(getPosition(Edge_Topleft).x + width / 2.0f, getPosition(Edge_Topleft).y + height / 2.f);
	mDimensions.set(width, height);
	setPosition(newPosition);
}

bool DropControl::isListActive()
{
	return mListActive;
}

int32_t DropControl::getCurrentSelection()
{
	return mCurrentSelection;
}

int32_t DropControl::getLastSelection()
{
	return mLastSelection;
}

float DropControl::getLargestWidth()
{
	return mLargestWidth;
}

// DropField Impl
// ==============
DropField::DropField(Vector2f position, FeanTexture* fieldImg, FeanTexture* maskImg, string text, uint32_t size)
{
	mText = new Text(text, *ResourceCache::getInstance()->retrieveFont("OpenSans"), size);
	mText->setColor(Colour::Black);

	mFieldPosition = position;
	mField		   = new Sprite(*fieldImg);
	mSelectionMask = new Sprite(*maskImg);

	mField->setPosition(position);
	mSelectionMask->setPosition(position);

	mTextWidth	 = mText->getLocalBounds().width;
	mWidthFactor = .0f;
	mDrawMask	 = false;

	whenEntered = bind(&DropField::enterCallback, this, placeholders::_1);
	whenExited  = bind(&DropField::exitCallback, this, placeholders::_1);
}

DropField::~DropField()
{
	delete mText;
	delete mField;
	delete mSelectionMask;
}

void DropField::render(RenderTarget* target)
{
	if(mField)						target->draw(*mField);
	if(mSelectionMask && mDrawMask) target->draw(*mSelectionMask);
	if(mText)						target->draw(*mText);
}

/// <summary>
/// Scales the sprite relative to the Y-Axis
/// </summary>
void DropField::scaleX(float factor)
{
	mField->setScale(factor, mField->getScale().y);
	mSelectionMask->setScale(factor, mSelectionMask->getScale().y);
	mTextWidth = mField->getTexture()->getSize().x /** factor*/;
}

Vector2f DropField::getGfxSize()
{
	return Vector2f(mField->getGlobalBounds().width, mField->getGlobalBounds().height);
}

Sprite* DropField::getField()
{
	return mField;
}

Text* DropField::getText()
{
	return mText;
}

Vector2f DropField::getFieldPosition()
{
	return mFieldPosition;
}

float DropField::getTextWidth()
{
	return mTextWidth;
}

void DropField::setWidthFactor(float factor)
{
	mWidthFactor = factor;
}

float DropField::getWidthFactor()
{
	return mWidthFactor;
}

void DropField::setPosition(Vector2f position)
{
	mFieldPosition = position;
	mSelectionMask->setPosition(mFieldPosition);
	mField->setPosition(mFieldPosition);
	mText->setPosition(mFieldPosition);
}

void DropField::setDrawMask(bool doesMask)
{
	mDrawMask = doesMask;
}

void DropField::enterCallback(MouseMoveEventArgs args)
{
	mDrawMask = true;
}

void DropField::exitCallback(MouseMoveEventArgs args)
{
	mDrawMask = false;
}
