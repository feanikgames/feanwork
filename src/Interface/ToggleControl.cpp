#include "Feanwork/Interface/ToggleControl.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/GameConsole.h"

ToggleControl::ToggleControl(Vector2f relativePos, string activeImg, string hoverImg, string defaultImg, Container* parent, bool canHover) :
	ControlBase(relativePos, defaultImg, parent)
{
	mActive		= false;
	mHovering   = false;
	mAllowHover = canHover;

	mHoverTex		= ResourceCache::getInstance()->retrieveTexture(hoverImg);
	mActiveTex		= ResourceCache::getInstance()->retrieveTexture(activeImg);
	mDefaultTex		= ResourceCache::getInstance()->retrieveTexture(defaultImg);
	mHoveringSprite = new Sprite(*mHoverTex);

	setPosition(relativePos + mParent->getAbsPosition());
	mHoveringSprite->setPosition(getPosition(Edge_Topleft));

	onMouseEnter.push_back(bind(&ToggleControl::enter, this, placeholders::_1));
	onMouseExit.push_back(bind(&ToggleControl::exit, this, placeholders::_1));
	onPressed.push_back(bind(&ToggleControl::switchToggle, this, placeholders::_1));
}

ToggleControl::~ToggleControl()
{
	delete mHoveringSprite;
}

bool ToggleControl::render(RenderTarget* target)
{
	bool rendered = ControlBase::render(target);
	if(mAllowHover && mHovering)
		target->draw(*mHoveringSprite);

	return rendered;
}
	
// Event Callbacks
/// <summary>
/// Switches the toggle based on active status
/// </summary>
void ToggleControl::switchToggle(MouseButtonEventArgs args)
{
	if(mActive)
	{
		mActive = false;
		this->getRenderMod()->loadGraphic(mDefaultTex);
		GameConsole::getInstance()->log("Toggle switched to inactive");
	}
	else
	{
		mActive = true;
		this->getRenderMod()->loadGraphic(mActiveTex);
		GameConsole::getInstance()->log("Toggle switched to active");
	}
	setPosition(mRelativePosition + mParent->getAbsPosition());
}

void ToggleControl::enter(MouseMoveEventArgs args)
{
	mHovering = true;
}

void ToggleControl::exit(MouseMoveEventArgs args)
{
	mHovering = false;
}

// Data Retrieval
bool ToggleControl::isActive()
{
	return mActive;
}

bool ToggleControl::canHover()
{
	return mAllowHover;
}

void ToggleControl::setHoverFlag(bool hover)
{
	mAllowHover = hover;
}
