#include "Feanwork/Interface/InterfaceController.h"
#include "Feanwork/Interface/ControlBase.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/InputHandler.h"

InterfaceController::InterfaceController(float pressTimeout)
{
	mFocusedControl = nullptr;
	mLastFocused    = nullptr;

	mAccum		    = pressTimeout;
	mPressTimeout   = pressTimeout;
	mLastMousePos   = Mouse::getPosition(*SceneManager::getInstance()->getScreen());

	mLeftMouseDown  = false;
	mRightMouseDown = false;
}

InterfaceController::~InterfaceController()
{
	for(auto container: mContainers)
		delete container;

	mFocusedControl = nullptr;
	mLastFocused    = nullptr;
	mContainers.clear();
}

/*void InterfaceController::init()
{
}*/

void InterfaceController::update()
{
	// Check timeout and get mouse position
	Vector2i mousePos = Mouse::getPosition(*SceneManager::getInstance()->getScreen());
	mAccum           += SceneManager::getInstance()->getDelta();

	// Update callbacks
	focus(mousePos);

	if(mLastFocused != mFocusedControl)
	{
		if(mFocusedControl) mFocusedControl->callMouseEntered(MouseMoveEventArgs(mousePos));
		if(mLastFocused)    mLastFocused->callMouseExited(MouseMoveEventArgs(mousePos));
							mLastFocused = nullptr;
	}
	
	Event evt;
	while(SceneManager::getInstance()->getScreen()->pollEvent(evt))
	{
		// Mouse Pressed Event
		if(evt.type == Event::MouseButtonPressed)
		{
			if(mFocusedControl)
				mFocusedControl->callPressed(MouseButtonEventArgs(evt.mouseButton.button, mousePos));
		}

		// Mouse Released Event
		else if(evt.type == Event::MouseButtonReleased)
		{
			for(auto container: mContainers)
			{
				for(auto control: container->getControls())
					control->callReleased(MouseButtonEventArgs(evt.mouseButton.button, mousePos));
			}
		}

		// Mouse Moved Event
		else if(evt.type == Event::MouseMoved)
		{
			if(mFocusedControl)
				mFocusedControl->callMouseMoved(MouseMoveEventArgs(mousePos));
		}

		// Text Entered Event
		else if(evt.type == Event::TextEntered)
		{
			if(mFocusedControl)
				mFocusedControl->callTextEntered(TextEventArgs());
		}
	}

	/* Update all controls */
	// Pre Update
	for(auto container: mContainers)
		container->update(UpdateStage_Pre);

	// Current Update
	for(auto container: mContainers)
		container->update(UpdateStage_Current);

	// Post Update
	for(auto container: mContainers)
		container->update(UpdateStage_Post);
}

void InterfaceController::render(RenderTarget* target)
{
	for(auto container: mContainers)
		container->render(target);
}

void InterfaceController::focus(Vector2i mouse)
{
	for(auto container: mContainers)
	{
		if(!container->intersects(mouse))
			continue;

		for(auto control: container->getControls())
		{
			if(control->intersects(mouse))
			{
				mLastFocused    = mFocusedControl;
				mFocusedControl = control;
			}
		}
	}
}

void InterfaceController::addContainer(Container* container)
{
	mContainers.push_back(container);
}

Container* InterfaceController:: getListener(uint32_t index)
{
	return mContainers.at(index);
}
