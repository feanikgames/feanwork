#include "Feanwork/Interface/ControlBase.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/Module/RenderModule.h"

ControlBase::ControlBase() : 
	Entity(Vector2f())
{
	mParent           = nullptr;
    mRelativePosition  = Vector2f(0.0f, 0.0f);
	mControlType	  = ControlType_Default;
	initDebugQuad();
}

ControlBase::ControlBase(Vector2f relativePos, string resource, Container* parent, ControlType type) :
	Entity(relativePos)
{
	mParent			  = parent;
	mRelativePosition = relativePos;
	mControlType	  = type;

	if(mParent && resource != "")
	{
		setPosition(relativePos + mParent->getAbsPosition());
		addModule(new RenderModule(resource));
	}
	initDebugQuad();
}

ControlBase::~ControlBase()
{
	mLinked.clear();
	mParent = nullptr;
}

bool ControlBase::render(RenderTarget* target)
{
	return Entity::render(target);
}

bool ControlBase::intersects(Vector2i num)
{
	if(mParent)
	{
		Vector2f position = getPosition(Edge_Topleft);
		if(num.x > (position.x + getDim().x)) return false;
		if(num.x <  position.x)				  return false;
		if(num.y > (position.y + getDim().y)) return false;
		if(num.y <  position.y)				  return false;
											  return true;
	}
	else
	{
		Vector2f position = getPosition();
        if (num.x > (position.x + getDim().x)) return false;
        if (num.x <  position.x)               return false;
        if (num.y > (position.y + getDim().y)) return false;
        if (num.y <  position.y)               return false;
                                                return true;
	}
}

bool ControlBase::quickIntersect(Vector2i mouse, Vector2f pos, Vector2u dim)
{
		 if(mouse.x <  pos.x)		   return false;
	else if(mouse.x > (pos.x + dim.x)) return false;
	else if(mouse.y <  pos.y)		   return false;
	else if(mouse.y > (pos.y + dim.y)) return false;
									   return true;
}

ControlType ControlBase::getControlType()
{
	return mControlType;
}

// Callbacks
void ControlBase::callPressed(MouseButtonEventArgs args)
{
	for(auto evt: onPressed)
		evt(args);
}

void ControlBase::callDoublePressed(MouseButtonEventArgs args)
{
	for(auto evt: onDoublePressed)
		evt(args);
}

void ControlBase::callReleased(MouseButtonEventArgs args)
{
	for(auto evt: onReleased)
		evt(args);
}

void ControlBase::callMiddlePressed(MouseWheelEventArgs args)
{
	for(auto evt: onMiddlePressed)
		evt(args);
}

void ControlBase::callWheelMoved(MouseWheelEventArgs args)
{
	for(auto evt: onWheelMoved)
		evt(args);
}

void ControlBase::callKeyPressed(KeyEventArgs args)
{
	for(auto evt: onKeyPressed)
		evt(args);
}

void ControlBase::callKeyReleased(KeyEventArgs args)
{
	for(auto evt: onKeyReleased)
		evt(args);
}

void ControlBase::callMouseEntered(MouseMoveEventArgs args)
{
	for(auto evt: onMouseEnter)
		evt(args);
}

void ControlBase::callMouseExited(MouseMoveEventArgs args)
{
	for(auto evt: onMouseExit)
		evt(args);
}

void ControlBase::callMouseMoved(MouseMoveEventArgs args)
{
	for(auto evt: onMouseMoved)
		evt(args);
}

void ControlBase::callTextEntered(TextEventArgs args)
{
	for(auto evt: onTextEntered)
		evt(args);
}
