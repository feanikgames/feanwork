#include "Feanwork/Interface/LabelControl.h"
#include "Feanwork/Interface/Container.h"

LabelControl::LabelControl(Vector2f relativePos, string text, Container* parent, uint32_t size) : 
	ControlBase(relativePos, "", parent, ControlType_Label)
{
	mText = new Text(text, *ResourceCache::getInstance()->retrieveFont("OpenSans"), size);
	mText->setColor(Colour(255, 255, 255, 255));

	if(mParent)
	{
		setPosition(relativePos + mParent->getAbsPosition());
		mText->setPosition(getPosition(Edge_Topleft));
	}
}

LabelControl::~LabelControl()
{
	delete mText;
}

bool LabelControl::render(RenderTarget* target)
{
	bool rendered = Entity::render(target);
	target->draw(*mText);
	return rendered;
}

void LabelControl::setText(string text)
{
	mText->setString(text);
}

string LabelControl::getText()
{
	return mText->getString();
}
