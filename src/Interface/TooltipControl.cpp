#include "Feanwork/Interface/TooltipControl.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/Module/RenderModule.h"

TooltipControl::TooltipControl(Vector2f relativePos, string frameImg, string desc, uint32_t size, Container* parent, bool hidden) :
	ControlBase(relativePos, frameImg, parent)
{
	mDescription = new Text("", *ResourceCache::getInstance()->retrieveFont("OpenSans"), size);
	mDescription->setColor(Colour(0, 0, 0, 255));
	mHidden		 = hidden;

	if(mParent)
	{
		setPosition(relativePos + mParent->getAbsPosition());
		mDescription->setPosition(getPosition(Edge_Topleft));
	}

	setDescription(desc);
}

TooltipControl::~TooltipControl()
{
	delete mDescription;
}

bool TooltipControl::render(RenderTarget* target)
{
	bool rendered = false;

	if(!mHidden)
	{
		rendered = ControlBase::render(target);
		target->draw(*mDescription);
	}

	return rendered;
}

/// <summary>
/// Sets the description of the tooltip and resizes the graphic accordingly
/// </summary>
void TooltipControl::setDescription(string text)
{
    RenderModule* module = this->getRenderMod();
    Vector2u graphicSize = module->getTexture()->getSize();
	mDescription->setString(text);

    float factorX = (mDescription->getLocalBounds().width  + 12.0f) / graphicSize.x;
    float factorY = (mDescription->getLocalBounds().height + 12.0f) / graphicSize.y;
    scaleFrame(factorX, factorY);

    Vector2f gfxPosition = module->getGraphic()->getPosition();
    float gfxWidth       = module->getGraphic()->getLocalBounds().width  * module->getGraphic()->getScale().x;
    float gfxHeight      = module->getGraphic()->getLocalBounds().height * module->getGraphic()->getScale().y;
    mDimensions.set(gfxWidth, gfxHeight);

    float diffX = (gfxPosition.x + (gfxWidth  / 2.0f)) - (mDescription->getPosition().x + (mDescription->getLocalBounds().width  / 2.0f));
    float diffY = (gfxPosition.y + (gfxHeight / 2.0f)) - (mDescription->getPosition().y + (mDescription->getLocalBounds().height / 2.0f));
    mDescription->move(diffX, diffY);
}

/// <summary>
/// Scales the graphic transform by a specified amount for wrapping around text
/// </summary>
void TooltipControl::scaleFrame(float factorX, float factorY)
{
    this->getRenderMod()->getGraphic()->scale(factorX, factorY);
}

void TooltipControl::setColour(Colour colour)
{
	mDescription->setColor(colour);
}

void TooltipControl::setHidden(bool hidden)
{
	mHidden = hidden;
}

void TooltipControl::show()
{
	mHidden = false;
}

bool TooltipControl::isHidden()
{
	return mHidden;
}

string TooltipControl::getDescription()
{
	return mDescription->getString();
}
