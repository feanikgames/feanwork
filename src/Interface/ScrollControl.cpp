#include "Feanwork/Interface/ScrollControl.h"
#include "Feanwork/Module/RenderModule.h"

ScrollControl::ScrollControl(Vector2f relativePos, string barImg, string thumbImg, string arrowImg, float arrowStep, ScrollAxis scrollAxis, Container* parent) :
	ControlBase(relativePos, barImg, parent, ControlType_Scroll)
{
	// Load Resources
	mThumb     = new ControlBase(relativePos, thumbImg, parent);
	mArrowUp   = new ControlBase(relativePos, arrowImg, parent);
	mArrowDown = new ControlBase(relativePos, arrowImg, parent);

	// Set event callbacks
	onReleased.push_back(bind(&ScrollControl::dropThumb, this, placeholders::_1));
	onMouseMoved.push_back(bind(&ScrollControl::mouseMove, this, placeholders::_1));
	onPressed.push_back(bind(&ScrollControl::dragThumb, this, placeholders::_1));
	mArrowUp->onPressed.push_back(bind(&ScrollControl::arrowUp, this, placeholders::_1));
	mArrowDown->onPressed.push_back(bind(&ScrollControl::arrowDown, this, placeholders::_1));

	// Init positions
	mArrowUp->setPosition(getPosition(Edge_Topleft)		  + mArrowUp->getHalfDim());
	mArrowDown->setPosition(getPosition(Edge_Bottomright) - mArrowDown->getHalfDim());

	RenderModule* module = mArrowDown->getRenderMod();
	module->getGraphic()->scale(1.f, -1.f);
	module->getGraphic()->move(.0f, mArrowDown->getDim().y);

	// Variables
	mArrowStep = arrowStep;
	mThumbDrag = false;

	if(scrollAxis == ScrollAxis_Vertical) mScrollAxis = Vector2f(0, 1);
	else								  mScrollAxis = Vector2f(1, 0);

	// Precalculate min/max thumb pos
	Vector2f offset = (getHalfDim() - mArrowUp->getDim() - mThumb->getHalfDim());
	offset			= Vectorial::vectorHadamard(mScrollAxis, offset);
	mMinThumb		= (getPosition() - offset);
	mMaxThumb		= (getPosition() + offset);
}

ScrollControl::~ScrollControl()
{
	delete mThumb;
	delete mArrowUp;
	delete mArrowDown;
}

bool ScrollControl::update(UpdateStage stage)
{
	mThumb->update(stage);
	mArrowUp->update(stage);
	mArrowDown->update(stage);
	return Entity::update(stage);
}

bool ScrollControl::render(RenderTarget* target)
{
	bool rendered = Entity::render(target);
	mThumb->render(target);
	mArrowUp->render(target);
	mArrowDown->render(target);
	return rendered;
}

void ScrollControl::setThumbPos(float position)
{
	// Clamp
	position  = min(position, 1.f);
	position  = max(position, .0f);
	mThumbPos = position;

	// Calculate Position
	Vector2f newPosition = (mMaxThumb - mMinThumb) * mThumbPos;
	newPosition			+= mMinThumb;
	mThumb->setPosition(newPosition);
}

// Callbacks
void ScrollControl::dropThumb(MouseButtonEventArgs args)
{
	mThumbDrag = false;
}

void ScrollControl::dragThumb(MouseButtonEventArgs args)
{
	Vector2i mouse = Vector2i(args.x, args.y);
	Vector2u dims  = Vector2u(mThumb->getDim().x, mThumb->getDim().y);

	if(quickIntersect(mouse, mThumb->getPosition(Edge_Topleft), dims))
		mThumbDrag = true;
}

void ScrollControl::mouseMove(MouseMoveEventArgs args)
{
	if(mThumbDrag)
	{
		Vector2f diff = Vector2f(args.x, args.y) - mMinThumb;
		
		// Project diff vector onto scrollbar axis
		float projLen = Vectorial::vectorDotprod(diff, mScrollAxis);

		// Set thumb pos
		setThumbPos(projLen / Vectorial::vectorDotprod(mScrollAxis, (mMaxThumb - mMinThumb)));
		if (onScrollMoved)
			onScrollMoved();
	}
}

void ScrollControl::arrowUp(MouseButtonEventArgs args)
{
	Vector2i mouse = Vector2i(args.x, args.y);
	Vector2u dims  = Vector2u(mArrowUp->getDim().x, mArrowUp->getDim().y);

	if(quickIntersect(mouse, mArrowUp->getPosition(Edge_Topleft), dims))
		setThumbPos(mThumbPos - mArrowStep);  
}

void ScrollControl::arrowDown(MouseButtonEventArgs args)
{
	Vector2i mouse = Vector2i(args.x, args.y);
    Vector2u dims  = Vector2u(mArrowDown->getDim().x, mArrowDown->getDim().y);

    if (quickIntersect(mouse, mArrowDown->getPosition(Edge_Topleft), dims))
        setThumbPos(mThumbPos + mArrowStep);
}

float ScrollControl::getThumbPos()
{
	return mThumbPos;
}

Vector2f ScrollControl::getScrollAxis()
{
	return mScrollAxis;
}
