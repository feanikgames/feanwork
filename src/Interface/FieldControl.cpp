#include "Feanwork/Interface/FieldControl.h"
#include "Feanwork/Interface/ScrollControl.h"

FieldControl::FieldControl(Vector2f relativePos, string frameImg, Container* parent, bool multiLine) : 
	ControlBase(relativePos, frameImg, parent)
{
	mMultiLine = multiLine;
	mMinRender = 0;
	mRenderRange = 0;
	mSeperation = .1f;

	mLines.push_back(new Text("", *ResourceCache::getInstance()->retrieveFont("OpenSans"), 14));
	mLines[0]->setPosition(getPosition(Edge_Topleft));
	mLines[0]->setColor(Colour(255, 255, 255, 255));

	mCursorPos.x = mCursorPos.y = 0;
	mIndicator.append(Vertex(Vector2f(getPosition(Edge_Topleft).x, getPosition(Edge_Topleft).y)));
	mIndicator.append(Vertex(Vector2f(getPosition(Edge_Topleft).x, getPosition(Edge_Topleft).y + mLines[0]->getLocalBounds().height)));

	onPressed.push_back(bind(&FieldControl::pressed, this, placeholders::_1));
	onTextEntered.push_back(bind(&FieldControl::entered, this, placeholders::_1));
}

FieldControl::~FieldControl()
{
	for(auto line: mLines)
		delete line;

	delete mScrollV;
	delete mScrollH;
	mLines.clear();
}

bool FieldControl::render(RenderTarget* target)
{
	bool rendered = ControlBase::render(target);
	for(auto line: mLines)
		target->draw(*line);

	target->draw(mIndicator);
	return rendered;
}

void FieldControl::updateLines()
{
	if(mLines.size() <= 0)
		return;

	for(int32_t i = 1; i < mLines.size(); i++)
	{
		float xPosition = getPosition(Edge_Topleft).x;
		float yPosition = mLines[i - 1]->getPosition().y + mLines[i - 1]->getLocalBounds().height * (1.0f + mSeperation);
		mLines[i]->setPosition(xPosition, yPosition);
	}
}

bool FieldControl::isEmpty()
{
	return (mLines.size() == 1 && mLines[0]->getString() == "");
}

void FieldControl::createScrollV(string barImg, string thumbImg, string arrowImg, float arrowStep)
{
	mCanScrollV = true;
	mScrollV	= new ScrollControl(mRelativePosition, barImg, thumbImg, arrowImg, arrowStep, ScrollAxis_Vertical, mParent);
}

void FieldControl::createScrollH(string barImg, string thumbImg, string arrowImg, float arrowStep)
{
	mCanScrollH = true;
	mScrollV	= new ScrollControl(mRelativePosition, barImg, thumbImg, arrowImg, arrowStep, ScrollAxis_Horizontal, mParent);
}

// Callback Impl
void FieldControl::entered(TextEventArgs args)
{
	string newCharacter = args.unicode;
	string newString	= "";

	// Backspace
	if(newCharacter[0] == 8)
	{
		if(mCursorPos.x > 0)
		{
			mCursorPos.x--;
			newString = remove(mLines[mCursorPos.y]->getString(), mCursorPos.x);
			mLines[mCursorPos.y]->setString(newString);
		}
		else if(mCursorPos.x <= 0)
		{
			// Go back a line
			string lastLine = mLines[mCursorPos.y]->getString();
			if(mCursorPos.y > 0)
			{
				mCursorPos.y--;
				mCursorPos.x = mLines[mCursorPos.y]->getString().getSize();
				mLines[mCursorPos.y]->setString(mLines[mCursorPos.y]->getString() + lastLine);
				mLines.erase(mLines.begin() + (mCursorPos.y - 1));
				updateLines();
			}
		}

		return;
	}

	// Enter
	if(newCharacter[0] == 13)
	{
		string oldLine = substr(mLines[mCursorPos.y]->getString(), mCursorPos.x);
        Text* newText  = new Text(oldLine, *ResourceCache::getInstance()->retrieveFont("OpenSans"), 14);

        if (mCursorPos.x < mLines[mCursorPos.y]->getString().getSize())
            mLines[mCursorPos.y]->setString(remove(mLines[mCursorPos.y]->getString(), mCursorPos.x));

        mCursorPos.y++;
        mCursorPos.x = 0;

        mLines.insert(mLines.begin() + mCursorPos.y, newText);
        mLines[mCursorPos.y]->setColor(Colour(255, 255, 255, 255));

        updateLines();
        return;
	}

	insert(*mLines[mCursorPos.y], newCharacter, mCursorPos.x);
	mCursorPos.x += newCharacter.size();
}

void FieldControl::pressed(MouseButtonEventArgs args)
{
	// Find closest line
    float height = 0.0f;
    float width  = 0.0f;
    mCursorPos.y = mLines.size() - 1;

    for(int i = 0; i < mLines.size(); i++)
    {
        height += mLines[i]->getGlobalBounds().height * (1.0f + mSeperation);
        if (height > args.y - getPosition(Edge_Topleft).y)
        {
            mCursorPos.y = i;
            break;
        }
    }

    // Find closest character
    sf::String line  = mLines[mCursorPos.y]->getString();
    mCursorPos.x = line.getSize();

    for (int c = 0; c < line.getSize(); c++)
    {
        Glyph g         = ResourceCache::getInstance()->retrieveFont("OpenSans")->getGlyph(line[c], 14, false);
        float charWidth = g.bounds.width;

        if (width > args.x - getPosition(Edge_Topleft).x)
        {
            mCursorPos.x = c;
            break;
        }
        width += charWidth;
    }
}

string FieldControl::remove(string str, uint32_t position)
{
	string newString = str;
	newString.erase(position);
	return newString;
}

string FieldControl::substr(string str, uint32_t position)
{
	string newString = "";
	for(int32_t i = position; i < str.size(); i++)
		newString += str[i];

	return newString;
}

string FieldControl::insert(Text& to, string from, uint32_t position)
{
	string newString = to.getString();
	newString.insert(position, from);
	to.setString(newString);
	return newString;
}
