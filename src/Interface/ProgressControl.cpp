#include "Feanwork/Interface/ProgressControl.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/GameConsole.h"

ProgressControl::ProgressControl(Vector2f relativePos, string barImg, Container* parent, uint32_t maxValue) :
	ControlBase(relativePos, barImg, parent, ControlType_Progress)
{
	setPosition(relativePos + mParent->getAbsPosition());
	mWidth  = this->getRenderMod()->getTexture()->getSize().x;
	mHeight = this->getRenderMod()->getTexture()->getSize().y;

	if(maxValue == 0) mMaxValue = mWidth;
	else mMaxValue				= maxValue;

	mCurrent		  = 0;
	mCompleteProgress = false;
	setPercentage(25); // NOTE: Test call
	onPressed.push_back(bind(&ProgressControl::pressed, this, placeholders::_1));
}

ProgressControl::~ProgressControl()
{
}

/// <summary>
/// Set the percentage of the progress bar (0 - 100)
/// </summary>
void ProgressControl::setPercentage(uint8_t percentage)
{
	if(percentage > 100)
	   percentage = 100;

	mCurrent	   = percentage;
	float newWidth = (percentage / 100.f) * mMaxValue;
	this->getRenderMod()->clipRect(Vector2f(newWidth, mHeight), Edge_Topleft);

	if(mCurrent == 100 && !mCompleteProgress)
	{
		if(onComplete != nullptr)
			onComplete();

		mCompleteProgress = true;
		GameConsole::getInstance()->log("Progress Control Completed");
	}
}

/// <summary>
/// Increment the progress bar by a specified amount (0 - 100)
/// </summary>
void ProgressControl::incrementPercentage(uint8_t percentage)
{
	mCurrent += percentage;
	if(mCurrent > 100)   
		mCurrent = 100;

	float newWidth = (mCurrent / 100.f) * mMaxValue;
	getRenderMod()->clipRect(Vector2f(newWidth, mHeight), Edge_Topleft);

	if(mCurrent == 100 && !mCompleteProgress)
	{
		if(onComplete != nullptr)
			onComplete();

		mCompleteProgress = true;
		GameConsole::getInstance()->log("Progress Control Completed");
	}
}

void ProgressControl::reset()
{
	mCompleteProgress = false;
	setPercentage(0);
}

void ProgressControl::pressed(MouseButtonEventArgs args)
{
	incrementPercentage(5);
}

bool ProgressControl::isComplete()
{
	return mCompleteProgress;
}

void ProgressControl::callComplete()
{
	if(onComplete != nullptr)
		onComplete();
}
