#include "Feanwork/Interface/SelectionControl.h"
#include "Feanwork/Interface/Container.h"
#include "Feanwork/GameConsole.h"

SelectionControl::SelectionControl(Vector2f relativePos, string selectedImg, string defaultImg, SelectionType type, Container* parent) :
	ControlBase(relativePos, "", parent)
{
	mSelectedTex   = ResourceCache::getInstance()->retrieveTexture(selectedImg);
	mDefaultTex    = ResourceCache::getInstance()->retrieveTexture(defaultImg);
	mSelectionType = type;

	mCurrentSelection = -1;
	onPressed.push_back(bind(&SelectionControl::selectOption, this, placeholders::_1));
	setPosition(relativePos + mParent->getAbsPosition());
}

SelectionControl::~SelectionControl()
{
	for(auto text: mText)
		delete text;

	mText.clear();
}

bool SelectionControl::render(RenderTarget* target)
{
	bool rendered = ControlBase::render(target);
	for(auto selection: mSelected)
		target->draw(*selection);

	for(auto text: mText)
		target->draw(*text);

	return rendered;
}

/// <summary>
/// Re-evaluate the bounds of the radio box for intersection checks
/// </summary>
void SelectionControl::calculateBounds()
{
	Vector2f position = getPosition(Edge_Topleft);
    float maxheight   = max(mText.back()->getGlobalBounds().height, (float)mDefaultTex->getSize().y);
    float width       = max(mDimensions.width, mDefaultTex->getSize().x + mText.back()->getGlobalBounds().width + 40.0f);
    float height      = maxheight + mText.back()->getPosition().y + 5.0f - position.y;

    mDimensions.set(width, height);
    setPosition(position + getHalfDim());
}

/// <summary>
/// Add an option to the list of radio selections
/// </summary>
void SelectionControl::addOption(string text)
{
	// init variables
    Text* newText   = new Text(text, *ResourceCache::getInstance()->retrieveFont("OpenSans"), 12);
    Sprite* newSpr  = new Sprite(*mDefaultTex);
    float sprHeight = mSelectedTex->getSize().y;

    // Position new Radio Buttons
    Vector2f position = getPosition(Edge_Topleft);
    newSpr->setPosition(position.x +  5.0f, position.y + 5.0f + sprHeight * mText.size());
    newText->setPosition(position.x + 35.0f, position.y + 5.0f + sprHeight * mText.size());

    float textY       = newText->getPosition().y + (newText->getLocalBounds().height / 2.0f);
    float sprY        = newSpr->getPosition().y + (newSpr->getTextureRect().height / 2.0f);
    float diff        = abs(textY - sprY);
    newText->move(0.0f, diff);

    // Push text + selection to lists & recaluate intersection bounds
    mText.push_back(newText);
    mSelected.push_back(newSpr);
    mCheckState.push_back(false);
    calculateBounds();
}

/// <summary>
/// Checks for intersections on the radio buttons and updates the current selection accordingly
/// </summary>
void SelectionControl::selectOption(MouseButtonEventArgs _args)
{
    for (uint32_t i = 0; i < mSelected.size(); i++)
    {
        Vector2i mouse = Vector2i(_args.x, _args.y);
        if (mouse.x < mSelected[i]->getPosition().x)									     continue;
        if (mouse.x > mSelected[i]->getPosition().x + mSelected[i]->getTextureRect().width)  continue;
        if (mouse.y < mSelected[i]->getPosition().y)									     continue;
        if (mouse.y > mSelected[i]->getPosition().y + mSelected[i]->getTextureRect().height) continue;

        if (mSelectionType == SelectionType_Radio)
        {
            if (mCurrentSelection == i) 
                continue;

			if(mCurrentSelection > -1)
				mSelected[mCurrentSelection]->setTexture(*mDefaultTex);

			mSelected[i]->setTexture(*mSelectedTex);
            mCurrentSelection = i;
            GameConsole::getInstance()->log("Changed radio selection to %i", mCurrentSelection);
        }
        else
        {
            if (mCheckState[i] == true)
            {
                mSelected[i]->setTexture(*mDefaultTex);
                mCheckState[i]    = false;
                mCurrentSelection = i;
                GameConsole::getInstance()->log("Changed checkbox selection %i to false", mCurrentSelection);
            }
            else
            {
                mSelected[i]->setTexture(*mSelectedTex);
                mCheckState[i]    = true;
                mCurrentSelection = i;
				GameConsole::getInstance()->log("Changed checkbox selection %i to true", mCurrentSelection);
            }
        }
        break;
    }
}

int SelectionControl::getCurrentSelection()
{
	return mCurrentSelection;
}

SelectionType SelectionControl::getSelectionType()
{
	return mSelectionType;
}
