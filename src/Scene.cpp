#include "Feanwork/Scene.h"
#include "Feanwork/ResourceCache.h"
#include "Feanwork/Layer.h"
#include "Feanwork/SceneManager.h"
#include "Feanwork/SoundManager.h"
#include "Feanwork/Camera.h"
#include "Feanwork/Entity.h"
#include "Feanwork/GameConsole.h"

#include "Feanwork/Lighting/LightManager.h"
#include "Feanwork/Interface/InterfaceController.h"
#include "Feanwork/Spatial/Grid.h"


////////////////////////////////////////////////////////////
Scene::Scene()
{
	mLayers.push_back(new Layer(this));
	mCameras.push_back(new Camera(mLayers.back()));

	mActive    = true;
	mSoundMgr  = new SoundManager;
	mLightMgr  = nullptr;
	mInterface = nullptr;

	mCurrentCamera = 0;
	mLastID		   = -1;
	mScreenShader  = nullptr;

	mBuffer = new RenderTexture();
	mBuffer->create(SceneManager::getInstance()->getWidth(), SceneManager::getInstance()->getHeight());
	GameConsole::getInstance()->log("Added new Scene");

	//mWaterTest = new Water(Vector2f(250.f, 250.f), Vector2f(1000.f, 150.f), Colour(58, 163, 218, 140));

	mSpatial = new Grid(Vector2f(-50, -50), Vector2i(25, 25), Vector2f(50.f, 50.f));
	/*mLevel     = nullptr;
	mSpatial   = nullptr;*/
}


////////////////////////////////////////////////////////////
Scene::~Scene()
{
	for(auto layer: mLayers)
		delete layer;

	for(auto camera: mCameras)
		delete camera;

	mLayers.clear();
	mCameras.clear();
}


////////////////////////////////////////////////////////////
void Scene::update()
{
	if(mActive)
	{
		// Update the SpatialBase
		if(mSpatial)
			mSpatial->update();

		// Update Layers
		for(auto layer: mLayers)
			layer->update();

		// Update Managers
		if(mLightMgr)  mLightMgr->update();
		if(mInterface) mInterface->update();
	}
}


////////////////////////////////////////////////////////////
void Scene::render()
{
	if(mActive)
	{
		mBuffer->clear();
		// Switch to camera & render the Scene
		mCameras[mCurrentCamera]->toCamera(mBuffer);

		/*if(mSpatial)
			mSpatial->debugDraw(mBuffer);*/

		for(auto layer: mLayers) 
			layer->render(mBuffer);

		//mWaterTest->render(mBuffer);

		// Render the lights & shadows
		if(mLightMgr)			 
			mLightMgr->render(mBuffer);

		//Double buffer & postprocessing
		/*static float accum = 0.0f;
		accum += SceneManager::getInstance()->getDelta();*/

		mBuffer->display();
		mCameras[mCurrentCamera]->toDefault();
		//mScreenShader->setParameter("tex", mBuffer->getTexture());
		//mScreenShader->setParameter("accum", accum);
		SceneManager::getInstance()->getScreen()->draw(Sprite(mBuffer->getTexture())/*, RenderStates(mScreenShader)*/);

		// Switch back to static camera & render Interface on top
		if(mInterface)
			mInterface->render(SceneManager::getInstance()->getScreen());
	}
}


////////////////////////////////////////////////////////////
bool Scene::initLevel(string level)
{
	// TODO: Load Level
	//return mLevel;
	return true;
}


////////////////////////////////////////////////////////////
void Scene::startLightManager()
{
	mLightMgr = new LightManager;
}


////////////////////////////////////////////////////////////
void Scene::startInterfaceController()
{
	mInterface = new InterfaceController;
}


////////////////////////////////////////////////////////////
void Scene::setShader(string shader)
{
	mScreenShader = ResourceCache::getInstance()->retrieveShader(shader);
}


////////////////////////////////////////////////////////////
void Scene::addCamera(Layer* linkedLayer)
{
	mLastID++;
	Camera* newCamera = new Camera(linkedLayer);
	newCamera->setID(mLastID);
	mCameras.push_back(newCamera);
}


////////////////////////////////////////////////////////////
Entity* Scene::addEntity(int32_t layer, Entity* entity)
{
	// Increment the current MaskID and insert the entity into the specified layer
	mLastID++;
	mLayers.at(layer)->insert(entity);

	// Assign a MaskID, the layer index and a pointer to the scene
	entity->setMaskID(mLastID);
	entity->setLayerIndex(layer);
	entity->setScenePtr(this);
	
	// Add the entity to the Spatial class
	if(mSpatial)
		mSpatial->insert(entity);

	return entity;
}


////////////////////////////////////////////////////////////
Entity* Scene::addEntity(int32_t layer, Prefab* prefab)
{
	// Create entity using prefab
	Entity* entity = new Entity();
	entity->applyPrefab(prefab);
	
	// Increment the current MaskID and insert the entity into the specified layer
	mLastID++;
	mLayers.at(layer)->insert(entity);

	// Assign a MaskID, the layer index and a pointer to the scene
	entity->setMaskID(mLastID);
	entity->setLayerIndex(layer);
	entity->setScenePtr(this);

	// Add the entity to the Spatial class
	if(mSpatial)
		mSpatial->insert(entity);

	return entity;
}


////////////////////////////////////////////////////////////
Entity* Scene::addEntity(int32_t layer, Prefab* prefab, Vector2f origin)
{
	// Store actual origin and apply temporary one
	Vector2f actualOrigin = prefab->origin;
	prefab->origin		  = origin;

	// Add entity and restore actual origin
	Entity* entity = addEntity(layer, prefab);
	prefab->origin = actualOrigin;
	
	return entity;
}


////////////////////////////////////////////////////////////
Entity* Scene::findEntity(int32_t entityID)
{
	// Loop through each layer looking for an entity with this ID
	Entity* found = nullptr;
	for(auto layer: mLayers)
	{
		found = layer->findEntity(entityID);
		if(found)
			break;
	}
	return found;
}


////////////////////////////////////////////////////////////
Layer* Scene::findLayer(int32_t ID)
{
	for(auto layer: mLayers)
	{
		if(layer->getID() == ID)
			return layer;
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
Camera*	Scene::findCamera(int32_t ID)
{
	for(auto camera: mCameras)
	{
		if(camera->getID() == ID)
			return camera;
	}
	return nullptr;
}


////////////////////////////////////////////////////////////
EventListener* Scene::findListener(int32_t ID)
{
	EventListener* listener = nullptr;

	     if(listener = findLayer(ID));
	else if(listener = findCamera(ID));
	return  listener;
}


////////////////////////////////////////////////////////////
Layer* Scene::addLayer(float coef)
{
	mLastID++;
	mLayers.push_back(new Layer(this, coef));
	mLayers.back()->setID(mLastID);
	return mLayers.back();
}


////////////////////////////////////////////////////////////
bool Scene::check()
{
	return (mSpatial && mInterface && mSoundMgr && mCameras.size() == 1);
}


////////////////////////////////////////////////////////////
bool Scene::isActive()
{
	return mActive;
}


////////////////////////////////////////////////////////////
vector<Layer*> Scene::getLayers()
{
	return mLayers;
}


////////////////////////////////////////////////////////////
vector<Entity*> Scene::getAllEntities()
{
	vector<Entity*> entities;
	for(auto layer: mLayers)
	{
		for(auto entity: layer->getEntities())
			entities.push_back(entity);
	}
	return entities;
}


////////////////////////////////////////////////////////////
SoundManager* Scene::getSoundMgr()
{
	return mSoundMgr;
}


////////////////////////////////////////////////////////////
LightManager* Scene::getLightMgr()
{
	return mLightMgr;
}


////////////////////////////////////////////////////////////
InterfaceController* Scene::getInterface()
{
	return mInterface;
}


////////////////////////////////////////////////////////////
SpatialBase* Scene::getSpatial()
{
	return mSpatial;
}


////////////////////////////////////////////////////////////
Camera*	Scene::getCamera(int32_t index)
{
	return mCameras[index];
}


////////////////////////////////////////////////////////////
Camera* Scene::getActiveCamera()
{
	return mCameras[mCurrentCamera];
}


////////////////////////////////////////////////////////////
Camera* Scene::getFirstCamera()
{
	return mCameras.front();
}


////////////////////////////////////////////////////////////
Camera* Scene::getLastCamera()
{
	return mCameras.back();
}
