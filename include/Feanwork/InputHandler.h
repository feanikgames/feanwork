#ifndef _INPUTHANDLER_H
#define _INPUTHANDLER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: InputHandler
 * Description: Catches input and the state of the key/input which is then mapped
 *				via a string.
 *
 */

#include "Util/Define.h"

class FeanExport InputHandler
{
public:
	static InputHandler* instance();
	void update();

	bool isButtonActive(string name);
	void addKeystate(Keystate* state);
	void destroy();

protected:
	InputHandler() { mGamepad = false; }
	virtual ~InputHandler() {}

	vector<Keystate*> mKeystates;
	bool			  mGamepad;
};

#endif // _INPUTHANDLER_H
