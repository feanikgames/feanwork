#ifndef _CAMERA_H
#define _CAMERA_H

/* Author(s): Jamie Massey
 * Date: 21.01.14
 * Filename: Camera
 * Description: The Camera is used to take care of world projection to the screen; it 
 *              uses a variety of functions to a achieve that such as locking on sprites 
 *              and using the parallax scrolling technique.
 *
 */

/* Features
 *  -Parallax Scrolling <-- [Done]
 *  -LookAt and following specific Entity (two seperate functions) [Done]
 *  -Zoom + Rotation [Done]
 *  -Ability to follow multiple entities (if more than one, use formula to work 
 *      out distance between the two and camera so both are on-screen) ['Doneish']
 *  -Assignment of level border limits (great for game and level editor!)
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/EventListener.h"

class FeanExport Camera : public EventListener, public IDMask
{
public:
	Camera();
	Camera(Layer* linkLayer);
	~Camera();

	void  update();
	void  render();
	void  tell(EventFlag flags, ListenerArgs* args) override;

	void	  stopFollowing(int index);
	void	  stopFollowing(Entity* entity);
	Vector2f  getEdge(Edge edge, View* view);

	void  queryLookAt(Entity* entity);
	void  setLinkedLayer(Layer* layer);
	void  toCamera(RenderTarget* target = nullptr);
	void  toDefault(RenderTarget* target = nullptr);

	void  zoom(float zoomFactor);
	void  rotate(float rotateFactor);
	
	void  lookAt(Vector2f position);
	void  lookAt(float x, float y);
	void  lookAt(Entity* entity);

	void	  shakeCamera(Vector2f amount, float duration);
	Vector2f  getCentre();
	View*	  getView();
	Layer*    getLinkedLayer();

	void     setID(int32_t id);
	int32_t  getID();

private:
	void  translate(float x, float y);
	bool  isInside();

protected:
	Dimension		 mDimensions;
	vector<Entity*>  mFollowing;
	View*			 mCamera;
	Layer*			 mLinkedLayer;

	Vector2f  mMinimum;
	Vector2f  mMaximum;
};

#endif // _CAMERA_H
