#ifndef _DEFINE_H
#define _DEFINE_H

#include <iostream>
#include <cstdint>
#include <vector>
#include <string>
#include <fstream>
#include <map>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

#include "Feanwork/Util/Exports.h"
//#include "Feanwork/Util/MemoryTracker.h"
#include "Feanwork/Util/Vectorial.h"
#include "Feanwork/ResourceCache.h"

// -.-
#define Colour Color

// Define PI (omnomnomnom...yummy)
#define PI      3.1415926536
#define TWO_PI  6.2831853072
#define HALF_PI 1.5707963268

// Windows specific include
#ifdef _WINDOWS
	#include <Windows.h>
#elif __linux
	#include <dirent.h>
	#define uint32_t u_int32_t
	#define uint8_t u_int8_t
#endif

using namespace std;
using namespace sf;

// Forward Declaration
class Camera;
class Directory;
class Entity;
class EventListener;
class GameConsole;
class InputHandler;
class Layer;
class Network;
class ResourceCache;
class Scene;
class SceneManager;
class SoundManager;
class Interval;
class Timer;

class Module;
class Animation;
class AnimationModule;
class EmitterModule;
class IntelligenceModule;
class PhysicsModule;
class RenderModule;
class SoundModule;
class Collision;

class AnimationParser;
class ConfigParser;
class LevelParser;
class PrefabParser;
class ScriptParser;

class Interpreter;
class Lexer;
class ASTree;

class SpatialBase;
class Grid;
class QuadTree;

class InterfaceController;
class Container;
class ControlBase;
class DropControl;
class FieldControl;
class LabelControl;
class ProgressControl;
class ScrollControl;
class SelectionControl;
class ToggleControl;
class TooltipControl;

class LightManager;
class Light;
class PointLight;

class FeanTexture;
class FeanFont;
class FeanConfig;

struct Frame;
// ---------------

// Type Definitions
typedef vector<EventListener*> ListenerSet;  ///< A Vector of EventListeners*
typedef vector<Entity*>		   EntitySet;    ///< A Vector of Entity*
typedef vector<Module*>		   ModuleSet;    ///< A Vector of Modules*
typedef vector<Animation*>	   AnimationSet; ///< A Vector of Animations*
typedef vector<Frame*>		   FrameSet;     ///< A Vector of Frames*

// Global Enum's & Def's
#define ModuleType			 int32_t
#define ModuleType_Render	 0
#define ModuleType_Animation 1
#define ModuleType_Intel	 2
#define ModuleType_Physics   3
#define ModuleType_Emitter   4
#define ModuleType_Sound	 5
#define ModuleType_Script	 6

enum EntityState
{
	EntityState_Active    = 0,
	EntityState_Inactive  = 1,
	EntityState_Destroyed = 2,
};

enum Edge
{
	Edge_Topleft	 = 0,
	Edge_Topright	 = 1,
	Edge_Bottomleft  = 2,
	Edge_Bottomright = 3,
};

enum UpdateStage
{
	UpdateStage_Pre     = 0,
	UpdateStage_Current = 1,
	UpdateStage_Post    = 2,
};

enum EventFlag 
{
	EventFlag_None = 0, // 0000
	EventFlag_Pos  = 1, // 0001
	EventFlag_PosX = 2, // 0010
	EventFlag_PosY = 4, // 0100
	EventFlag_Dim  = 8, // 1000

	EventFlag_KeyPress   = 16,  // 0001 0000
	EventFlag_KeyRelease = 32,  // 0010 0000
	EventFlag_JoyPress   = 64,  // 0100 0000
	EventFlag_JoyRelease = 128, // 1000 0000

	EventFlag_Collision = 256,  // 0001 0000 0000
};

enum ParseMode
{
	ParseIn  = 0,
	ParseOut = 1,
};

struct IDMask
{
public:
	void setMaskID(int32_t id)
	{
		idMask = id;
	}

	int32_t getMaskID()
	{
		return idMask;
	}

	IDMask() :
		idMask(-1)
	{
	}

protected:
	int32_t idMask;
};

struct Prefab
{
	vector<int32_t> listenerIDs;
	vector<Module*> modules;
	Vector2f	    origin;
	uint8_t			customType;

	Prefab() :
		origin(0.f, 0.f),
		customType(-1)
	{
	}

	void parseCfg(string cfgFile); // TODO ME
};

struct Keystate
{
	string  inputName;
	int32_t key;
	int32_t button;
	int32_t axis;
	float   cooldown;
	float   current;
	bool    onCooldown;

	Keystate() :
		inputName(""), 
		key(-1), 
		button(-1), 
		axis(-1), 
		cooldown(10), 
		current(0), 
		onCooldown(true)
	{
	}
};

//TODO: Extend to generic ConvexHull class
struct BoxHull
{
	BoxHull(Vector2f origin, Vector2f dim)
	{
		dim /= 2.0f;
		points[0]   = origin   - dim;
		points[1].x = origin.x - dim.x;
		points[1].y = origin.y + dim.y;
		points[2]	= origin   + dim;
		points[3].x = origin.x + dim.x;
		points[3].y = origin.y - dim.y;
	}

	Vector2f getNormal(uint8_t edge)
	{
		if(edge > 3)
			return Vector2f();

		if(edge == 0) return Vector2f(-1,  0);
		if(edge == 1) return Vector2f( 0,  1);
		if(edge == 2) return Vector2f( 1,  0);
		if(edge == 3) return Vector2f( 0, -1);

		// TODO: Take this out
		return Vector2f();
	}

	Vector2f points[4];
};

struct Dimension
{
public:
	float width;
	float height;
	float halfWidth;
	float halfHeight;

	Dimension()
	{
		width	   = .0f;
		height	   = .0f;
		halfWidth  = .0f;
		halfHeight = .0f;
	}

	void set(float _width, float _height)
	{
		width	   = _width;
		height	   = _height;
		halfWidth  = _width / 2.0f;
		halfHeight = _height / 2.0f;
	}
};

// Axis-Aligned Bounding Box Impl
class FeanExport AABB
{
public:
	AABB() {}
	AABB(Vector2f origin, Vector2f halfDims)
	{
		mOrigin 		= origin;
		mHalfDimensions = halfDims;
	}

	AABB(float x, float y, float width, float height)
	{
		mOrigin.x 		  = x;
		mOrigin.y 		  = y;
		mHalfDimensions.x = width;
		mHalfDimensions.y = height;
	}

	AABB(float x, float y, Vector2f halfDims)
	{
		mOrigin.x       = x;
		mOrigin.y       = y;
		mHalfDimensions = halfDims;
	}

	~AABB() {}
	
	bool containsPoint(Vector2f point)
	{
		if (point.x < mOrigin.x - mHalfDimensions.x) return false;
		if (point.y < mOrigin.y - mHalfDimensions.y) return false;
		if (point.x > mOrigin.x + mHalfDimensions.x) return false;
		if (point.y > mOrigin.y + mHalfDimensions.y) return false;
													 return true;
	}

	bool intersects(AABB* boundingBox)
	{
		if (boundingBox->getOrigin().x < mOrigin.x - mHalfDimensions.x) return false;
		if (boundingBox->getOrigin().y < mOrigin.y - mHalfDimensions.y) return false;
		if (boundingBox->getOrigin().x > mOrigin.x + mHalfDimensions.x) return false;
		if (boundingBox->getOrigin().y > mOrigin.y + mHalfDimensions.y) return false;
                                                             			return true;
	}

	Vector2f getOrigin()  { return mOrigin;			}
	Vector2f getHalfDim() { return mHalfDimensions; }

	Vector2f getMin() { return (mOrigin - mHalfDimensions); }
	Vector2f getMax() { return (mOrigin + mHalfDimensions); }
	
protected:
	Vector2f mOrigin;
    Vector2f mHalfDimensions;
};

struct EventArgs
{
};

struct MouseButtonEventArgs : EventArgs
{
	int32_t button;
	int32_t	x;
	int32_t	y;
	
	MouseButtonEventArgs(int32_t button, int32_t x, int32_t y) :
		button(-1), 
		x(0), 
		y(0)
	{
		this->button = button;
		this->x		 = x;
		this->y		 = y;
	}

	MouseButtonEventArgs(int32_t button, Vector2i position) :
		button(-1), 
		x(0), 
		y(0)
	{
		this->button = button;
		this->x		 = position.x;
		this->y		 = position.y;
	}
};

struct MouseMoveEventArgs : EventArgs
{
	int32_t x;
	int32_t y;

	MouseMoveEventArgs(int32_t x, int32_t y) :
		x(0), 
		y(0)
	{
		this->x = x;
		this->y = y;
	}

	MouseMoveEventArgs(Vector2i position) :
		x(0), 
		y(0)
	{
		this->x = position.x;
		this->y = position.y;
	}
};

struct MouseWheelEventArgs : EventArgs
{
	int32_t delta;
	int32_t x;
	int32_t y;

	MouseWheelEventArgs(int32_t delta, int32_t x, int32_t y) :
		delta(0), 
		x(0), 
		y(0)
	{
		this->delta = delta;
		this->x     = x;
		this->y     = y;
	}
};

struct KeyEventArgs : EventArgs
{
	bool     alt;
	int32_t  key;
	bool	 control;
	bool	 shift;
	bool	 system;

	KeyEventArgs() :
		alt(false), 
		key(-1), 
		control(false), 
		shift(false), 
		system(false)
	{
	}
};

struct TextEventArgs : EventArgs
{
	string unicode;

	TextEventArgs() :
		unicode("")
	{
	}
};
// --------------------

#endif // _DEFINE_H
