#ifndef _TIMER_H
#define _TIMER_H

/* Author(s): Bruno L�ders
 * Date: 01.05.14
 * Filename: Timer
 * Description: Read the tin :| 3rd party: project yellow code
 *
 */

#include <chrono>
#include "Define.h"

//! Generic Timer class
class FeanExport Timer
{
public:
	Timer();
	~Timer();

	//! Restart the timer
	void  restart();

	//! Returns the elapsed time in milliseconds as a float
	float getElapsedTimeMs() const;

	//! Returns the elapsed time in seconds
	float getElapsedTimeS()  const;

protected:
	//! Stores the point in time the timer was reset
	chrono::steady_clock::time_point mLastTime; 
};

#endif // _TIMER_H
