#ifndef _INTERVAL_H
#define _INTERVAL_H

/* Author(s): Jamie Massey
 * Date: 27.04.14
 * Filename: Interval
 * Description: A generic implementation to call functions over a specified period of time
 *				in tiny intervals; hence Interval.h
 *
 */

#include <functional>
#include "Feanwork/Util/Define.h"

enum IntervalState
{
	IntervalState_Active   = 0,
	IntervalState_Paused   = 1,
	IntervalState_Finished = 2
};

// Typedefs
typedef function<void(void*, float)> IntervalFunc;
typedef vector<Interval>			 IntervalSet; // A Vector of Intervals
typedef vector<Interval*>			 IntervalPtrs;
typedef IntervalSet::iterator		 IntervalIt;

class FeanExport Interval
{
public:
	Interval(IntervalFunc func, float time, void* dataPtr = nullptr);
	Interval(const Interval& interval);
	~Interval();

	bool update(float deltaTime);
	bool funcExists();

	void		  subscribe(Interval* interval);
	IntervalState state();

	void pause();
	void play();

protected:
	IntervalFunc  mIntervalFunc;
	IntervalState mIntervalState;
	IntervalPtrs  mSubscribers;

	float mElapsedT;
	float mTotalT;
	void* mDataPtr;
};

#endif // _INTERVAL_H
