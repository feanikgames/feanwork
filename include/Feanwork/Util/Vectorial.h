#ifndef _VECTORIAL_H
#define _VECTORIAL_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: Vectorial
 * Description: Global functions used all over the code, accessed via Vectorial:: they
 *				provide functionality for vector-based calaculations.
 *
 */

#include <cmath>
#include <SFML/Graphics.hpp>

#include "Feanwork/Util/Exports.h"

using namespace sf;

namespace Vectorial
{
	extern FeanExport float	   vectorLengthF(Vector2f vec);
	extern FeanExport float	   vectorLengthFSq(Vector2f vec);
	extern FeanExport float	   vectorLengthI(Vector2i vec);
	extern FeanExport float	   vectorDotprod(Vector2f a, Vector2f b);
	extern FeanExport Vector2f vectorNormal(Vector2f vec);

	extern FeanExport Vector2f vectorHadamard(Vector2f a, Vector2f b);
	extern FeanExport Vector2f vectorInvert(Vector2f vec);
	extern FeanExport void	   vectorZero(Vector2f& vec);
	extern FeanExport Vector2f vectorFabs(Vector2f vec);
	extern FeanExport Vector2i vectorAbs(Vector2i vec);

	extern FeanExport float	   manhattanDistance(Vector2f a, Vector2f b);
}

#endif // _VECTORIAL_H
