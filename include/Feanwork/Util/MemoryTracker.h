#ifndef _MEMORYTRACKER_H
#define _MEMORYTRACKER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: MemoryTacker
 * Description: Tracks all memory in the program and produces a Memory Leak Report.
 *
 */

#ifdef _DEBUG
	#include <vector>
	#include <cstddef>
	#include <cstdint>
	#include <malloc.h>

#include "Feanwork/Util/Exports.h"

enum TrackTag
{
	TrackTag_Normal = 0,
	TrackTag_NoTrack
};

class FeanExport MemoryTracker
{
	public:
		struct Trackee
		{
			char		file[128];
			const void*	addr;
			uint16_t	line;
			uint32_t	size;
		};

		void track(const void* _addr, const char* _file, uint16_t _line, uint32_t _size);
		void remove(const void* _addr);

		const std::vector<Trackee>& getTrackees() const	{ return mTrackees; }
		void compileReport();

		static MemoryTracker* getSingleton()
		{
			static MemoryTracker singleton;
			return &singleton;
		}

	private:
		MemoryTracker(){}
		~MemoryTracker(){}

	public:
		std::vector<Trackee> mTrackees;
};



	inline void* operator new(size_t _size, const char* _file, uint16_t _line, TrackTag _tag)
	{
		void* ptr = malloc(_size);

		if(!ptr)
			throw std::bad_alloc();

		if(_tag != TrackTag_NoTrack)
			MemoryTracker::getSingleton()->track(ptr, _file, _line, _size);

		return ptr;
	}

	inline void* operator new[](size_t _size, const char* _file, uint16_t _line, TrackTag _tag)
	{
		void* ptr = malloc(_size);

		if(!ptr)
			throw std::bad_alloc();

		if(_tag != TrackTag_NoTrack)
			MemoryTracker::getSingleton()->track(ptr, _file, _line, _size);

		return ptr;
	}

	inline void operator delete(void* _ptr)
	{
		MemoryTracker::getSingleton()->remove(_ptr);
		free(_ptr);
	}

	inline void operator delete[](void* _ptr)
	{
		MemoryTracker::getSingleton()->remove(_ptr);
		free(_ptr);
	}

	#define new new(__FILE__, __LINE__, TrackTag_Normal)
	#define newNoTrack new(__FILE__, __LINE__, TrackTag_NoTrack)
#else
	#define newNoTrack new //Still need this to be defined even in release mode
#endif // _DEBUG
#endif // _MEMORYTRACKER_H
