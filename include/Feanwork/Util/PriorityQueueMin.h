#ifndef _PRIORITYQUEUEMIN_H
#define _PRIORITYQUEUEMIN_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: PriorityQueueMin
 * Description: Essentially a Binary Heap that switches variables in and out down the queue.
 *				Used im the IntelligenceModule for pathfinding.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/GameConsole.h"
#include <functional>

template<class T>
class PriorityQueueMin
{
public:
	typedef function<bool(T&, T&)> CompareFunction;

	PriorityQueueMin(uint32_t size, CompareFunction compareFunc)
	{
		mCount 			 = 0;
		mQueue			 = new T[size];
		mComparisionFunc = compareFunc;
	}

	void insert(T& data)
	{
        /*if (mCount == mQueue.size())
            return;*/

        mQueue[mCount] = data;
        mCount++;

        int32_t curIndex    = (mCount - 1);
        int32_t parentIndex = floor((curIndex - 1.0f) / 2.0f);
        int32_t counter		= 0;
        while (parentIndex >= 0)
        {
            counter++;
            if (mComparisionFunc(mQueue[curIndex], mQueue[parentIndex]))
            {
                T& temp             = mQueue[parentIndex];
                mQueue[parentIndex] = mQueue[curIndex];
                mQueue[curIndex]    = temp;
                curIndex            = parentIndex;
            }
            else break;

            parentIndex = floor((curIndex - 1.0f) / 2.0f);
        }
	}

	T pop(int32_t index)
    {
		// TODO: fix this
       if (isEmpty())
		return T(0);

        T root        = mQueue[index];
        mQueue[index] = mQueue[mCount - 1];
        mCount--;

        int curIndex   = index;
        int childIndex = 2 * curIndex + 1;
        while (childIndex < mCount)
        {
            if (mComparisionFunc(mQueue[childIndex], mQueue[curIndex]))
            {
                //Swap with smaller child
                if (mComparisionFunc(mQueue[childIndex + 1], mQueue[childIndex]))
                    childIndex = (childIndex + 1);

                T temp             = mQueue[childIndex];
                mQueue[childIndex] = mQueue[curIndex];
                mQueue[curIndex]   = temp;
                curIndex           = childIndex;
            }
            else break;

            childIndex = 2 * curIndex + 1;
        }

        return root;
    }

	void updatePriority(T& data)
    {
        for(int32_t i = 0; i < mCount; i++)
        {
            if (mQueue[i] == data)
            {
                T element = pop(i);
                insert(element);
                break;
            }
        }
    }

    // Data Retrieval
    bool isEmpty()         
	{
		return (mCount == 0);          
	}

    bool contains(T& data) 
	{ 
		for(int32_t i = 0; i < mCount; i++)
		{
			if(mQueue[i] == data)
				return true;
		}
		return false;
	}

	uint32_t getCount()
	{
		return mCount;
	}

protected:
	CompareFunction mComparisionFunc;
	T* 				mQueue;
	uint32_t		mCount;
};

#endif // _PRIORITYQUEUEMIN_H
