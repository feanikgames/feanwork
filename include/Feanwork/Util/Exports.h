#ifndef _EXPORTS_H
#define _EXPORTS_H

/* Author(s): Jamie Massey
 * Date: 30.03.14
 * Filename: Exports
 * Description: Definitions for how to handle the DLL export
 *
 */

#if defined _WINDOWS || defined __CYGWIN__
	#ifdef _BUILDDLL
	#ifdef __GNUC__
	#define FeanExport __attribute__ ((dllexport))
	#else
	#define FeanExport __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
	#endif
	#else
	#ifdef __GNUC__
	#define FeanExport __attribute__ ((dllimport))
	#else
	#define FeanExport __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
	#endif
#endif
#define DLL_LOCAL
	#else
	#if __GNUC__ >= 4
	#define FeanExport __attribute__ ((visibility ("default")))
	#else
	#define FeanExport
	#endif
#endif

#endif /* _EXPORTS_H */
