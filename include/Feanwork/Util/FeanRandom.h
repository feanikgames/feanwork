#ifndef _FEANRANDOM_H
#define _FEANRANDOM_H

/* Author(s): Jamie Massey
 * Date: 28.02.14
 * Filename: FeanRandom
 * Description: Global functions used all over the code, accessed via FeanRandom:: they
 *				provide functionality for random calls.
 *
 */

#include <cmath>
#include "Feanwork/Util/Define.h"

namespace FeanRandom
{
	extern FeanExport int	   randRange(int min, int max);
	extern FeanExport float	   randRangeF(float min, float max);
	extern FeanExport double   randRangeD(double min, double max);
	extern FeanExport Vector2f randomUnitVector();
}

#endif // _FEANRANDOM_H
