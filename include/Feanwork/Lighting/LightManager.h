#ifndef _LIGHTMANAGER_H
#define _LIGHTMANAGER_H

/* Author(s): Jamie Massey
 * Date: 22.03.14
 * Filename: LightManager
 * Description: Manages lights, can be instanced multiple times for each scene that
 *				is active within the SceneManager.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport LightManager
{
public:
	LightManager();
	~LightManager();

	void render(RenderTarget* target);
	void update();

	// Data Retrieval
	void		    addLight(Light* light);
	Light*		    getLight(int32_t index);
	vector<Light*>& getLights();
	int32_t			getLightCount();

protected:
	vector<Light*> mLights;
	RenderTexture* mLightBuffer;
	RenderTexture* mAccumBuffer;
};

#endif // _LIGHTMANAGER_H
