#ifndef _LIGHT_H
#define _LIGHT_H

/* Author(s): Jamie Massey
 * Date: 22.03.14
 * Filename: Light
 * Description: A light that can be drawn and processed in the world
 *
 */

#include "Feanwork/Util/Define.h"

/////////////////////////////////////////////////
/// \brief Enumeration of derived light types
///
/////////////////////////////////////////////////
enum LightType
{
	LightType_Unknown	 = 0,
	LightType_PointLight = 1,
};

/////////////////////////////////////////////////
/// \brief Abstract base class for every light source in the game
///
/////////////////////////////////////////////////
class FeanExport Light
{
public:
	/////////////////////////////////////////////////
	/// \brief Default Constructor
	///
    /////////////////////////////////////////////////
	Light() { mShadowCaster = true; }
	
	/////////////////////////////////////////////////
	/// \brief Constructor Base
	///
	/// Creates a light using variables passed via
	/// the params
	///
	/// \param origin Position to create light at
	/// \param colour Colour of the light
	/// \param intensity Intensity of the light (0 - 255)
	///
	/////////////////////////////////////////////////
	Light(Vector2f origin, Colour colour, uint8_t intensity);
	
	/////////////////////////////////////////////////
	/// \brief Constructor Base
	///
	/// Creates a light that is attached to an entity
	///
	/// \param origin Position to create light at
	/// \param colour Colour of the light
	/// \param intensity Intensity of the light (0 - 255)
	/// \param target Entity to attach the light to
	///
	/////////////////////////////////////////////////
	Light(Vector2f origin, Colour colour, uint8_t intensity, Entity*  target);
	
	/////////////////////////////////////////////////
	/// \brief Constructor Base
	///
	/// Creates a moving light with a fixed movement speed
	/// 
	/// \param origin Position to create light at
	/// \param colour Colour of the light
	/// \param intensity Intensity of the light (0 - 255)
	/// \param maxSpeed Maximum speed of the moving light
	/// \param target Position in the world that the light will travel to at its maximum speed
	///
	/////////////////////////////////////////////////
	Light(Vector2f origin, Colour colour, uint8_t intensity, float maxSpeed, Vector2f target);
	
	/////////////////////////////////////////////////
	/// \brief Virtual Destructor
	///
	/////////////////////////////////////////////////
	virtual ~Light();

	/////////////////////////////////////////////////
	/// \brief Renders the light to the screen
	///
	/// \param target sf::RenderTarget to draw the light to
	/////////////////////////////////////////////////
	virtual void render(RenderTarget* target);
	
	/////////////////////////////////////////////////
	/// \brief Updates the light
	///
	/// Recalculates the shadow geometry of the light
	/// source if needed, as well as its position in
	/// in case of a moving light
	///
	/// \param stage Current stage in the update loop
	///
	/////////////////////////////////////////////////
	virtual void update(UpdateStage stage);
	
	/////////////////////////////////////////////////
	/// \brief Checks whether the light's shadow geometry need to be updated
	///
	/// \param objects List of scene objects to perform the check with
	///
	/////////////////////////////////////////////////
	virtual bool needsUpdate(vector<Entity*>* objects) = 0;
	
	/////////////////////////////////////////////////
	/// \brief Recalculated the light's shadow geometry
	///
	/// \param objects List of scene objects to include in the calculation
	///
	/////////////////////////////////////////////////
	virtual void updateShadowGeom(vector<Entity*>& objects) = 0;
	
	/////////////////////////////////////////////////
	/// \brief Parse out the light to a file
	///
	/// Allows the parsing of each individual variable
	/// that the light holds to a file for re-loading
	/// at a later date
	///
	/// \param stream Opened file stream for moving data
	/// \param mode Parsing mode (either in or out)
	///
	/////////////////////////////////////////////////
	virtual void parse(fstream& stream, ParseMode mode) = 0;

	////////////////////////////////////////////////////////////
	// Data Retrieval
	////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////
	/// \brief Retrieves the light's colour
	///
	/////////////////////////////////////////////////
	Colour	  getColour();
	
	/////////////////////////////////////////////////
	/// \brief Retrieves the light's intensity
	///
	/////////////////////////////////////////////////
	uint8_t   getIntensity();
	
	/////////////////////////////////////////////////
	/// \brief Retrieves the light's derived type
	///
	/////////////////////////////////////////////////
	LightType getLightType();
	
	/////////////////////////////////////////////////
	/// \brief Checks whether or not the light can cast shadows
	///
	/////////////////////////////////////////////////
	bool	  isShadowCaster();
	
	/////////////////////////////////////////////////
	/// \brief Affects whether or not the light can cast shadows
	///
	/// \param flag True if the light is supposed to produce shadows, false otherwise
	///
	/////////////////////////////////////////////////
	void	  setShadowCaster(bool flag);

protected:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	Vector2f		    mOrigin;		///< The light's position in the world
	VertexArray		    mVertices;		///< The light's light geometry
	vector<VertexArray> mShadowGeoms;	///< The light's shadow geometry
	Transform			mTransform;		///< Holds the light's position, scale and rotation parameters
	Colour				mColour;		///< The colour of the light
	uint8_t				mIntensity;		///< The intensity of the light
	LightType			mLightType;		///< The light's derived type
	bool				mShadowCaster;  ///< Whether or not the light can create shadows

	////////////////////////////////////////////////////////////
	// Dynamic Variables
	////////////////////////////////////////////////////////////
	Entity*  mTarget;	///< An entity the light is attached to
	Vector2f mPoint;	///< A point in the world the light is moving towards
	float    mMaxSpeed; ///< The speed at which the light travels towards mPoint
};

#endif // _LIGHT_H
