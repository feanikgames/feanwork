#ifndef _POINTLIGHT_H
#define _POINTLIGHT_H

/* Author(s): Jamie Massey
 * Date: 22.03.14
 * Filename: PointLight
 * Description: Specify a light with a position and radius, at that point it will
 *				emit a shaft of light in a circle. It can also produce shadow geometry.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Lighting/Light.h"

class FeanExport PointLight : public Light
{
public:
	PointLight() {}
	PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity);
	PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity, Entity* target);
	PointLight(Vector2f origin, Colour colour, float radius, uint8_t intensity, float maxSpeed, Vector2f target);
	~PointLight() {}

	bool needsUpdate(vector<Entity*>* objects) override;
	void updateShadowGeom(vector<Entity*>& objects) override;
	void parse(fstream& stream, ParseMode mode) override;

	void setTarget(Entity* target);
	void setPoint(Vector2f point);

protected:
	void init(Colour colour, float radius, uint8_t intensity);

	float mRadius;
};

#endif // _POINTLIGHT_H
