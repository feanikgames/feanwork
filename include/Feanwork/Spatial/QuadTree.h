#ifndef _QUADTREE_H
#define _QUADTREE_H

/* Author(s): Jamie Massey
 * Date: 28.02.14
 * Filename: QuadTree
 * Description: Quad Trees store information into segments of the map, this can speed
 *              up the process of rendering to the screen, checking intersections between
 *              entities and Artificial Intelligence.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Spatial/SpatialBase.h"

class AABB;
class FeanExport QuadTree : public SpatialBase
{
public:
	QuadTree(AABB* boundary, int32_t nodeCapacity);
	~QuadTree();

	bool			update() override;
	bool 			insert(Entity* entity) override;
	bool			erase(Entity* entity) override { return true; }

	void 			subdivide();
	vector<Entity*> queryRange(AABB range) override;
	void		    debugDraw(RenderTarget* target) override;

protected:
	int32_t         mNodeCapacity;
	AABB*	        mBoundary;
	vector<Entity*> mEntities;
	
	// Children to this QuadTree
    QuadTree* mNorthWest;
    QuadTree* mNorthEast;
    QuadTree* mSouthWest;
    QuadTree* mSouthEast;
};

#endif // _QUADTREE_H
