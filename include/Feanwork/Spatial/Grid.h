/* Author(s): Bruno L�ders, Jamie Massey
 * Date: 28.02.14
 * Filename: Grid
 * Description: Grids store information into segments of the map, this can speed up 
 *              the process of rendering to the screen, checking intersections between
 *              entities and Artificial Intelligence.
 *
 */

#ifndef _GRID_H
#define _GRID_H

#include "Feanwork/Util/Define.h"
#include "Feanwork/Spatial/SpatialBase.h"

/////////////////////////////////////////////////
/// \brief Class for culling calls via a Grid
///
/////////////////////////////////////////////////
class FeanExport Grid : public SpatialBase
{
private:
	/////////////////////////////////////////////////
	// Forward
	/////////////////////////////////////////////////
	struct GridEntity;
	struct CellEntity;
	struct Cell;

public:
	/////////////////////////////////////////////////
	/// \brief Constructor
	/// 
	/// Defines a grid that can be passed to the SceneManager
	///
	/// \param position The position in the world where the grid starts
	/// \param size Amount of cells to create in the grid
	/// \param spacing Size of each cell
	///
	/////////////////////////////////////////////////
	Grid(Vector2f position, Vector2i size, Vector2f spacing);

	/////////////////////////////////////////////////
	/// \brief Destructor
	///
	/// Deletes all the cells from memory
	///
	/////////////////////////////////////////////////
	~Grid();

	/////////////////////////////////////////////////
	/// \brief Updates all entities stored in Grid
	///
	/// Each updated frame the function removes all
	/// entities from the cells and re-inserts them
	/// using their new positions
	///
	/// \return True if grid updated
	///
	/////////////////////////////////////////////////
	bool update() override;

	/////////////////////////////////////////////////
	/// \brief Insert an entity into the grid
	///
	/// Calls down a lower level to the internal function
	/// _insert(entity* ent)
	///
	/// \param entity The entity to be inserted
	/// 
	/// \return True if entity inserted otherwise false
	/// 
	/// \see erase
	///
	/////////////////////////////////////////////////
	bool insert(Entity* entity) override;

	/////////////////////////////////////////////////
	/// \brief Erase an entity from the grid
	///
	/// Using the cell posiiton of an entity this function
	/// will erase it from the grid
	///
	/// \param entity Entity to be erased
	///
	/// \return True if erased successfully otherwise false
	///
	/// \see insert
	///
	/////////////////////////////////////////////////
	bool erase(Entity* entity)  override;

	/////////////////////////////////////////////////
	/// \brief Get a range of entities based on range
	///
	/// Using an AABB the function picks the top-left
	/// coordinate and the top-right coordinate to select
	/// a range of entities that exist in the cells
	///
	/// \param range Axis-Aligned Boundingbox specifying ranges
	///
	/// \return Returns a set of entities to be manipulated
	///
	/// \see getIndexRange
	///
	/////////////////////////////////////////////////
	EntitySet queryRange(AABB range) override;

	/////////////////////////////////////////////////
	/// \brief Get a range of indices based on range
	///
	/// Using an AABB the function picks the top-left
	/// coordinate and the top-right coordinate to select
	/// a range of indices
	/// 
	/// \param range Axis-Aligned Boundingbox specifying ranges
	/// \param min Minimum of the range, result directly stored to variable
	/// \param max Maximum of the range, result directly stored to variable
	///
	/// \return True if it found a range, false if not
	///
	/// \see queryRange
	///
	/////////////////////////////////////////////////
	bool getIndexRange(AABB range, Vector2i* min, Vector2i* max);

	/////////////////////////////////////////////////
	/// \brief Finds the closest cell indices
	///
	/// Using a point on the map (Vector2f) the function
	/// will calculate the closest indices
	///
	/// \param position Point in the game
	///
	/// \return Returns the indices of a cell
	///
	/////////////////////////////////////////////////
	Vector2i posToIndices(Vector2f position);

	/////////////////////////////////////////////////
	/// \brief Find the position of the grid
	///
	/// \return Returns the position of the grid
	///
	/////////////////////////////////////////////////
	Vector2f getPosition();

	/////////////////////////////////////////////////
	/// \brief Find the size of the grid
	///
	/// \return Returns the size of the grid
	///
	/////////////////////////////////////////////////
	Vector2i getSize();

	/////////////////////////////////////////////////
	/// \brief Locate a specific cell
	///
	/// Using indices find a specific cell in the grid
	///
	/// \param x Row coordinate of the cell
	/// \param y Column coordinate of the cell
	///
	/// \return Returns a Cell* all its data
	///
	/////////////////////////////////////////////////
	Cell* getCell(int32_t x, int32_t y);

	/////////////////////////////////////////////////
	/// \brief Draw the grid to the scene
	///
	/// \param target Target of where the debug data will be drawn
	///
	/////////////////////////////////////////////////
	void debugDraw(RenderTarget* target) override;

protected:
	/////////////////////////////////////////////////
	/// \brief Protected and full implementation of insertion
	///
	/// Scans the grid and inserts the entity using
	/// the indices by getIndexRange of the entities
	/// AABB
	///
	/// \param entity Entity to be processed
	///
	/// \return True if entity was inserted
	///
	/////////////////////////////////////////////////
	bool _insert(GridEntity* entity);

protected:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	Vector2f mPosition;  ///< Position of the grid in the world
	Vector2f mSpacing;   ///< Size of each cell to be created
	Vector2i mSize;		 ///< Amount of cells in both directions
	uint32_t mNumCells;  ///< Number of cells in total
	Cell*    mCells;     ///< Array of cells for the grid

	////////////////////////////////////////////////////////////
    // Types
    ////////////////////////////////////////////////////////////
	typedef vector<CellEntity>  CellEntitySet; ///< Vector holding CellEntity
	typedef vector<GridEntity*> GridEntitySet; ///< Vector holding GridEntity*

private:
	////////////////////////////////////////////////////////////
	/// \brief Define a GridEntity that holds an insertion flag
	///
	////////////////////////////////////////////////////////////
	struct GridEntity
	{
		GridEntity(Entity* entity) : ent(entity) {}

		Entity* ent;	  ///< Pointer to entity
		bool	inserted; ///< Insertion flag
	};

	////////////////////////////////////////////////////////////
	/// \brief Define a CellEntity holding a GridEntity and updated flag
	///
	////////////////////////////////////////////////////////////
	struct CellEntity
	{
		CellEntity(GridEntity* entity) : gridEnt(entity) {}

		GridEntity* gridEnt; ///< Pointer to GridEntity
		bool		updated; ///< Updated flag
	};

	////////////////////////////////////////////////////////////
	/// \brief Cell structure holding a set of entities and a pair of indices
	///
	////////////////////////////////////////////////////////////
	struct Cell
	{
		Cell() {}
		Cell(Vector2f pos) : position(pos) {}

		CellEntitySet entities; ///< Vector of stored entities
		Vector2f	  position; ///< Indices of the cell
	};
};

#endif // _GRID_H
