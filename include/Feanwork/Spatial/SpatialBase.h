#ifndef _SPATIALBASE_H
#define _SPATIALBASE_H

/* Author(s): Jamie Massey
 * Date: 28.02.14
 * Filename: SpatialBase
 * Description: Ties any kind of spatial partitioning class to a base class, acting
 *				like an interface and extension for passing to other classes. It 
 *				also hold a spatial type.
 *
 */

#include "Feanwork/Util/Define.h"

enum SpatialType
{
	SpatialType_Undeclared = 0,
	SpatialType_Grid	   = 1, 
	SpatialType_QuadTree   = 2,
};

class FeanExport SpatialBase
{
public:
	SpatialBase();
	SpatialBase(SpatialType type);
	~SpatialBase() {}

	virtual bool update()				= 0;
	virtual bool insert(Entity* entity) = 0;
	virtual bool erase(Entity* entity)  = 0;

	virtual vector<Entity*> queryRange(AABB range) = 0;

	SpatialType getSpatialType();

	virtual void debugDraw(RenderTarget* target) = 0;

protected:
	SpatialType mSpatialType;
};

#endif // _SPATIALBASE_H
