#ifndef _EVENTLISTENER_H
#define _EVENTLISTENER_H

/* Author(s): Jamie Massey
 * Date: 21.01.14
 * Filename: Listener
 * Description: Used to distribute and deal with events being passed between
 *              modules, entities and managers.
 *
 */

#include "Util/Define.h"

struct Collision;
struct ListenerArgs
{
public:
	Vector2f		position;
	Dimension		dimensions;
	Keyboard::Key	keyCode;
	vector<Entity*> collisions;

	FeanExport ListenerArgs(Vector2f pos);
	FeanExport ListenerArgs(Dimension dim);
	FeanExport ListenerArgs(Keyboard::Key key);
	FeanExport ListenerArgs(const vector<Collision*>& cols);
};

class FeanExport EventListener : public IDMask
{
public:
	EventListener();
	virtual ~EventListener() {}

	virtual void tell(EventFlag flags, ListenerArgs* args);
};

#endif // _EVENTLISTENER_H
