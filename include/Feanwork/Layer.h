#ifndef _LAYER_H
#define _LAYER_H

#include "Util/Define.h"
#include "EventListener.h"

/* Author(s): Jamie Massey
 * Date: 02.02.14
 * Filename: Layer
 * Description: Layer class to sort entities, check collision and help the implementation of Parallax Scrolling
 *
 */

class FeanExport Layer : public EventListener, public IDMask
{
public:
	Layer(Scene* scenePtr, float coef = .0f);
	~Layer();

	void update();
	void render(RenderTarget* target);
	void tell(EventFlag flags, ListenerArgs* args) override;

	Entity*			insert(Entity* entity);
	vector<Entity*> insert(vector<Entity*> entities, bool alpha = false);
	Entity*			findEntity(int32_t entityID);

	float			getCoef();
	Vector2f		getPosition();
	vector<Entity*> getEntities();
	void			clearLayer();
	void			setPosition(Vector2f position);

	void	setID(int32_t id);
	int32_t getID();

protected:
	vector<Entity*> mEntities;
	Vector2f		mPosition;
	float		    mCoef;
	Scene*			mScenePtr;
};

#endif // _LAYER_H
