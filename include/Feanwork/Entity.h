/* Author(s): Jamie Massey
 * Date: 06.12.13
 * Filename: Entity
 * Description: Base class for every entity in the game, should hold at least a position (... well that escalated quickly).
 *
 */

#ifndef _ENTITY_H
#define _ENTITY_H

#include <functional>
#include "Feanwork/Util/Define.h"
#include "Feanwork/Util/Interval.h"
#include "Feanwork/EventListener.h"
#include "Feanwork/Parser/Parser.h"

/////////////////////////////////////////////////
/// \brief Base class for every entity in the game
///
/////////////////////////////////////////////////
class FeanExport Entity : public IDMask, public Parser
{
public:
	/////////////////////////////////////////////////
	// Types
	/////////////////////////////////////////////////
	typedef function<void(Entity*)>				 DestroyEvent; ///< Function delegate for when the entity is destroyed
	typedef function<void(Entity*)>				 InputEvent;   ///< Function delegate for input events
	typedef function<void(Entity*, UpdateStage)> UpdateEvent;  ///< Function delegate for an update call

public:
	/////////////////////////////////////////////////
	/// \brief Default Constructor
	///
	/////////////////////////////////////////////////
	Entity();

	/////////////////////////////////////////////////
	/// \Brief Dominant Constructor
	///
	/// Creates an entity using variables passed via
	/// the params, with the option to copy a set of
	/// modules
	///
	/// \param origin Position to create entity at
	/// \param modules Set of modules to create entity with
	/// \param customType Entity type
	///
	/////////////////////////////////////////////////
	Entity(Vector2f origin, ModuleSet* modules = nullptr, uint8_t customType = 0);

	/////////////////////////////////////////////////
	/// \Brief Standard Constructor
	///
	/// Creates an entity using variables passed via
	/// the params
	///
	/// \param origin Position to create entity at
	/// \param customType Entity type
	///
	/////////////////////////////////////////////////
	Entity(Vector2f origin, uint8_t customType);

	/////////////////////////////////////////////////
	/// \brief Virtual Destructor
	///
	/////////////////////////////////////////////////
	virtual ~Entity();

	/////////////////////////////////////////////////
	/// \brief Initialize the debugging quad
	///
	/// \param outline Thickness of the quad outline
	///
	/////////////////////////////////////////////////
	void initDebugQuad(float outline = 1.f);

	/////////////////////////////////////////////////
	/// \brief Initialize the debugging quad
	///
	/// \param colour Colour to set the quad outline
	/// \param outline Thickness of the quad outline
	///
	/////////////////////////////////////////////////
	void initDebugQuad(Colour colour, float outline = 1.f);

	/////////////////////////////////////////////////
	/// \brief Manipulate the quad to a new width and height
	///
	/// \param width New quad width
	/// \param height New quad height
	///
	/////////////////////////////////////////////////
	void manipulateQuad(float width = .0f, float height = .0f);
	
	/////////////////////////////////////////////////
	/// \brief Update the Entity
	///
	/// Each frame when the entity is updated it will go
	/// through three passes of updating; that is pre, current
	/// and post. When passing a function being called inside
	/// update using the stage will specify in which step
	/// it will be executed.
	///
	/// \param stage Current update stage
	///
	/// \return True if entity is active, false if it is inactive or destroyed
	///
	/////////////////////////////////////////////////
	virtual bool update(UpdateStage stage);

	/////////////////////////////////////////////////
	/// \brief Manipulate the quad to a new width and height
	///
	/// \param width New quad width
	/// \param height New quad height
	///
	/////////////////////////////////////////////////
	virtual bool render(RenderTarget* target);

	/////////////////////////////////////////////////
	/// \brief Parse out the Entity to a file
	///
	/// Allows the parsing of each individual variable
	/// that the entity holds to a file for re-loading
	/// at a later date
	///
	/// \param stream Opened file stream for moving data
	/// \param mode Parsing mode (either in or out)
	///
	/// \return True if file was open and parsed succesfully, false if not
	///
	/// \see _parseModules
	///
	/////////////////////////////////////////////////
	bool parse(fstream& stream, ParseMode mode) override;

	/////////////////////////////////////////////////
	/// \brief Adds a listener to the entity
	///
	/// \param listener The listener to be added
	///
	/////////////////////////////////////////////////
	void addListener(EventListener* listener);

	/////////////////////////////////////////////////
	/// \brief Adds a listener to the entity
	///
	/// \param listenerID ID of a listener to be added
	///
	/////////////////////////////////////////////////
	void addListener(int32_t listenerID);

	/////////////////////////////////////////////////
	/// \brief Adds multiple listeners to the entity
	///
	/// \param listenerIDs A vector of int's (IDs)
	///
	/////////////////////////////////////////////////
	void addListeners(vector<int32_t>& listenerIDs);

	/////////////////////////////////////////////////
	/// \brief Add a module to the Module Set
	///
	/// \param module Module to be added
	///
	/////////////////////////////////////////////////
	void addModule(Module* module);

	/////////////////////////////////////////////////
	/// \brief Add an interval to the queue
	///
	/// \param interval Interval to be added
	///
	/// \return Returns a pointer to the added interval
	///
	/////////////////////////////////////////////////
	Interval* addInterval(Interval interval);

	/////////////////////////////////////////////////
	/// \brief Broadcast an event to all modules and listeners
	///
	/// When a broadcast is sent via an entity any listeners
	/// or modules will receieve that event with the flags
	/// and arguments contained inside of it, what it will
	/// then do with that information is up to the module
	/// or listener.
	///
	/// \param flags Combined flags of the event to broadcast
	/// \param args Arguments sent along with event
	///
	/////////////////////////////////////////////////
	void broadcast(EventFlag flags, ListenerArgs* args);

	/////////////////////////////////////////////////
	/// \brief Re-construct the entity using a prefab
	///
	/// \param prefab Prefab template to use
	///
	/////////////////////////////////////////////////
	void applyPrefab(Prefab* prefab);

	/////////////////////////////////////////////////
	/// \brief Destroys all modules associated with this entity
	///
	/////////////////////////////////////////////////
	void destroyModules();

	/////////////////////////////////////////////////
	/// \brief Change the dimensions of the entity
	///
	/// \param width New width of dimensions
	/// \param height New height of dimensions
	///
	/////////////////////////////////////////////////
	void setDimensions(float width, float height);

	/////////////////////////////////////////////////
	/// \brief Add to the position of the entity
	///
	/// Adds to the current position of the entity
	/// and also dispatches a position change event
	/// using the broadcast function
	///
	/// \param num Number to modify position by
	///
	/// \see setPosition
	///
	/////////////////////////////////////////////////
	void addPosition(Vector2f num);

	/////////////////////////////////////////////////
	/// \brief Add to the position of the entity
	///
	/// Adds to the current position of the entity
	/// and also dispatches a position change event
	/// using the broadcast function
	///
	/// \param x Number to modify position by on x-axis
	/// \param y Number to modify position by on y-axis
	///
	/// \see setPosition
	///
	/////////////////////////////////////////////////
	void addPosition(float x, float y);

	/////////////////////////////////////////////////
	/// \brief Add to the position of the entity
	///
	/// Sets the entities current position and also
	/// dispatches a position change event using the
	/// broadcast function
	///
	/// \param num Number to modify position by
	///
	/// \see addPosition
	///
	/////////////////////////////////////////////////
	void setPosition(Vector2f num);

	/////////////////////////////////////////////////
	/// \brief Add to the position of the entity
	///
	/// Sets the entities current position and also
	/// dispatches a position change event using the
	/// broadcast function
	///
	/// \param x Number to modify position by on x-axis
	/// \param y Number to modify position by on y-axis
	///
	/// \see addPosition
	///
	/////////////////////////////////////////////////
	void setPosition(float x, float y);

	/////////////////////////////////////////////////
	/// \brief Set the index layer of which the entity belongs
	///
	/// \param index Layer index
	///
	/////////////////////////////////////////////////
	void setLayerIndex(int32_t index);

	/////////////////////////////////////////////////
	/// \brief Set the custom type of the entity
	///
	/// \param type Type mask (unsigned 8-bit integer)
	///
	/////////////////////////////////////////////////
	void setCustomType(uint8_t type);

	/////////////////////////////////////////////////
	/// \brief Custom user data allocator
	///
	/// \param data Pointer to unspecified data type
	///
	/////////////////////////////////////////////////
	void setUserData(void* data);

	/////////////////////////////////////////////////
	/// \brief Set the colour of an entity
	///
	/// Sets the colour of an entity however it must 
	/// possess a RenderModule to do so
	///
	/// \param colour Colour to change to
	///
	/////////////////////////////////////////////////
	virtual void setColour(Colour colour);

	/////////////////////////////////////////////////
	/// \return The colour of the RenderModule sprite
	///
	/////////////////////////////////////////////////
	virtual Colour getColour();

	/////////////////////////////////////////////////
	/// \return The entities position
	///
	/////////////////////////////////////////////////
	Vector2f getPosition();

	/////////////////////////////////////////////////
	/// \return The entities position
	///
	/// \param edge Specified edge to return
	///
	/////////////////////////////////////////////////
	Vector2f getPosition(Edge edge);

	/////////////////////////////////////////////////
	/// \return Current Dimensions of Entity
	///
	/////////////////////////////////////////////////
	Vector2f getDim();

	/////////////////////////////////////////////////
	/// \return The Dimensions of the entity split in half
	///
	/////////////////////////////////////////////////
	Vector2f getHalfDim();

	/////////////////////////////////////////////////
	/// \return Returns the calculated Boundingbox of the entity
	///
	/////////////////////////////////////////////////
	AABB getAABB();

	/////////////////////////////////////////////////
	/// \brief Iterate through the modules to find a specific one
	///
	/// \param type Module type to search for
	///
	/// \return Returns a module
	///
	/////////////////////////////////////////////////
	Module*	getModule(ModuleType type);

	/////////////////////////////////////////////////
	/// \return The entities position
	///
	/////////////////////////////////////////////////
	int32_t getLayerIndex();

	/////////////////////////////////////////////////
	/// \return Return the current state of the entity
	///
	/////////////////////////////////////////////////
	EntityState getEntityState();

	/////////////////////////////////////////////////
	/// \return Returns the entities custom type (unsigned 8-bit integer)
	///
	/////////////////////////////////////////////////
	uint8_t	getCustomType();

	/////////////////////////////////////////////////
	/// \return Return the user's stored custom data
	///
	/////////////////////////////////////////////////
	void* getUserData();

	/////////////////////////////////////////////////
	/// \return Returns all listeners stored by the Entity
	///
	/////////////////////////////////////////////////
	ListenerSet getListeners();

	/////////////////////////////////////////////////
	/// \return Returns all modules stored by the entity
	///
	/////////////////////////////////////////////////
	ModuleSet getModules();

	/////////////////////////////////////////////////
	/// Get the flag contained in entity that says
	/// whether or not it can display debugging data
	///
	/// \return Returns the flag
	///
	/////////////////////////////////////////////////
	bool canDebug();

	/////////////////////////////////////////////////
	/// \return Returns a flag telling if the entity moved
	///
	/////////////////////////////////////////////////
	bool hasMoved();

	/////////////////////////////////////////////////
	/// \brief Set the unique ID of the Entity
	///
	/////////////////////////////////////////////////
	void setID(int32_t id);

	/////////////////////////////////////////////////
	/// \return Return the unique ID of the entity
	///
	/////////////////////////////////////////////////
	int32_t getID();

	/////////////////////////////////////////////////
	/// \brief Kill the entity
	///
	/////////////////////////////////////////////////
	void kill();

	/////////////////////////////////////////////////
	/// \brief Revive the entity
	///
	/////////////////////////////////////////////////
	void revive();

	/////////////////////////////////////////////////
	/// \brief Destroy the entity
	///
	/// Warning: Once the entity has been destroyed it
	/// cannot be revived and will be removed from memory
	///
	/////////////////////////////////////////////////
	void destroy();

	/////////////////////////////////////////////////
	/// \brief Change to which scene the entity belongs
	///
	/// \param scene Pointer to the scene
	///
	/////////////////////////////////////////////////
	void setScenePtr(Scene* scene);

	/////////////////////////////////////////////////
	/// \return Returns the Scene Pointer
	///
	/////////////////////////////////////////////////
	Scene* getScenePtr();

	/////////////////////////////////////////////////
	/// \return Return the Render Module
	///
	/////////////////////////////////////////////////
	RenderModule* getRenderMod();

	/////////////////////////////////////////////////
	/// \return Return the Animation Module
	///
	/////////////////////////////////////////////////
	AnimationModule* getAnimationMod();

	/////////////////////////////////////////////////
	/// \return Return the Physics Module
	///
	/////////////////////////////////////////////////
	PhysicsModule* getPhysicsMod();

	/////////////////////////////////////////////////
	/// \return Return the Emitter Module
	///
	/////////////////////////////////////////////////
	EmitterModule* getEmitterMod();

	/////////////////////////////////////////////////
	/// \return Return the Intelligence Module
	///
	/////////////////////////////////////////////////
	IntelligenceModule* getIntelMod();

	/////////////////////////////////////////////////
	/// \return Return the Sound Module
	///
	/////////////////////////////////////////////////
	SoundModule* getSoundMod();

	/////////////////////////////////////////////////
	/// \brief Position Traversal Function
	///
	/// Allows the entity to traverse to a position
	/// in a specified amount of time that is counted
	/// by an interval, during that time the interval
	/// will call a callback after each tick
	///
	/// \param traversalTime Time to traverse to position
	/// \param targetPos Destination point in the game
	/// \param subscriber Interval to be called after this one has finished
	///
	/// \return Returns a pointer to the new interval
	///
	/// \see traverseSize, traverseColour, traverseRotation
	///
	/////////////////////////////////////////////////
	Interval* traversePosition(float traversalTime, Vector2f targetPos, Interval* subscriber = nullptr);

	/////////////////////////////////////////////////
	/// \brief Size Traversal Function
	///
	/// Allows the entity to traverse to a size
	/// in a specified amount of time that is counted
	/// by an interval, during that time the interval
	/// will call a callback after each tick
	///
	/// \param traversalTime Time to traverse to size
	/// \param targetSize Size to interpolate to
	/// \param subscriber Interval to be called after this one has finished
	///
	/// \return Returns a pointer to the new interval
	///
	/// \see traversePosition, traverseColour, traverseRotation
	///
	/////////////////////////////////////////////////
	Interval* traverseSize(float traversalTime, Vector2f targetSize, Interval* subscriber = nullptr);

	/////////////////////////////////////////////////
	/// \brief Position Traversal Function
	///
	/// Allows the entity to traverse to a colour
	/// in a specified amount of time that is counted
	/// by an interval, during that time the interval
	/// will call a callback after each tick
	///
	/// \param traversalTime Time to traverse to colour
	/// \param startColour Starting Colour
	/// \param targetColour Colour to interpolate to
	/// \param subscriber Interval to be called after this one has finished
	///
	/// \return Returns a pointer to the new interval
	///
	/// \see traversePosition, traverseSize, traverseRotation
	///
	/////////////////////////////////////////////////
	Interval* traverseColour(float traversalTime, Colour startColour, Colour targetColour, Interval* subscriber = nullptr);

	/////////////////////////////////////////////////
	/// \brief Rotation Traversal Function
	///
	/// Allows the entity to traverse to a rotation
	/// in a specified amount of time that is counted
	/// by an interval, during that time the interval
	/// will call a callback after each tick
	///
	/// \param traversalTime Time to traverse to rotation
	/// \param targetRotation Rotation to interpolate to
	/// \param subscriber Interval to be called after this one has finished
	///
	/// \return Returns a pointer to the new interval
	///
	/// \see traversePosition, traverseSize, traverseColour
	///
	/////////////////////////////////////////////////
	Interval* traverseRotation(float traversalTime, float targetRotation, Interval* subscriber = nullptr);

private:
	/////////////////////////////////////////////////
	// Traversals
	/////////////////////////////////////////////////
	static void  _traversePosition(void* entity, float ratio);
	static void  _traverseSize(void* entity, float ratio);
	static void  _traverseColour(void* entity, float ratio);
	static void  _traverseRotation(void* entity, float ratio);

protected:
	/////////////////////////////////////////////////
	/// \brief Module Parsing Function
	///
	/// Calls all modules and their respective parsing
	/// functions and then re-creates them when parsing
	/// in
	///
	/// \param stream Stream for the file
	/// \param mode Parsing mode (in or out)
	///
	/// \see parse
	///
	/////////////////////////////////////////////////
	virtual void  _parseModules(fstream& stream, ParseMode mode);

public:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	InputEvent   onInput;	  ///< Input Callback
	UpdateEvent  onUpdate;	  ///< Update Callback
	DestroyEvent onDestroyed; ///< Destroy Callback

protected:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	ListenerSet		mListeners;    ///< A set of entities or objects listening to entity
	ModuleSet		mModules;      ///< A set of modules attached to the entity
	IntervalSet		mIntervals;    ///< A set of intervals to update 
	Vector2f		mOrigin;	   ///< The point of the entity in the game
	Dimension		mDimensions;   ///< Dimensions of the entity
	EntityState		mEntityState;  ///< Current state of the entity
	Scene*			mScenePtr;     ///< A pointer to the Scene of which the entity belongs
	int32_t		    mLayerIndex;   ///< Layer index to which the entity belongs
	bool		    mHasMoved;     ///< Movement flag to tell if the entity actually moved last frame
	bool		    mDrawDebug;    ///< Drawing flag for debugging data
	RectangleShape  mQuad;         ///< Quad representing entity boundingbox
	uint8_t			mCustomType;   ///< Custom type of the entity
	void*			mUserData;     ///< Unspecified user data stored with entity

private:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	Vector2f  mStartPos;	   ///< Starting Position
	Vector2f  mTargetPos;	   ///< Target Position
	Vector2f  mStartSize;	   ///< Starting Size
	Vector2f  mTargetSize;	   ///< Target Size
	Colour    mStartColour;	   ///< Starting Colour
	Colour    mTargetColour;   ///< Target Colour
	float	  mStartRotation;  ///< Starting Rotation
	float	  mTargetRotation; ///< Target Rotation
};

#endif // _ENTITY_H
