#ifndef _CONFIGPARSER_H
#define _CONFIGPARSER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: ConfigParser
 * Description: Parse in pure text .cfg files for configuration purposes, this 
 *				can be predefined variables for the framework/game.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Parser.h"

typedef map<string, string> ConfigData;

class FeanExport ConfigParser final : public Parser
{
public:
	ConfigParser(string fileName, ConfigData* cfgData);
	virtual ~ConfigParser() {}

	void  insert(string varName, string value);
	bool  parse(fstream& stream, ParseMode mode) override;

	string	 getRawData(string varName);
	int32_t  getAsInt(string varName);
	float    getAsFloat(string varName);
	bool	 getAsBool(string varName);

protected:
	void  _trimline(string& line);

protected:
	ConfigData*  mDataSet;
};

#endif // _CONFIGPARSER_H
