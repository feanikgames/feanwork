#ifndef _LEVELPARSER_H
#define _LEVELPARSER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: LevelParser
 * Description: Parse whole levels to a file using entities, their modules and
 *				maskIDs for listeners.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Parser.h"

struct LevelData
{
	vector<Layer*> layers;
	vector<Camera*> cameras;
};

class FeanExport LevelParser final : public Parser
{
public:
	LevelParser(string fileName, LevelData* data);
	virtual ~LevelParser() {}

	void	    initLevel(Scene* scene);
	LevelData*  getLevel();

	bool  parse(fstream& stream, ParseMode mode) override;

protected:
	LevelData*  mLevel;
};

#endif // _LEVELPARSER_H
