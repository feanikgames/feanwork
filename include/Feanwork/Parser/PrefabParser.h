#ifndef _PREFABPARSER_H
#define _PREFABPARSER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: PrefabParser
 * Description: Parse in and out whole predefined and entity module structures. This can then 
 *				be stored in prefab structures to be loaded in by entities.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Parser.h"

class FeanExport PrefabParser : public Parser
{
public:
	PrefabParser(string fileName, Prefab* prefab);
	virtual ~PrefabParser() {}

	bool parse(fstream& stream, ParseMode mode) override;

protected:
	Prefab* mPrefab;
};

#endif // _PREFABPARSER_H
