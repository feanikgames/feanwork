#ifndef _ANIMATIONPARSER_H
#define _ANIMATIONPARSER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: AnimationParser
 * Description: Parse Animation frames into a readable file; we can then
 *				use the file to load animations via files rather than
 *				hardcoding frames.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Parser.h"

class FeanExport AnimationParser : public Parser
{
public:
	AnimationParser(string fileName, AnimationSet* anims);
	virtual ~AnimationParser() {}

	bool  parse(fstream& stream, ParseMode mode) override;

protected:
	AnimationSet*  mAnimations;
};

#endif // _ANIMATIONPARSER_H
