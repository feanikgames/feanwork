#ifndef _PARSER_H
#define _PARSER_H

/* Author(s): Jamie Massey
 * Date: 13.05.14
 * Filename: Parser
 * Description: Generic implementation for parsing, by extending this class you can program a new
 *				parser that follows the same convensions as the ones used in the framework or you
 *				can inherit it for use inside a class.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport Parser
{
public:
	Parser();
	virtual ~Parser() {}

	virtual bool  save(string file = "");
	virtual bool  load(string file = "");
	virtual bool  parse(fstream& stream, ParseMode mode) = 0;

	void  setFileName(string fileName);
	void  setDirectory(string directory);

	string  getFileName();
	string  getExtension();
	string  getDirectory();
	string  getFullFile();
	string  getFullPath();

	int32_t  getTotalBytes();
	int32_t  getTotalBytesParsed();

protected:
	void  _setExtension(string extension);

protected:
	string  mFileName;
	string  mExtension;
	string  mDirectory;

	int32_t  mTotalBytes;
	int32_t  mTotalBytesParsed;

	//friend class Entity;
};

#endif // _PARSER_H
