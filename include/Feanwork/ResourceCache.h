#ifndef _RESOURCECACHE_H
#define _RESOURCECACHE_H

/* Author(s): Jamie Massey
 * Date: 22.02.14
 * Filename: ResourceCache
 * Description: Handles memory management of resources such as images & sound files
 *              placing them to an indexed location for the manager. You can then use 
 *              a descriptor to load a file. It checks with the manager to see if the
 *              resource has already been allocated to memory and if so will just assign
 *              that to the specified element.
 *
 */

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>

#include "Feanwork/Util/Exports.h"

using namespace sf;
using namespace std;

// RESOURCE DEFINITION
// -------------------
enum ResourceType 
{
	ResourceType_Texture   = 0,
	ResourceType_Font	   = 1,
	ResourceType_Level	   = 2,
	ResourceType_Audio	   = 3,
	ResourceType_Script	   = 4,
	ResourceType_Animation = 5,
	ResourceType_Config    = 6,
	ResourceType_Shader    = 7,
	ResourceType_Other	   = 8,
};

class FeanExport Resource
{
public:
	Resource(ResourceType type, string path, string name);
	virtual ~Resource() {}

	ResourceType getType() const { return mType;   }
	string		 getName() const { return mName;   }
	bool		 exists()  const { return mExists; }

	virtual bool load() = 0;

protected:
	ResourceType mType;
	bool		 mExists;
	string		 mPath;
	string		 mName;
};

class FeanExport FeanTexture : public Resource, public Texture
{
public:
	FeanTexture(string path, string name);
	~FeanTexture() {}

	bool load();
};

class FeanExport FeanFont : public Resource, public Font
{
public:
	FeanFont(string path, string name);
	~FeanFont() {}

	bool load();
};

class FeanExport FeanConfig : public Resource
{
public:
	FeanConfig(string path, string name);
	~FeanConfig() {}

	bool load();
	string mConfigName;
	map<string, string> mConfigData;
};

class FeanExport FeanSound : public Resource, public Sound
{
public:
	FeanSound(string path, string name);
	~FeanSound() {}
	
	bool load();

	string mSoundName;
};

class FeanExport FeanShader : public Resource, public Shader
{
public:
	FeanShader(string vertPath, string fragPath, string name);
	~FeanShader() {}

	bool load();

	string mFragPath;
	string mFragName;
};

typedef map<string, FeanTexture*>::iterator TextureIt;
typedef map<string, FeanFont*>::iterator    FontIt;
typedef map<string, FeanConfig*>::iterator  ConfigIt;
typedef map<string, FeanSound*>::iterator   SoundIt;
typedef map<string, FeanShader*>::iterator  ShaderIt;
// -------------------

// RESOURCE CACHE
// --------------
struct FragFile
{
	string fragPath;
	string fragName;

	FragFile(string path, string name) :
		fragPath(path),
		fragName(name)
	{
	}
};

typedef vector<string> Directories;

class FeanExport ResourceCache final
{
public:
	static ResourceCache* getInstance();

	FeanTexture* retrieveTexture(string tex);
	FeanFont*	 retrieveFont(string font);
	FeanConfig*  retrieveConfig(string cfg);
	FeanSound*	 retrieveSound(string sound);
	FeanShader*  retrieveShader(string shader);

	void addDirectory(string dir);
	void unloadAllResources();
	void destroy();

	Directories getDirectories();

protected:
	void refreshCache();

private:
	ResourceCache() {}
	~ResourceCache() {}

	map<string, FeanTexture*> mTextureCache;
	map<string, FeanFont*>    mFontCache;
	map<string, FeanConfig*>  mConfigCache;
	map<string, FeanSound*>   mSoundCache;
	map<string, FeanShader*>  mShaderCache;
	Directories			      mDirectories;
};
// --------------

#endif // _RESOURCECACHE_H
