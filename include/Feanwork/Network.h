#ifndef _NETWORK_H
#define _NETWORK_H

#include "Feanwork/Util/Define.h"

enum NetworkType
{
	NetworkType_Client = 0,
	NetworkType_Host
};

class FeanExport Network
{
public:
	static Network* instance();

	void init(NetworkType type, uint16_t port);
	void connect(string ip);
	void sendPacket(uint16_t eventID, Packet* packet);
	void receivePackets();

protected:
	Network() {}
	~Network() {}

	uint16_t	mPort;
	UdpSocket	mSocket;
	NetworkType	mNetworkType;

	vector<IpAddress> mPending;
	vector<IpAddress> mClients;
	vector<Packet>    mReceived;
};

#endif // _NETWORK_H
