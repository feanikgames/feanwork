/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: Scene
 * Description: The scene holds entities, managers and layers so they can be organized
 *				and then rendered and updated by the ScenManager, each scene also has 
 *				an 'active' flag.
 *
 */

#ifndef _SCENE_H
#define _SCENE_H

#include "Feanwork/Util/Define.h"
#include "Feanwork/Experimental/Water.h"

/////////////////////////////////////////////////
/// \brief A Scene holds entities and implements functionality to manage them
///
/////////////////////////////////////////////////
class FeanExport Scene
{
public:
	/////////////////////////////////////////////////
	/// \brief Default Constructor
	///
	/////////////////////////////////////////////////
	Scene();

	/////////////////////////////////////////////////
	/// \brief Virtual Destructor
	///
	/////////////////////////////////////////////////
	virtual ~Scene();

	/////////////////////////////////////////////////
	/// \brief Update the Scene
	///
	/// Every frame the scene should be updated, this will
	/// affect entities, cameras, sound, interface etc. and
	/// anything else that the Scene has to deal with
	///
	/////////////////////////////////////////////////
	virtual void update();

	/////////////////////////////////////////////////
	/// \brief Render the Scene
	///
	/// Each frame draw everything on the screen (entities,
	/// debug information or anything else) to a drawing
	/// buffer which then can be manipulated by a shader.
	///
	/////////////////////////////////////////////////
	virtual void render();

	/////////////////////////////////////////////////
	/// \brief Parse a level from a file into the Scene
	///
	/// The function takes a string which points to the
	/// location of the level you want to load into the Scene
	/// via the Parse class.
	///
	/// \param level Level file location
	///
	/// \return True if Parsing of the level was successful
	///
	/////////////////////////////////////////////////
	virtual bool initLevel(string level);

	/////////////////////////////////////////////////
	/// \brief Initiate the Light Manager
	///
	/////////////////////////////////////////////////
	void startLightManager();

	/////////////////////////////////////////////////
	/// \brief Initiate the Interface Controller
	///
	/////////////////////////////////////////////////
	void startInterfaceController();

	/////////////////////////////////////////////////
	/// \brief Apply a shader to the whole Scene
	///
	/// \param shader Name of the Shader to be applied
	///
	/////////////////////////////////////////////////
	void setShader(string shader);

	/////////////////////////////////////////////////
	/// \brief Add a camera that can manipulate the view of the Scene
	///
	/// \param linkedLayer Pointer to the layer to which it correlates
	///
	/////////////////////////////////////////////////
	void addCamera(Layer* linkedLayer);

	/////////////////////////////////////////////////
	/// \brief Add a new entity to be stored by the Scene
	///
	/// \param layer The layer into which the entity must be inserted
	/// \param entity Pointer to the entity for insertion
	///
	/// \return A pointer to the entity that has just been inserted
	///
	/////////////////////////////////////////////////
	Entity* addEntity(int32_t layer, Entity* entity);

	/////////////////////////////////////////////////
	/// \brief Add a new entity to be stored by the Scene
	///
	/// \param layer The layer into which the entity must be inserted
	/// \param prefab Pointer to the prefab which will be duplicated for insertion
	///
	/// \return A pointer to the entity that has just been inserted
	///
	/////////////////////////////////////////////////
	Entity*	addEntity(int32_t layer, Prefab* prefab);

	/////////////////////////////////////////////////
	/// \brief Add a new entity to be stored by the Scene
	///
	/// \param layer The layer into which the entity must be inserted
	/// \param prefab Pointer to the prefab which will be duplicated for insertion
	/// \param origin New position for the just inserted Entity
	///
	/// \return A pointer to the entity that has just been inserted
	///
	/////////////////////////////////////////////////
	Entity*	addEntity(int32_t layer, Prefab* prefab, Vector2f origin);

	/////////////////////////////////////////////////
	/// \brief Attach another layer to the scene
	///
	/// Adds a new layer to the Scene so that entities now
	/// layer on-top of each other and have a seperate layer
	/// of collision.
	///
	/// \param coef Co-Efficient for the speed of the layer, allowing for parallax scrolling
	///
	/// \return A pointer to the newly created layer
	///
	/////////////////////////////////////////////////
	Layer* addLayer(float coef);

	/////////////////////////////////////////////////
	/// \brief Locate an Entity with a specified ID from all layers
	///
	/// \param ID The id of the Entity
	///
	/// \return The Entity found
	///
	/////////////////////////////////////////////////
	Entity*	findEntity(int32_t ID);

	/////////////////////////////////////////////////
	/// \brief Locate a specified Layer with an ID
	///
	/// \param ID The id of the Layer
	///
	/// \return The Layer found
	///
	/////////////////////////////////////////////////
	Layer* findLayer(int32_t ID);

	/////////////////////////////////////////////////
	/// \brief Locate a Camera with a specified ID
	///
	/// \param ID The id of the Camera
	///
	/// \return The Camera found
	///
	/////////////////////////////////////////////////
	Camera*	findCamera(int32_t ID);

	/////////////////////////////////////////////////
	/// \brief Locate a EventListener with a specified ID
	///
	/// \param ID The id of the EventListener
	///
	/// \return The EventListener found
	///
	/////////////////////////////////////////////////
	virtual EventListener* findListener(int32_t ID);
	
	/////////////////////////////////////////////////
	/// \brief Check to make sure required external components are initialized
	///
	/// \return Result of the check
	///
	/////////////////////////////////////////////////
	virtual bool check();

	/////////////////////////////////////////////////
	/// \return Returns true if the Scene is active
	///
	/////////////////////////////////////////////////
	bool isActive();

	/////////////////////////////////////////////////
	/// \return Vector of Layers associated with the Scene
	///
	/////////////////////////////////////////////////
	vector<Layer*> getLayers();

	/////////////////////////////////////////////////
	/// \return Vector of Entities associated with the Scene
	///
	/////////////////////////////////////////////////
	vector<Entity*> getAllEntities();

	/////////////////////////////////////////////////
	/// \return Pointer to the SoundManager
	///
	/////////////////////////////////////////////////
	SoundManager* getSoundMgr();

	/////////////////////////////////////////////////
	/// \return Pointer to the LightManager
	///
	/////////////////////////////////////////////////
	LightManager* getLightMgr();

	/////////////////////////////////////////////////
	/// \return Pointer to the InterfaceController
	///
	/////////////////////////////////////////////////
	InterfaceController* getInterface();

	/////////////////////////////////////////////////
	/// \return Pointer to the SpatialBase
	///
	/////////////////////////////////////////////////
	SpatialBase* getSpatial();

	/////////////////////////////////////////////////
	/// \param index Index of the Camera
	///
	/// \return Returns a pointer to the found Camera
	///
	/////////////////////////////////////////////////
	Camera*	getCamera(int32_t index);

	/////////////////////////////////////////////////
	/// \return Returns a pointer to the active Camera
	///
	/////////////////////////////////////////////////
	Camera* getActiveCamera();

	/////////////////////////////////////////////////
	/// \return Returns the first Camera in the Vector
	///
	/////////////////////////////////////////////////
	Camera* getFirstCamera();

	/////////////////////////////////////////////////
	/// \return Returns the last Camera in the Vector
	///
	/////////////////////////////////////////////////
	Camera* getLastCamera();

protected:
	////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
	SpatialBase*		 mSpatial;		 ///< Spatial Management for Scene Optimization (must exist)
	LightManager*		 mLightMgr;		 ///< Light Manager to add lights for the Scene
	SoundManager*		 mSoundMgr;		 ///< Manages Sound emitting in the Scene
	InterfaceController* mInterface;	 ///< Management of Interface input and interaction
	vector<Camera*>		 mCameras;		 ///< Vector of Cameras to manipulate the Scene view (must have at least one)
	vector<Layer*>		 mLayers;		 ///< Vector of Layers for entity management and Parallax Scrolling
	RenderTexture*		 mBuffer;		 ///< Render buffer for pre-drawing the Scene and applying shaders
	uint32_t			 mLastID;		 ///< Reference to the last ID to be used and allocated
	uint32_t			 mCurrentCamera; ///< Reference to the current Camera in use
	bool				 mActive;		 ///< Flag for whether the Scene is active
	Shader*				 mScreenShader;  ///< Storage for the Scene shader
	Water*				 mWaterTest;	 ///< Experimental Class
};

#endif // _SCENE_H
