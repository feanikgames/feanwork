#ifndef _SOUNDMANAGER_H
#define _SOUNDMANAGER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: SoundManager
 * Description: Contains and controls all sounds in the game, it has three primary slots; the first is
 *				for music, the second for ambiance and the third is a list of SoundFX to be played.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport SoundManager
{
public:
	SoundManager();
	~SoundManager();

	void update(UpdateStage stage);

	// Music Controllers
	void setMusic(string file);
	void setAmbiance(string file);

	void playAmbiance();
	void pauseAmbiance();
	void stopAmbiance();

	void playMusic();
	void pauseMusic();
	void stopMusic();

	// Sound FX Controllers
	void addSoundFX(Sound* sound, Vector2f position, float offset = 0.f);
	void removeSoundFX(Sound* sound);
	void removeSoundFX(int32_t index);

	// Grouped functionality
	void playAll();
	void pauseAll();
	void stopAll();
	void clearSounds();

	// Variable Manipulation
	void changeVolume(int32_t soundIndex, float volume);
	void changeVolume(Sound* sound, float volume);
	void changePitch(int32_t soundIndex, float pitch);
	void changePitch(Sound* sound, float pitch);

protected:
	vector<Sound*> mSounds;
	Music		   mMusic;
	Music		   mAmbiance;
};

#endif // _SOUNDMANAGER_H
