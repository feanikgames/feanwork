#ifndef _SCENEMANAGER_H
#define _SCENEMANAGER_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: SceneManager
 * Description: The SceneManager contains all existing scenes and handles them accordingly, it also
 *				has a transitional mask, the time delta and attributes of the window.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/IntelligenceModule.h"

class FeanExport SceneManager final
{
public:
	static SceneManager* getInstance();
	
	void init(string gameConfig);
	void setIcon(string icon);
	void run();
	void destroy();

	void		  resetAlpha();
	RenderWindow* getScreen();
	uint32_t 	  getWidth();
	uint32_t	  getHeight();

	vector<Layer*> getLayers();
	Scene*		   getScene(int32_t index);
	float		   getDelta();

	void toDefault();

private:
	vector<Scene*> mScenes;
	RenderWindow*  mWindow;

	bool		   mRunning;
	uint32_t	   mWidth;
	uint32_t	   mHeight;
	string		   mName;

	AStar*			aStar;
	SimpleNavGraph* navGraph;

	// Timed
	Time	  mDelta;
	sf::Clock mDeltaClock;

	SceneManager();
	~SceneManager();
};

#endif // _SCENEMANAGER_H
