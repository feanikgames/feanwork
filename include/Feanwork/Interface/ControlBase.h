#ifndef _CONTROLBASE_H
#define _CONTROLBASE_H

/* Author(s): Jamie Massey
 * Date: 09.02.14
 * Filename: ControlBase
 * Description: Base class for user interface elements
 */

/* TODO of what is not working and features
 * 1) Intersections don't work properly on DropControls
 * 2) Can select two drop fields at once (weird)
 * 3) Text alignment in drop fields (related to intersection issues)
 * 4) Need to implement text fields
 * 5) Add masking functionaility to buttons
 */

#include <functional>
#include "Feanwork/Util/Define.h"
#include "Feanwork/Entity.h"

// Delegate Definitions
typedef function<void(MouseButtonEventArgs)> MouseEvent;
typedef function<void(MouseMoveEventArgs)>   MouseMoveEvent;
typedef function<void(MouseWheelEventArgs)>  MouseWheelEvent;
typedef function<void(KeyEventArgs)>		 KeyboardEvent;
typedef function<void(TextEventArgs)>		 TextEntered;
typedef function<void()>					 ScrollEvent;
typedef function<void()>					 CompleteEvent;

enum ControlType
{
	ControlType_Default	  = 0,
	ControlType_Label	  = 1,
	ControlType_Drop	  = 2,
	ControlType_Field	  = 3,
	ControlType_Progress  = 4, 
	ControlType_Scroll	  = 5,
	ControlType_Selection = 6,
	ControlType_Toggle	  = 7,
	ControlType_Tooltip   = 8,
};

class FeanExport ControlBase : public Entity
{
public:
	ControlBase();
	ControlBase(Vector2f relativePos, string resource, Container* parent, ControlType type = ControlType_Default);
	~ControlBase();

	void		 init(Vector2f relativePos, string resource, Container* parent, ControlType type);
	virtual bool intersects(Vector2i num);
	virtual bool render(RenderTarget* target) override;

	bool		 quickIntersect(Vector2i mouse, Vector2f pos, Vector2u dim);
	ControlType	 getControlType();

	void callPressed(		MouseButtonEventArgs args);
	void callDoublePressed( MouseButtonEventArgs args);
	void callReleased(	    MouseButtonEventArgs args);
	void callMiddlePressed( MouseWheelEventArgs  args);
	void callWheelMoved(    MouseWheelEventArgs  args);
	void callKeyPressed(	KeyEventArgs		 args);
	void callKeyReleased(	KeyEventArgs		 args);
	void callMouseEntered(	MouseMoveEventArgs   args);
	void callMouseExited(	MouseMoveEventArgs	 args);
	void callMouseMoved(	MouseMoveEventArgs   args);
	void callTextEntered(	TextEventArgs		 args);

	// Function Callbacks Storage
	vector<MouseEvent>		onPressed;
	vector<MouseEvent>		onDoublePressed;
	vector<MouseEvent>		onReleased;
	vector<MouseWheelEvent> onMiddlePressed;
	vector<MouseWheelEvent> onWheelMoved;
	vector<KeyboardEvent>	onKeyPressed;
	vector<KeyboardEvent>	onKeyReleased;
	vector<MouseMoveEvent>	onMouseEnter;
	vector<MouseMoveEvent>	onMouseExit;
	vector<MouseMoveEvent>	onMouseMoved;
	vector<TextEntered>		onTextEntered;

protected:
	vector<Entity*> mLinked;
	Vector2f		mRelativePosition;
	Container*		mParent;
	ControlType		mControlType;
};

#endif // _CONTROLBASE_H
