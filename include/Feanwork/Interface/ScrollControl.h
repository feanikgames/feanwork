#ifndef _SCROLLCONTROL_H
#define _SCROLLCONTROL_H

/* Author(s): Bruno Lüders
 * Date: 15.02.14
 * Filename: ScrollControl
 * Description: IT'S A SCROLL BAR, WHAT'S TO EXPLAIN???
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

enum ScrollAxis
{
	ScrollAxis_Vertical   = 0,
	ScrollAxis_Horizontal = 1,
};

class FeanExport ScrollControl : public ControlBase
{
public:
	ScrollControl(Vector2f relativePos, string barImg, string thumbImg, string arrowImg, float arrowStep, ScrollAxis scrollAxis, Container* parent);
	~ScrollControl();

	bool update(UpdateStage stage)    override;
	bool render(RenderTarget* target) override;

	void setThumbPos(float position);

	// Event Callback Impl
	void dropThumb(MouseButtonEventArgs args);
	void dragThumb(MouseButtonEventArgs args);
	void mouseMove(MouseMoveEventArgs args);
	void arrowUp(MouseButtonEventArgs args);
	void arrowDown(MouseButtonEventArgs args);

	// Data Retrieval
	float    getThumbPos();
	Vector2f getScrollAxis();

protected:
	ScrollEvent  onScrollMoved;
	ControlBase* mThumb;
	ControlBase* mArrowUp;
	ControlBase* mArrowDown;

	float	 mThumbPos;
	Vector2f mScrollAxis;
	float	 mArrowStep;

	bool	 mThumbDrag;
	Vector2f mMinThumb, mMaxThumb;
};

#endif // _SCROLLCONTROL_H
