#ifndef _FIELDCONTROL_H
#define _FIELDCONTROL_H

/* Author(s): Jamie Massey
 * Date: 15.02.14
 * Filename: FieldControl
 * Description: Extension on Control class that allows you to write text to a field
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class FeanExport FieldControl : public ControlBase
{
public:
	FieldControl(Vector2f relativePos, string frameImg, Container* parent, bool multiLine = false);
	~FieldControl();

	bool render(RenderTarget* target) override;
	void updateLines();
	bool isEmpty();

	void createScrollV(string barImg, string thumbImg, string arrowImg, float arrowStep);
	void createScrollH(string barImg, string thumbImg, string arrowImg, float arrowStep);

	// Event Callback Impl
	void entered(TextEventArgs args);
	void pressed(MouseButtonEventArgs args);

protected:
	string remove(string str, uint32_t position);
	string substr(string str, uint32_t position);
	string insert(Text& to, string from, uint32_t position);

	vector<Text*>  mLines;
	ScrollControl* mScrollV;
	ScrollControl* mScrollH;

	bool mCanScrollV;
	bool mCanScrollH;
	bool mMultiLine;

	uint32_t mMinRender;
	uint32_t mRenderRange;
	float	 mSeperation;

	Vector2i	mCursorPos;
	VertexArray mIndicator;
};

#endif // _FIELDCONTROL_H
