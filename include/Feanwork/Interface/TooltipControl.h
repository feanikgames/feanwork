#ifndef _TOOLTIPCONTROL_H
#define _TOOLTIPCONTROL_H

/* Author(s): Jamie Massey
 * Date: 15.02.14
 * Filename: TooltipControl
 * Description: Extension on Control class that allows tooltips
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class FeanExport TooltipControl : public ControlBase
{
public:
	TooltipControl(Vector2f relativePos, string frameImg, string desc, uint32_t size, Container* parent, bool hidden = true);
	~TooltipControl();

	bool render(RenderTarget* target) override;
	void setDescription(string text);
	void scaleFrame(float factorX, float factorY);

	// Data Retrieval
	void   setColour(Colour colour);
	void   setHidden(bool hidden);
	void   show();
	bool   isHidden();
	string getDescription();

protected:
	Text* mDescription;
	bool  mHidden;
};

#endif // _TOOLTIPCONTROL_H
