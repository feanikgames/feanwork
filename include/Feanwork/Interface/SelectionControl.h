#ifndef _SELECTIONCONTROL_H
#define _SELECTIONCONTROL_H

/* Author(s): Jamie Massey
 * Date: 13.02.14
 * Filename: SelectionControl
 * Description: Extension on Control class that allows selection of items
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

enum SelectionType
{
	SelectionType_Radio	   = 0,
	SelectionType_Checkbox = 1,
};

class FeanExport SelectionControl : public ControlBase
{
public:
	SelectionControl(Vector2f relativePos, string selectedImg, string defaultImg, SelectionType type, Container* parent);
	~SelectionControl();

	bool render(RenderTarget* target) override;
	void calculateBounds();

	void addOption(string text);
	void selectOption(MouseButtonEventArgs args);

	// Data Retrieval
	int			  getCurrentSelection();
	SelectionType getSelectionType();

private:
	vector<Text*>	mText;
	vector<Sprite*> mSelected;

	SelectionType mSelectionType;
	FeanTexture*  mSelectedTex;
	FeanTexture*  mDefaultTex;

	int32_t		 mCurrentSelection;
	vector<bool> mCheckState;
};

#endif // _SELECTIONCONTROL_H
