#ifndef _CONTAINER_H
#define _CONTAINER_H

/* Author(s): Jamie Massey
 * Date: 09.02.14
 * Filename: Container
 * Description: Containers group together generic controls for position placement 
 *              and grouped intersecting calls.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport Container
{
public:
	Container();
	Container(Vector2f position, int width = 0, int height = 0);
	~Container();

	void update(UpdateStage stage);
	void render(RenderTarget* target);
	bool intersects(Vector2i mousePosition);

	void	 setAbsPosition(Vector2f position);
	Vector2f getAbsPosition();
	void	 insert(ControlBase* control);
	void	 fitControls();

	// Data Retrieval
	ControlBase*		 getControl(int index);
	vector<ControlBase*> getControls();
	Dimension			 getDimensions();

protected:
	vector<ControlBase*> mControls;
	Vector2f			 mAbsPosition;
	Dimension			 mDimensions;
	RectangleShape		 mDebugShape;
};

#endif // _CONTAINER_H
