#ifndef _LABELCONTROL_H
#define _LABELCONTROL_H

/* Author(s): Jamie Massey
 * Date: 14.02.14
 * Filename: LabelControl
 * Description: Extension on Control class that allows text placement on-screen
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class FeanExport LabelControl : public ControlBase
{
public:
	LabelControl(Vector2f relativePos, string text, Container* parent, uint32_t size = 16);
	~LabelControl();

	bool   render(RenderTarget* target) override;
	void   setText(string text);
	string getText();

protected:
	Text* mText;
};

#endif // _LABELCONTROL_H
