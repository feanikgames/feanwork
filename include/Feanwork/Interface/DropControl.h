#ifndef _DROPCONTROL_H
#define _DROPCONTROL_H

/* Author(s): Jamie Massey
 * Date: 15.02.14
 * Filename: DropControl
 * Description: Extension on Control class that allows items in a list
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class DropField;

class FeanExport DropControl : public ControlBase
{
public:
	DropControl(Vector2f relativePos, string fieldImg, string maskImg, string selectionImg, string dropImg, Container* parent);
	~DropControl();

	bool render(RenderTarget* target) override;
	void addField(string text, uint32_t size);
	void setSelection(uint32_t selection);

	// Event Callback Impl
	void pressed(MouseButtonEventArgs args);
	void moving(MouseMoveEventArgs args);
	void hide(MouseMoveEventArgs args);

	// Data Retrieval
	bool    isListActive();
	int32_t getCurrentSelection();
	int32_t getLastSelection();
	float   getLargestWidth();

protected:
	vector<DropField*> mFields;
	uint32_t		   mCurrentSelection;
	uint32_t		   mLastSelection;
	bool			   mListActive;
	float			   mLargestWidth;

	Text*   mCurrentText;
	Sprite* mDropSprite;

	FeanTexture* mDropTex;
	FeanTexture* mFieldTex;
	FeanTexture* mMaskTex;
};

class FeanExport DropField
{
public:
	DropField(Vector2f position, FeanTexture* fieldImg, FeanTexture* maskImg, string text, uint32_t size);
	~DropField();

	void render(RenderTarget* target);
	void scaleX(float factor);

	Vector2f getGfxSize();
	Sprite*  getField();
	Text*    getText();
	Vector2f getFieldPosition();

	void  setWidthFactor(float factor);
	float getTextWidth();
	float getWidthFactor();


	void setPosition(Vector2f position);
	void setDrawMask(bool doesMask);

	// Event Callback Impl
	void enterCallback(MouseMoveEventArgs args);
	void exitCallback(MouseMoveEventArgs args);

protected:
	MouseMoveEvent whenEntered;
	MouseMoveEvent whenExited;

	Text* mText;
	float mTextWidth;
	float mWidthFactor;

	Vector2f mFieldPosition;
	Sprite*  mField;
	Sprite*  mSelectionMask;
	bool	 mDrawMask;
};

#endif // _DROPCONTROL_H
