#ifndef _TOGGLECONTROL_H
#define _TOGGLECONTROL_H

/* Author(s): Jamie Massey
 * Date: 15.02.14
 * Filename: ToggleControl
 * Description: Extension on Control class that holds a flag to toggle a button
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class FeanExport ToggleControl : public ControlBase
{
public:
	ToggleControl(Vector2f relativePos, string activeImg, string hoverImg, string defaultImg, Container* parent, bool canHover = true);
	~ToggleControl();

	bool render(RenderTarget* target) override;
	
	// Callback Impl
	void switchToggle(MouseButtonEventArgs args);
	void enter(MouseMoveEventArgs args);
	void exit(MouseMoveEventArgs args);

	// Data Retrieval
	bool isActive();
	bool canHover();
	void setHoverFlag(bool hover);

protected:
	bool mActive;
	bool mHovering;
	bool mAllowHover;

	Sprite* mHoveringSprite;
	FeanTexture* mHoverTex;
	FeanTexture* mActiveTex;
	FeanTexture* mDefaultTex;
};

#endif // _TOGGLECONTROL_H
