#ifndef _PROGRESSCONTROL_H
#define _PROGRESSCONTROL_H

/* Author(s): Jamie Massey
 * Date: 15.02.14
 * Filename: ProgressControl
 * Description: Extension on Control class that visually displays the progress by clipping the rectangles of a bar
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Interface/ControlBase.h"

class FeanExport ProgressControl : public ControlBase
{
public:
	ProgressControl(Vector2f relativePos, string barImg, Container* parent, uint32_t maxValue = 0);
	~ProgressControl();

	void setPercentage(uint8_t percentage);
	void incrementPercentage(uint8_t percentage);
	void reset();

	// Callback Impl
	void pressed(MouseButtonEventArgs args);

	// Data Retrieval
	bool isComplete();
	void callComplete();

protected:
	float mHeight;
	float mWidth;
	float mMaxValue;

	uint32_t	  mCurrent;
	bool		  mCompleteProgress;
	CompleteEvent onComplete;
};

#endif // _PROGRESSCONTROL_H
