#ifndef _INTERFACECONTROLLER_H
#define _INTERFACECONTROLLER_H

/* Author(s): Jamie Massey
 * Date: 11.02.14
 * Filename: InterfaceController
 * Description: Windows group together generic controls for position placement and
 *              grouped intersecting calls.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport InterfaceController
{
public:
	InterfaceController(float pressTimeout = 150.f);
	~InterfaceController();

	//void init();
	void update();
	void render(RenderTarget* target);
	void focus(Vector2i mouse);

	void	   addContainer(Container* container);
	Container* getListener(uint32_t index);

protected:
	vector<Container*> mContainers;
	ControlBase*	   mFocusedControl;
	ControlBase*	   mLastFocused;

	float	 mAccum;
	float	 mPressTimeout;
	Vector2i mLastMousePos;

	bool mLeftMouseDown;
	bool mRightMouseDown;
};

#endif // _INTERFACECONTROLLER_H
