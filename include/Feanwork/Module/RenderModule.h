#ifndef _RENDERMODULE_H
#define _RENDERMODULE_H

/* Author(s): Jamie Massey
 * Date: 06.12.13
 * Filename: RenderModule
 * Description: Rendering based module that uses SFML and Entity to draw on the screen
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"

class FeanExport RenderModule : public Module, public EventListener, public Parser
{
public:
	RenderModule(const RenderModule& module);
	RenderModule(Entity* parent = nullptr);
	RenderModule(string texture, Entity* parent = nullptr);
	~RenderModule();

	void	render(RenderTarget* target)			  override;
	void	update(UpdateStage stage)				  override;
	void	tell(EventFlag flags, ListenerArgs* args) override;
	Module* duplicate()								  override;
	bool    parse(fstream& stream, ParseMode mode)    override;

	void clipRect(Vector2f dimensions);
	void clipRect(Vector2f dimensions, Edge edge);
	void clipRect(Vector2f relativePos, Vector2f dimensions);
	void setParent(Entity* parent) override;

	void loadGraphic(string texture, string shader = "");
	void loadGraphic(FeanTexture* texture, string shader = "");

	// Data Retrieval Functions
	Sprite*		 getGraphic();
	FeanTexture* getTexture();
	FeanShader*  getShader();

protected:
	Sprite*		 mGraphic;
	FeanTexture* mTexture;
	FeanShader*  mShader;

	// Name of resources loaded
	string		 mShaderName;
	string		 mGraphicName;
};

#endif // _RENDERMODULE_H
