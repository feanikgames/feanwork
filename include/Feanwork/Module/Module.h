#ifndef _MODULE_H
#define _MODULE_H

/* Author(s): Jamie Massey
 * Date: 06.12.13
 * Filename: Module
 * Description: Base class for Module. It stores an enum for the type of module it is #
				and has extensive functions such as render, update, tell, parse and duplicate
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/EventListener.h"
#include "Feanwork/Parser/Parser.h"

class FeanExport Module : public Parser
{
public:
	Module(ModuleType type, Entity* parent = nullptr);
	virtual ~Module();

	virtual void	render(RenderTarget* target);
	virtual void	update(UpdateStage stage);
	virtual void	tell(EventFlag flags, ListenerArgs* args);
	virtual void    tellNetwork(Packet* object);
	virtual void	setParent(Entity* parent);
	virtual Module* duplicate() = 0;

	ModuleType getType();
	Entity*    getParent();

protected:
	ModuleType mType;
	Entity*	   mParent;

	friend class Entity;
};

#endif // _MODULE_H
