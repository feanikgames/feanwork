#ifndef _INTELLIGENCEMODULE_H
#define _INTELLIGENCEMODULE_H

/* Author(s): Jamie Massey
 * Date: 16.02.14
 * Filename: IntelligenceModule
 * Description: The intelligence module takes care of obstacle avoidance, pathfinding and finite state machines
 *              for logical behviours inside the game. All functionality in this module will be wrapped up into
 *              Feanscript for quick access and controlling of entities giving the illusion of intelligence.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"

// Forward
class NavNode;
class NavEdge;
class NavGraph;
class AStar;
class Path;

struct AIGroup
{
	vector<Entity*> mNeighbours;
	float			mRadiusSq;

	FeanExport bool isWithinRadius(Entity* entityA, Entity* entityB);
};

// -- IntelligenceModule Impl --
// =============================
class FeanExport IntelligenceModule : public Module, public Parser
{
public:
	IntelligenceModule(const IntelligenceModule& module);
	IntelligenceModule(Entity* parent = nullptr);
	~IntelligenceModule();

	void	update(UpdateStage stage)			   override;
	Module* duplicate()							   override;
	bool    parse(fstream& stream, ParseMode mode) override;

	Vector2f calculate();
	Vector2f getHidingPosition(Vector2f positionOb, float radiusOb, Vector2f positionTar);

	// Steering Behaviours
	Vector2f seek(Vector2f target);
	Vector2f flee(Vector2f target);
	Vector2f pursue(Entity* target);
	Vector2f evade(Entity* target);
	Vector2f arrive(Vector2f target, float decleration);
	Vector2f wander();
	Vector2f interpose(Entity* entityA, Entity* entityB);
	Vector2f hide(Entity* target, vector<Entity*>& obstacles);

	// Grouped Behaviours
	Vector2f separation();
	Vector2f alignment();
	Vector2f cohesion();

	// Functionality
	Vector2f avoidObstacles(vector<Entity*>& entities);
	Vector2f followPath();

protected:
	Vector2f mWanderTarget;
	float	 mMaxWanderOffset;
	float	 mWanderRadius;
	float	 mWanderDist;
	float	 mObstAvoidDist;
	float	 mPanicDistSq;

	AIGroup* mGroup;
	Path*	 mPath;
	float	 mNodeDistSq;

	// TODO: Change this with scritping
	Entity*			mTarget;
	vector<Entity*> mObstacles;
};

// -- Path Class Impl --
// =====================
enum PathMode
{
	PathMode_OneWay = 0,
	PathMode_Patrol = 1,
	PathMode_Loop   = 2,
};

class FeanExport Path
{
public:
	Path();
	virtual ~Path() {}

	void	 nextNode();
	NavNode* getCurrentNode();

	bool			  isFinished();
	vector<NavNode*>& getNodes();

protected:
	vector<NavNode*> mNodes;
	PathMode		 mMode;
	int32_t			 mCurIndex;
	int32_t			 mDirection;
	bool			 mFinished;
};

// -- A* Pathfinding Impl --
// =========================
class FeanExport AStar
{
public:
	AStar(NavGraph* graph);
	~AStar();

	Path* findPath(int32_t start, int32_t target);
	bool  compareNodes(int32_t& a, int32_t& b);

	void			 setGraph(NavGraph* graph);
	void			 debugRender(RenderTarget* target);
	vector<NavNode*> getPath() const;

protected:
	float*			 mGCosts;
	float*			 mFCosts;
	NavGraph*		 mGraph;
	vector<NavNode*> mSPT;
	Path*			 mPath;
};

// -- Abstract Navigation Graph Base --
// ====================================
class FeanExport NavGraph
{
public:
	virtual ~NavGraph();
	virtual int32_t getClosestNode(Vector2f position) = 0;
	uint32_t		numNodes();

	uint32_t mNodeCount;
	NavNode* mNodes;
};

class FeanExport SimpleNavGraph : public NavGraph
{
public:
	SimpleNavGraph(int width, int height, float spacing, Vector2f origin);
	~SimpleNavGraph();

	int32_t  getClosestNode(Vector2f position);
	void     debugRender(RenderTarget* target);

protected:
	int32_t	 mWidth;
	int32_t  mHeight;
	Vector2f mTopleft;
	float	 mSpacing;
};

// -- Navigation Node Impl --
// ==========================
class FeanExport NavNode
{
public:
	NavNode();
	NavNode(Vector2f position);
	~NavNode();

	// Data Retrieval
	vector<NavEdge*>& getEdges();
	Vector2f		  getPosition();
	void			  setPosition(Vector2f position);
	void			  mergeNodes(vector<NavEdge*>& edges);

protected:
	vector<NavEdge*> mEdges;
	Vector2f		 mPosition;
};

class FeanExport NavEdge
{
public:
	NavEdge(int32_t from, int32_t to, float cost = .0f);
	~NavEdge();

	// Data Retrieval
	void  setCost(float cost);
	float getCost();

	int32_t mFrom;
	int32_t mTo;

protected:
	float	mCost;
};

#endif // _INTELLIGENCEMODULE_H
