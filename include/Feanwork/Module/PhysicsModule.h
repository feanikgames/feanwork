#ifndef _PHYSICSMODULE_H
#define _PHYSICSMODULE_H

/* Author(s): Aaron Smith, Jamie Massey
 * Date: 11.12.13
 * Filename: PhysicsModule
 * Description: Physics module can be attached to any entity in the game and then 
 *              manipulate it
 *
 */

#include <functional>
#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"

typedef function<bool(Entity* caller, vector<Collision*>& collisions)> CollisionEvent;

// Forward
struct Ray;
struct RayCollision;
struct Collision;
struct PhysicsMaterial;

// Definitions
enum PhysicsType
{
	PhysicsType_Static  = 0,
	PhysicsType_Dynamic = 1,
	PhysicsType_Actor   = 2,
	PhysicsType_Slope   = 3,
	PhysicsType_Ladder  = 4,
};

enum Direction
{
	Direction_None	 = 0,
	Direction_Left	 = 1,
	Direction_Right  = 2,
	Direction_Top	 = 3,
	Direction_Bottom = 4,
};

struct Collision
{
	Entity*  collider;
	Entity*  target;
	Vector2f normal;
	float	 overlap;

	Collision()
	{
		collider = nullptr;
		target   = nullptr;
		overlap  = .0f;
	}
};

struct Ray
{
	Vector2f mDirection;
	Vector2f mOrigin;
	float	 mLength;
	uint32_t mBitmaskGroup;

	Ray() :
		mLength(0.f), mOrigin(Vector2f()), mDirection(Vector2f()), mBitmaskGroup(-1)
	{
	}

	Ray(Vector2f origin, Vector2f direction, float length, uint32_t maskGroup = 1)
	{
		mOrigin	      = origin;
		mDirection    = direction;
		mLength       = length;
		mBitmaskGroup = maskGroup;
	}

	Ray(Vector2f from, Vector2f to, uint32_t maskGroup = 1)
	{
		mDirection    = (to - from);
		mLength       = Vectorial::vectorLengthF(mDirection);
		mDirection   /= mLength;
		mOrigin		  = from;
		mBitmaskGroup = maskGroup;
	}
};

struct RayCollision
{
	Ray		 ray;
	Entity*  target;
	Vector2f pointA;
	Vector2f pointB;
	float	 factorA;
	float	 factorB;

	RayCollision() {}
};

struct PhysicsMaterial
{
	float restitution;
	float staticFriction;
	float dynamicFriction;

	PhysicsMaterial()
	{
		restitution		= 0.01f;
		staticFriction  = 0.8f;
		dynamicFriction = 0.6f;
	}
};

// Impl
class FeanExport PhysicsModule : public Module, public Parser
{
public:
	PhysicsModule(const PhysicsModule& module);
	PhysicsModule(Entity* parent = nullptr);
	PhysicsModule(PhysicsType physicsType, Entity* parent = nullptr);
	~PhysicsModule();

	void initVars(float maxSpeed, float dynamicFriction, float staticFriction, float restitution, uint32_t maskGroup = 1);
	void createBoundingBox(int32_t x = 0, int32_t y = 0, int32_t width = 0, int32_t height = 0);

	// Override Module Funcs
	void	update(UpdateStage stage)			   override;
	Module* duplicate()							   override;
	bool    parse(fstream& stream, ParseMode mode) override;

	// Collision Impl
	vector<Collision*> collidesWith(vector<Entity*> entities);
	Collision*		   collidesWith(Entity* entity);
	RayCollision*      collidesWith(Ray& ray);
	void			   collisionResolution(vector<Collision*>& collisions);

	void applyImpulse(float x, float y);
	void applyImpulse(Vector2f impulse);

	// Data Retrieval
	float		getSpeed();
	float		getMaxSpeed();
	float		getOuterRadius();
	float		getOuterRadiusSq();
	Vector2f	getVelocity();
	PhysicsType getPhysicsType();
	uint32_t	getBitmaskGroup();

	void setAcceleration(Vector2f value);
	void setVelocity(Vector2f value);
	void setVelocity(float x, float y);
	void setBitmaskGroup(uint32_t value);

	// Collision Event
	CollisionEvent onCollision;

protected:
	static const Vector2f mGravity;
	PhysicsType	mPhysicsType;
	Vector2f	mAnchorPoint;
	Direction	mDirection;

	float    mMaxSpeed;
	Vector2f mAcceleration;
	Vector2f mVelocity;

	bool	 mLastCollision;
	IntRect	 mBoundingBox;
	Vector2f mOrigin;
	uint32_t mBitmaskGroup;
	
	PhysicsModule*  mPlatform;
	PhysicsMaterial mMaterial;
};

#endif // _PHYSICSMODULE_H
