#ifndef _SOUNDMODULE_H
#define _SOUNDMODULE_H

/* Author(s): Aaron Smith
 * Date: 21.03.14
 * Filename: SoundModule
 * Description: Can emit sounds from positions in the scene; called
 *				from SoundManager.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"

class FeanExport SoundModule : public Module, public Parser
{
public:
	SoundModule(const SoundModule& module);
	SoundModule(bool loops, Entity* parent = nullptr);
	~SoundModule();

	Module* duplicate()							   override;
	bool    parse(fstream& stream, ParseMode mode) override;

	void	   insert(string name);
	void	   trigger(string soundName);
	FeanSound* getSound(string soundName);

protected:
	vector<FeanSound*> mSounds;
	bool			   mLoops;

	friend class Entity;
};

#endif // _SOUNDMODULE_H
