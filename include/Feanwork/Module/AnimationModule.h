#ifndef _ANIMATIONMODULE_H
#define _ANIMATIONMODULE_H

/* Author(s): Aaron Smith, Jamie Massey
 * Date: 19.02.14
 * Filename: AnimationModule
 * Description: The animation module can use the parser to specify points of a spritesheet
 *              over time.
 *
 */

#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"
#include "Feanwork/Parser/Parser.h"

enum AnimationMode
{
	AnimationMode_Wrap	   = 0,
	AnimationMode_Once	   = 1,
	AnimationMode_PingPong = 2,
	AnimationMode_Reverse  = 3,
};

enum AnimationDirection
{
	AnimationDirection_Right = 0,
	AnimationDirection_Left  = 2,
};

class FeanExport AnimationModule : public Module, public Parser
{
public:
	AnimationModule(const AnimationModule& module);
	AnimationModule(Entity* parent = nullptr);
	~AnimationModule();

	void	 update(UpdateStage stage)				override;
	Module*  duplicate()							override;
	bool     parse(fstream& stream, ParseMode mode) override;

	void  addAnimation(string name, float speed, AnimationMode mode);
	void  addAnimation(Animation* anim);
	void  setAnimation(int index);
	void  setAnimation(string animation);

	void  queueAnimation(int index);
	void  queueAnimation(string animation);

	Animation*	   getCurrentAnimation();
	Animation*	   getAnimation(string animation);
	AnimationSet&  getAnimations();
	int32_t		   animationCount();

	void  replaceAnimations(AnimationSet& anims);

protected:
	AnimationSet  mAnimations;
	vector<int>	  mAnimationQueue;
	int			  mCurrentAnimation;
};

class FeanExport Animation
{
public:
	Animation(const Animation& animation, AnimationMode mode);
	Animation(string name, float speed, AnimationMode mode);
	~Animation();

	void  update(Entity* entity);
	void  reset();

	// Data Retrieval
	string  getName();
	bool	hasEnded();
	float	getSpeed();

	void  addFrame(Frame* frame);
	void  setFrames(FrameSet frames);
	bool  setFrameRange(int32_t startFrame, int32_t endFrame); // Footnote: Adding a new frame will reset the current frame!

	Frame*	   getCurrentFrame();
	FrameSet&  getFrames();

	AnimationDirection  animationDirection();
	AnimationMode	    animationMode();
	int32_t			    frameCount();

protected:
	void  _setFrame(Entity* entity, int frame);
	void  _setAnimationMode(AnimationMode mode);
	void  _updateFrameRange();

protected:
	AnimationMode	    mAnimationMode;
	AnimationDirection  mAnimationDirection;
	FrameSet			mFrames;
	
	string	  mName;
	uint32_t  mCurrentFrame;
	bool	  mHasEnded;
	bool	  mDirection;

	float  mSpeed;
	float  mAccum;

	int32_t  mStartFrame;
	int32_t  mEndFrame;
};

struct Frame
{
public:
	Vector2f  position;
	uint32_t  width;
	uint32_t  height;
};

#endif // _ANIMATIONMODULE_H
