#ifndef _EMITTERMODULE_H
#define _EMITTERMODULE_H

/* Author(s): Jamie Massey
 * Date: 20.02.14
 * Filename: EmitterModule
 * Description: A module to emit multiple images at fast rates for in-game particle
 *              effects. You can store multiple emitter in one Entity using this 
 *              module.
 *
 */

#include <cmath>
#include "Feanwork/Util/Define.h"
#include "Feanwork/Module/Module.h"

// Forward Decl
class Emitter;
class Particle;

enum EmitterType
{
    EmitterType_Directional = 0,
    EmitterType_Circular    = 1,
    EmitterType_Beam        = 2,
};

enum DenoteType
{
    DenoteType_Start        = 0,
    DenoteType_End          = 1,
    DenoteType_StartVariant = 2,
    DenoteType_EndVariant   = 3,
};

class FeanExport EmitterModule : public Module, public Parser
{
public:
	EmitterModule(const EmitterModule& module);
	EmitterModule(Entity* parent = nullptr);
	~EmitterModule();
	
	void	update(UpdateStage stage)			   override;
	void	render(RenderTarget* target)		   override;
	Module* duplicate()							   override;
	bool	parse(fstream& stream, ParseMode mode) override;

	void addEmitter(Emitter* newEmitter);

protected:
	vector<Emitter*> mEmitters;
};

// Emitter Impl
// ============
class FeanExport Emitter
{
public:
	Emitter(const Emitter& emitter);
	Emitter(Vector2f pos, Vector2f direction, EmitterType type, string resource, bool canLoop = true);
	~Emitter();
	
	void init(uint32_t count, float lifeLimit, float lifeLimitVariant, float pulseRate, float pulseLength, Vector2f posVariant);
	void update();
	void render(RenderTarget* target);
	
	void emitParticle(Particle& particle);
	void calculateBirthRate();
	void setParticleCount(uint32_t count);

	// Data Retrieval
	void 	 setPosition(Vector2f pos);
    void     setPosition(float x, float y);
    
	Vector2f getPosition();
	Vector2f getDirection();
	string	 resourceString();

    bool		isActive();
    void		setEmitType(EmitterType type);  
	EmitterType getEmitType();

	uint32_t getParticleCount();
	float	 getBirthRate();
	float	 getPulseRate();
	float	 getPulseLength();
	bool	 canLoop();

	Vector2f getPositionVariant();
	float	 getLifeLimit();
	float	 getLifeLimitVariant();

	// Particle Specific
    Colour mColours[4];
    float mSizes[4];
    float mSpeeds[4];

protected:
	string	 mResource;
    Vector2f mPosition;
    Vector2f mDirection;
    Vector2f mForce;

    Particle*   mParticles;
    EmitterType mEmitterType;
    uint32_t    mParticleCount;
    float       mBirthRate;
    float       mPulseRate;   // Hz = 1/s
    float       mPulseLength; // ms

    float mAccumulated;
    bool  mCanLoop;
    bool  mActive;

    // Particle Specific
    float    mLifeLimit;
    float    mLifeLimitVariant;
    Vector2f mPositionVariant;
};

// Particle Impl
// =============
class FeanExport Particle
{
public:
	Particle(const Particle& particle);
	Particle(string particleSprite = "");
	~Particle();
	
	// Maniplulation Functions
	void move(Vector2f amount);
    void setPosition(Vector2f num);
    void setScale(float scaleX, float scaleY);
    void setColour(Colour colour);
	
	// Utility Functions
	void 	setSprite(Sprite* spr);
    void 	setSprite(FeanTexture* texture);
    void 	setSprite(string texture);
    Sprite* getSprite();
	
	// Public Variables
    Vector2f mDirection;

    float mSpeed;
    float mLife;
    float mLifeLimit;

    Colour mStartColour;
    Colour mEndColour;
    float mStartSize;
    float mEndSize;
    float mStartSpeed;
    float mEndSpeed;

protected:
	Sprite*		 mSprite;
	FeanTexture* mSpriteTexture;
	string		 mSpriteName;
};

#endif // _EMITTERMODULE_H
