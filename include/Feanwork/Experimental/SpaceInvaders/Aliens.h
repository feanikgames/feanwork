/* Author(s): Jamie Massey; Bruno L�ders
 * Date: 07.06.14
 * Filename: Aliens
 * Description: Alien class and factory class
 *
 */

#ifndef _ALIENS_H
#define _ALIENS_H

#include "Feanwork/Util/Define.h"

class Alien
{
public:
	Alien(Entity* ent);
	~Alien();

	void onHit();
	bool isDead() const { return !mAlive; }

	static void onUpdate(Entity* caller, UpdateStage stage);
	static bool onBulletHit(Entity* caller, vector<Collision*>& collisions);

protected:
	float		   mFireDelay;
	int8_t		   mHealth;
	Entity*		   mEntity;
	bool		   mAlive;
	static Prefab* mBulletPrefab;
	Timer*		   mFireRateTimer;

	friend class AlienFactory;
};

class AlienFactory
{
public:
	AlienFactory();
	~AlienFactory();

protected:
	vector<Alien*> mAliens;
	Prefab*		   mAlienPrefab;
};

#endif // _ALIENS_H
