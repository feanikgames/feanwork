/* Author(s): Jamie Massey; Bruno L�ders
 * Date: 07.06.14
 * Filename: SpaceInvaders
 * Description: Constructor and management class for space invaders clone
 *
 */

#ifndef _SPACEINVADERS_H
#define _SPACEINVADERS_H

enum ObjectType
{
	ObjectType_Player = 1,
	ObjectType_Alien,
	ObjectType_Base,
	ObjectType_Bullet,
	ObjectType_Misc
};

// Init call for game
void initSpaceInvaders();
void addWall(float x, float y, float w, float h);

#endif // _SPACEINVADERS_H
