/* Author(s): Jamie Massey; Bruno L�ders
 * Date: 07.06.14
 * Filename: Player
 * Description: Player template class for Space Invaders clone
 *
 */

#ifndef _PLAYER_H
#define _PLAYER_H

#include "Feanwork/Util/Define.h"

class Player
{
public:
	Player(Scene* scene);
	~Player(){}

	// Callbacks
	static void onInput(Entity* caller);
	static bool onBulletHit(Entity* caller, vector<Collision*>& collisions);
	void onHit();

protected:
	Entity*		   mEntity;
	static Timer   mFireRateTimer;
	static Prefab* mBulletPrefab;
};

#endif // _PLAYER_H
