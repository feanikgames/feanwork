#ifndef _WATER_H
#define _WATER_H

#include "Feanwork/Util/Define.h"

class Water
{
public:
	Water(Vector2f position, Vector2f size, Colour colour);
	~Water();

	void render(RenderTarget* target);

protected:
	VertexArray mVertices;
};

#endif // _WATER_H
