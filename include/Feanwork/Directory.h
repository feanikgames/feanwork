#ifndef _DIRECTORY_H
#define _DIRECTORY_H

/* Author(s): Jamie Massey
 * Date: 19.03.14
 * Filename: Directory
 * Description: The Directory class uses directories to split and organize file data
 *				(extensions, names and attributes) for parsing or caching.
 *
 */

#include "Feanwork/Util/Define.h"

class FeanExport Directory
{
public:
	enum FileType
	{
		FileType_File,
		FileType_Folder
	};

	struct FileInfo
	{
		string   path;
		string   fileName;
		string   extension;
		FileType type;

		string getFullPath() const
		{
			return (path + "/" + fileName + "." + extension);
		}
	};

	Directory(string path);
	~Directory();

	bool getFirstFile(FileInfo& info);
	bool getNextFile(FileInfo& info);
	void releaseHandle();

	bool		getAllFiles(vector<FileInfo>& files);
	static void splitFilename(string input, string& path, string& file, string& ext);

#ifdef _WINDOWS
	typedef HANDLE		   DirHandle;
	#define INVALID_HANDLE INVALID_HANDLE_VALUE
#elif __linux
	typedef DIR*			  DirHandle;
	#define INVALID_HANDLE	  nullptr
#endif

protected:
	string	  mPath;
	DirHandle mHandle;
};

#endif // _DIRECTORY_H
