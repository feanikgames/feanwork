/*
 * ScriptFunction.h
 *
 *  Created on: Mar 12, 2014
 *      Author: bryce
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

#include "Logger.h"
#include "LookupTable.h"

#include <string>
#include <vector>
#include <unordered_map>
#include <functional>

namespace infernix
{

	enum ByteCode
	{
		//General
		DECV, //Declare Var
		DEFV, //Define Var
		DECF, //Declare Function
		DEFF, //Define Function
		EOFN, //End of function
		EODF, //End of (var) definition
		CHK, //Check conditional
		JMP, //Branch to group
		BGP, //Begin Group
		EGP, //End Group
		ESIG, //End (function) signature

		//Arithmetic
		EXP, //Exponent
		MUL, //Multiply
		DIV, //Divide
		ADD, //Add
		SUB, //Subtract
		ASN, //Assign
		STO, //Store

		//Booleans
		GTN, //Greater than
		LTN, //Less than
		EQT, //Equal to
		GTEQ, //Greater than/ equal to
		LTEQ, //Less than/equal to
		AND,
		OR,
		NOT,

		//Branching
		RTRN, //Return to point in code (loops) / return from function
		DOIF, //Block to be executed if CHK is true
		END, //Ends a block

		//Misc
		ARG, //Argument name
		MOD, //Modifier
		CALL, //Call (function)

	};


	struct Instruction
	{
		ByteCode opcode;
		std::string target;

		Instruction* parent; //Helps to create loops, jumps, and depends

		Instruction* next; //Chains series of instructions together (as in a function)
		Instruction* prev;
	};


	enum VariableType
	{
		VariableType_Undefined = - 1,
		VariableType_String,
		VariableType_Number,

	};

	class Variable
	{
	public:


		Variable() : mName(""), mValue(""), mType(VariableType_Undefined) {}
		Variable(const std::string& _name, const std::string& _value);
		virtual ~Variable();

		void operator+=(const Variable& _other);
		void operator-=(const Variable& _other);
		void operator/=(const Variable& _other);
		void operator*=(const Variable& _other);

		Variable operator+(const Variable& _other) const;
		Variable operator-(const Variable& _other) const;
		Variable operator/(const Variable& _other) const;
		Variable operator*(const Variable& _other) const;


		Variable operator==(const Variable& _other) const;
		Variable operator&&(const Variable& _other) const;
		Variable operator||(const Variable& _other) const;
		Variable operator>(const Variable& _other) const;
		Variable operator<(const Variable& _other) const;
		Variable operator>=(const Variable& _other) const;
		Variable operator<=(const Variable& _other) const;

		void operator=(const Variable& _other);

		void assign(const Variable& _other);

		bool isTemporary() const{ return mName.find("infx_tmp") != std::string::npos; }

		void setName(const std::string& _name) { mName = _name; }
		void setValue(const std::string& _value);

		const std::string& getName() const { return mName; }
		const std::string& getValue() const { return mValue; }

		operator const char*(){ return mValue.c_str(); }
		operator double() { return std::stof(mValue.c_str()); }

	protected:
		std::string mName;
		std::string mValue;
		VariableType mType;

	};

	class ScriptFunction
	{
	public:
		/////////////////////////////////////////////////////////////
		ScriptFunction(const ScriptFunction& _func);
		ScriptFunction(const std::string& _name,FuncLookupTable& _funcLookup, VarLookupTable& _varLookup);
		virtual ~ScriptFunction();

		void define(Instruction* _i); //Defines the signature
		void add(Instruction* _i); //Defines the body

		virtual void call();

		void finish();
		bool hasSignature() const { return  mHasSignature; }

		const std::string& getName() const { return mName; }

	protected:
		void declareVariable(Instruction* _i);
		void defineVariable(Instruction* _i);
		void callFunction(Instruction* _i);
		void simplifyArithmetic(Instruction* _i);
		void assignVariable(Instruction* _i);
		void getArithmeticResult(Instruction* _i);

		bool branch(Instruction* _i);

		Instruction* getNext() { return  mInstructions[mCurPointer++]; }
		bool hasInstructions() { return !mInstructions.empty() && mCurPointer < mInstructions.size(); }

		void reset() { mCurPointer = 0; }

	protected:
		std::string mName;

		uint32_t mCurPointer;
		uint32_t mNumParams;

		bool mHasSignature; //Set to true when the signature is complete

		std::vector<Instruction*> mInstructions;

		FuncLookupTable& mFuncLookup;
		VarLookupTable& mVarLookup;
	};

	template <std::size_t ...> struct index_sequence {};

	template <std::size_t I, std::size_t ...Is>
	struct make_index_sequence : make_index_sequence < I - 1, I - 1, Is... > {};

	template <std::size_t ... Is>
	struct make_index_sequence<0, Is...> : index_sequence<Is...> {};



	template<typename _R, typename... _Args>
	class ExternFunction : public ScriptFunction
	{
	public:
		ExternFunction(const std::function<_R(_Args...)>& _func,uint64_t _argCount, const std::string& _name,
				FuncLookupTable& _funcLookup, VarLookupTable& _varLookup) :
					ScriptFunction(_name, _funcLookup, _varLookup), mFunc(_func)
		{
			mNumParams = _argCount;
		}

		virtual ~ExternFunction(){}

		template<typename _Arr, std::size_t... I>
		auto callFuncImpl(const std::function<_R(_Args...)>& _fn, _Arr _array, index_sequence<I...>)
			-> decltype(_fn(static_cast<_Args>(*_array[I])...))
		{
			return _fn(static_cast<_Args>(*_array[I])...);
		}

		template<std::size_t _N, typename _Idx = make_index_sequence<_N> >
		auto callFunc(const std::function<_R(_Args...)>& _fn,const std::vector<Variable*>& _args)
			-> decltype(this->callFuncImpl(_fn, _args, _Idx()))
		{
			return callFuncImpl(_fn, _args, _Idx());
		}

		void call()
		{
			size_t count = mVarLookup.count_of({mName+"_inputs", mName+"_outputs"});
			if(count > mNumParams)
			{
				logMsg(LogMsgType_Warning, "Function %s called with too many input parameters! "
						"Function not called.", mName.c_str() );
				return;
			}

			if(count < mNumParams)
			{
				logMsg(LogMsgType_Warning, "Function %s called with too few input parameters! "
						"Function not called", mName.c_str() );
				return;
			}


			callFunc<sizeof...(_Args)>(mFunc, mVarLookup.getOrderedList({mName+"_inputs", mName+"_outputs"}));

			return;
		}

	protected:
		std::function<_R(_Args...)> mFunc;
	};



} /* namespace infernix */

#endif /* FUNCTION_H_ */
