/*
 * Script.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bryce
 */

#ifndef SCRIPT_H_
#define SCRIPT_H_

#include "ResourceManager.h"
#include "ScriptFunction.h"

#include <unordered_map>
#include <vector>
#include <cassert>

namespace infernix
{

	class Script: public Resource
	{
	public:
		Script();
		virtual ~Script();

		//TODO: As it is now, functions are bound on a per script basis. Make them global to all scripts
		template <typename _R, typename ..._Args>
		void bindFunc(_R (*_fn)(_Args...), const std::string& _name)
		{
			auto func = std::function<_R(_Args...)>(_fn);
			ScriptFunction* fn = new ExternFunction<_R, _Args...>(
					func, sizeof...(_Args), _name, mFuncLookup, mVarLookup);

			mFuncLookup.insert("global", _name, fn);
		}

		template <typename _R, class _T, typename ..._Args>
		void bindFunc(_R (_T::*_fn)(_Args...), _T* _obj, const std::string& _name, _Args... _args)
		{
			std::function<_R(_Args...)> func = std::bind(_fn, _obj, _args...);
			ScriptFunction* fn = new ExternFunction<_R, _Args...>(
					func, sizeof...(_Args), _name, mFuncLookup, mVarLookup);

			mFuncLookup.insert("global", _name, fn);
		}

		int32_t execute();
	protected:

		bool make(ResourceFileParser& _data);

		void declareVariable(Instruction* _ins);
		void declareFunction(Instruction* _ins);
		void defineVariable(Instruction* _ins);
		void defineFunction(Instruction* _ins);
		void assignVariable(Instruction* _ins);

		void getArithmeticResult(Instruction* _ins);

		Instruction* getNext(){ /*assert(mCurPointer >= mInstructions.size());*/ return  mInstructions[mCurPointer++]; }
		bool hasInstructions() { return mCurPointer < mInstructions.size(); }


	protected:
		std::vector<Instruction*> mInstructions;

		Instruction* mCurrent; //If we're inside of a secondary scope, this will point to the parent
		uint32_t mCurPointer;

		FuncLookupTable mFuncLookup;
		VarLookupTable mVarLookup;
	};

} /* namespace infernix */


#endif /* SCRIPT_H_ */
