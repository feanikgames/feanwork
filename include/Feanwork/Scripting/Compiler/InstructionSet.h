/*
 * InstructionSet.h
 *
 *  Created on: Oct 21, 2013
 *      Author: bryce
 */

#ifndef INSTRUCTIONSET_H_
#define INSTRUCTIONSET_H_

#include "Instruction.h"
#include "Expression.h"
#include "Token.h"
#include "LookupTable.h"

#include <map>
#include <list>
#include <string>
#include <unordered_map>

/**
 * The main worker of the script. This class will group instructions and attempt to
 * execute the instructions in an order that makes logical sense. It does so by determining
 * the overall type of instructions each expression or group of expressions represent.
 */


class Variable
{
public:
	std::string name;
	std::string value;
	bool isDefined;
};

class Function
{
public:
	std::string name;
	bool isDefined;
	std::vector<std::pair<bool,Variable*> > signature;
};


class InstructionSet
{
public:
	InstructionSet(const std::string& _scope = "");
	virtual ~InstructionSet();

	void render(); //DEBUG
	void finalize();


	/**
	 * @note Invalid until finalize is called.
	 * @return Pointer to compiled program data
	 */
	uint8_t* getCompiledData();
	/**
	 * @note Invalid until finalize is called.
	 * @return Size of compiled data
	 */
	uint32_t getDataSize();
	/**
	 * @note Invalid until finalize is called.
	 * @return Number of instructions generated
	 */
	uint32_t getInstructionCount(){ return mInstructionCount;}

	void add(Instruction* _i, bool _throw = true);

	bool hasFunction(const std::string& _name);
	bool hasFunctionDefined(const std::string& _name);

	bool hasVariable(const std::string& _name, std::initializer_list<std::string> _additionalScopes = {});
	bool hasVariableDefined(const std::string& _name, std::initializer_list<std::string> _additionalScopes = {});

protected:
	void declareVariable(Instruction* _ins);
	void declareFunction(Instruction* _ins);
	void defineVariable(Instruction* _ins);
	void defineFunction(Instruction* _ins);
	void callFunction(Instruction* _ins);
	void resolveArgs(Instruction* _ins);
	void checkSignature(Instruction* _ins);
protected:
	Instruction* mRoot; //Pointer to the first instruction in the program
	Instruction* mTail; //Pointer to the last instruction

	VarLookupTable mVarDictionary;	///The dynamic name look-up table
	FuncLookupTable mFuncDictionary;

	uint8_t* mCompiledData;
	uint32_t mDataSize;
	uint32_t mInstructionCount;

	//Helps keep track of function signatures to make sure they're the same. true = in, false = out
	std::unordered_map<std::string, std::vector<std::pair<bool,Variable*> > > mSignatureTable;

private:
	struct Scope{
		Scope(const std::string& _name) : prev(nullptr), name(_name){}
		Scope *prev;
		std::string name;
	} *mScope;

	bool mInSignature;
};

#endif /* INSTRUCTIONSET_H_ */
