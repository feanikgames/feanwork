/*
 * Token.h
 *
 *  Created on: Nov 13, 2013
 *      Author: bryce
 */

#ifndef TOKEN_H_
#define TOKEN_H_

#include <string>

enum ValueType
{
	ValueType_Unknown 		= 1<<0,
	ValueType_String 		= 1<<1,
	ValueType_Number 		= 1<<2,
	ValueType_Delimiter 	= 1<<3,
	ValueType_Separator 	= 1<<4,
	ValueType_Keyword 		= 1<<5,
	ValueType_OpenGroup 	= 1<<6,
	ValueType_CloseGroup 	= 1<<7,
	ValueType_Operand 		= 1<<8,
	ValueType_Boolean 		= 1<<9,
	ValueType_Variable 		= 1<<10,
	ValueType_Function 		= 1<<11,
	ValueType_Temporary 	= 1<<12,
	ValueType_Count
};

enum Priority
{
	Priority_Discard = 0,
	Priority_Four = 1,
	Priority_Three,
	Priority_Two,
	Priority_One,
	Priority_Infinite,
};


class Token
{
public:
	Token() :
			 mPriority(Priority_Discard), mType(ValueType_Unknown), mLine(0)
	{
	}

	Token(ValueType _type, const std::string& _data = "", uint32_t _line = 0) : mData(_data), mPriority(Priority_Discard)
	, mType(_type), mLine(_line)
	{
	}

	Token(const std::string mToken, uint32_t _line = 0);

	bool operator==(const Token& _o) const
	{
		return !(mData.compare(_o.mData)) && mType == _o.mType;
	}

	bool is(ValueType _value) const { return  mType & _value; }
	bool is(const std::string& _s) const { return !mData.compare(_s); }

	void setType(ValueType _t) { mType = _t; }
	void setData(const std::string& _data) { mData = _data; }

	uint32_t getTypes() const { return mType; }
	std::string getData() const { return mData; }

	Priority getPriority() const { return mPriority; }
	void setPriority(Priority _p) { mPriority = _p; }

	uint32_t getLine() const { return mLine; }

	void render();

protected:
	friend class Expression; //For fast access

	std::string mData;
	Priority mPriority;
	uint32_t mType;

	uint32_t mLine; //Used for error reporting
};

#endif /* TOKEN_H_ */
