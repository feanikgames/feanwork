/*
 * Expression.h
 *
 *  Created on: Oct 20, 2013
 *      Author: bryce
 */

#ifndef EXPRESSION_H_
#define EXPRESSION_H_

#include "Instruction.h"
#include "Token.h"

#include <string>
#include <vector>
#include <stack>
#include <cassert>

/*
 * The script is made up of individual sections I'm going to call expressions. For example: var x = 1;
 * This is a single expression made up of 5 parts.
 * (1) var This is a keyword that declares a variable
 * (2) x This is the name of the variable
 * (3) = The assignment operator
 * (4) 1 The value of the assignment
 * (5) ; The delimiter
 *
 * This class will break down these sections and create Instructions based on each of the components.
 * The first step would be to determine what the parts mean in terms of code now that we have them
 */

enum ExpressionType
{
	ExpressionType_VariableDec,
	ExpressionType_VariableDef,
	ExpressionType_FunctionDec,
	ExpressionType_FunctionCall,
	ExpressionType_FunctionDef,
	ExpressionType_ForLoop,
	ExpressionType_WhileLoop,
	ExpressionType_IfBlock,
	ExpressionType_Assignment,
	ExpressionType_Return,
	ExpressionType_Unknown,
};

class InstructionSet;

class Expression
{
public:

	enum ElementType
	{
		ElementType_Child,
		ElementType_Token,
	};

	typedef std::vector<Expression*> ExpressionList;
	typedef std::vector<Token> TokenList;


	Expression(ExpressionType _type = ExpressionType_Unknown);
	Expression(const std::vector<Token>& _tokens, ExpressionType _type = ExpressionType_Unknown);

	virtual ~Expression();

	void render();

	void replaceRange(uint32_t _begin, uint32_t _end, const Token& _t);

	int32_t find(uint32_t _start, const std::string& _token) const;
	int32_t findMatching(uint32_t _startGroup) const;
	int32_t rfindMatching(uint32_t _endGroup);

	/*
	 * Creates a children set of expressions within the scope of this expression
	 */
	Expression* createChild(ExpressionType _type = ExpressionType_Unknown);
	Expression* getParent() const { return mParent; }
	ExpressionList getChildren() const { return mChildren; }

	void addToken(const Token& _t);
	void removeToken(const Token& _t);

	Token& getToken(size_t _i) { assert(_i < mTokens.size()); return mTokens[_i]; }
	const Token& getToken(size_t _i) const { assert(_i < mTokens.size()); return mTokens[_i]; }
	TokenList getTokens() const { return mTokens; }

	void validate();

	int32_t getHighestPriorityToken(Priority* _code);
	std::vector<Token> getArgumentList() const;

	ExpressionType getType() const { return mType; }
	void setType(ExpressionType _t) { mType = _t; }

	static std::string toString(ExpressionType _et);
protected:


	friend class InstructionSet;

	Expression* mParent;
	ExpressionList mChildren;
	TokenList mTokens;

	///If this is a child expression, this represents where it
	///falls relative to other tokens.
	size_t mIndex;

	ExpressionType mType;

	enum Expecting
	{
		Expecting_Semicolon,
		Expecting_EndLoop,
		Expecting_EndIf,
		Expecting_EndFunc,
		Expecting_RParenthesis,
	};

	std::string translate(Expecting _e);

	std::stack<Expecting> mQueue;

	static std::vector<std::string> mExpressionStrings;
	static std::vector<std::string> mExpectingStrings;

};

#endif /* EXPRESSION_H_ */
