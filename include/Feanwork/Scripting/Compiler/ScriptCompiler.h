/*
 * ScriptCompiler.h
 *
 *  Created on: Oct 19, 2013
 *      Author: bryce
 */

#ifndef SCRIPTCOMPILER_H_
#define SCRIPTCOMPILER_H_

#include "Expression.h"
#include "InstructionSet.h"

#include <cstdint>
#include <string>
#include <vector>
#include <unordered_map>

class InstructionSet;
class Expression;
class Instruction;

/*
 * Basically, this will take the text of the script file and convert it to a set of
 * simple instructions.
 */

typedef uint32_t Address;

class ScriptCompiler
{
public:
	typedef std::vector<Token> TokenList;
	typedef std::vector<Instruction*> InstructionList;
	typedef std::vector<Expression*> ExpressionList;

	ScriptCompiler();
	virtual ~ScriptCompiler();

	struct ProgramData
	{
		uint8_t* data;
		uint32_t dataSize;
		uint32_t instructionCount;
	};

	ProgramData compile(const char* data);

protected:
	TokenList generateTokens(std::string _exp);
	ExpressionList generateExpressions();
	void generateInstructions(Expression* _e, InstructionSet* _s);

	void handleNestedBlock(const Token& _keyword, Expression* _parent);
	void handleReturn(const Token& _keyword, Expression* _parent);
	void handleNestedDeclaration(const Token& _keyword, Expression* _parent);
	void handleFunctionCall(const Token& _func, Expression* _parent);
	void handleAssignments(const Token& _target, Expression* _parent);

	Expression* createVariableExpression(Token& _start);

	/**
	 * Extract tokens starting at 'func' until a matching 'end' is found
	 */
	Expression* createFunctionalExpression(Token& _start);
	Expression* createDeclarationExpression(Token& _start);

	Token& getToken();
	Token peekToken();

	bool hasTokens() const
	{
		uint32_t s = mTokens.size();
		return mCurrentToken < s;
	}
protected:
	void declareVar(Expression* _e, InstructionSet* _s);
	void defineVar(Expression* _e, InstructionSet* _s);
	void declareFunc(Expression* _e, InstructionSet* _s);
	void defineFunc(Expression* _e, InstructionSet* _s);
	void callFunc(Expression* _e, InstructionSet* _s);
	void assignVar(Expression* _e, InstructionSet* _s);
	void defineIf(Expression* _e, InstructionSet* _s);

	void decomposeBoolean(Expression* _e, InstructionSet* _s);
	void decomposeArithmetic(Expression* _e, InstructionSet* _s);

	std::string getTemporaryName();
protected:
	TokenList mTokens; //! All the tokens generated from the script
	uint32_t mCurrentToken;
	uint32_t mNextTemp;

	uint32_t mLevel;

};

#endif /* SCRIPTCOMPILER_H_ */
