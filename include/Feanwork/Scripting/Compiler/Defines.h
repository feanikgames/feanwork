/*
 * Defines.h
 *
 *  Created on: Oct 25, 2013
 *      Author: bryce
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include <vector>
#include <string>

extern std::vector<std::string> gKeywords;
extern std::vector<std::string> gTypeStrings;
#endif /* DEFINES_H_ */
