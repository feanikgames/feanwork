/*
 * Instruction.h
 *
 *  Created on: Oct 21, 2013
 *      Author: bryce
 */

#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_


#include "Token.h"

#include <vector>

//Define a set of ASM style opcodes
enum ByteCode
{
	//General
	DECV, //Declare Var
	DEFV, //Define Var
	DECF, //Declare Function
	DEFF, //Define Function
	EOFN, //End of function
	EODF, //End of (var) definition
	CHK, //Check conditional
	JMP, //Branch to group
	BGP, //Begin Group
	EGP, //End Group
	ESIG, //End (function) signature

	//Arithmetic
	EXP, //Exponent
	MUL, //Multiply
	DIV, //Divide
	ADD, //Add
	SUB, //Subtract
	ASN, //Assign
	STO, //Store

	//Booleans
	GTN, //Greater than
	LTN, //Less than
	EQT, //Equal to
	GTEQ, //Greater than/ equal to
	LTEQ, //Less than/equal to
	AND,
	OR,
	NOT,

	//Branching
	RTRN, //Return to point in code (loops) / return from function
	DOIF, //Block to be executed if CHK is true
	END, //Ends a block

	//Misc
	ARG, //Argument name
	MOD, //Modifier
	CALL, //Call (function)


};

class Instruction
{
public:
	Instruction(ByteCode _op) : mNext(nullptr), mPrev(nullptr), mOpCode(_op){}
	Instruction(ByteCode _op, const Token& _id) : mNext(nullptr), mPrev(nullptr),
			mOpCode(_op), mID(_id){}
	virtual ~Instruction();

	ByteCode getCode() const { return mOpCode; }
	const Token& getToken() const { return mID; }

	void render();
protected:
	Instruction *mNext;
	Instruction *mPrev;
	friend class InstructionSet;
	ByteCode mOpCode;
	Token mID;
};


#endif /* INSTRUCTION_H_ */
