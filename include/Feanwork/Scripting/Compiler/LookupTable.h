/*
 * LookupTable.h
 *
 *  Created on: Mar 18, 2014
 *      Author: bryce
 */

#ifndef LOOKUPTABLE_H_
#define LOOKUPTABLE_H_

#include <string>
#include <unordered_map>
#include <cstdint>
#include <vector>

class Function;
class Variable;

struct KeyType
{
	KeyType() : index(-1) {}
	KeyType(const char* _name, int32_t _idx) : name(_name), index(_idx) {}
	KeyType(const char* _name) : name(_name), index(-1) {}
	KeyType(const std::string& _name, int32_t _idx) : name(_name), index(_idx) {}
	KeyType(const std::string& _name) : name(_name), index(-1) {}
	KeyType(int32_t _idx) : index(_idx) {}
	KeyType(const KeyType& _key) : name(_key.name), index(_key.index) {}


	//Intentionally OR'd to allow lookup by either name or index
	bool operator==(const KeyType& _o) const { return name == _o.name || index == _o.index; }
	bool operator==(int32_t _index) const { return index == _index; }
	bool operator==(const std::string& _name) const { return name == _name; }

	std::string name;
	int32_t index;
};

namespace std
{
	template <> struct hash<KeyType>
	{
		size_t operator()(const KeyType & x) const
		{
			//XXX: Change this when/if I implement KeyType::index
			return hash<string>()(x.name);
		}
	};
}

	class Variable;
	class Function;


class VarLookupTable
{
public:
	VarLookupTable();
	virtual ~VarLookupTable();


	typedef std::unordered_map<KeyType, Variable*>::iterator item_iterator;
	typedef std::unordered_map<std::string, std::unordered_map<KeyType, Variable*> >::iterator scope_iterator;

	void insert(const std::string& _scope, const KeyType& _key,const Variable& _obj);
	void replace(const std::string& _scope, const KeyType& _key,const Variable& _obj);

	Variable* find(const KeyType& _key, std::initializer_list<std::string> _scopes = { "global" });

	std::pair<item_iterator,item_iterator>
	equal_range(const std::string& _scope);

	std::size_t count_of(std::initializer_list<std::string> _scopes );
	std::vector<Variable*> getList(std::initializer_list<std::string> _scopes);

protected:
	static uint32_t mID;
	typedef std::unordered_map<std::string,std::unordered_map<KeyType, Variable*> > Table;
	Table mLookup;
};

class FuncLookupTable
{
public:
	FuncLookupTable();
	virtual ~FuncLookupTable();


	typedef std::unordered_map<KeyType, Function*>::iterator item_iterator;
	typedef std::unordered_map<std::string, std::unordered_map<KeyType, Function*> >::iterator scope_iterator;

	void insert(const std::string& _scope, const KeyType& _key,const Function& _obj);
	Function* find(const KeyType& _key, std::initializer_list<std::string> _scopes = { "global" });

	std::pair<item_iterator,item_iterator>
	equal_range(const std::string& _scope);

	std::size_t count_of(std::initializer_list<std::string> _scopes);
	std::vector<Function*> getList(std::initializer_list<std::string> _scopes);

protected:
	typedef std::unordered_map<std::string,std::unordered_map<KeyType, Function*> > Table;
	Table mLookup;
};


#endif /* LOOKUPTABLE_H_ */
