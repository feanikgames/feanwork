#ifndef _GAMECONSOLE_H
#define _GAMECONSOLE_H

/* Author(s): Jamie Massey
 * Date: 21.01.14
 * Filename: GameConsole
 * Description: Draws a console in the game which can show output messages from the 
 *				internal program.
 *
 */

#include "Util/Define.h"

class FeanExport GameConsole final
{
public:
	static GameConsole* getInstance();

	void init(Vector2f position);
	void update();
	void render(RenderTarget* target);
	void destroy();

	void log(const char* formatStr, ...);

protected:
	vector<Text*> mLines;
	int32_t		  mLineSize;
	int32_t 	  mLinePosition;
	Vector2f	  mPosition;
	FeanFont*	  mDefaultFont;
};

#endif // _GAMECONSOLE_H
