#include "Feanwork/SceneManager.h"
#include "Feanwork/Scene.h"
#include "Feanwork/Entity.h"
#include "Feanwork/Camera.h"
#include "Feanwork/InputHandler.h"
#include "Feanwork/ResourceCache.h"
#include "Feanwork/Util/FeanRandom.h"

#include "Feanwork/Module/Module.h"
#include "Feanwork/Module/RenderModule.h"
#include "Feanwork/Module/AnimationModule.h"
#include "Feanwork/Module/EmitterModule.h"
#include "Feanwork/Module/PhysicsModule.h"
#include "Feanwork/Module/IntelligenceModule.h"

#include <fstream>

using namespace FeanRandom;

int main()
{
	ResourceCache::getInstance()->addDirectory("../Resources");
	SceneManager::getInstance()->init("game");
	SceneManager::getInstance()->setIcon("../Resources/icon.png");

	// Keys -----
	// Left
	Keystate* left	= new Keystate;
	left->inputName = "left";
	left->key		= Keyboard::A;
	InputHandler::getInstance()->addKeystate(left);

	// Right
	Keystate* right  = new Keystate;
	right->inputName = "right";
	right->key		 = Keyboard::D;
	InputHandler::getInstance()->addKeystate(right);

	// Up
	Keystate* up  = new Keystate;
	up->inputName = "up";
	up->key		  = Keyboard::W;
	InputHandler::getInstance()->addKeystate(up);

	// Down
	Keystate* down	= new Keystate;
	down->inputName = "down";
	down->key		= Keyboard::W;
	InputHandler::getInstance()->addKeystate(down);
	// ===============================

	Scene* scene			  = SceneManager::getInstance()->getScene(0);

	Entity* player			  = new Entity(Vector2f(250.f, 250.f));
	RenderModule* render	  = new RenderModule("player");
	AnimationModule* anim	  = new AnimationModule();
	EmitterModule* emitter	  = new EmitterModule();
	PhysicsModule* physics	  = new PhysicsModule(PhysicsType_Actor);
	IntelligenceModule* intel = new IntelligenceModule();

	anim->addAnimation("idle", 500.f, true);
	anim->addAnimation("walk", 800.f, true);

	for(uint32_t i = 0; i < 5; i++)
	{
		Frame* newFrame  = new Frame();
		newFrame->width  = 48;
		newFrame->height = 80;
		newFrame->position = Vector2f(48 * i, 0);
		anim->getAnimation("idle")->addFrame(newFrame);
	}

	for(uint32_t i = 0; i < 5; i++)
	{
		Frame* newFrame  = new Frame();
		newFrame->width  = 48;
		newFrame->height = 80;
		newFrame->position = Vector2f(48 * i, 80);
		anim->getAnimation("walk")->addFrame(newFrame);
	}

	Emitter* newEmitter = new Emitter(Vector2f(400.f, 400.f), Vector2f(1.f, 0.f), EmitterType_Directional, "tex");
	newEmitter->init(250.f, 2000.f, 1000.f, 0.f, 200.f, Vector2f(20.f, 20.f));
	newEmitter->mColours[DenoteType_Start]		  = Color(randRange(50, 175), 0, 0, 200);
	newEmitter->mColours[DenoteType_End]		  = Color(0, randRange(100, 235), randRange(20, 255), 20);
	newEmitter->mColours[DenoteType_StartVariant] = Color(0, 0, 0, 0);
    newEmitter->mColours[DenoteType_EndVariant]	  = Color(0, 0, 0, 0);

	newEmitter->mSizes[DenoteType_Start]		= 1.0f;
    newEmitter->mSizes[DenoteType_End]			= 1.0f;
    newEmitter->mSizes[DenoteType_StartVariant] = .0f;
    newEmitter->mSizes[DenoteType_EndVariant]   = .0f;

    newEmitter->mSpeeds[DenoteType_Start]		 = 0.04f;
    newEmitter->mSpeeds[DenoteType_End]			 = 0.02f;
    newEmitter->mSpeeds[DenoteType_StartVariant] = 0.005f;
    newEmitter->mSpeeds[DenoteType_EndVariant]	 = 0.005f;

	emitter->addEmitter(newEmitter);

	player->addModule(render);
	player->addModule(anim);
	player->addModule(emitter);
	player->addModule(physics);
	player->addModule(intel);

	physics->initVars(0.3f, 0.6f, 0.8f, 0.8f);
	physics->createBoundingBox();
	player->initDebugQuad();

	// Floor
	Entity*		   floor	 = new Entity(Vector2f(50.0f, 600.0f));
	RenderModule*  floorRnd  = new RenderModule("floor");
	PhysicsModule* floorPhys = new PhysicsModule(PhysicsType_Static);

	floor->addModule(floorRnd);
	floor->addModule(floorPhys);
	floorPhys->createBoundingBox();
	floor->initDebugQuad();

	scene->addEntity(0, player);
	scene->addEntity(0, floor);
	scene->getCamera(0)->lookAt(player);

	SceneManager::getInstance()->run();
	SceneManager::getInstance()->destroy();

	return 0;
}