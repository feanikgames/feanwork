#version 120

uniform float time;
varying out float x;

void main()
{
	gl_FrontColor  = gl_Color;
	gl_Position    = gl_Vertex;
	gl_Position.y += 5.0f*sin(0.05f*(gl_Position.x + time));
	x = gl_Position.x;
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Position;
}
