#version 120

uniform float time;
varying in float x;

void main()
{
	float sinSq = 2.0f*sin(0.03f*time);
	sinSq *= sinSq;
	float f = x / 1000.0f;
	gl_FragColor = (vec4(1, 0, 0, 1) + vec4(0, 0, 1, 1))*f * sinSq;
}
