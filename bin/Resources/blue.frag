#version 120

uniform sampler2D tex;
//uniform float 	  accum;

void main()
{
    vec2 uv 	  = gl_TexCoord[0].st;
	/*float freq    = 20.0f * cos(0.0002*accum) + 80.0f;
	uv.y 		 += (cos(uv.y*freq)*0.03);
	uv.x 		 += (cos(uv.x*freq)*0.03);*/
	gl_FragColor  = texture2D(tex, uv);

	/*float i = length(gl_FragColor)*1.2f;
	gl_FragColor *= i*i*vec4(1, 1, 1, 1);*/
}